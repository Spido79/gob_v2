<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
$today=new DateTime('first day of this month');
$interval = new DateInterval('P1Y');
$today->sub($interval);

$allGlobal=\GOBLINS\Manage::getAll(-1);

$params=array(
	'list_tane'	=> [\APP\Parameter::getSpec('id_tana_nazionale')],
);
$all=\GOBLINS\Manage::getAll(0, $params);

$params=array(
	'list_tane'	=> [\APP\Parameter::getSpec('id_tana_nazionale')],
	'associated_from' => $today->format('Y-m-d'),
);
$allLastYear=\GOBLINS\Manage::getAll(0, $params);

$params=array(
	'list_tane'	=> [0],
);
$notAss=\GOBLINS\Manage::getAll(0, $params);

$params=array(
	'list_tane'	=> [0],
	'subscribed_from' => $today->getTimestamp(),
);
$notAssLastYear=\GOBLINS\Manage::getAll(0, $params);

$myAss=\GOBLINS\Manage::getAll(-1);

$params=array(
	'list_tane' => [\APP\Parameter::getSpec('id_tana_nazionale')],
	'associated_from' => $today->format('Y-m-d'),
);
$myAssYear=\GOBLINS\Manage::getAll(-1, $params);
$percMyAssYear=number_format(((100*count($myAssYear))/count($myAss)), 2, '.', ',');


$params=array(
	'list_tane' => [\APP\Parameter::getSpec('id_tana_nazionale')],
	'map_fields'	=> array(
		'Nick'				=>'',
		'id_goblin'			=>'',
		'dataNascita_goblin' =>'',
	),
);
$myAssNa=\GOBLINS\Manage::getAll(0,$params);


$params=array(
	'map_fields'	=> array(
		'Nick'				=>'',
		'id_goblin'			=>'',
		'dataNascita_goblin' =>'',
	),
);
$otherAss=\GOBLINS\Manage::getAll(0, $params);



$now=new DateTime();

$count=0;
$mediaAnniMiei=0;
foreach ($myAssNa as $gob) {
	$date = new DateTime($gob['dataNascita_goblin']);
	$interval = $now->diff($date);
 	if ($interval->y<=0 || $interval->y>120){
		continue;
	}
 	$mediaAnniMiei+=$interval->y;
 	$count++;
}
if ($count>0){
	$mediaAnniMiei=number_format(($mediaAnniMiei/$count),1);
}

$count=0;
$mediaAnniGlobal=0;
foreach ($otherAss as $gob) {
	$date = new DateTime($gob['dataNascita_goblin']);
	$interval = $now->diff($date);
	if ($interval->y<=0 || $interval->y>120){
		continue;
	}
 	$mediaAnniGlobal+=$interval->y;
 	$count++;
}
if ($count>0){
	$mediaAnniGlobal=number_format(($mediaAnniGlobal/$count),1);
}

?>
<div class="wrapper wrapper-content animated fadeIn">

	<div class="p-w-md m-t-sm">
		<div class="row">

			<div class="col-sm-4">
				<h1 class="m-b-xs text-danger">
					<?php echo number_format(count($allGlobal), 0, '.',','); ?>
				</h1>
				<small>
					Goblins totali
				</small>
				<div id="sparkline1" class="m-b-sm"></div>
				<div class="row">
					<div class="col-4">
						<small class="stats-label">Tesserati alla Tana Nazionale</small>
						<h4><?php echo number_format(count($all), 0, '.',','); ?></h4>
					</div>
				</div>

			</div>
			<div class="col-sm-4">
				<h1 class="m-b-xs">
					<?php echo number_format(count($notAss), 0, '.',','); ?>
				</h1>
				<small>
					Iscritti non tesserati
				</small>
				<div id="sparkline2" class="m-b-sm"></div>
				<div class="row">
					<div class="col-4">
						<small class="stats-label">Non tesserati ultimo anno</small>
						<h4><?php echo number_format(count($notAssLastYear), 0, '.',','); ?></h4>
					</div>
				</div>


			</div>
			<div class="col-sm-4">

				<div class="row m-t-xs">
					<div class="col-6">
						<h5 class="m-b-xs">Tesserati ultimo anno</h5>
						<h1 class="no-margins"><?php echo number_format(count($myAssYear), 0, '.',','); ?></h1>
						<div class="font-bold text-navy"><?php echo $percMyAssYear; ?>% <i class="fa fa-bolt"></i></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="small float-left col-md-3 m-l-lg m-t-md">
					<strong>12 mesi</strong> 
				</div>
				<div class="small float-right col-md-3 m-t-md text-right">
					Statistica dei tui <strong>Goblins</strong> Tesserati
				</div>
				<div class="flot-chart m-b-xl">
					<div class="flot-chart-content" id="flot-dashboard5-chart"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<div class="table-responsive">
							<table class="table table-striped table_stats_json">
								<thead>
									<tr>
										<th>Tana</th>
										<th>N.Tesserati</th>
										<th>% Tesserati</th>
									</tr>
								</thead>
								<tbody>
								<?php
									foreach (\GOBLINS\Tane::getActive() as $key => $value) {
										if ($value['id_tana']==\APP\Parameter::getSpec('id_tana_nazionale')){
											$trim='<i>Tana Nazionale</i>';
										} else {
											$trim=str_replace('La Tana dei Goblin', '', $value['Nome']);	
										}
										$params=array(
											'list_tane'	=> [$value['id_tana']],
										);
										$tana=count(\GOBLINS\Manage::getAll(0, $params));
										$percTana=($tana*100)/count($allGlobal);
										echo "<tr>
											<td>{$trim}</td>
											<td class='text-right'>".number_format($tana, 0, '.',',')."</td>
											<td class='text-right'>".number_format($percTana, 2, '.',',')."%</td>
										</tr>";
									}
								?>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Età tesserati tana nazionale:
							<small>media età <?php echo $mediaAnniMiei;?> anni</small>
						</h5>
					</div>
					<div class="ibox-content">
						<div>
							<canvas id="yearsChartMine" height="140"></canvas>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Età dei tesserati globali (compresa nazionale):
							<small>media età <?php echo $mediaAnniGlobal;?> anni</small>
						</h5>
					</div>
					<div class="ibox-content">
						<div>
							<canvas id="yearsChartGlobal" height="140"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>