<?php
//Pagina di reset da TOKEN esterno
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
if (count(HTML\Page::getParameter())!=2) {
	?>
	<script type="text/javascript">
		window.location.href='<?php echo APP_URL;?>';
	</script>
	<?
	exit;
}
if (\IP_GEST\IP_list::verBlocked()){
	?>
	<div class="middle-box text-center loginscreen animated fadeInDown">
		<div>
			<div>
				<img alt="La Tana dei Goblin" class="" src="<?php echo \IMG\Logo::png('logo_long');?>">
			</div>
			<h2 class="text-white">Attenzione!</h2>
			<p class="text-danger">
				IP BLOCCATO
			</p>
			<div class="alert alert-danger">
				Non puoi richiedere un reset di password perchè il tuo <strong>IP</strong> è <strong>BLOCCATO</strong>.
				<br/>
				Richiedi all'amministratore di sistema di sbloccare il tuo indirizzo IP.
			</div>
			<div class="m-t">
				<a href="<?php echo APP_URL;?>" class="btn btn-primary block full-width m-b">
					Torna alla home page
				</a>
			</div>
			<p class="m-t"> <small>La Tana dei Goblin - webapp &copy; 2017 by Spido</small> </p>
		</div>
	</div>
	<?php
	exit;
}
//verifico che il token sia della mail e non scaduto, se no errore.
$token=HTML\Page::getParameter()[0];
$email=HTML\Page::getParameter()[1];

if (!\USERS\Identify::VerifyToken($email, $token)){
	\IP_GEST\IP_list::IncraseIP($email,$token);
	?>
	<div class="middle-box text-center loginscreen animated fadeInDown">
		<div>
			<div>
				<img alt="La Tana dei Goblin" class="" src="<?php echo \IMG\Logo::png('logo_long');?>">
			</div>
			<h2 class="text-white">Attenzione!</h2>
			<p>
				Stai cercando di fare un reset di password non esistente.
			</p>
			<div class="alert alert-danger">
				Il <strong>token</strong> che stai usando non è valido, <strong>non ricaricare</strong> questa pagina, in quanto ogni tentativo forzato aumenterà il rischio di <strong>bloccare</strong> il tuo indirizzo <strong>IP</strong> di connessione.
			</div>
			<div class="m-t">
				<a href="<?php echo APP_URL;?>" class="btn btn-primary block full-width m-b">
					Torna alla home page
				</a>
			</div>
			<p class="m-t"> <small>La Tana dei Goblin - webapp &copy; 2017 by Spido</small> </p>
		</div>
	</div>
	<?php
	exit;
}


//se passo bottone di conferma "RESET"

?>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <img alt="La Tana dei Goblin" class="" src="<?php echo \IMG\Logo::png('logo_long');?>">
        </div>
        <h2 class="text-white">Reset della password</h2>
        <p>
            Sei a questa pagina perchè stai resettando la password per la tua utenza.
        </p>
        <div class="alert alert-info">
				Reset per <strong><?php echo $email?></strong>: verrà creata una nuova password che ti sarà inviata via email in circa <strong>2 minuti</strong>.
			</div>
        <div class="m-t" >
            <button value="<?php echo $token; ?>" data-email="<?php echo $email;?>" type="button" class="btn btn-danger block full-width m-b btn_confirmResetPWS">Conferma Reset Password</button>
            <h4 style="margin-top:-8px;"> - oppure -</h4>
            <a href="<?php echo APP_URL;?>" class="btn btn-default block full-width m-b">
				Annulla e torna alla home page
			</a>
        </div>
        <p class="m-t"> <small>La Tana dei Goblin - webapp &copy; 2017 by Spido</small> </p>
    </div>
</div>
