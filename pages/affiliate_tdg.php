<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}

$tane=\GOBLINS\Tane::getAll();
$numTane=count($tane);
?>
<div class="row">
    <div class="col-sm-8">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Gestione Affiliate TdG</h2>
                <p>
                    <strong>N.B.:</strong> Per esportare il <b>CSV</b> o il <b>PDF</b>, seleziona dal menu <i>Mostra</i> il valore <strong>Tutti</strong>, alternativamente l'esportazione sarà solo degli elementi visualizzati.
                </p>
                <ul class="nav nav-tabs">
                    <span class="pull-right small text-muted disp_countTotalTane"><?php echo $numTane;?> elementi</span>
                    <input class='hidden countTotalTane' value='<?php echo $numTane; ?>' />
                    <li class="active">
                        <a data-toggle="tab" href="#tab-listTdgAffiliate" aria-expanded="true">
                            <i class="fa fa-user"></i> TdG Affiliate
                        </a>
                    </li>
                    <li>
                        <button class="btn btn-sm btn-primary btn_NewTana" data-id='0' style='margin:4px 0 0 10px'>
                            <i class="fa fa-plus"></i> Affilia Nuova Tana
                        </button>
                    </li>
                    <li>
                        <div class="checkbox checkbox-danger disabled_chk" style='margin:12px 0 0 10px' data-placement='bottom' style='cursor:pointer' title='Visualizza le tane disabilitate o che hanno dato disdetta.'>
                            <input id="checkbox_tdg_disa" type="checkbox">
                            <label for="checkbox_tdg_disa" >
                                Visualizza disdette
                            </label>
                        </div>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="listTdgAffiliate" class="tab-pane active">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table_TdG_json" style="max-width: 100%!important">
                                <thead>
                                    <tr>
                                        <th>Tana</th>
                                        <th>Presidente</th>                                        
										<th>Costituzione</th>
										<th>Rinnovo</th>
										<th>Scadenza</th>
										<th>Disdetta entro il</th>
                                        <th>Città</th>
                                        <th>Data_Risoluzione</th>
                                        <th>id</th>
                                        <th>expireStatus</th>
                                        <th>tanaStatus</th>
                                    </tr>
                                </thead>
                                <tbody style='cursor: pointer;'></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 detail_TdGAffiliate">   
    </div>
</div>
<!--- CONSTRUCT END --->

<!--modal edit/add tana -->
<div class="modal inmodal modal_editTana" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-book modal-icon"></i>
                <h4 class="modal-title field" data-name="Nick">Gestione Tana</h4>
                <input type="hidden" class="field" data-name="id_tana"/>
                <small class="font-bold subtitle_modal_tdgAff">Aggiungi o modifica una tana.</small>
            </div>
            <div class="modal-body text-center">
                <div class="row">
                    <div class="col-sm-6 form-group text-left">
                        <label>Nome</label>
                        <input type="text" placeholder="Nome" data-name="Nome" class="form-control field" required>
                    </div>
                    <div class="col-sm-3 form-group text-left">
                        <label>Data Costituzione</label>
                        <input type="text" placeholder="Data Costituzione" data-name="Data_Costituzione" class="form-control field" required>
                    </div>

                    <div class="col-sm-3 form-group text-left">
                        <label>Data ultimo rinnovo</label>
                        <input type="text" placeholder="Data rinnovo" data-name="Data_UltimoRinnovo" class="form-control field" required>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12 form-group text-left">
                        <label>Indirizzo</label>
                        <input type="text" placeholder="Indirizzo: via, piazza, ..." data-name="Indirizzo" class="form-control field" required>
                    </div>
                </div>

                <div class="row" data-nazionale="<?php echo \APP\Parameter::getSpec('id_tana_nazionale'); ?>">
                    <div class="col-sm-4 form-group text-left">
                        <label>Competenza</label>
                        <br>
                        <div class="radio radio-warning radio-inline">
                            <input type="radio" class="field" name="competenza_Prov" data-name="competenza_Prov" id="competenza_Prov_1" value="0">
                            <label for="competenza_Prov_1">
                                Provinciale
                            </label>
                        </div>
                        <div class="radio radio-primary radio-inline">
                            <input type="radio"  class="field" name="competenza_Prov" data-name="competenza_Prov" id="competenza_Prov_2" value="1" checked="">
                            <label for="competenza_Prov_2">
                                Cittadina
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4 form-group text-left">
                        <label>Città</label>
                        <input type="text" maxlength="5" placeholder="Citta" data-name="Citta" class="form-control field" required>
                    </div>
                    <div class="col-sm-4 form-group text-left">
                        <label>Prov.</label>
                        <input type="text" maxlength="2" placeholder="Prov." data-name="Provincia" class="form-control field" required>
                    </div>

                    <div class="col-sm-4 form-group text-left">
                        <label>Presidente</label>
                        <input type="text" placeholder="Presidente" data-name="nick_presidente" class="form-control field" required>
                    </div>
                </div>
                

                <div class="row">
                    <div class="col-sm-12 form-group text-left">
                        <label>Note</label>
                        <textarea  placeholder="Note" data-name="Note" class="form-control field" rows="5" style='resize: none;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary field" data-name="button-modTana"><i class='fa fa-floppy-o'></i> Salva</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal edit/add tana -->