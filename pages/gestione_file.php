<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
$countFiles=count(\FILES\Upload::get());
?>
<div class="row">
	<div class="col-md-6">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Carica nuovi file</h5>
				<div class="ibox-tools">
					<a class="collapse-link call_collapse">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
				<form class="dropzone zipGestDrop">
					<div class="fallback">
						<input name="file" type="file" multiple />
					</div>
				</form>
			</div>
		</div>

		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Elenco files</h5>
				<button class="btn btn-xs btn-danger hidden btn_selAllFile" style='float: right !important;margin:-1px 0 0px 10px'>Seleziona visibili</button>
				<div class="checkbox checkbox-danger multiSelect_file_chk" style='float: right !important;margin:-1px 0 0px 10px' data-placement='bottom' style='cursor:pointer' title='Seleziona più file per gestire in massa i tag e le affiliate'>
					<input id="checkbox_myFile_multi" type="checkbox">
					<label for="checkbox_myFile_multi" >
						Multi Selezione
					</label>
				</div>
			</div>
			<div class="ibox-content">
				<div class="table-responsive">
					<span class="hidden view_table_reload" ><i class="fa fa-pulse fa-spinner"></i> Attendere...</span>
					<table class="table table-striped table-hover table_files_json" style="max-width: 100%!important">
						<thead>
							<tr>
								<th>Data</th>
								<th>File</th>
								<th>Size</th>
								<th>Tag</th>
								<th>id</th>
							</tr>
						</thead>
						<tbody style='cursor: pointer;'></tbody>
					</table>
					<input type="hidden" class="countTotalFiles" value='<?php echo $countFiles;?>'/>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 detail_file"></div>
</div>