<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<div class="row">
    <div class="col-sm-3">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Importa ID da BGG</h2>
                <p>
                    <strong>N.B.:</strong> questa è una schermata di debug per importare fisicamente un ID e sovrascrivere il record attuale nel database.
                </p>
                <div class="input-group">
					<input type="text" target-click=".btn_admin_bggimport_import" placeholder="ID Bgg..." class="form-control enter-focus btn_admin_bggimport_text" />
					<span class="input-group-btn">
						<button type="button" class="btn btn-primary btn_admin_bggimport_import">Importa!</button>
					</span>
				</div>
            </div>
        </div>
    </div>
    <div class="col-md-9 admin_bggimport_right"></div>
</div>