<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<div class="row">
	<div class="col-md-4">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Goblins: core options</h5>
			</div>
			<div class="ibox-content">
				<div class="alert alert-warning">
					<strong><i class="fa fa-warning"></i> Attenzione:</strong> in questa area potrai cambiare parametri vitali di un utente, dalla modificha del <strong>numero di tessera</strong>, al <strong>sostituire</strong> un <strong>nick</strong> con un'altro rimpiazzandone in dati.
				</div>
				<p>
					Scegli un nick per MODIFICARE il numero di tessera o per ELIMINARE il nick attuale rimpiazzandolo poi con uno nuovo.

				</p>
				<div class="input-group">
					<input type="text" target-click=".btn_coreOptions_selectNick" placeholder="Nick goblin..." class="select2_core_data_goblins form-control enter-focus" />
					<span class="input-group-btn">
						<button type="button" class="btn btn-primary btn_coreOptions_selectNick">Vai!</button>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8 admin_core_options_right"></div>
</div>

<!--modal change card number -->
<div class="modal inmodal modal_core_options" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-credit-card modal-icon"></i>
				<h4 class="modal-title">Cambia Tessera</h4>
				<small class="font-bold">Puoi cambiare il numero di tessera a questo utente.</small>
			</div>
			<div class="modal-body text-center body_core_options"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Chiudi</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal change card number -->

<!--modal change card number -->
<div class="modal inmodal modal_core_optionsNick" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-recycle modal-icon"></i>
				<h4 class="modal-title">Cambia Nick</h4>
				<small class="font-bold">Puoi cambiare il nick attuale con uno nuovo.</small>
			</div>
			<div class="modal-body text-center body_core_optionsNick"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Chiudi</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal change card number -->