<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<div class="row">
	<div class="col-lg-12 admin_utenti_box_left"></div>
</div>

<!--modal change card number -->
<div class="modal inmodal modal_adminuser" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-user modal-icon"></i>
				<h4 class="modal-title title_adminuser_modal">Gestione utente</h4>
				<small class="font-bold">Gestisci i dati di accesso dell'utente.</small>
			</div>
			<div class="modal-body text-center body_adminuser_options"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
				<button type="button" class="btn btn-primary btn_save_mod_user_portal"><i class='fa fa-floppy-o'></i> Salva</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal change card number -->


<!--modal picture profile -->
<div class="modal inmodal modal_pictureProfile_user" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-user modal-icon"></i>
                <h4 class="modal-title">Cambia profilo immagine</h4>
                <small class="font-bold">Cambia la foto del profilo.</small>
            </div>
            <div class="modal-body text-center">
            	<div class="row">
            		<div class="col-md-9">
            			<div class="image-crop-profile"><img /></div>
            		</div>
            		<div class="col-md-3">
            			<div class="btn-group">
            				<label title="Upload image file" for="inputImageProfileUser" class="btn btn-primary">
            					<input type="file" accept="image/*" name="file" id="inputImageProfileUser" class="hide">
            					Carica immagine
            				</label>
            			</div>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" value="0" class="btn btn-primary btn_save_picProfile_user"><i class="fa fa-floppy-o"></i> Salva</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal picture profile -->