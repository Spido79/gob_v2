<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<div class="row">
	<div class="col-sm-12">
		<div class="ibox">
			<div class="ibox-content">
				<h2>Lista LOG FILE </h2>
				<p>
					Ricerca libera
				</p>
				<div class="input-group">
					<input type="text" placeholder="Ricerca libera" target-click=".btn_searchLOGS" class="enter-focus input form-control search_log">
					<span class="input-group-btn">
						<button type="button" class="btn btn btn-primary btn_searchLOGS"> <i class="fa fa-search"></i> Cerca</button>
					</span>
				</div>

				
				<ul class="nav nav-tabs">
					<span class="pull-right small text-muted">ultima modifica <?php echo \LOGS\Log::last_modify();?></span>
					<li class="active">
						<a data-toggle="tab" href="#tab-listIPBlocked" aria-expanded="true">
							<i class="fa fa-globe"></i> Lista
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="tab-listIPBlocked" class="tab-pane active">
						<div class="table-responsive load_logs_file"></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>