<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
$hiddenZero=null;
if (count(\IP_GEST\IP_list::listIP())>0){
	$hiddenZero='hidden';
}
?>
<div class="row">
	<div class="col-sm-8">
		<div class="ibox">
			<div class="ibox-content">
				<h2>Lista IP </h2>
				<p>
					Ricerca un IP Specifico
				</p>
				<div class="input-group">
					<input type="text" placeholder="Cerca IP" target-click=".btn_searchIP" class="enter-focus input form-control search_ip">
					<span class="input-group-btn">
						<button type="button" class="btn btn btn-primary btn_searchIP"> <i class="fa fa-search"></i> Cerca</button>
					</span>
				</div>
				
				<ul class="nav nav-tabs">
					<span class="pull-right small text-muted"><?php echo count(\IP_GEST\IP_list::listIP());?> elementi</span>
					<li class="active">
						<a data-toggle="tab" href="#tab-listIPBlocked" aria-expanded="true">
							<i class="fa fa-globe"></i> Lista
						</a>
					</li>
					<li>
						<button class="btn btn-sm btn-danger btn_cleanIP" style='margin:4px 0 0 10px'>
							<i class="fa fa-trash"></i> Pulisci IP selezionati
						</button>
					</li>
				</ul>
				<div class="tab-content">
					<div id="tab-listIPBlocked" class="tab-pane active">
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<thead>
									<th>&nbsp;</th>
									<th>IP</th>
									<th>Connessioni tentate</th>
									<th>Ultima connessione</th>
									<th>Utente tentato</th>
									<th>Pass tentata</th>
								</thead>
								<tbody>
									<tr class="<?php echo $hiddenZero;?> ip-list_nobody">
										<td colspan="6" style='padding-left:30px;'><i>Nessun IP Trovato</i></td>
									</tr>
								<?php
								foreach (\IP_GEST\IP_list::listIP() as $item) {
									$dateT=new \DateTime($item['date_lastTry']);
									$word_try=$item['word_try']? $item['word_try'] : '---';
									$pass_try=$item['pass_try']? $item['pass_try'] : '---';
									echo "<tr>
											<td>
												<div class='checkbox checkbox-danger'>
													<input value='{$item['ip']}' class='checkbox_selection_ip' id='check_ip_{$item['ip']}' type='checkbox'>
													<label for='check_ip_{$item['ip']}'></label>
												</div>
											</td>
											<td class='text-center ip-list' data-ip='".long2ip($item['ip'])."'>".long2ip($item['ip'])."</td>
											<td class='text-center ip-connection'>{$item['tryConnection']}</td>
											<td class='text-center'>".$dateT->format('d M Y H:i')."</td>
											<td class=''>{$word_try}</td>
											<td class=''>{$pass_try}</td>
										</tr>";
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>