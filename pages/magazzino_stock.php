<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
$numTotStock=\MAGAZZINO\Stock::qta(1);
?>
<div class="row">
	<div class="col-sm-5">
		<div class="ibox">
			<div class="ibox-content">
				<h2>Stock</h2>
                <p>
                    <strong>N.B.:</strong> Per esportare il <b>CSV</b> o il <b>PDF</b> di tutto lo stock, seleziona dal menu <i>Mostra</i> il valore <strong>Tutti</strong>, alternativamente l'esportazione sarà solo degli elementi visualizzati.
                </p>
                <p>
                    <small>In questa sezione puoi vedere e gestire tutto lo stock presente nel database.</small>
                </p>
                 <ul class="nav nav-tabs">
                    <span class="pull-right small text-muted disp_countTotalStock"><?php echo $numTotStock;?> elementi</span>
                    <input class='hidden countTotalStock' value='<?php echo $numTotStock; ?>' />
                    <li class="active">
                        <a data-toggle="tab" href="#tab-listStock" aria-expanded="true">
                            <i class="fa fa-table"></i> Stock articoli
                        </a>
                    </li>
                    <li>
                        <button class="btn btn-sm btn-primary btn_magStockNewStock" data-id='0' style='margin:4px 0 0 10px'>
                            <i class="fa fa-plus"></i> Aggiungi articolo
                        </button>
                    </li>
                    <li>
                        <div class="checkbox checkbox-danger magazzino_stock_chk_dismiss" style='margin:12px 0 0 10px' data-placement='bottom' style='cursor:pointer'>
                            <input id="magazzino_stock_chk_dismiss" type="checkbox">
                            <label for="magazzino_stock_chk_dismiss" >
                                Stock Dismesso
                            </label>
                        </div>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="listStock" class="tab-pane active">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table_stock_json" style="max-width: 100%!important">
                                <thead>
                                    <tr>
                                        <th>Articolo</th>
                                        <th>Qtà</th>
                                        <th>id_ana</th>
                                    </tr>
                                </thead>
                                <tbody style='cursor: pointer;'></tbody>
                            </table>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
    <div class="col-sm-7 magazzino_stock_details">
    </div>
</div>

<!--modal log stock -->
<div class="modal inmodal modal_stock_stock_log" data-backdrop="static" data-keyboard="true" role="dialog" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-history modal-icon"></i>
                <h4 class="modal-title">Movimenti articolo</h4>
                <h3 class="">Lista movimenti dell'articolo UID: <strong class="stock_stock_uid"></strong></h3>
            </div>
            <div class="modal-body text-center ibox">
                <div class="ibox-content body_stockLogStock_modal text-left"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<!--modal add stock -->
<div class="modal inmodal modal_stockMagStock" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-shopping-basket modal-icon"></i>
                <h4 class="modal-title stockMag-action-addModd">Inserimento stock</h4>
                <h3 class="text-danger font-bold stockMag-title-add">Aggiungi un gioco allo stock.</h3>
            </div>
            <div class="modal-body text-center body_stockMagStock_modal">
                <input type="hidden" class="data-field" data-field="id_ana" value="0" />
                <div class='row'>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <label>Magazzino</label>
                            <select placeholder="Seleziona un magazzino" class="data-field form-control" data-field="id_store" required>
                            <?php
                            $params=array(
                                'active'=>true,
                            );
                            $stores=\MAGAZZINO\Stores::getAll(array('id_store', 'store') , false, $params);
                            foreach ($stores as $item) {
                                echo "<option value='{$item['id_store']}'>{$item['store']}</option>";
                            }
                            ?>
                            </select>
                        </div>
                    </div>

                    <div class='col-md-6'>
                        <div class="form-group">
                            <label>Propietà</label>
                            <select placeholder="Seleziona una tana" class="data-field form-control" data-field="id_owner" required>
                            <?php
                            $stores=\GOBLINS\Tane::getActive();
                            foreach ($stores as $item) {
                                $nomeTana=str_replace('La Tana dei Goblin', '', $item['Nome']);
                                if ($nomeTana==''){
                                    $nomeTana='Tana Nazionale';
                                }
                                echo "<option value='{$item['id_tana']}'>{$nomeTana}</option>";
                            }
                            ?>
                            </select>
                        </div>
                    </div>

                </div>
                <div class='row'>
                    <div class='col-md-4'>
                        <div class="form-group">
                            <label>Data Acquisto</label>
                            <input placeholder="Inserisci la data" class="form-control data-field" data-field="data_acquisto" type="text" value="" required/>
                        </div>
                    </div>
                    <div class='col-md-4'>
                        <div class="form-group">
                            <label>Origine</label>
                            <select placeholder="Seleziona provenienza" class="data-field form-control" data-field="origin" required>
                            <?php
                            $array=array(
                                'acquisto',
                                'demo da autore',
                                'demo da editore',
                                'donazione',
                            );
                            foreach ($array as $item) {
                                echo "<option value='{$item}'>{$item}</option>";
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class='col-md-4'>
                        <div class="form-group">
                            <div class='checkbox checkbox-danger checkbox-circle' style="margin-top:0;">
                                <input id='chk_stockAnaNew' type='checkbox' class='data-field' value='1' data-field='hidden'/>
                                <label for='chk_stockAnaNew' style="display:inline!important;">
                                    <b>Nascosto</b>
                                    <br><span class='small text-muted'>Se nascosto, questo pezzo sarà visibile solo dal propietario e non disponibile per il prestito.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-md-9'>
                        <div class="form-group">
                            <label>Note</label>
                            <textarea style="resize:none;" placeholder="Note articolo..." class="data-field form-control" data-field="notes" rows="5"></textarea>
                        </div>
                    </div>

                    <div class='col-md-3'>
                        <div class="form-group">
                            <label>Locazione</label>
                            <input type="text" placeholder="Locazione..." class="data-field form-control" data-field="locazione" maxlength =15 />
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary btn_save_modal_stockMagStock" data-id="0"><i class='fa fa-floppy-o'></i> Salva</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal add stock -->

<!--modal add main stock -->
<div class="modal inmodal modal_magazzino_stock_main_stock" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-user modal-icon"></i>
                <h4 class="modal-title">Seleziona Articolo</h4>
                <small class="font-bold">Seleziona un gioco da aggiungere al tuo stock.</small>
            </div>
            <div class="modal-body text-center">
                <div class="input-group">
                    <input type="text" target-click=".btn_modal_magazzino_stock_main_stock_selectGame" placeholder="Articolo.." class="select2_stock_main_stock_selectGame form-control enter-focus" />
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn_stock_main_stock_selectGame">Seleziona</button>
                    </span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal add main stock -->