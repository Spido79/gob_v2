<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<div class="row animated fadeInRightBig">
	<div class="col-md-8 white-bg">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1>TdG Portale Gestione</h1>
				<p class="text-danger">Esclusiva per lo staff</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<img src="img/landing/iphone.jpg" class="img-responsive" alt="dashboard">
			</div>
			<div class="col-md-8 features-text text-left wow fadeInRight">
				<p>
					Questo sito di amministrazione è per lo staff de La Tana dei Goblin.
					<br>
					Per problematiche di
					<a href="javascript:void(0)" data-toggle="popover" data-placement="bottom" data-content="<span class=''>Errori di campi sovrapposti, errori di scrittura, errori di posizionamento degli elementi</span>" data-html="true" title="Visualizzazione"> 
						<code>visualizzazione</code>

					</a>
					o di 
					<a href="javascript:void(0)" data-toggle="popover" data-placement="bottom" data-content="<span class=''>Impossibilità di aggiornare, consultare, inserire campi o informazioni</span>" data-html="true" title="Incompatibilità"> 
						<code>incompatibilità</code>
					</a>
					del sito, ti chiedo gentilmente di conttatarmi a questi recapiti:
					<br>
					<i class="fa fa-envelope"></i> Mail: <a href="mailto:jones@goblins.net">jones@goblins.net</a>
				</p>
				<p class="text-right">
					<i>Cerco di rendere migliore il tuo lavoro,<br/>
					grazie per la tua collaborazione.<br/>
					<strong></strong></i>
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="ibox ">
			<div class="ibox-title">
				<h5><i class="fa fa-map-marker"></i> Le Affiliate d'Italia</h5>
			</div>
			<div class="ibox-content">
				<div class="google-map" id="map1"></div>
			</div>
		</div>
	</div>
</div>