<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <img alt="La Tana dei Goblin" class="" src="<?php echo \IMG\Logo::png('logo_long');?>">
        </div>
        <h2 class="text-white">Portale di Gestione</h2>
        <p>
            Il sito di gestione delle nostre affiliate a <br/>La Tana dei Goblin
        </p>
        <p class="text-white">Inserisci le tue credenziali:</p>
        <div class="m-t" role="form" action="index.html">
            <div class="form-group">
                <input type="email" target-click=".btn_login" class="enter-focus field_user form-control" placeholder="Nome Utente" required="">
            </div>
            <div class="form-group">
                <input type="password" target-click=".btn_login" class="enter-focus field_pass form-control" placeholder="Password" required="">
            </div>
            <button type="button" class="btn btn-primary block full-width m-b btn_login">Login</button>

            <a href="#" data-toggle="modal" data-target=".modal_login"><small>Password dimenticata?</small></a>
        </div>
        <p class="m-t"> <small>La Tana dei Goblin - webapp &copy; 2017 by Spido</small> </p>
    </div>
</div>

<!--modal password forgot -->
<div class="modal inmodal modal_login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Recupero password</h4>
                <small class="font-bold">Segui passo passo la procedura per il recuper password.</small>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ibox-content">
                            <h3 class="font-bold">Indirizzo email</h3>
                            <p>
                                Inserisci il tuo indirizzo email per ricevere il link di reset della tua password.
                            </p>
                            <div class="row">
                                <div class="col-lg-12">
                                 <div class="form-group">
                                    <input type="email" class="form-control email_forgotSend" placeholder="Indirizzo email" required="">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary btn_forgotSend">Recupera</button>
            </div>
        </div>
    </div>
</div>
<!--end modal password forgot -->