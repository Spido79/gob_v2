<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<div class="row">
	<div class="col-md-4">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Anagrafica Articoli</h5>
			</div>
			<div class="ibox-content">
				<div class="alert alert-info">
					<strong><i class="fa fa-warning"></i> Attenzione:</strong> tutti gli articoli anagrafici sono importanti ogni giorno da <b>BGG</b>.
				</div>
				<p>
					Scrivi il titolo del gioco per gestirne l'anagrafica:
				</p>
				<p>
					Grammatiche ammesse:
					<ul>
						<li>Ricerca id BGG: <code>id:&lt;id bgg&gt;</code> - Esempio: <code>id:3076</code> (Puerto Rico)
						<li>Ricerca testo esatto: <code>t:&lt;titolo esatto&gt;</code> - Esempio: <code>t:puerto rico</code>
				</p>
				<div class="input-group">
					<input type="text" target-click=".btn_magAna_selectGame" placeholder="Titolo..." class="select2_magAna_title form-control enter-focus" />
					<span class="input-group-btn">
						<button type="button" class="btn btn-primary btn_magAna_selectGame">Vai!</button>
					</span>
				</div>
			</div>
		</div>
		<div class="ibox float-e-margins mag_ana_gameList"></div>
	</div>

	<div class="col-md-8 admin_mag_ana_right"></div>
</div>

<!--modal log stock -->
<div class="modal inmodal modal_ana_stock_log" data-backdrop="static" data-keyboard="true" role="dialog" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-history modal-icon"></i>
				<h4 class="modal-title">Movimenti articolo</h4>
				<h3 class="">Lista movimenti dell'articolo UID: <strong class="ana_stock_uid"></strong></h3>
			</div>
			<div class="modal-body text-center ibox">
				<div class="ibox-content body_anaLogStock_modal text-left"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Chiudi</button>
			</div>
		</div>
	</div>
</div>
<!--modal add stock -->
<div class="modal inmodal modal_anaMagStock" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-shopping-basket modal-icon"></i>
				<h4 class="modal-title anaMag-action-addModd">Inserimento stock</h4>
				<h3 class="text-danger font-bold anaMag-title-add">Aggiungi un gioco allo stock.</h3>
			</div>
			<div class="modal-body text-center body_anaMagStock_modal">
				<input type="hidden" class="data-field" data-field="id_ana" value="0" />
				<div class='row'>
					<div class='col-md-6'>
						<div class="form-group">
							<label>Magazzino</label>
							<select placeholder="Seleziona un magazzino" class="data-field form-control" data-field="id_store" required>
							<?php
							$params=array(
								'active'=>true,
							);
							$stores=\MAGAZZINO\Stores::getAll(array('id_store', 'store') , false, $params);
							foreach ($stores as $item) {
								echo "<option value='{$item['id_store']}'>{$item['store']}</option>";
							}
							?>
							</select>
						</div>
					</div>

					<div class='col-md-6'>
						<div class="form-group">
							<label>Propietà</label>
							<select placeholder="Seleziona una tana" class="data-field form-control" data-field="id_owner" required>
							<?php
							$stores=\GOBLINS\Tane::getActive();
							foreach ($stores as $item) {
								$nomeTana=str_replace('La Tana dei Goblin', '', $item['Nome']);
								if ($nomeTana==''){
									$nomeTana='Tana Nazionale';
								}
								echo "<option value='{$item['id_tana']}'>{$nomeTana}</option>";
							}
							?>
							</select>
						</div>
					</div>

				</div>
				<div class='row'>
					<div class='col-md-4'>
						<div class="form-group">
							<label>Data Acquisto</label>
							<input placeholder="Inserisci la data" class="form-control data-field" data-field="data_acquisto" type="text" value="" required/>
						</div>
					</div>
					<div class='col-md-4'>
						<div class="form-group">
							<label>Origine</label>
							<select placeholder="Seleziona provenienza" class="data-field form-control" data-field="origin" required>
							<?php
							$array=array(
								'acquisto',
								'demo da autore',
								'demo da editore',
								'donazione',
							);
							foreach ($array as $item) {
								echo "<option value='{$item}'>{$item}</option>";
							}
							?>
							</select>
						</div>
					</div>
					<div class='col-md-4'>
						<div class="form-group">
							<div class='checkbox checkbox-danger checkbox-circle' style="margin-top:0;">
								<input id='chk_stockAnaNew' type='checkbox' class='data-field' value='1' data-field='hidden'/>
								<label for='chk_stockAnaNew' style="display:inline!important;">
									<b>Nascosto</b>
									<br><span class='small text-muted'>Se nascosto, questo pezzo sarà visibile solo dal propietario e non disponibile per il prestito.</span>
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class='row'>
					<div class='col-md-9'>
						<div class="form-group">
							<label>Note</label>
							<textarea style="resize:none;" placeholder="Note articolo..." class="data-field form-control" data-field="notes" rows="5"></textarea>
						</div>
					</div>

					<div class='col-md-3'>
						<div class="form-group">
							<label>Locazione</label>
							<input type="text" placeholder="Locazione..." class="data-field form-control" data-field="locazione" maxlength =15 />
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
				<button type="button" class="btn btn-primary btn_save_modal_anaMagStock" data-id="0"><i class='fa fa-floppy-o'></i> Salva</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal add stock -->