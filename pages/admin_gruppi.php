<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<div class="row">
	<div class="col-lg-6 admin_gruppi_box_left"></div>
	<div class="col-lg-6 admin_gruppi_box_right"></div>
</div>
<!--modal change group user -->
<div class="modal inmodal modal_admin_gruppi" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Cambia Gruppo</h4>
				<small class="font-bold">Puoi cambiare gruppo a questo utente.</small>
			</div>
			<div class="modal-body body_admin_gruppi"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
				<button type="button" class="btn btn-primary btn_AdminGruppiModUser">Cambia gruppo</button>
			</div>
		</div>
	</div>
<!--end modal change group user -->