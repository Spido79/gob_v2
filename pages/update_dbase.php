<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<div class="row">
	<div class="col-md-4">
		<div class="ibox">
			<div class="ibox-title">
				<span class="label label-success pull-right">COMUNI</span>
				<h5>Aggiornamento tabella COMUNI</h5>
			</div>
			<div class="ibox-content">
				<h4>Tabella: <code>tbl_comnuni</code></h4>
				<p>
					Questa procedura aggiorna la lista comuni rimpiazzando la tabella attuale con i dati trovati al link:
					<small>https://raw.githubusercontent.com/matteocontrini/comuni-json/master/comuni.json</small>
				</p>
				<div>
					<span>Attuali voci nel database:</span>
					<div class="stat-percent" data-return="update_comuni" ><?php echo \COMUNI\Istat::countComuni(); ?></div>
				</div>
				<div class="row" style='margin-top:4px;'>
					<div class="col-xs-12">
						<button value="update_comuni" class=" pull-right btn btn-sm btn-danger btn_updateDbase"><i class="fa fa-cog"></i> Aggiorna</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>