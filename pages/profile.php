<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<div class="row">
	<div class="col-sm-6 col-lg-4">
		<div class="widget-head-color-box navy-bg p-lg text-center">
			<div class="m-b-md">
				<h2 class="font-bold no-margins">
					<?php 
					echo \USERS\Identify::get('user_name');
					echo ' ';
					echo \USERS\Identify::get('user_surname');
					?>
				</h2>
				<small><?php echo \USERS\Identify::get('label_group');?></small>
			</div>
			
			<div class="img_overlay">
				<img src="<?php echo \IMG\Logo::png(\USERS\Identify::UserID(), 'img/profiles');?>" class="profile_personal_img img-fluid img-circle circle-border m-b-md" alt="profile" style="width: 250px;">
				<button class='btn btn-success btn-xs btn_changeProfile_picture'>
					<i class='fa fa-camera'></i> cambia foto
				</button>
			</div>
			<div>
				Data di creazione utente:
				<nobr>
				<?php 
					$user= new \USERS\Detail(\USERS\Identify::UserID());
					$creation=new DateTime($user->get('user_dateCreation'));
					echo $creation->format('d M Y');
				?>
				</nobr>
			</div>
		</div>
		<div class="widget-text-box">
			<h4 class="media-heading">Telefono</h4>
			<p>
				<?php 
					if ($user->get('user_telephone')){
						echo '<i class="fa fa-phone"></i> '.$user->get('user_telephone');
					} else {
						echo '<i>Nessun numero di telefono associato</i>';
					}
				?>
			</p>
		</div>
	</div>

	<div class="col-sm-6 col-lg-4">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5> Password <small>Modifica la tua password attuale</small></h5>
			</div>
			<div class="ibox-content">
				<div class="row row_changePassword_success hidden">
					<div class="col-sm-12">
						<div class="alert alert-success">
							<i class="fa fa-check-circle-o"></i> Password modificata correttamente.
						</div>
					</div>
				</div>
				<div class="row row_changePassword_main">
					<div class="col-sm-12" style='margin-bottom:10px;'>
						<small>La nuova password non dovrà coincidere con quella attuale, inoltre dovrà avere almeno una lunghezza di 5 caratteri.</small>
					</div>
					<div class="col-sm-12 b-r">
						<div class="form-group">
							<label>Password attuale</label>
							<input type="password" target-click=".btn_changePassword" placeholder="Attuale" class="enter-focus actual_password form-control">
						</div>
						
						<div class="form-group">
							<label>Nuova Password</label>
							<input type="password" target-click=".btn_changePassword" placeholder="Nuova" class="enter-focus new_password form-control">
						</div>

						<div class="form-group">
							<label>Ripeti nuova Password</label>
							<input type="password" target-click=".btn_changePassword" placeholder="Ripeto" class="enter-focus newB_password form-control">
						</div>
						
						<div>
							<button class="btn btn-xs btn-primary pull-right btn_changePassword">
								<i class="fa fa-check-circle-o"></i>  Cambia password
							</button>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--modal picture profile -->
<div class="modal inmodal modal_pictureProfile" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-user modal-icon"></i>
                <h4 class="modal-title">Cambia profilo immagine</h4>
                <small class="font-bold">Cambia la tua foto del profilo.</small>
            </div>
            <div class="modal-body text-center">
            	<div class="row">
            		<div class="col-md-9">
            			<div class="image-crop"><img /></div>
            		</div>
            		<div class="col-md-3">
            			<div class="btn-group">
            				<label title="Upload image file" for="inputImageProfile" class="btn btn-primary">
            					<input type="file" accept="image/*" name="file" id="inputImageProfile" class="hide">
            					Carica immagine
            				</label>
            			</div>
            		</div>
            	</div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary btn_save_picProfile"><i class="fa fa-floppy-o"></i> Salva</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal picture profile -->