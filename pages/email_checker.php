<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
$countFiles=count(\FILES\Upload::get());
?>
<div class="row">
	<div class="col-lg-2">
		<div class="ibox float-e-margins">
			<div class="ibox-content mailbox-content">
				<div class="file-manager">
					<h5>Cartelle</h5>
					<ul class="folder-list m-b-md" style="padding: 0">
						<li><a class="load_folder" data-type="1"> <i class="fa fa-paper-plane-o"></i> Inviata <span class="label label-default pull-right folder_counter" data-folder="sent">0</span> </a></li>
						<li><a class="load_folder" data-type="2"> <i class="fa fa-pause"></i> In uscita <span class="label label-warning pull-right folder_counter" data-folder="sending">0</span> </a></li>
					</ul>
					<h5>Categorie</h5>
					<ul class="category-list" style="padding: 0">
						<li><a class="load_folder" data-type="3"> <i class="fa fa-circle text-navy"></i> Gest. Goblins </a></li>
						<li><a class="load_folder" data-type="4"> <i class="fa fa-circle text-danger"></i> Blocco IP</a></li>
						<li><a class="load_folder" data-type="5"> <i class="fa fa-circle text-primary"></i> Credenziali</a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-10 email_checker_inbox"></div>
</div>

<!--modal show email -->
<div class="modal inmodal modal_emailChecker" data-backdrop="static" data-keyboard="true" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-envelope-o modal-icon"></i>
				<h4 class="modal-title">Dettaglio email:</h4>
				<small class="font-bold subject_email_checker"></small>
			</div>
			<div class="modal-body text-center body_email_checker"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Chiudi</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal show email -->