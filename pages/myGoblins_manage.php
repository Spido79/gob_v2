<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}

$taneRif=explode(',', \USERS\Identify::get('tane_riferimento'));
$found=0;
foreach ($taneRif as $value) {
    if (intval($value)>0) {
        $found++;
    }
}

$counted=0;
if (count($taneRif)>0){
  $paramsPass=array(
    'list_tane' => $taneRif,
    'order'     => array(
      'field' => 'Nick',
      'direction' => 'asc',
    )
  );
  $allMyGobs=\GOBLINS\Manage::getAll(0, $paramsPass);
}
if ($found==0){
    ?>
    <div class="row">
    <div class="col-sm-8">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Goblins</h2>
                <p>
                    <span class='text-danger'>Non sei responsabile di nessuna tana</span>
                </p>
            </div>
        </div>
    </div>
    </div>
    <?php
    exit;
}
$paramsPass=array(
    'list_tane' => $taneRif,
);
$mumberGobl=count(\GOBLINS\Manage::getAll(0, $paramsPass));
?>
<div class="row">
    <div class="col-sm-8">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Goblins - I tuoi tesserati</h2>
                <p>
                    <strong>N.B.:</strong> Per esportare il <b>CSV</b> o il <b>PDF</b> di tutti i tuoi tesserati, seleziona dal menu <i>Mostra</i> il valore <strong>Tutti</strong>, alternativamente l'esportazione sarà solo degli elementi visualizzati.
                </p>
                <ul class="nav nav-tabs">
                    <span class="pull-right small text-muted disp_countTotalMyGoblins"><?php echo $mumberGobl;?> elementi</span>
                    <input class='hidden countTotalMyGoblins' value='<?php echo $mumberGobl; ?>' />
                    <li class="active">
                        <a data-toggle="tab" href="#tab-listMyGoblins" aria-expanded="true">
                            <i class="fa fa-user"></i> Goblins tesserati
                        </a>
                    </li>
                    <li>
                        <button class="btn btn-sm btn-primary btn_NewGoblin" data-id='0' style='margin:4px 0 0 10px'>
                            <i class="fa fa-plus"></i> Tessera Nuovo Goblin
                        </button>
                    </li>
                    <?php
                    if (\GOBLINS\Request::checkTane($taneRif)['requests']>0){
                    ?>
                    <li>
                        <div class="checkbox checkbox-warning multiSelect_chk" style='margin:12px 0 0 10px' data-placement='bottom' style='cursor:pointer' title='Elenca le richieste di trasferimento goblin pendenti'>
                            <input id="checkbox_myGob_req" type="checkbox" checked>
                            <label for="checkbox_myGob_req" >
                                Richieste Pendenti
                            </label>
                        </div>
                    </li>
                    <?php
                    } else {
                        echo "<input type='hidden' id='checkbox_myGob_req' />";
                    }
                    ?>
                    <li>
                        <div class="checkbox checkbox-danger multiSelect_chk" style='margin:12px 0 0 10px' data-placement='bottom' style='cursor:pointer' title='Azioni su un elenco personalizzato di Goblins.<br><strong>Singolo click</strong> sul nick per aggiungere alla lista'>
                            <input id="checkbox_myGob_multi" type="checkbox">
                            <label for="checkbox_myGob_multi" >
                                Multi Selezione
                            </label>
                        </div>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="listMyGoblins" class="tab-pane active">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table_users_json" style="max-width: 100%!important">
                                <thead>
                                    <tr>
                                        <th>Nick</th>
                                        <th>Cognome</th>
                                        <th>Nome</th>
                                        <th>id</th>
                                        <th>Email</th>
                                        <th>req</th>
                                        <th>Tana</th>
                                    </tr>
                                </thead>
                                <tbody style='cursor: pointer;'></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 detail_myGoblins">   
    </div>
</div>
<!--- CONSTRUCT END --->

<!--modal edit/add goblin -->
<div class="modal inmodal modal_editGoblin" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-user modal-icon"></i>
                <h4 class="modal-title field" data-name="Nick">Gestione Goblin</h4>
                <input type="hidden" class="field" data-name="id_goblin"/>
                <small class="font-bold subtitle_modal_selectMyGoblin">Tessera e gestisci i dati di questo goblin.</small>
            </div>
            <div class="modal-body text-center modal_editGoblin_body">
                <div class="row">
                    <div class="col-xs-12 form-group text-left">
                        <label class="text_for_span hidden" data-name="tana_name" >Utente attualmento tesserato a:</label>
                        <span class="hidden text-danger field" data-name="tana_name" style="font-weight:bold"></span> 
                        <label class="text_for_select" data-name="id_tana" >Tessera per "La tana dei Goblin":</label>
                        <div class="text_for_select_move hidden" data-name="tana_name" >
                            Richiedi trasferimento alla tua Tana di:
                        </div>
                        <select data-name="id_tana" class="form-control field" required>
                        <?php
                            $mieTane=\GOBLINS\Tane::spec($taneRif);
                            foreach ($mieTane as $id_tana => $name) {
                                if (\GOBLINS\Tane::isActive($id_tana)){
                                    if ($id_tana==\APP\Parameter::getSpec('id_tana_nazionale')){
                                        $trim='Tana Nazionale';
                                    } else {
                                        $trim=str_replace('La Tana dei Goblin', '', $name);
                                    }
                                    echo '<option value="'.$id_tana.'">'.$trim.'</option>';
                                }
                            }
                        ?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 form-group text-left">
                        <label>Nome</label>
                        <input type="text" placeholder="Nome" data-name="nome_goblin" class="form-control field" required>
                    </div>
                    <div class="col-sm-4 form-group text-left">
                        <label>Cognome</label>
                        <input type="text" placeholder="Cognome" data-name="cognome_goblin" class="form-control field" required>
                    </div>

                    <div class="col-sm-4 form-group text-left">
                        <label>Data Nascita</label>
                        <input type="text" placeholder="Data di nascita" data-name="dataNascita_goblin" class="form-control field" required>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12 form-group text-left">
                        <label>Indirizzo</label>
                        <input type="text" placeholder="Indirizzo: via, piazza, ..." data-name="indirizzo_goblin" class="form-control field" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 form-group text-left">
                        <label>Città</label>
                        <input type="text" placeholder="Città" data-name="citta_goblin" class="form-control field" required  data-city="1" city-field="1">
                    </div>
                    <div class="col-sm-3 form-group text-left">
                        <label>Cap</label>
                        <input type="text" maxlength="5" placeholder="Cap" data-name="cap_goblin" class="form-control field" required  data-city="1" city-field="2">
                    </div>
                    <div class="col-sm-3 form-group text-left">
                        <label>Prov.</label>
                        <input type="text" maxlength="2" placeholder="Prov." data-name="provincia_goblin" class="form-control field" required readonly>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 form-group text-left">
                        <label>Email</label>
                        <input type="email" placeholder="Email" data-name="email_goblin" class="form-control field" required>
                    </div>
                    <div class="col-sm-6 form-group text-left">
                        <label>Telefono</label>
                        <input type="text" placeholder="Telefono" data-name="contatto_goblin" class="form-control field">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12 form-group text-left">
                        <label>Note</label>
                        <textarea  placeholder="Note" data-name="note_goblin" class="form-control field" style='resize: none;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary field" data-name="button-mod"><i class='fa fa-floppy-o'></i> Salva</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal edit/add goblin -->

<!--modal edit/add goblin -->
<div class="modal inmodal modal_selectMyGoblin" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-user modal-icon"></i>
                <h4 class="modal-title">Seleziona Goblin</h4>
                <small class="font-bold">Seleziona un nuovo Goblin da tesserare alla tua tana.</small>
            </div>
            <div class="modal-body text-center">
                <div class="input-group">
                    <input type="text" target-click=".btn_myGoblins_selectNick" placeholder="Nick goblin..." class="select2_myGoblinse_data_goblins form-control enter-focus" />
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn_myGoblins_selectNick">Seleziona</button>
                    </span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal edit/add goblin -->