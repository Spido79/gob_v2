<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<div class="row">
	<div class="col-lg-6">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Elenco dei miei files</h5>
			</div>
			<div class="ibox-content">
				<div class="json_tree"></div>
			</div>
		</div>
	</div>

	<div class="col-lg-6 detail_myFile"></div>
</div>