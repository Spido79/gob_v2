<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
$gobGest=count(\GOBLINS\Manage::getAll(-2));
$gobForum=count(\GOBLINS\Manage::getAllForum());
$diff = $gobGest -$gobForum;

$color='text-danger';
if ($diff==0){
	$color='text-primary';
} else if ($diff >0){
	$color='text-warning';
}

?>
<div class="row">
	<div class="col-md-4">
		<div class="ibox">
			<div class="ibox-title">
				<span class="label label-success pull-right">NICK</span>
				<h5>Pairing nick FORUM -> Gestionale</h5>
			</div>
			<div class="ibox-content">
				<h4>Tabella: <code>tbl_goblins</code></h4>
				<p>
					Questa procedura aggiorna la lista dei nick registrati nel forum de La Tana dei Goblin con il portale gestionale.
				</p>
				<div>
					<span>Attuali nick nel database GESTIONALE:</span>
					<div class="stat-percent" data-return="update_nick_gest" ><?php echo $gobGest; ?></div>
				</div>
				<div>
					<span>Attuali nick nel database FORUM:</span>
					<div class="stat-percent" data-return="update_nick_forum" ><?php echo $gobForum; ?></div>
				</div>
				<hr style='margin-top:5px;margin-bottom: 5px;'>
				<div>
					<span>Differenza:</span>
					<div class="stat-percent <?php echo $color;?>" data-return="update_nick_diff" ><?php echo $diff; ?></div>
				</div>
				
				<div class="row" style='margin-top:4px;'>
					<div class="col-xs-12">
						<button value="update_nick" class="pull-right btn btn-sm btn-danger btn_updateDbaseNick"><i class="fa fa-cog"></i> Aggiorna</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="ibox">
			<div class="ibox-title">
				<span class="label label-warning pull-right">Anomalie</span>
				<h5>Nick presenti nel gestionale NON presenti nel forum</h5>
			</div>
			<div class="ibox-content">
				<div class='nick_anomalie_message'></div>
				<div class='nick_anomalie_table'></div>
			</div>
		</div>
	</div>
</div>