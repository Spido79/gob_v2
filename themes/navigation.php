<?php
//ATTENZIONE PAGINA COMUNE!
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}?>
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element">
					<span>
						<img alt="image" class="profile_personal_img img-circle" src="<?php echo \IMG\Logo::png(\USERS\Identify::UserID(), 'img/profiles');?>" style="width:80px;" />
					</span>
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<span class="clear"> 
							<span class="block m-t-xs"> 
								<strong class="font-bold">
									<?php 
										echo \USERS\Identify::get('user_name');
										echo ' ';
										echo \USERS\Identify::get('user_surname');
									?>
								</strong>
							</span>
							<span class="text-muted text-xs block">
								<?php echo \USERS\Identify::get('label_group');?> <b class="caret"></b>
							</span>
						</span>
					</a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li><a class="btn_ribbon" data-hash="#profile" data-icon="fa fa-user">Profilo</a></li>
						<li class="divider"></li>
						<li><a class="btn_logout">Logout</a></li>
					</ul>
				</div>
				<div class="logo-element">
					TdG+
				</div>
			</li>
			<?php 
				//creazione menu
				echo \MENU\Primary::drawMenu(\MENU\Primary::$Menu);
			?>
		</ul>
	</div>
</nav>
