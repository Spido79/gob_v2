<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<div class="row wrapper border-bottom white-bg page-heading hidden-xs">
	<div class="col-xs-12">
		<h2>La Tana dei Goblin - Portale Gestione</h2>
		<button class="btn btn-primary dim btn_ribbon" data-hash="#home" type="button"><i class="fa fa-home"></i></button>
		<div class="button_ribbon" style='display: inline-block;'></div>
	</div>
</div>