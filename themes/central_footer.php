<?php
//ATTENZIONE PAGINA COMUNE!
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}?>
<div class="footer">
	<div class="pull-right">
		Portale <strong>Gestione</strong> v<?php echo \APP\Parameter::release();?>
	</div>
	<div>
		<img src="<?php echo \IMG\Logo::png('goblin_face')?>" style='width:20px;' /> <strong>Copyright</strong> La Tana dei Goblin &copy; 2017
	</div>
</div>