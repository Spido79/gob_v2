<?php
//ATTENZIONE PAGINA COMUNE!
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
//Ricordarsi: \APP\Parameter::version(); serve da mettere accanto ai css e js per la chache
//Ricordarsi: \APP\Parameter::release(); è la versione del software
//Ricordarsi: \APP\Parameter::serial(); è per la cache delle immagini
$coloBG=null;
if (\HTML\Page::SpecialPage()){
	$coloBG='green-bg';
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo \Dictonary\Translation::word('Title');?></title>
	<!-- MAIN CSS-->
	<?php \CSS_JS\CSS::Start();?>
</head>
<body class="<?php echo $coloBG;?>">