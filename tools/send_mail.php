<?php
if (isset($post)==false){
    $virtualPath='.';
    require_once(__dir__."/../lib/init.php");
}

//invia le email da crontabbare ogni X minuti
$Sql='SELECT * FROM _system where param = "sending_mail" and value=0;';
\SQL::Execute($Sql);
if (\SQL::$Rows!=1){
	LOGS\Log::write('MAIL', "STOP: Cron send_mail is already started", false);
	exit;
}

$Sql='UPDATE _system SET value=1 WHERE param = "sending_mail" and value=0;';
\SQL::Execute($Sql);

\MAIL\Create::Send();

$Sql='UPDATE _system SET value=0 WHERE param = "sending_mail" and value=1;';
\SQL::Execute($Sql);