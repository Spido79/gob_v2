<?php
if (isset($post)==false){
    $virtualPath='.';
    require_once(__dir__."/../lib/init.php");
}
$reciever=array(
	'mail'	=> 'jones@goblins.net',
	'name'	=> 'Jones'
);


//da schedulare ogni giorno alla 6 di mattina
$arrayRenew=array(); //0 auto renew,

$today=new DateTime();
$today->setTime(0, 0, 0);

foreach (\GOBLINS\Tane::getActive() as $tana) {
	$DataScadenzaDate=new DateTime($tana['Data_UltimoRinnovo']);
	$DataScadenzaDate->setTime(0, 0, 0);
	$DataScadenzaDate->add(new DateInterval('P2Y'));
	$interval = $today->diff($DataScadenzaDate);
	$daysPassed=intval($interval->format('%R%a'));

	if($daysPassed<=0){
		//Auto Renew
		$array=array(
			'Data_UltimoRinnovo' => $DataScadenzaDate->format('Y-m-d')
		);
		\GOBLINS\Tane::change($tana['id_tana'], $array);

		$arrayRenew[]=array(
			'nome'	=> $tana['Nome'],
			'type'		=> 0,
			'expire' => $DataScadenzaDate->format('d-m-Y'),
		);
	} else if($daysPassed<=90){
		switch ($daysPassed) {
			case 90:
			case 75:
			case 60:
			case 45:
			case 30:
			case 15:
			case 5:
			case 2:
			case 1:
				$arrayRenew[]=array(
					'nome'	=> $tana['Nome'],
					'type'		=> $daysPassed,
					'expire' => $DataScadenzaDate->format('d-m-Y'),
				);
				break;
		}
	}
}


//create and send message
if (count($arrayRenew)>0){
	$subject ="Portale gestione - Tane in scadenza";
	\MAIL\Create::setVal('mail_from','spido@goblins.net');
	\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');

	\MAIL\Create::setVal('mail_to',$reciever['mail']);
	\MAIL\Create::setVal('name_to',$reciever['name']);
	\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
	\MAIL\Create::setVal('mail_subject',$subject);

	$dateTime=new \DateTime();
	$expired='';
	$autorenewed='';
	foreach ($arrayRenew as $item) {
		if ($item['type']>0){
			$expired.="-{$item['nome']} (<b>{$item['type']}</b> giorni) : {$item['expire']}<br>";
		} else {
			$autorenewed.="-{$item['nome']}<br>";
		}
	}
	if ($expired==''){
		$expired='-<i>Nessuna tana in scadenza</i>';
	}

	if ($autorenewed==''){
		$autorenewed='-<i>Nessuna tana auto rinnovata</i>';
	}
	$arrayReplace=array(
		'subject'	=> $subject,
		'date'		=> $dateTime->format('d M Y'),
		'name'		=> $reciever['name'],
		'expired'	=> $expired,
		'autorenewed'	=> $autorenewed,
	);
	\MAIL\Create::setTemplate('tane/renew_expire',$arrayReplace);
	\MAIL\Create::insert();

}