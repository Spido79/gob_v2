<!DOCTYPE html>
<!--[if IE 8]> <html lang="it" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="it" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="it"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page not found</title>
    <?php \CSS_JS\CSS::Start();?>
</head>
<body class="gray-bg">
    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">Page Not Found</h3>

        <div class="error-desc">
            <p>
            Sorry, but the page you are looking for has note been found. Try checking the URL for error, then hit the refresh button on your browser or try found something else in our app.
            </p>
            <p>
                <a href='<?php echo APP_URL;?>' class="btn btn-primary">home</a>
            </p>
        </div>
    </div>
    <?php \CSS_JS\JS::Start();?>
</body>
</html>