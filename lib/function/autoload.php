<?php

spl_autoload_register(function($class) {
	$dir = array(
		__DIR__ .'/../class',
		__DIR__ .'/../class/PHPMailer',
		__DIR__ .'/../class/tcpdf',
		);
	foreach ($dir as $dirItem) {
		$filesArray = scandir($dirItem);
		foreach ($filesArray as $key => $value) {
			if (!in_array($value,array(".","..")) && !is_dir($dirItem.DIRECTORY_SEPARATOR.$value)) {
				if (pathinfo($value)['extension']!='php'){
					continue;
				}
				include_once($dirItem.DIRECTORY_SEPARATOR.$value);
			}
		}
	}

});