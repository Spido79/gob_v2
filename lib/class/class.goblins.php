<?php

namespace GOBLINS;

class Manage{
	protected static $goblins= array();
	protected static $goblins_forum= array();
	protected static $spec_goblin= array();
	protected static $len_tessera_code = 32;
	public static function getCards(){
		$Sql='SELECT tessera_numero from tbl_goblins where tessera_numero>0 order by tessera_numero;';
		\SQL::Execute($Sql);
		$cards=\SQL::$Result;
		$cardArray=array();
		foreach ($cards as $card) {
			$cardArray[]=$card['tessera_numero'];
		}
		return $cardArray;
	}

	public static function setCards($id_goblin=0, $new_card=0){
		$new_card=intval($new_card);
		$id_goblin=intval($id_goblin);
		if ($id_goblin<=0 || $new_card<=0){
			return false;
		}

		$Sql='UPDATE tbl_goblins set tessera_numero ='.$new_card.' where id_goblin='.$id_goblin.';';
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		} else {
			return false;
		}
	}

	public static function addCards($id_goblin=0, $new_card=0){
		$id_goblin=intval($id_goblin);
		$chars='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$codice_tessera=null;
		for ($i=0; $i <\GOBLINS\Manage::$len_tessera_code ; $i++) {
			$codice_tessera.=$chars[rand(0, (strlen($chars) - 1))];
		}
		$Sql='UPDATE tbl_goblins
				LEFT JOIN (SELECT MAX(tessera_numero) as LastTessera FROM tbl_goblins) as MaxTable ON id_goblin=id_goblin
				SET codice_tessera='.\SQL::Escape($codice_tessera).' ,
				tessera_numero=(LastTessera+1)
				WHERE id_goblin='.$id_goblin.'
				AND (tessera_numero=0 OR tessera_numero IS NULL)';
		\SQL::Execute($Sql);
	}

	public static function delSpec($id_goblin=0){
		$id_goblin=intval($id_goblin);
		if ($id_goblin<=0){
			return false;
		}

		$Sql='DELETE FROM tbl_goblins where id_goblin='.$id_goblin.';';
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		} else {
			return false;
		}
	}

	public static function setSpec($id_goblin=0, $data=array()){
		$id_goblin=intval($id_goblin);

		if (!is_array($data)|| count($data)<=0 || $id_goblin<=0){
			return false;
		}

		$Sql='UPDATE tbl_goblins SET ';
		$count=0;
		foreach ($data as $key => $value) {
			$Sql.= $count ? ',' : null;
			$Sql.="`{$key}` = ".\SQL::Escape($value);
			$count++;
		}
		$Sql.=" WHERE id_goblin={$id_goblin};";
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		} else {
			return false;
		}
	}

	public static function getSpec($id_goblin=0, $nick=null, $email=null){
		if ($id_goblin>0){
			if (isset(\GOBLINS\Manage::$spec_goblin['id'][$id_goblin])){
				return \GOBLINS\Manage::$spec_goblin['id'][$id_goblin];
			}
			$Sql='SELECT * FROM tbl_goblins WHERE id_goblin='.intval($id_goblin).';';

		} else if ($nick!=''){
			if (isset(\GOBLINS\Manage::$spec_goblin['nick'][$nick])){
				return \GOBLINS\Manage::$spec_goblin['nick'][$nick];
			}
			$Sql='SELECT * FROM tbl_goblins WHERE Nick='.\SQL::Escape($nick).';';
		} else if ($email!=''){
			$Sql='SELECT * FROM tbl_goblins WHERE email_goblin='.\SQL::Escape($email).';';
		} else {
			return false;
		}

		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			$result=\SQL::$Result[0];
			\GOBLINS\Manage::$spec_goblin['nick'][$result['Nick']]=$result;
			\GOBLINS\Manage::$spec_goblin['id'][$result['id_goblin']]=$result;
			return $result;
		}
		return false;
	}

	public static function getAll($typeTana=0, $params=array()){
		/*
			$params=array(
				'list_tane'		=> array(),
				'list_tane_exclude'	=> false,
				'list_goblins'  => array(),
				'left_join'	=> array(
						'tbl_tane'	=> array(
							'id_tana'	=> 'id_tana',
						)
					),
				'map_fields'	=> array(
					'Nick'				=>'',
					'cognome_goblin'	=>'',
					'nome_goblin'		=>'',
					'id_goblin'			=>'',
					'email_goblin'		=>'',
					),
				'having_nome'	=> '',
				'having_cognome'=> '',
				'id_to_exclude'	=> 0,
				'offset'		=> 0,
				'limit'			=> 0,
				'search'		=> '',
				'search_tana'	=> '',
				'action'		=> null,
				'associated_from'=> null,
				'associated_to'	 => null,
				'subscribed_from'=> null,
				'subscribed_to'	=> null,
				'order'			=> array(
							'field' 	=> '',
							'direction' => '', // asc/desc
								)
			);
		*/
		$arrayExport = array(
			'Nick'				=>'',
			'cognome_goblin'	=>'',
			'nome_goblin'		=>'',
			'id_goblin'			=>'',
			'email_goblin'		=>'',
		);
		if (isset($params['map_fields']) && is_array($params['map_fields']) && count($params['map_fields'])>0){
			$arrayExport = $params['map_fields'];
		}

		$Sql='SELECT ';
		$count=0;
		foreach ($arrayExport as $key => $value) {
			$Sql.= $count>0 ? ', '.$key : $key;
			if (trim($value)!=''){
				$Sql.= " AS ".$value;
			}
			$count++;
		}

		$Sql.=' FROM tbl_goblins ';
		if (isset($params['left_join']) && is_array($params['left_join']) && count($params['left_join'])>0){
			foreach ($params['left_join'] as $key => $table) {
				$Sql.=' LEFT JOIN `'.$key.'` ON ';
				$count_join=0;
				foreach ($table as $field => $map) {
					$Sql.= $count_join>0? ' AND ': '';
					if (strpos($map, 'value:')===0){
						$Sql.= $key.'.'.$field.' = '.\SQL::Escape(explode(':',$map )[1]);
					} else {
						$Sql.= $key.'.'.$field.' = tbl_goblins.'.$map;
					}
					$count_join++;
				}
			}
		}
		$temp_where=false;
		switch (intval($typeTana)) {
			case -2: //TUTTE le tane
				break;

			case -1: //solo tana != 0
				$Sql.=' WHERE tbl_goblins.id_tana != 0';
				$temp_where=true;
				break;

			default:
				if (isset($params['list_tane']) && is_array($params['list_tane']) && count($params['list_tane'])>0){
					$trimImplode=trim(implode(',',$params['list_tane']));
					if ($trimImplode!=''){
						$not='';
						if (isset($params['list_tane_exclude']) && $params['list_tane_exclude']){
							$not='NOT ';
						}
						$Sql.=' WHERE tbl_goblins.id_tana '.$not.'IN ('.$trimImplode.')';
					} else {
						$Sql.=' WHERE 1=2 ';
					}
					$temp_where=true;
				}
				break;
		}

		if (isset($params['list_goblins']) && is_array($params['list_goblins']) && count($params['list_goblins'])>0){
			$trimImplode=trim(implode(',',$params['list_goblins']));
			if ($trimImplode!=''){
				$Sql.= $temp_where ? ' AND ' : ' WHERE ';
				$Sql.=' tbl_goblins.id_goblin IN ('.$trimImplode.')';
				$temp_where=true;
			}
		}

		if (isset($params['associated_from']) && !is_null($params['associated_from'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' data_associato >='.\SQL::Escape($params['associated_from']);
		}

		if (isset($params['associated_to']) && !is_null($params['associated_to'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' data_associato <='.\SQL::Escape($params['associated_to']);
		}

		if (isset($params['subscribed_from']) && !is_null($params['subscribed_from'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' user_regdate >='.\SQL::Escape($params['subscribed_from']);
		}

		if (isset($params['subscribed_to']) && !is_null($params['subscribed_to'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' user_regdate <='.\SQL::Escape($params['subscribed_to']);
		}

		if (isset($params['having_nome']) && !is_null($params['having_nome'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' nome_goblin ='.\SQL::Escape($params['having_nome']);
		}

		if (isset($params['having_cognome']) && !is_null($params['having_cognome'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' cognome_goblin ='.\SQL::Escape($params['having_cognome']);
		}

		if (isset($params['action']) && !is_null($params['action'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' action=0';
		}

		if (isset($params['id_to_exclude']) && $params['id_to_exclude']>0){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' tbl_goblins.id_goblin!='.intval($params['id_to_exclude']);
		}

		if (isset($params['search']) && !is_null($params['search']) && (!isset($params['search_tana'])|| is_null($params['search_tana']))){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$Sql.=' ( tbl_goblins.Nick like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' OR ';
			$Sql.=' tbl_goblins.nome_goblin like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' OR ';
			$Sql.=' tbl_goblins.cognome_goblin like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' ) ';
			$temp_where=true;
		}

		if (isset($params['search_tana']) && !is_null($params['search_tana'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			if (strtolower($params['search_tana'])=='tana nazionale' || strtolower($params['search_tana'])=='la tana dei goblin' ){
				$Sql.=' ( tbl_tane.Nome like "La Tana dei Goblin" ';
			} else {
				$Sql.=' ( tbl_tane.Nome like '.\SQL::Escape('%'.str_replace('%', '', $params['search_tana']).'%').' ';
			}

			if (isset($params['search']) && !is_null($params['search'])){
				$Sql.=' OR tbl_goblins.Nick like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' OR ';
				$Sql.=' tbl_goblins.nome_goblin like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' OR ';
				$Sql.=' tbl_goblins.cognome_goblin like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' ';
				$temp_where=true;
			}
			$Sql.=' ) ';
			$temp_where=true;
		}

		if (isset($params['order']) && is_array($params['order']) && isset($params['order']['field']) && !is_null($params['order']['field']) ) {
			$Sql.=' ORDER BY '.($params['order']['field']);
			if (isset($params['order']['direction']) && strtolower($params['order']['direction']) =='desc' ){
				$Sql.=' DESC ';
			}
		}

		if (isset($params['limit']) && isset($params['offset'])){
			$Sql.=' LIMIT '.intval($params['offset']).','.intval($params['limit']);
		} elseif (isset($params['limit'])) {
			$Sql.=' LIMIT '.intval($params['limit']);
		}

		$Sql.=';';
		\SQL::Execute($Sql);
		\GOBLINS\Manage::$goblins=\SQL::$Result;
		return \GOBLINS\Manage::$goblins;
	}

	public static function resetGoblin(){
		\GOBLINS\Manage::$goblins=array();
	}

	public static function getAllForum(){
		if (count(\GOBLINS\Manage::$goblins_forum)>0){
			return \GOBLINS\Manage::$goblins_forum;
		}
		$_database_forum= \APP\Parameter::getSpec('database_forum');
		$_database_forum_table = \APP\Parameter::getSpec('database_forum_table');
		$_username=\APP\Parameter::getSpec('database_forum_table_username');
		$_user_email=\APP\Parameter::getSpec('database_forum_table_user_email');
		$_user_regdate=\APP\Parameter::getSpec('database_forum_table_user_regdate');

		$Sql='	SELECT `'.$_database_forum_table.'`.`'.$_username.'`,`'.$_database_forum_table.'`.`'.$_user_email.'`
					FROM `'.$_database_forum.'`.`'.$_database_forum_table.'`;';
		\SQL::Execute($Sql);
		\GOBLINS\Manage::$goblins_forum=\SQL::$Result;
		return \GOBLINS\Manage::$goblins_forum;
	}


	public static function anomalieList(){
		//select all nick in gestione not in forum
		$Sql='SELECT Nick, email_goblin as Email, id_goblin as ID
				FROM tbl_goblins
				WHERE lower(Nick) NOT IN (';
		$count=0;
		$_username=\APP\Parameter::getSpec('database_forum_table_username');
		foreach (self::getAllForum() as $value) {
			$Sql.= $count >0 ? ', ' : '';
			$Sql.=\SQL::Escape(strtolower($value[$_username]));
			$count++;
		}

		$Sql.=');';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function updateCreationDate(){
		//update
		$Sql='SELECT Nick, email_goblin, user_regdate FROM tbl_goblins WHERE user_regdate IS NULL;';
		\SQL::Execute($Sql);
		$_database_forum= \APP\Parameter::getSpec('database_forum');
		$_database_forum_table = \APP\Parameter::getSpec('database_forum_table');
		$_username=\APP\Parameter::getSpec('database_forum_table_username');
		$_user_email=\APP\Parameter::getSpec('database_forum_table_user_email');
		$_user_regdate=\APP\Parameter::getSpec('database_forum_table_user_regdate');

		$Sql='SELECT `'.$_database_forum_table .'`.`'.$_username.'`, `'.$_database_forum_table .'`.`'.$_user_regdate.'`
				FROM `'.$_database_forum.'`.`'.$_database_forum_table.'`
				WHERE lower(`'.$_database_forum_table.'`.`'.$_username.'`) IN (';
		$count=0;
		foreach (\SQL::$Result as $value) {
			$Sql.= $count >0 ? ', ' : '';
			$Sql.=\SQL::Escape(strtolower($value['Nick']));
			$count++;
		}
		$Sql.=');';

		\SQL::Execute($Sql);
		$Sql='UPDATE `'.\APP\Parameter::getSpec('sql_database_name').'`.`tbl_goblins` SET user_regdate = CASE ';

		foreach (\SQL::$Result as $value) {
			$Sql.='WHEN Nick ='.\SQL::Escape($value[$_username]).' THEN '.\SQL::Escape($value[$_user_regdate]).' ';
		}
		$Sql.='END WHERE Nick IN (';

		$count=0;
		foreach (\SQL::$Result as $value) {
			$Sql.= $count >0 ? ', ' : '';
			$Sql.=\SQL::Escape($value[$_username]);
			$count++;
		}
		$Sql.=');';
		if ($count>0){
			\SQL::Execute($Sql);
		}
	}

	public static function pairingNick(){
		//update
		$Sql='SELECT Nick, email_goblin, user_regdate FROM tbl_goblins ORDER BY user_regdate;';
		\SQL::Execute($Sql);
		$resQ=\SQL::$Result;
		if (isset($resQ[0]) && is_null($resQ[0]['user_regdate'])){
			\GOBLINS\Manage::updateCreationDate();
		}
		$_database_forum= \APP\Parameter::getSpec('database_forum');
		$_database_forum_table = \APP\Parameter::getSpec('database_forum_table');
		$_username=\APP\Parameter::getSpec('database_forum_table_username');
		$_user_email=\APP\Parameter::getSpec('database_forum_table_user_email');
		$_user_regdate=\APP\Parameter::getSpec('database_forum_table_user_regdate');

		$Sql='SELECT `'.$_database_forum_table.'`.`'.$_username.'`,`'.$_database_forum_table.'`.`'.$_user_email.'`, `'.$_database_forum_table.'`.`'.$_user_regdate.'`
				FROM `'.$_database_forum.'`.`'.$_database_forum_table.'`
				WHERE lower(`'.$_database_forum_table.'`.`'.$_username.'`) NOT IN (';
		$count=0;
		foreach ($resQ as $value) {
			$Sql.= $count >0 ? ', ' : '';
			$Sql.=\SQL::Escape(strtolower($value['Nick']));
			$count++;
		}

		if ($count==0){
			$Sql.='\'\'';
		}
		$Sql.=');';
		\SQL::Execute($Sql);
		$Sql='INSERT INTO `'.\APP\Parameter::getSpec('sql_database_name').'`.`tbl_goblins` (Nick,email_goblin, user_regdate) VALUES ';
		$count=0;
		foreach (\SQL::$Result as $value) {
			$Sql.= $count >0 ? ', ' : '';
			$Sql.=' ('.\SQL::Escape($value[$_username]).','.\SQL::Escape($value[$_user_email]).','.\SQL::Escape($value[$_user_regdate]).') ';
			$count++;
		}
		$Sql.=';';
		if ($count>0){
			\SQL::Execute($Sql);
		}


		$array=array(
			'gest' => count(self::getAll(-2)),
			'forum' => count(self::getAllForum()),
		);

		return $array;
	}
}

class Tane{
	protected static $list=array();
	protected static $listActive=array();
	protected static $listResponable=array();

	public static function change($id_tana, $params=array()){
		$id_tana=intval($id_tana);
		$Sql="UPDATE tbl_tane";
		$f=false;
		$arrayVer=array(
			'Note',	'Provincia', 'Citta','competenza_Prov','Indirizzo',	'Nome','Data_UltimoRinnovo','Data_Costituzione', 'id_presidente', 'motivo_disdetta' ,'Data_Risoluzione');
		foreach ($arrayVer as $key ) {
			if (isset($params[$key]) && trim($params[$key])!='' && $params[$key]!= null ){
				$Sql.= $f ? ', ': ' SET ';
				$f=true;
				$Sql.=$key.'='.\SQL::Escape($params[$key]);
			}
		}

		if (isset($params['status'])){
			$Sql.= $f ? ', ': ' SET ';
			$f=true;
			$Sql.='status_tana='.intval($params['status']);
		}

		$Sql.=" WHERE id_tana={$id_tana};";
		if ($f){
			\SQL::Execute($Sql);
		}

		if (isset($params['detach_gb']) && intval($params['detach_gb'])>=0){
			$Sql='UPDATE tbl_goblins SET id_tana='.intval($params['detach_gb']).' WHERE id_tana='.$id_tana.';';
			\SQL::Execute($Sql);
			$f=true;
		}

		if (isset($params['detach_us']) && $params['detach_us']==true ){
			\USERS\Detail::disableTana($id_tana);
			$f=true;
		}
		return $f;
	}

	public static function insert($params=array()){
		$Sql="INSERT INTO tbl_tane";
		$f=false;
		$arrayVer=array(
			'Note',	'Provincia', 'Citta','competenza_Prov','Indirizzo',	'Nome','Data_UltimoRinnovo','Data_Costituzione', 'id_presidente', 'motivo_disdetta' ,'Data_Risoluzione', 'status_tana');
		foreach ($arrayVer as $key ) {
			if (isset($params[$key]) && trim($params[$key])!='' && $params[$key]!= null ){
				$Sql.= $f ? ', ': ' ( ';
				$f=true;
				$Sql.=$key;
			}
		}

		$f=false;
		foreach ($arrayVer as $key ) {
			if (isset($params[$key]) && trim($params[$key])!='' && $params[$key]!= null ){
				$Sql.= $f ? ', ': ') VALUES ( ';
				$f=true;
				$Sql.=\SQL::Escape($params[$key]);
			}
		}

		$Sql.=");";
		if ($f){
			\SQL::Execute($Sql);
			return \SQL::$LastID;
		}
		return $f;
	}

	public static function getResponsable($id_tana){
		if (isset(\GOBLINS\Tane::$listResponable[$id_tana]) && count(\GOBLINS\Tane::$listResponable[$id_tana])>0){
			return \GOBLINS\Tane::$listResponable[$id_tana];
		}
		$arrayTemp=array();
		foreach (\USERS\Detail::getActive() as $key => $value) {
			if(in_array($id_tana, explode(',', $value['tane_riferimento']))){
				$arrayTemp[]=$value['user_id'];
			}
		}
		\GOBLINS\Tane::$listResponable[$id_tana]=$arrayTemp;
		return \GOBLINS\Tane::$listResponable[$id_tana];
	}

	public static function getAll($params=array()){
		/*
			$params=array(
				'left_join'	=> array(
						'tbl_goblins' => array(
							'id_goblin'	=> 'id_presidente',
						)
					),
				'map_fields'	=> array(
					'Nome'		=>'',
					),
				'offset'		=> 0,
				'list_tane'		=> array(),
				'list_tane_exclude'	=> false,
				'limit'			=> 0,
				'search'		=> '',
				'action'		=> null,
				'order'			=> array(
					'field' 	=> '',
					'direction' => '', // asc/desc
					)
			);
		*/
		/*if (count(\GOBLINS\Tane::$list)>0){
			return \GOBLINS\Tane::$list;
		}*/


		$arrayExport = array();
		if (isset($params['map_fields']) && is_array($params['map_fields']) && count($params['map_fields'])>0){
			$arrayExport = $params['map_fields'];
		}

		$Sql='SELECT ';
		$count=0;
		foreach ($arrayExport as $key => $value) {
			$Sql.= $count>0 ? ', '.$key : $key;
			if (trim($value)!=''){
				$Sql.= " AS ".$value;
			}
			$count++;
		}
		if ($count ==0 ){
			$Sql.=' * ';
		}

		$Sql.=" FROM tbl_tane";

		if (isset($params['left_join']) && is_array($params['left_join']) && count($params['left_join'])>0){
			foreach ($params['left_join'] as $key => $table) {
				$Sql.=' LEFT JOIN `'.$key.'` ON ';
				$count_join=0;
				foreach ($table as $field => $map) {
					$Sql.= $count_join>0? ' AND ': '';
					if (strpos($map, 'value:')===0){
						$Sql.= $key.'.'.$field.' = '.\SQL::Escape(explode(':',$map )[0]);
					} else {
						$Sql.= $key.'.'.$field.' = tbl_tane.'.$map;
					}
					$count_join++;
				}
			}
		}

		$temp_where=false;

		if (isset($params['action']) && !is_null($params['action'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$temp_where=true;
			$Sql.=' status_tana!=2';
		}

		if (isset($params['search']) && !is_null($params['search'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$Sql.=' ( tbl_tane.Nome like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' OR ';
			$Sql.=' tbl_tane.Citta like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' ) ';
			$temp_where=true;
		}

		if (isset($params['list_tane']) && is_array($params['list_tane']) && count($params['list_tane'])>0){
			$trimImplode=trim(implode(',',$params['list_tane']));
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			if ($trimImplode!=''){
				$not='';
				if (isset($params['list_tane_exclude']) && $params['list_tane_exclude']){
					$not='NOT ';
				}
				$Sql.=' tbl_tane.id_tana '.$not.'IN ('.$trimImplode.')';
			} else {
				$Sql.=' 1=2 ';
			}
			$temp_where=true;
		}

		if (isset($params['order']) && is_array($params['order']) && isset($params['order']['field']) && !is_null($params['order']['field']) ) {
			$Sql.=' ORDER BY '.($params['order']['field']);
			if (isset($params['order']['direction']) && strtolower($params['order']['direction']) =='desc' ){
				$Sql.=' DESC ';
			}
		}

		if (isset($params['limit']) && isset($params['offset'])){
			$Sql.=' LIMIT '.intval($params['offset']).','.intval($params['limit']);
		} elseif (isset($params['limit'])) {
			$Sql.=' LIMIT '.intval($params['limit']);
		}
		$Sql.=";";
		\SQL::Execute($Sql);
		\GOBLINS\Tane::$list=\SQL::$Result;
		return \GOBLINS\Tane::$list;
	}

	public static function spec($arrayTane){
		if (count(\GOBLINS\Tane::$list)<=0){
			\GOBLINS\Tane::getAll();
		}

		$array=array();
		foreach (\GOBLINS\Tane::$list as $value) {
			if (in_array($value['id_tana'],$arrayTane)){
				$array[$value['id_tana']]=$value['Nome'];
			}
		}
		asort($array);
		return $array;
	}

	public static function isActive($id_tana){
		if (count(\GOBLINS\Tane::$listActive)<=0){
			\GOBLINS\Tane::getActive();
		}
		foreach (\GOBLINS\Tane::$listActive as $item) {
			if ($item['id_tana']==$id_tana){
				return true;
			}
		}
		return false;
	}

	public static function getActive(){
		if (count(\GOBLINS\Tane::$listActive)>0){
			return \GOBLINS\Tane::$listActive;
		}
		if (count(\GOBLINS\Tane::$list)<=0){
			$params=array(
				'order'			=> array(
					'field' 	=> 'Nome',
					'direction' => 'asc',
				)
			);
			\GOBLINS\Tane::getAll($params);
		}
		foreach (\GOBLINS\Tane::$list as $item) {
			if($item['status_tana']==1){
				\GOBLINS\Tane::$listActive[]=$item;
			}
		}
		return \GOBLINS\Tane::$listActive;
	}

	public static function getDate($dateStart){
		/*
			STATUS:
			0=error
			1=ok
			2=less then 90 day
			Day to send email:
			90,75,60,45,30,15,5,1
		*/
		$arrayRe=array(
			'rinnovo'	=> new \DateTime($dateStart),
			'rinnovo_f'	=> '-',
			'scadenza'	=> new \DateTime($dateStart),
			'scadenza_f'	=> '-',
			'disdetta'	=>  new \DateTime($dateStart),
			'disdetta_f'	=> '-',
			'day_to_expire'	=> 0,
			'day_to_revoke'	=> 0,
			'status'		=> 0,
		);
		if ($dateStart=='' || $dateStart==null || $dateStart=='0000-00-00'){
			return $arrayRe;
		}

		$arrayRe['scadenza']->modify('+2 year');
		$arrayRe['disdetta']->modify('+2 year');
		$arrayRe['disdetta']->modify('-3 month');

		$today=new \DateTime();
		$interval = $today->diff($arrayRe['scadenza']);
		$arrayRe['day_to_expire']=$interval->format('%R%a');

		$revoke = $today->diff($arrayRe['disdetta']);
		$arrayRe['day_to_revoke']=$revoke->format('%R%a');
		if ($arrayRe['day_to_expire']<=0){
			$arrayRe['status']=0;
		} else if ($arrayRe['day_to_expire']<=90){
			$arrayRe['status']=2;
		} else {
			$arrayRe['status']=1;
		}
		$arrayRe['rinnovo_f']=$arrayRe['rinnovo']->format('d.m.Y');
		$arrayRe['scadenza_f']=$arrayRe['scadenza']->format('d.m.Y');
		$arrayRe['disdetta_f']=$arrayRe['disdetta']->format('d.m.Y');

		return $arrayRe;
	}

	public static function cleanTane(){
		/* clean tane rif from:
			tbl_goblins
			tbl_request_goblin
			tbl_files
			tbl_users
			tbl_mag_stores
		*/

		$activeTane=\GOBLINS\Tane::getActive();
		$activeIds=array(0);
		foreach ($activeTane as $key => $value) {
			$activeIds[]=$value['id_tana'];
		}
		// Clean from tbl_goblins
		$Sql='UPDATE tbl_goblins SET
				id_tana='.intval(\APP\Parameter::getSpec('id_tana_nazionale')).'
				WHERE !FIND_IN_SET(id_tana, '.\SQL::Escape(implode(',', $activeIds)).');';
		\SQL::Execute($Sql);
		$row=\SQL::$Rows;

		if ($row>0){
			\LOGS\Log::write('CLEAN', "Cleaned {$row} rows from tbl_goblins", false);
		}

		// Clean from tbl_request_goblin
		$Sql='DELETE FROM tbl_request_goblin
				WHERE !FIND_IN_SET(tana_req, '.\SQL::Escape(implode(',', $activeIds)).');';
		\SQL::Execute($Sql);

		if ($row>0){
			\LOGS\Log::write('CLEAN', "Cleaned {$row} rows from tbl_request_goblin", false);
		}

		// Clean from tbl_files
		$Sql='SELECT id_file, tane_riferimento FROM tbl_files
				WHERE tane_riferimento !="ALL_TANE" AND tane_riferimento IS NOT NULL AND tane_riferimento!="";';
		\SQL::Execute($Sql);
		$result=\SQL::$Result;
		foreach ($result as $item) {
			$tempTane=explode(',', $item['tane_riferimento']);
			foreach ($tempTane as $tempTana) {
				if (!in_array($tempTana, $activeIds)){
					$tempTane=array_diff($tempTane, array($tempTana));
				}
			}
			$taneImplode=implode(',', $tempTane);
			if ($taneImplode!=$item['tane_riferimento']){
				if (count($tempTane)==count($activeIds)){
					$Sql = 'UPDATE tbl_files SET tane_riferimento="ALL_TANE" WHERE id_file = '.$item['id_file'].';';
				} else {
					$Sql = 'UPDATE tbl_files SET tane_riferimento='.\SQL::Escape($taneImplode).' WHERE id_file = '.$item['id_file'].';';
				}
				\SQL::Execute($Sql);
				\LOGS\Log::write('CLEAN', "Cleaned id file:{$item['id_file']} from tbl_files", false);
			}
		}

		// Clean from tbl_users
		$Sql='SELECT user_id, tane_riferimento FROM tbl_users
				WHERE tane_riferimento IS NOT NULL AND tane_riferimento!="";';
		\SQL::Execute($Sql);
		$result=\SQL::$Result;
		foreach ($result as $item) {
			$tempTane=explode(',', $item['tane_riferimento']);
			foreach ($tempTane as $tempTana) {
				if (!in_array($tempTana, $activeIds)){
					$tempTane=array_diff($tempTane, array($tempTana));
				}
			}
			$taneImplode=implode(',', $tempTane);
			if ($taneImplode!=$item['tane_riferimento']){
				$Sql = 'UPDATE tbl_users SET tane_riferimento='.\SQL::Escape($taneImplode).' WHERE user_id = '.$item['user_id'].';';
				\SQL::Execute($Sql);
				\LOGS\Log::write('CLEAN', "Cleaned id file:{$item['user_id']} from tbl_users", false);
			}
		}

		//clean empty store of disabled tane
		$Sql='SELECT tbl_mag_stores.id_tana, tbl_mag_stores.id_store, count(id_stock) as qta
				FROM tbl_mag_stores
				LEFT JOIN tbl_mag_stock ON tbl_mag_stock.id_store = tbl_mag_stores.id_store
				LEFT JOIN tbl_tane ON tbl_tane.id_tana = tbl_mag_stores.id_tana
				WHERE tbl_mag_stores.delete=0 AND tbl_tane.status_tana!=1
				GROUP by id_tana, tbl_mag_stores.id_store
				HAVING qta=0';
		\SQL::Execute($Sql);
		$result=\SQL::$Result;
		foreach ($result as $item) {
			$Sql='UPDATE tbl_mag_stores set `delete`= 1 WHERE id_store= '.$item['id_store'].';';
			\SQL::Execute($Sql);
			\LOGS\Log::write('CLEAN', "Cleaned id store:{$item['id_store']} from tbl_mag_stores", false);
		}
	}
}

class Request{
	protected static $goblins= array();

	public static function checkTane($array){
		$trimImplode=trim(implode(',',$array));
		$Sql='SELECT count(id_request) as requests FROM tbl_request_goblin
				LEFT JOIN tbl_goblins ON tbl_request_goblin.id_goblin = tbl_goblins.id_goblin
				WHERE id_tana IN ('.$trimImplode.') AND action=0;';
		\SQL::Execute($Sql);

		return \SQL::$Result[0];
	}

	public static function check($id_goblin){
		if (!\GOBLINS\Request::sql($id_goblin)){
			return false;
		}
		return true;
	}

	public static function get($id_goblin){
		if (!\GOBLINS\Request::sql($id_goblin)){
			return false;
		}
		return \GOBLINS\Request::sql($id_goblin);
	}

	public static function add($id_goblin, $id_tana){
		$id_goblin=intval($id_goblin);
		$id_tana=intval($id_tana);
		$Sql='INSERT INTO tbl_request_goblin (id_goblin, tana_req, id_requestor) VALUES (
				'.$id_goblin.',
				'.$id_tana.',
				'.\USERS\Identify::UserID().'
			);';
		\SQL::Execute($Sql);
	}

	public static function answer($id_goblin, $answer){
		$id_goblin=intval($id_goblin);
		$answer=intval($answer);
		$today= new \DateTime();
		$Sql='UPDATE tbl_request_goblin SET data_action='.\SQL::Escape($today->format('Y-m-d H:i:s')).', id_user_action='.\USERS\Identify::UserID().',  action = '.$answer.' WHERE id_goblin ='.$id_goblin.' AND action =0;';
		\SQL::Execute($Sql);
	}

	protected static function sql($id_goblin){
		$id_goblin=intval($id_goblin);
		if (isset(\GOBLINS\Request::$goblins[$id_goblin])){
			return \GOBLINS\Request::$goblins[$id_goblin];
		}
		$Sql='SELECT data_request,
				CONCAT (user_name, " ", user_surname) as user_name_surname,
				user_id
				user_login,
				Nome as tana_name,
				tana_req as id_tana
				FROM tbl_request_goblin
				LEFT JOIN tbl_users ON id_requestor = user_id
				LEFT JOIN tbl_tane ON tana_req = id_tana
				WHERE id_goblin = '.$id_goblin.' AND action=0;';
		\SQL::Execute($Sql);
		if (isset(\SQL::$Result[0])){

			\GOBLINS\Request::$goblins[$id_goblin]=\SQL::$Result[0];
			return \GOBLINS\Request::$goblins[$id_goblin];
		}
		return false;
	}
}

