<?php
namespace MAGAZZINO;
class Games {
	protected static $spec_game= array();

	public static $maxElements=25;
	public static function getAll($fields=array(), $deleted=false){

		$Sql='SELECT '.implode(',', $fields).' FROM tbl_mag_ana ';
		if ($deleted==false){
			$Sql.=' WHERE `delete`=0 ';
		}
		$Sql.=' ORDER by title Limit '.(\MAGAZZINO\Games::$maxElements+10).';';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function getActive($fields=array()){
		$Sql='SELECT '.implode(',', $fields).' FROM tbl_mag_ana ';
		$Sql.=' WHERE `delete`=0 and active =1 ';
		$Sql.=' ORDER by title;';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function getSpec($id, $text, $multi=false){
		if ($id>0){
			if (isset(\MAGAZZINO\Games::$spec_game['id'][$id])){
				return \MAGAZZINO\Games::$spec_game['id'][$id];
			}
			$Sql='SELECT * FROM tbl_mag_ana WHERE `delete`=0 AND id_ana='.intval($id).'  ORDER BY title Limit '.(\MAGAZZINO\Games::$maxElements+10).';';

		} else if ($text!=''){
			if (isset(\MAGAZZINO\Games::$spec_game['title'][$text])){
				return \MAGAZZINO\Games::$spec_game['title'][$text];
			}
			//special case: in case of format: id:122121 take id bgg:
			preg_match('/^\s*id\:([0-9]*)\s*/', $text, $matches);
			preg_match('/^\s*t\:([a-zA-Z0-9\s\:\"\'_\-\$\%\&\=\!\?\(\)]*)\s*/', $text, $matchesB);

			if (is_array($matches) && isset($matches[1])){
				 $Sql='SELECT * FROM tbl_mag_ana WHERE `delete`=0 AND id_bgg= '.intval($matches[1]).' ORDER BY title Limit '.(\MAGAZZINO\Games::$maxElements+10).';';
			} else if (is_array($matchesB) && isset($matchesB[1])){
				$Sql='SELECT * FROM tbl_mag_ana WHERE `delete`=0 AND
					(
					all_title = '.\SQL::Escape($matchesB[1]).' OR
					all_title like '.\SQL::Escape($matchesB[1].'|#|%').' OR
					all_title like '.\SQL::Escape('%|#|'.$matchesB[1]).' OR
					all_title like '.\SQL::Escape('%|#|'.$matchesB[1].'|#|%').'
					)
					ORDER BY title Limit '.(\MAGAZZINO\Games::$maxElements+10).';';
			} else{
				$Sql='SELECT * FROM tbl_mag_ana WHERE `delete`=0 AND all_title like '.\SQL::Escape('%'.$text.'%').' ORDER BY title Limit '.(\MAGAZZINO\Games::$maxElements+10).';';
			}

		} else {
			return false;
		}

		\SQL::Execute($Sql);
		if (\SQL::$Rows==1 && $multi==false){
			$result=\SQL::$Result[0];
			\MAGAZZINO\Games::$spec_game['title'][$result['title']]=$result;
			\MAGAZZINO\Games::$spec_game['id'][$result['id_ana']]=$result;
			return $result;
		} else if ($multi==true && \SQL::$Rows>0){
			return \SQL::$Result;
		}
		return false;
	}

	public static function getStock($id, $dismiss=0){
		$Sql='SELECT id_stock, tbl_mag_stock.id_store, hidden, data_store, data_acquisto,
			tbl_mag_stores.store, tbl_mag_stores.active as store_active,
			Nome as nome_tana, id_owner, origin, tbl_mag_stock.notes, locazione, deleted
			FROM tbl_mag_stock
			LEFT JOIN tbl_mag_stores ON tbl_mag_stores.id_store=tbl_mag_stock.id_store
			LEFT JOIN tbl_tane ON tbl_tane.id_tana = id_owner
			WHERE id_ana='.intval($id);
			if ($dismiss==1){
				$Sql.=' and deleted=1 ';
			} elseif ($dismiss==0){
				$Sql.=' and deleted=0 ';
			}
			$Sql.=';';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function qtaStock($id){
		$Sql='SELECT count(id_stock) as qta FROM tbl_mag_stock WHERE id_ana='.intval($id).' and deleted=0 GROUP by id_ana ;';
		\SQL::Execute($Sql);
		if (isset(\SQL::$Result[0])){
			return intval(\SQL::$Result[0]['qta']);
		} else {
			return 0;
		}

	}

	public static function delAna($id){
		$id=intval($id);
		if ($id<=0){
			return false;
		}

		$Sql="UPDATE tbl_mag_ana SET active = 0, `delete`=1 WHERE id_ana= {$id};";
		\SQL::Execute($Sql);
		$Sql="UPDATE tbl_mag_stock SET `deleted`=1 WHERE id_ana= {$id};";
		\SQL::Execute($Sql);
		//update log stock
		$Sql="INSERT INTO tbl_mag_stock_log (id_stock, user_in, action, icon)
				SELECT tbl_mag_stock.id_stock, ".\USERS\Identify::UserID().", 'Eliminato articolo - Anagrafica eliminata', 'fa-times'
				FROM tbl_mag_stock
				WHERE tbl_mag_stock.id_ana = {$id};";
		\SQL::Execute($Sql);
		return true;
	}

	public static function updateActive($id, $active){
		$active=intval(filter_var($active, FILTER_VALIDATE_BOOLEAN));
		$id=intval($id);
		if ($id<=0){
			return false;
		}

		$Sql="UPDATE tbl_mag_ana SET active = {$active}
			WHERE id_ana= {$id};";
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}

	public static function updateTitle($id, $title){
		$title_sql=\SQL::Escape($title);
		$id=intval($id);
		if ($id<=0){
			return false;
		}

		$Sql="UPDATE tbl_mag_ana SET title = {$title_sql}
			WHERE id_ana= {$id};";
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}

}

class Stock{
	public static function getAllAna($params){
		$Sql='SELECT title as Articolo, count(id_stock) as Qta, tbl_mag_ana.id_ana, `desc` as Note
		FROM tbl_mag_ana
		LEFT JOIN tbl_mag_stock on tbl_mag_stock.id_ana = tbl_mag_ana.id_ana
		';
		$temp_where=false;

		if (isset($params['search']) && !is_null($params['search']) && (!isset($params['search_tana'])|| is_null($params['search_tana']))){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$Sql.=' ( tbl_mag_ana.title like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' OR ';
			$Sql.=' tbl_mag_ana.all_title like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' ) ';
			$temp_where=true;
		}

		if (isset($params['dismiss']) && intval($params['dismiss'])==1 ) {
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$Sql.= ' tbl_mag_stock.deleted=1 ';
			$temp_where=true;
		} else {
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$Sql.= ' tbl_mag_stock.deleted=0 ';
			$temp_where=true;
		}


		$Sql.= $temp_where ? ' AND ' : ' WHERE ';
		$Sql.=' `delete` =0
			GROUP BY tbl_mag_ana.id_ana
			HAVING Qta>0 ';

		if (isset($params['order']) && is_array($params['order']) && isset($params['order']['field']) && !is_null($params['order']['field']) ) {
			$Sql.=' ORDER BY '.($params['order']['field']);
			if (isset($params['order']['direction']) && strtolower($params['order']['direction']) =='desc' ){
				$Sql.=' DESC ';
			}
		}

		\SQL::Execute($Sql);
		return \SQL::$Result;
	}
	public static function qta($group=0){
		if ($group){
			$Sql='SELECT count(distinct id_ana) as qta FROM tbl_mag_stock WHERE deleted=0;';
		} else {
			$Sql='SELECT count(id_stock) as qta FROM tbl_mag_stock WHERE deleted=0;';
		}

		\SQL::Execute($Sql);
		if (isset(\SQL::$Result[0])){
			return intval(\SQL::$Result[0]['qta']);
		} else {
			return 0;
		}
	}


	public static function get_color_icon($icon){

		switch ($icon) {
			case 'fa-question-circle-o': //request transer
				$bg='lazure-bg';
				break;

			case 'fa-exchange': //transfer
				$bg='white-bg';
				break;

			case 'fa-check': //create
				$bg='white-bg';
				break;

			case 'fa-times': //deelte
				$bg='red-bg';
				break;

			case 'fa-pencil': //modify
				$bg='yellow-bg';
				break;

			case 'fa-recycle': //modify
				$bg='yellow-bg';
				break;

			default:
				$bg='grey-bg';
				break;
		}
		return $bg;
	}

	public static function log($id, $order = 0){
		$id=intval($id);
		if ($id<=0){
			return array();
		}

		$direction='ASC';
		if ($order==1){
			$direction='DESC';
		}
		$Sql="SELECT action, tbl_mag_stock_log.data_in, icon, notes, concat(user_name, ' ', user_surname) as user
			FROM tbl_mag_stock_log
			LEFT JOIN tbl_mag_stock on tbl_mag_stock.id_stock=tbl_mag_stock_log.id_stock
			LEFT JOIN tbl_users on tbl_users.user_id=tbl_mag_stock_log.user_in
			WHERE tbl_mag_stock_log.id_stock= {$id} ORDER BY tbl_mag_stock_log.data_in {$direction};";
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function delete($id){
		$id=intval($id);
		if ($id<=0){
			return false;
		}

		$Sql="UPDATE tbl_mag_stock SET `deleted`=1 WHERE id_stock= {$id};";
		\SQL::Execute($Sql);
		return true;
	}

	public static function reenable($id){
		$id=intval($id);
		if ($id<=0){
			return false;
		}

		$Sql="UPDATE tbl_mag_stock SET `deleted`=0 WHERE id_stock= {$id};";
		\SQL::Execute($Sql);
		return true;
	}

	public static function insert($fields=array()){
		$Sql='INSERT INTO tbl_mag_stock ( ';
		$count=0;
		foreach ($fields as $key => $value) {
			$Sql.=$count>0?', ':'';
			$Sql.='`'.$key.'`';
			$count++;
		}
		$Sql.=') VALUES (';
		$count=0;
		foreach ($fields as $key => $value) {
			$Sql.=$count>0?', ':'';
			$Sql.=\SQL::Escape($value);
			$count++;
		}
		$Sql.=');';
		\SQL::Execute($Sql);
		return \SQL::$LastID;
	}

	public static function modify($fields=array()){
		$Sql='UPDATE tbl_mag_stock set ';
		$count=0;
		foreach ($fields as $key => $value) {
			if ($key=='id_stock'){
				continue;
			}
			$Sql.=$count>0?', ':'';
			$Sql.='`'.$key.'` = '.\SQL::Escape($value);
			$count++;
		}
		$Sql.=' WHERE id_stock='.intval($fields['id_stock']).';';
		\SQL::Execute($Sql);
		return intval($fields['id_stock']);
	}

	static public function logWrite($id_stock,$textPassed, $icon, $id_owner, $id_store){
		$Sql="INSERT tbl_mag_stock_log ( id_stock,user_in,action, icon, id_owner, id_store) VALUES
				(".intval($id_stock).",".\USERS\Identify::UserID().",".\SQL::Escape($textPassed).",".\SQL::Escape($icon).",".intval($id_owner).",".intval($id_store).");";
		\SQL::Execute($Sql);
		return \SQL::$LastID;
	}

	static public function logRead($id_stock, $order=0){
		$Sql="SELECT user_in, action, icon,  data_in FROM tbl_mag_stock_log WHERE id_stock = ".intval($id_stock);
		$Sql.= $order==0 ? " ORDER BY data_in ASC" : " ORDER BY data_in DESC" ;
		$Sql.=";";
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}


}

class Stores {
	public static function getSpec($id){
		$Sql='SELECT store, notes, active, id_tana from tbl_mag_stores WHERE id_store= '.intval($id).' and `delete`=0;';
		\SQL::Execute($Sql);
		$res=\SQL::$Result;
		if (isset($res[0])){
			return $res[0];
		}
		return array();
	}

	public static function getAll($fields=array(), $deleted=false, $params=array()){
		/*
		$params=(
			'active' => true
			'left_join' => 'string join'
		)
		*/
		$Sql='SELECT '.implode(',', $fields).' FROM tbl_mag_stores ';

		if (isset($params['left_join'])){
			$Sql.=' LEFT JOIN '.$params['left_join'].' ';
		}

		$where=0;
		if ($deleted==false){
			$Sql.=' WHERE `delete`=0 ';
			$where=1;
		}

		if (isset($params['active'])){
			$Sql.=$where==0? ' WHERE ': ' AND ';
			$Sql.='active='.$params['active'];
		}
		$Sql.=' ORDER by store;';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function qtaStock($id){
		$Sql='SELECT count(id_stock) as qta FROM tbl_mag_stock WHERE id_store='.intval($id).' and deleted=0 GROUP by id_store ;';
		\SQL::Execute($Sql);
		if (isset(\SQL::$Result[0])){
			return intval(\SQL::$Result[0]['qta']);
		} else {
			return 0;
		}
	}

	public static function getStock($id){
		$Sql='SELECT id_stock, tbl_mag_stock.id_store, hidden, data_store, data_acquisto,
			tbl_mag_stores.store, tbl_mag_stores.active as store_active, tbl_mag_ana.id_ana,
			tbl_mag_ana.title,
			Nome as nome_tana, id_owner, origin, tbl_mag_stock.notes, locazione
			FROM tbl_mag_stock
			LEFT JOIN tbl_mag_stores ON tbl_mag_stores.id_store=tbl_mag_stock.id_store
			LEFT JOIN tbl_mag_ana ON tbl_mag_ana.id_ana=tbl_mag_stock.id_ana
			LEFT JOIN tbl_tane ON tbl_tane.id_tana = id_owner
			WHERE tbl_mag_stock.id_store='.intval($id).' and deleted=0 ;';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function delete($id, $destination_stock){
		$id=intval($id);
		$destination_stock=intval($destination_stock);
		$Sql="UPDATE tbl_mag_stores SET `active`=0,`delete`=1  WHERE id_store= {$id};";
		\SQL::Execute($Sql);
		//update log stock
		$Sql="INSERT INTO tbl_mag_stock_log (id_stock, user_in, action, icon)
				SELECT tbl_mag_stock.id_stock, ".\USERS\Identify::UserID().", concat('Trasferimento articolo - Chiusura store: ',store), 'fa-exchange'
				FROM tbl_mag_stock
				LEFT JOIN tbl_mag_stores ON tbl_mag_stores.id_store = tbl_mag_stock.id_store
				WHERE tbl_mag_stock.id_store = {$id} and tbl_mag_stock.deleted=0;";
		\SQL::Execute($Sql);

		$Sql="UPDATE tbl_mag_stock SET `id_store`={$destination_stock} WHERE id_store= {$id};";
		\SQL::Execute($Sql);

		return true;
	}

	public static function activate($id, $activate){
		$Sql='UPDATE tbl_mag_stores set active='.intval($activate).' WHERE id_store='.intval($id).' and `delete`=0;';
		\SQL::Execute($Sql);
		return true;
	}

	public static function updateStore($id, $name, $notes, $tana){
		if ($id>0){
			$Sql='UPDATE tbl_mag_stores set id_tana='.intval($tana).', store='.\SQL::Escape($name).', notes='.\SQL::Escape($notes).' WHERE id_store='.intval($id).' and `delete`=0;';
		} else {
			$Sql='INSERT INTO tbl_mag_stores (id_tana, store, notes) VALUES(
				'.intval($tana).',
				'.\SQL::Escape($name).',
				'.\SQL::Escape($notes).');';
		}

		\SQL::Execute($Sql);
		return true;
	}


}