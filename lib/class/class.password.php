<?php
/**
 * Created by Riccardo Scotti.
 * User: Riccardo Scotti
 * Date: 18/08/15
 */

namespace PASSWORD;

class Generator {
    static protected $PrMix='Goblin'; //var sh Pre
    static protected $PoMix='Jones'; //var sh Post

    static public $Plain=null;
    static public $DB_Encode=null;

    public static function NewPassword($length=8) {
        \PASSWORD\Generator::$Plain = substr(\md5(time().rand()), 0, $length);
        \PASSWORD\Generator::$DB_Encode=hash_hmac('sha512',\PASSWORD\Generator::$PrMix.\PASSWORD\Generator::$Plain.\PASSWORD\Generator::$PoMix,\APP\Parameter::getSecret());
    }

    static public function generateEncode($password) {
        return hash_hmac('sha512',\PASSWORD\Generator::$PrMix.$password.\PASSWORD\Generator::$PoMix,\APP\Parameter::getSecret());
    }

    static public function compressPsw($pass){
        return gzcompress(substr(sha1(mt_rand()),0,2).$pass.substr(sha1(mt_rand()),0,2),9);
    }

    static public function getPswClean($userID) {
        $today=new \DateTime();
        $Sql='SELECT user_id,user_login,t
        FROM tbl_users
        LEFT JOIN _tbl_parole on _tbl_parole.id=user_id
        WHERE user_id="'.intval($userID).'";';
        \SQL::Execute($Sql);
        if (\SQL::$Rows==1){
            return substr(@gzuncompress(\SQL::$Result[0]['t']),2,-2);
        }
        return null;
    }
}