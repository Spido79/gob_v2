<?php

namespace CSS_JS;

class CSS { //class per eseguire gli output css
	protected static $cssList=array( //lista css in ordine
		'/css/bootstrap.min.css',
		'/font-awesome/css/font-awesome.min.css',
		'/css/plugins/switchery/switchery.min.css',
		'/css/plugins/toastr/toastr.min.css',
		'/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.css',
		'/css/plugins/jsTree/style.min.css',
    	'/css/plugins/dropzone/dropzone.min.css',
    	'/css/plugins/dropzone/basic.min.css',
    	'/css/plugins/cropper/cropper.min.css',
		'/css/plugins/dataTables/datatables.min.css',
		'/css/plugins/datapicker/datepicker3.min.css',
		'/css/plugins/sweetalert/sweetalert.min.css',
		'/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.min.css',
		'/css/plugins/select2/select2.min.css',
		'/css/animate.min.css',
		'/css/style.min.css',
		);
	
	public static function Start(){
		foreach (\CSS_JS\CSS::$cssList as $item) {
			
			if (file_exists(APP_PATH_WWW.$item)){
				echo '<link href="'.APP_URL."{$item}?".\APP\Parameter::version()."\" rel=\"stylesheet\">";	
			} else {
				\LOGS\Log::write('CSS Loading Failed', $item, true);
			}
		}
	}
}

class JS { //class per eseguire gli output css
	protected static $jsList=array( //lista css in ordine
		'/js/jquery-3.1.1.min.js',
		'/js/bootstrap.min.js',
		'/js/plugins/switchery/switchery.min.js',
		'/js/plugins/chartJs/Chart.min.js',
		'/js/plugins/metisMenu/jquery.metisMenu.min.js',
		'/js/plugins/slimscroll/jquery.slimscroll.min.js',
		'/js/plugins/pace/pace.min.js',
		'/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
		'/js/plugins/toastr/toastr.min.js',
		'/js/plugins/jsTree/jstree.min.js',
		'/js/plugins/dropzone/dropzone.min.js',
		'/js/plugins/cropper/cropper.min.js',
		'/js/plugins/sparkline/jquery.sparkline.min.js',
		'/js/plugins/flot/jquery.flot.js',
		'/js/plugins/flot/jquery.flot.tooltip.min.js',
		'/js/plugins/flot/jquery.flot.spline.js',
		'/js/plugins/flot/jquery.flot.resize.js',
		'/js/plugins/flot/jquery.flot.pie.js',
		'/js/plugins/flot/jquery.flot.symbol.js',
		'/js/plugins/flot/jquery.flot.time.js',
		'/js/plugins/dataTables/datatables.min.js',
		'/js/plugins/datapicker/bootstrap-datepicker.min.js',
		'/js/plugins/select2/select2.full.min.js',
		'/js/plugins/sweetalert/sweetalert.min.js',
		'/js/plugins/typehead/bootstrap3-typeahead.min.js',
		'/js/inspinia.min.js',
		'/js/common.min.js',
		'/js/plugins/hashchange/hashchange.min.js',

		);
	
	public static function Start(){
		foreach (\CSS_JS\JS::$jsList as $item) {

			if (file_exists(APP_PATH_WWW.$item)){
				echo '<script data-id="'.basename($item).'" data-site="'.APP_URL.'" src="'.APP_URL."{$item}?".\APP\Parameter::version()."\" ></script>";	
			} else {
				\LOGS\Log::write('JS Loading Failed', $item, true);
			}
		}
	}
}

	
