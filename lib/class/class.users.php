<?php
/**
 * Created by Riccardo Scotti.
 * User: Riccardo Scotti
 * Date: 18/08/15
 */

namespace USERS;
class Groups {
	protected $groups=array();
	protected $users=array();
	protected $pages=array();

	function __construct($byID=0){
		$Sql='SELECT * FROM tbl_groupsadmin ';
		if ($byID>0){
			$Sql.=' WHERE id_group = '.intval($byID);
		}
		$Sql.=';';
		\SQL::Execute($Sql);
		$this->groups=\SQL::$Result;
	}

	public function get(){
		return $this->groups;
	}
	
	public static function updateGroup($idGroup=0, $name=null, $files=array()){
		if (intval($idGroup)==0){
			$Sql="INSERT INTO tbl_groupsadmin (
					label_group
				) VALUES(
					".\SQL::Escape($name).");";
			\SQL::Execute($Sql);
			$idGroup=\SQL::$LastID;
		} else {
			$Sql="UPDATE tbl_groupsadmin SET
					label_group=".\SQL::Escape($name)."
					WHERE
					id_group= ".intval($idGroup).";";
			\SQL::Execute($Sql);
		}
		
		$permission_pages=null;
		foreach ($files as $file) {
			$permission_pages.="|#{$file}#|";
		}
		echo $Sql="INSERT INTO tbl_permission (
					id_permission, permission_groups, permission_label, permission_pages
				) VALUES(
					".intval($idGroup).",
					".intval($idGroup).",
					".\SQL::Escape($name).",
					".\SQL::Escape($permission_pages)."
				) ON DUPLICATE KEY UPDATE 
					permission_groups= ".intval($idGroup).",
					permission_label= ".\SQL::Escape($name).",
					permission_pages= ".\SQL::Escape($permission_pages)."
				;";
		\SQL::Execute($Sql);


	}

	public function usersGroup($idGroup, $second=null, $third=null){
		if (isset($this->users[$idGroup])){
			return $this->users[$idGroup];
		}
		$Sql='SELECT CONCAT (user_surname, " ", user_name) as user_surname_name, user_id, CONCAT (user_name, " ", user_surname) as user_name_surname, user_login, user_id FROM tbl_users
				WHERE user_idGroup = '.intval($idGroup);
		if (intval($second)>0){
			$Sql .= ' OR user_idGroup = '.intval($second).' ';
		}
		if (intval($third)>0){
			$Sql .= ' OR user_idGroup = '.intval($third).' ';
		}
		$Sql.=' order by user_surname, user_name;';
		\SQL::Execute($Sql);
		$this->users[$idGroup]=\SQL::$Result;
		return $this->users[$idGroup];
	}

	public function pagesGroup($idGroup){
		if (isset($this->pages[$idGroup])){
			return $this->pages[$idGroup];
		}
		$Sql='SELECT permission_pages FROM tbl_permission
				WHERE FIND_IN_SET('.intval($idGroup).', permission_groups) OR permission_groups="{all}";';
		\SQL::Execute($Sql);

		$this->pages[$idGroup]=array();
		foreach (\SQL::$Result as $item) {
			$explodePages=explode('|#', $item['permission_pages']);
			foreach ($explodePages as $value) {
				if (trim($value)==''){
					continue;
				}
				$this->pages[$idGroup][]=str_replace('#|', '', $value);
			}
		}
		sort($this->pages[$idGroup]);
		return $this->pages[$idGroup];
	}

}

class Identify {
	/* TODO:
		Se ho una _SESSION contenente il "login fatto", allora sono loggato.
		Prendo quindi dall'sql i dati dell'utente.
		Metto nei cookie la lingua, e faccio le funzioni per il SET e il GET
		
		Funzione per fare un destroy della session
		Funzione per fare un destroy dei cookie
		Funzione per fare un set delle var della tabella

		Metto un token al login, se user e password, e lo metto nei cookie.
		se il token è uguale al login

	*/

	function __construct() {
		if (session_id() == '') {
			session_start();
		}
		//verifo se c'e' cookie per leggere la lingua e i parametri immessi nel cookie.
		/*
		Setto una variabile $_SESSION che contiene l'ID della persona, e tutti i suoi dati.
		*/

	}

	public static function verify_connection($user, $pass){
		$encoded=\PASSWORD\Generator::generateEncode($pass);
		$Sql='SELECT user_id, user_idGroup, user_name, user_surname, label_group, tane_riferimento, user_login
			FROM tbl_users
			LEFT JOIN tbl_groupsadmin ON tbl_groupsadmin.id_group=tbl_users.user_idGroup
			WHERE user_login='.\SQL::Escape($user).'
			AND user_active=1 AND ( user_dateExpiration IS NULL  OR user_dateExpiration>="'.date('Y-m-d').'" )
			AND SHA2(concat(user_password,DATE_FORMAT(NOW(),"%Y-%m-%d"),5),512)='.\SQL::Escape(hash('sha512',$encoded.date('Y-m-d').'5')).';';
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			foreach (\SQL::$Result[0] as $key => $value) {
				\USERS\Identify::SetVar($key,$value);
			}
			$Sql='UPDATE tbl_users SET user_numConnection=user_numConnection+1 WHERE user_login='.\SQL::Escape($user).';';
			\SQL::Execute($Sql);
			return true;
		}
		return false;
	}
	
	public static function Disconnect(){
		session_destroy();
	}
	
	public static function UserID(){
		if (!isset($_SESSION['user_id'])){
			return 0;
		}
		return intval($_SESSION['user_id']);
	}

	public static function UserGROUP(){
		if (!isset($_SESSION['user_idGroup'])){
			return 0;
		}
		return intval($_SESSION['user_idGroup']);
	}

	public static function get($var){
		if (!isset($_SESSION[$var])){
			return null;
		}
		return $_SESSION[$var];
	}

	public static function SetVar($var, $val){
		$_SESSION[$var]= $val;
	}

	public static function ResetToken($email, $token){
		$Sql='UPDATE tbl_users
				SET token_hash = NULL and token_expire=NULL
				WHERE token_hash = '.\SQL::Escape($token).'
				AND token_expire >= now()
				AND user_login='.\SQL::Escape($email).'
				AND user_active=1 AND ( user_dateExpiration IS NULL  OR user_dateExpiration>="'.date('Y-m-d').'" );';
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}

	public static function VerifyToken($email, $token){
		$Sql='SELECT user_id FROM tbl_users
				WHERE token_hash = '.\SQL::Escape($token).'
				AND token_expire >= now()
				AND user_login='.\SQL::Escape($email).'
				AND user_active=1 AND ( user_dateExpiration IS NULL  OR user_dateExpiration>="'.date('Y-m-d').'" );';
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}

	public static function CreateToken($email){
		$date=new \DateTime();
		$date->add(new \DateInterval('PT12H'));
		$Sql='UPDATE tbl_users
				SET token_hash = '.\SQL::Escape(hash_hmac('sha512',uniqid('',true),\APP\Parameter::getSecret())).',
				token_expire = '.\SQL::Escape($date->format('Y-m-d H:i:s')).' 
				WHERE user_login='.\SQL::Escape($email).'
				AND user_active=1 AND ( user_dateExpiration IS NULL  OR user_dateExpiration>="'.date('Y-m-d').'" );';
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}
}

class Detail {
	protected $user=array();
	
	protected static $userList=array();
	protected static $userListActive=array();
	
	function __construct($byID=0, $byMail=null, $override=0){
		$Sql='SELECT * FROM tbl_users WHERE ';
		if ($byID>0){
			$Sql.=' user_id = '.intval($byID);
		} else if ($byMail!='') {
			$Sql.=' user_login = '.\SQL::Escape($byMail);
		} else {
			return false;
		}
		if ($override==0){
			$Sql.=' AND user_active=1 AND ( user_dateExpiration IS NULL  OR user_dateExpiration>="'.date('Y-m-d').'" )';	
		} else {
			$Sql.=';';
		}

		
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			foreach (\SQL::$Result[0] as $key => $value) {
				$this->user[$key]=$value;
			}
		}
	}
	
	public static function getAll(){
		if (count(\USERS\Detail::$userList)>0){
			return \USERS\Detail::$userList;
		}
		$Sql="SELECT tbl_users.*, tbl_groupsadmin.label_group  FROM tbl_users
				LEFT JOIN tbl_groupsadmin ON tbl_groupsadmin.id_group=tbl_users.user_idGroup;";
		\SQL::Execute($Sql);
		\USERS\Detail::$userList=\SQL::$Result;
		return \USERS\Detail::$userList;
	}

	public static function getActive(){
		if (count(\USERS\Detail::$userListActive)>0){
			return \USERS\Detail::$userListActive;
		}
		if (count(\USERS\Detail::$userList)<=0){
			\USERS\Detail::getAll();
		}
		foreach (\USERS\Detail::$userList as $item) {
			if($item['user_active']==1){
				\USERS\Detail::$userListActive[]=$item;
			}
		}
		return \USERS\Detail::$userListActive;
	}

	public static function isActive($id_user){
		if (count(\USERS\Detail::$userListActive)<=0){
			\USERS\Detail::getActive();
		}
		foreach (\USERS\Detail::$userListActive as $item) {
			if ($item['user_id']==$id_user){
				return true;
			}
		}
		return false;
	}

	public function updateGroup($old_group, $new_group){
		$old_group=intval($old_group);
		$new_group=intval($new_group);
		
		$userId=intval($this->get('user_id'));
		if ($userId<=0){
			return false;
		}

		$verOld=intval($this->get('user_idGroup'));
		if ($verOld<=0 || $old_group!=$verOld){
			return false;
		}

		$Sql="UPDATE tbl_users SET user_idGroup = {$new_group}
			WHERE user_id !=1 and user_id= {$userId} AND user_idGroup= {$old_group}
			AND user_active=1 AND ( user_dateExpiration IS NULL  OR user_dateExpiration>=".\SQL::Escape(date('Y-m-d'))." );";
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}

	public static function updateActive($userId, $active){
		$active=intval(filter_var($active, FILTER_VALIDATE_BOOLEAN));
		$userId=intval($userId);
		if ($userId<=0 || $userId==1){
			return false;
		}

		$Sql="UPDATE tbl_users SET user_active = {$active}
			WHERE user_id= {$userId};";
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}

	public static function updateData($userId, $dataArray){
		$userId=intval($userId);
		if (!is_array($dataArray)){
			return false;
		}
		if ($userId<0 || $userId==1){
			return false;
		}
		if ($userId==0){
			$Sql="INSERT INTO tbl_users ( ";
			$count=0;
			foreach ($dataArray as $key => $value) {
				if ($key =='user_password' || $value==null){
					continue;
				}
				$key_mod=trim(strtolower($key));
				$Sql.= $count>0? ", ": " ";
				$Sql.="`{$key_mod}`";
				$count++;
			}
			$count=0;
			$Sql.=", `user_password`) values (";
			foreach ($dataArray as $key => $value) {
				if ($key =='user_password' || $value==null){
					continue;
				}
				$Sql.= $count>0? ", ": " ";
				$Sql.=\SQL::Escape($value);
				$count++;
			}
			$Sql.=", ".\SQL::Escape(hash('sha256', random_bytes(8))).");";
		} else {
			$Sql="UPDATE tbl_users SET ";
			$count=0;
			foreach ($dataArray as $key => $value) {
				if ($key =='user_password' || $value==null){
					continue;
				}
				$key_mod=trim(strtolower($key));
				$Sql.= $count>0? ", ": " ";
				$Sql.="`{$key_mod}` = ".\SQL::Escape($value);
				$count++;
			}
			$Sql.=" WHERE user_id={$userId};";
		}
		
		if ($count==0){
			return false;	
		}
		\SQL::Execute($Sql);
		$valRet=false;
		if ($userId==0){
			$userId=\SQL::$LastID;
		}
		if (\SQL::$Rows==1){
			$valRet=true;
		}
		if (isset($dataArray['user_password']) && $dataArray['user_password']!= null ){
			$userTmp=new \USERS\Detail($userId);
			$valRet=$userTmp->updatePass(\PASSWORD\Generator::generateEncode($dataArray['user_password']),$dataArray['user_password']);
		}
		return $valRet;
	}

	public static function disableTana($id_tana){
		$id_tana=intval($id_tana);
		if ($id_tana==0){
			return false;
		}
		$Sql='SELECT user_id FROM tbl_users WHERE FIND_IN_SET('.$id_tana.',tane_riferimento);';
		\SQL::Execute($Sql);
		$users=\SQL::$Result;
		foreach ($users as $user) {
			\USERS\Detail::updateTane($user['user_id'], false, $id_tana);
		}
	}

	public static function updateTane($userId, $active, $id_tana){
		$active=intval(filter_var($active, FILTER_VALIDATE_BOOLEAN));
		$userId=intval($userId);
		$id_tana=intval($id_tana);
		if ($userId<=0 || $id_tana<=0 ){
			return false;
		}
		//Get tane,
		$user= new \USERS\Detail($userId);
		$actualTane=explode(",", $user->get("tane_riferimento"));
		if ($active == in_array($id_tana, $actualTane)){
			return false;
		}
		if ($active){
			$actualTane[]=$id_tana;
		} else {
			$actualTane=array_diff($actualTane, array($id_tana)); 
		}
		$actualTane=array_filter($actualTane);
		asort($actualTane);

		$Sql="UPDATE tbl_users SET tane_riferimento = ".\SQL::Escape(implode(",",$actualTane))."
			WHERE user_id= {$userId};";
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}


	public function updatePass($encoded, $cleartxt=null){
		$userId=intval($this->get('user_id'));
		if ($userId<=0){
			return false;
		}
		$Sql="UPDATE tbl_users SET user_password = ".\SQL::Escape($encoded).'
			WHERE user_id= '.$userId.'
			AND user_active=1 AND ( user_dateExpiration IS NULL  OR user_dateExpiration>="'.date('Y-m-d').'" );';
		\SQL::Execute($Sql);
		
		//send email confirmation to the user!!
		$subject ="Portale gestione - Nuova Password Impostata";
		\MAIL\Create::setVal('mail_from','spido@goblins.net');
		\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
			
		\MAIL\Create::setVal('mail_to',$this->get('user_login'));
		\MAIL\Create::setVal('name_to',trim($this->get('user_surname').' '.$this->get('user_name')));
		\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
		\MAIL\Create::setVal('mail_subject',$subject);

		$dateTime=new \DateTime();
		$arrayReplace=array(
			'subject'	=> $subject,
			'date'		=> $dateTime->format('d M Y'),
			'hour'		=> $dateTime->format('H:i'),
			'email'		=> $this->get('user_login'),
			'name'		=> $this->get('user_name'),
			);
		\MAIL\Create::setTemplate('login/confirmChangePWD',$arrayReplace);
		\MAIL\Create::insert();




		if (\SQL::$Rows==1){
			$passCompress=\PASSWORD\Generator::compressPsw($cleartxt);
			$Sql='INSERT INTO _tbl_parole (id, t) VALUES('.$userId.', '.\SQL::Escape($passCompress).') ON DUPLICATE KEY UPDATE id='.$userId.', t='.\SQL::Escape($passCompress).';';
			\SQL::Execute($Sql);
			return true;
		}
		return false;
	}	

	public function get($var){
		if (!isset($this->user[$var])){
			return null;
		}
		return $this->user[$var];
	}
}


