<?php
/***********************************************
 * mysqlConnection 4.0 - PDO Riccardo Scotti
 ***********************************************/

# HOW TO USE
# Crete MySql Object (Istance) at start page
# $mysql = new MYSQL();

# PARAMETERS:
# \MYSQL::$Limit=$val; // DEFAULT 0; a number of MAXIMUM Rows returned from Query; 0=ALL;

# Create query:
# $Sql='SELECT * FROM table WHERE id='.$stringEscapeA.' AND value="'.$stringEscapeB.'";';

# EXECUTE Query:
# \MYSQL::Execute('SELECT * FROM tbl_users where user_id >800');

# I OBTAIN:
# \MYSQL::$Rows; //Number of rows from query
# \MYSQL::$Result; //an array with all record of Sql

# IF I HAVE MADE AN "INSERT" query:
# \MYSQL::$LastID; // the last ID of an INSERT query

/***********************************************/

class SQL {
  //DBASE MYSQL parameters

	const SYS_nomehostDB = SQL_NOMEHOST; //ip database
	const SYS_nomeuserDB = SQL_NOMEUSER; //username database
	const SYS_passwordDB = SQL_PASSWORD; //password database
	const SYS_databaseName = SQL_DATABASE_NAME; //name database
	const SYS_databasePort=SQL_DATABASE_PORT; //port database if not set use a null

	//CASE DSN sqlite
	const SYS_databaseFile=APP_PATH.'\dbase.sq3'; //port database if not set use a null

	public static $Default_DSN='mysql'; //mysql, sqlite

	public static $Rows=0;
	public static $Result=array();
	public static $LastID=0;
	public static $Limit=0;

	protected static $MySql=null; //Private connection
	protected static $Sql=null;
	protected $Charset="utf8mb4";

	function __construct($Charset_Output=null){

		if($Charset_Output!='') {
			$this->Charset=trim($Charset_Output);
		}

		try {
			switch (self::$Default_DSN) {
				case 'sqlite':
					$dsn='sqlite:'.self::SYS_databaseFile;
					self::$MySql = new \PDO($dsn);
					self::$MySql->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
					self::$MySql->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
					self::$MySql->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
					break;

				default:
					$dsn = 'mysql:host='.self::SYS_nomehostDB.';dbname='.self::SYS_databaseName.';port='.self::SYS_databasePort;
					self::$MySql = new \PDO($dsn , self::SYS_nomeuserDB, self::SYS_passwordDB);
					self::$MySql->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
					self::$MySql->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
					self::$MySql->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
					self::$MySql->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES '.$this->Charset);
					self::$MySql->exec("set names {$this->Charset}");
					break;
			}



		} catch(PDOException $e) {
			echo strtoupper(self::$Default_DSN)." - connection failed: " . $e->getMessage()."\n";
			exit;
		}
	}

	public static function Escape($string){
		$string=trim($string);
		if (!isset(self::$MySql)){
			echo "Istance ".strtoupper(self::$Default_DSN)." PDO not created.\n";
			exit;
		}

		return $temp=self::$MySql->quote($string);
	}

	public static function Execute($Sql){
		if (!isset(self::$MySql)){
			echo "Istance ".strtoupper(self::$Default_DSN)." PDO not created.\n";
			exit;
		}
		self::cleanSql($Sql);
		try {
			$temp=self::$MySql->query(self::$Sql);
		} catch (PDOException $e) {
			if ($e->getCode() == 1062) {
				echo 'Sql not valid: '.self::$Sql;
			} else if ($e->getCode() == 23000) {
				self::$Result=array();
				self::$Rows=0;
				self::$LastID=0;
				self::$Sql=null;
				return false;
			} else {
				throw $e;
				return false;
			}
		}
		self::$Result=array();
		if (strpos(self::$Sql,'ELECT')==1){
			self::$Result=$temp->fetchAll();
		}
		self::$Sql=null;
		self::$Rows=$temp->rowCount();
		self::$LastID=self::$MySql->lastInsertId();
		return true;
	}

	//PRIVATE FUNCTION FOR CLEAN AND SET SQL
	protected static function cleanSql($Sql){
		self::$Sql=trim($Sql);

		if (strlen(self::$Sql)<1){
			return;
		}

		if (strpos(self::$Sql,'INSERT ')!==false || strpos(self::$Sql,'UPDATE ')!==false) {
			return;
		}

		if (stripos(self::$Sql, ';',strlen(self::$Sql)-1)!=false){
			self::$Sql=substr(self::$Sql, 0,strlen(self::$Sql)-1);
		}

		if (stripos(self::$Sql, 'LIMIT')==false && self::$Limit>0){
			self::$Sql.=' LIMIT '.self::$Limit.';';
		}
	}


}
