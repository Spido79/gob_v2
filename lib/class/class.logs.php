<?php
namespace LOGS;
class Log {
	CONST FILE_LOCATION =LOG_PATH.'/gestionale.log';
	CONST MAX_MB_LOG =20;
	public static $Verbose=true;
	public static $Html=false;

	public static function read($search){
		if (!file_exists(self::FILE_LOCATION)){
			return array();
		}
		if ($search!=''){
			$contents = file_get_contents(self::FILE_LOCATION);
			$pattern = preg_quote($search, '/');
			// finalise the regular expression, matching the whole line
			$pattern = "/^.*$pattern.*\$/mi";
			// search, and store all matching occurences in $matches
			if(preg_match_all($pattern, $contents, $matches)){
				return $matches[0];
			} else {
				return array();
			}
		}
		$file = new \SplFileObject(self::FILE_LOCATION);
		$file->seek(PHP_INT_MAX);
		$total_lines = $file->key();
		$subtract=$total_lines-100;
		if ($subtract<0){
			$subtract=0;
		}
		$reader = new \LimitIterator($file, $subtract);
		return $reader;
	}

	public static function last_modify(){
		$today=new \DateTime();
		if (!file_exists(self::FILE_LOCATION)){
			return $today->format('d M Y H:i:s');
		}
		$today->setTimestamp(filemtime(self::FILE_LOCATION));
		return $today->format('d M Y H:i:s');
	}
	public static function start(){
		/*
			Special case for make a breakpoint in the log
		 */
		if (!file_exists(dirname(self::FILE_LOCATION))){
			echo "Warning: LOG folder not exist!\n\n";
            exit;
		}

		/*$today=new \DateTime();
		$line=$today->format('Y-m-d H:i:s').'|';
		$line.=str_pad('--- Start ', 47,"-", STR_PAD_RIGHT)."\n";
		file_put_contents(self::FILE_LOCATION, $line,FILE_APPEND | LOCK_EX);
		*/
	}

	public static function write($stringType,$stringMsg,$videoOutput=false,$userID=0){
		/**
		 *
		 * Log Creation
		 * Grammar: Date (Y-m-d H:i:s) | Remote Addr (str 15) | Type Log (str 8) | PageScript | Message
		 *
		 */
		$today=new \DateTime();
		//verify Max Dimension file reached
		\LOGS\Log::MaxFileReached();

		$PageScript = 'ERROR';

		if (isset($_SERVER['SCRIPT_FILENAME'])){
			$PageScript = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']: $_SERVER['SCRIPT_FILENAME'];
		}
		$line=$today->format('Y-m-d H:i:s').'|';
		$line.=str_pad($PageScript, 10," ", STR_PAD_RIGHT).'|';
		$line.=str_pad(intval($userID), 5," ", STR_PAD_LEFT).'|';
		$line.=str_pad($stringType, 21," ", STR_PAD_RIGHT).'|';
		$line.=$stringMsg."\n";

		if (!file_exists(dirname(self::FILE_LOCATION))){
			echo "Warning: LOG folder not exist!\n\n";
            exit;
		}
		if ($videoOutput && \LOGS\Log::$Verbose){
			file_put_contents(self::FILE_LOCATION, $line,FILE_APPEND | LOCK_EX);

			$escape="\n";
			if (\LOGS\Log::$Html){
				$escape.="<br>";
			}
			if (LOG_SCREEN){
				print_r($today->format('Y-m-d H:i:s').' (User: '.$userID.') '.$stringType.' : '.$stringMsg.$escape);
			}
			$f = fopen('php://stderr', 'w');
			if (LOG_PRINT){
				fputs($f, $today->format('Y-m-d H:i:s').' (User: '.$userID.') '.$stringType.' : '.$stringMsg.$escape);
			}


		}
	}

	protected static function MaxFileReached() {
		if (file_exists(self::FILE_LOCATION)){
			$maxDimension=self::MAX_MB_LOG*(pow(1024,2));
			if (filesize(self::FILE_LOCATION)>$maxDimension){
				$today=new \DateTime();
				$dimension=\LOGS\Log::FileSizeConvert(filesize(self::FILE_LOCATION));
				file_put_contents(self::FILE_LOCATION, "\n-------------------------------------------------\n{$today->format('Y-m-d H:i:s')} Log Archived, max file reached: {$dimension}\n",FILE_APPEND | LOCK_EX);

				$info = pathinfo(self::FILE_LOCATION);
				$file_name =  basename(self::FILE_LOCATION,'.'.$info['extension']);
				$folderName=dirname(self::FILE_LOCATION);
				$fileToTar=$folderName.'/'.$file_name.'_'.$today->format('Ymd\_His');
				if (!file_exists($fileToTar)){
					$tarGz = new \PharData($fileToTar.'.tar');
					// ADD FILES TO archive.tar FILE
					$tarGz->addFile(self::FILE_LOCATION);
					$tarGz->compress(\Phar::GZ);
					unlink($fileToTar.'.tar');
					unlink(self::FILE_LOCATION);
				}

			}
		}
	}

	public static function FileSizeConvert($bytes) {
		$bytes = floatval($bytes);
		$result=0;
		$arBytes = array(
			0 => array(
				"UNIT" => "TB",
				"VALUE" => pow(1024, 4)
				),
			1 => array(
				"UNIT" => "GB",
				"VALUE" => pow(1024, 3)
				),
			2 => array(
				"UNIT" => "MB",
				"VALUE" => pow(1024, 2)
				),
			3 => array(
				"UNIT" => "KB",
				"VALUE" => 1024
				),
			4 => array(
				"UNIT" => "B",
				"VALUE" => 1
				),
			);
		foreach($arBytes as $arItem){
			if($bytes >= $arItem["VALUE"]) {
				$result = $bytes / $arItem["VALUE"];
				$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
				break;
			}
		}
		return $result;
	}

}

class Tesserato{
	static public function write($id_goblin,$textPassed, $icon){
		$Sql="INSERT tbl_loggoblin ( id_goblin,id_user,description, icon) VALUES
				(".intval($id_goblin).",".\USERS\Identify::UserID().",".\SQL::Escape($textPassed).",".\SQL::Escape($icon).");";
		\SQL::Execute($Sql);
		return \SQL::$LastID;
	}

	static public function read($id_goblin, $order=0){
		$Sql="SELECT id_user, description, icon,  dataMod FROM tbl_loggoblin WHERE id_goblin = ".intval($id_goblin);
		$Sql.= $order==0 ? " ORDER BY dataMod ASC" : " ORDER BY dataMod DESC" ;
		$Sql.=";";
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}
}