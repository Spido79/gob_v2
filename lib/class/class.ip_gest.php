<?php
/**
 * Created by PhpStorm.
 * User: riccardo
 * Date: 23/12/2015
 * Time: 11:51
 */

namespace IP_GEST;

class IP_list {
    const ConnectionMax=15;
    protected static $ver_ip=null;
    protected static $list_ip=array();

    static public function listIP(){
        if (count(\IP_GEST\IP_list::$list_ip)>0){
            return \IP_GEST\IP_list::$list_ip;
        }
        $Sql='SELECT * FROM tbl_ipblocked order by tryConnection desc, date_lastTry desc, ip;';
        \SQL::Execute($Sql);
        \IP_GEST\IP_list::$list_ip = \SQL::$Result;
        return \IP_GEST\IP_list::$list_ip;
    }

    static public function verBlocked(){
        if (\IP_GEST\IP_list::$ver_ip!==null){
            return \IP_GEST\IP_list::$ver_ip;
        }

        $Sql='SELECT ip FROM tbl_ipblocked WHERE ip='.intval(ip2long($_SERVER['REMOTE_ADDR'])).' AND tryConnection >='.self::ConnectionMax.';';
        \SQL::Execute($Sql);
        if (\SQL::$Rows>=1){
            \IP_GEST\IP_list::$ver_ip=true;
            return true;
        }
        \IP_GEST\IP_list::$ver_ip=false;
        return false;
    }

    static public function CleanIP($id=null){
        if (!is_null($id)){
            $ipToWrite=$id;
        } else {
            $ipToWrite=ip2long($_SERVER['REMOTE_ADDR']);
        }
        $date=new \DateTime();
        $Sql='UPDATE tbl_ipblocked SET tryConnection=1, pass_try="", word_try="" , date_lastTry="'.$date->format('Y-m-d H:i:s').'" WHERE ip="'.intval($ipToWrite).'";';
        \SQL::Execute($Sql);
    }

    static public function IncraseIP($wordtry,$tryPass){
        $Sql="INSERT INTO tbl_ipblocked (
                pass_try, word_try, tryConnection, date_lastTry, ip
                ) VALUES(
                    ".\SQL::Escape($tryPass).",
                    ".\SQL::Escape($wordtry).",
                    1,
                    now(),
                    ".intval(ip2long($_SERVER['REMOTE_ADDR']))."
                ) ON DUPLICATE KEY UPDATE 
                    tryConnection=tryConnection+1, 
                    word_try= ".\SQL::Escape($wordtry).",
                    pass_try= ".\SQL::Escape($tryPass).",
                    date_lastTry=now()
                ;";
        \SQL::Execute($Sql);
    }
}