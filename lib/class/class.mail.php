<?php
namespace MAIL;
class Create {
	public static $offset=25;

	protected static $field=array(
		'mail_from'		=>null,
		'mail_to'		=>null,
		'name_from'		=>null,
		'name_to'		=>null,
		'reply_to'		=>null,
		'mail_cc'		=>null,
		'mail_bcc'		=>null,
		'mail_subject'	=>null,
		'mail_message'	=>null
		);
	public static function insert(){
		$Sql="INSERT INTO tbl_mail (";
		$count=0;
		foreach (\MAIL\Create::$field as $key => $value) {
			if ($count>0){ $Sql.=", "; }
			$Sql.="`{$key}`";
			$count++;
		}
		$Sql.=") VALUES (";

		$count=0;
		foreach (\MAIL\Create::$field as $key => $value) {
			if ($count>0){ $Sql.=", "; }
			if ($key=='mail_subject' || $key=='mail_message' ){
				$Sql.=\SQL::Escape(gzcompress($value,9));
			} else {
				$Sql.=\SQL::Escape($value);
			}

			\MAIL\Create::$field[$key]=null; //clean the static array for future call
			$count++;
		}
		$Sql.=");";
		\SQL::Execute($Sql);
		return \SQL::$LastID;
	}

	public static function setTemplate($file, $arrayReplace){

		if (!file_exists(APP_PATH.'/themes/template_mail/'.$file.'.html')){
			return null;
		}
		if (!is_array($arrayReplace)){
			return false;
		}
		$fileRead=file_get_contents(APP_PATH.'/themes/template_mail/'.$file.'.html');
		foreach ($arrayReplace as $key => $value) {
			$fileRead=str_ireplace('{$'.$key.'}', $value, $fileRead);
		}
		\MAIL\Create::$field['mail_message']=$fileRead;
	}


	public static function setVal($key, $val){
		if (!array_key_exists($key, \MAIL\Create::$field) || $key=='mail_message'){
			return false;
		}
		\MAIL\Create::$field[$key]=$val;
		return true;
	}

	public static function Send(){
		$Sql='SELECT * FROM tbl_mail where sent=0;';
		\SQL::Execute($Sql);
		$result=\SQL::$Result;

		foreach ($result as $key => $value) {
			$mail = new \PHPMailer();
			$mail->IsSMTP();
			//$mail->IsSendmail();
			$mail->CharSet = 'UTF-8';
			$mail->SMTPAuth   = false;
		        $mail->SMTPOptions = array(
    			    'ssl' => array(
        			'verify_peer' => false,
        			'verify_peer_name' => false,
        			'allow_self_signed' => true
			    )
			);
			$mail->Port       = 25;
			$mail->Host       = "127.0.0.1";

			$mail->SMTPDebug = 4;
			//$mail->Username   = "";
			//$mail->Password   = "";
			if ($value['reply_to']!=''){
				$mail->AddReplyTo($value['reply_to'],$value['name_from']);
			} else {
				$mail->AddReplyTo($value['mail_from'],$value['name_from']);
			}

			$mail->SetFrom($value['mail_from'],$value['name_from']);
			$mail->AddAddress($value['mail_to'],$value['name_to']);
			if ($value['mail_to']!=''){
				$mail->Subject = gzuncompress($value['mail_subject']);
			} else {
				$mail->Subject = "";
			}

			$mail->AltBody    = "Per vedere il messaggio usare un programma compatibile con HTML!"; // optional, comment out and test
			if ($value['mail_to']!=''){
				$mail->MsgHTML(gzuncompress($value['mail_message']));
			} else {
				$mail->MsgHTML("");
			}
			$mail->AddEmbeddedImage(APP_PATH_WWW."/img/logo_long.png", "logo", "logo.png");
			if($mail->Send()) {

				if (in_array(gzuncompress($value['mail_subject']), array('Portale gestione - Nuova Password','Portale gestione - Reset Password'))){
					$mail_message=gzcompress('<span style="color:#de0000"><i>Contenuto protetto per questione di privacy.</i></span>',9);
					echo $Sql='UPDATE tbl_mail set mail_message='.\SQL::Escape($mail_message).', sent=1 where id='.$value['id'].';';
				} else {
					$Sql='UPDATE tbl_mail set sent=1 where id='.$value['id'].';';
				}

				\SQL::Execute($Sql);
			} else {
				\LOGS\Log::write('ERROR', "PHPMailer: ".$mail->ErrorInfo, false);
			}
		}
	}

	public static function get($sent=null, $category=null, $page=null){
		$categories=array(
			'Blocco IP' => array(
				'Portale gestione - BLOCCO IP',
			),
			'Credenziali' => array(
				"Portale gestione - Reset Password",
				"Portale gestione - Nuova Password",
				"Portale gestione - Nuova Password Impostata",
			),
			'Gest. Goblins' =>array('%'),
		);

		$Sql='SELECT * FROM tbl_mail ';

		$where=false;
		if (!is_null($sent)){
			$where=true;
			$Sql.='WHERE sent='.intval($sent);
		}

		if (!is_null($category)){
			$Sql.= $where ? ' AND ': 'WHERE';
			$where=true;
			$Sql.=' (';
			if (!isset($categories[$category])){
				return false;
			}
			$count=0;
			foreach ($categories[$category] as $cateSearch) {
				if ($cateSearch=='%'){
					foreach ($categories as $key => $catOther) {
						if ($key==$category){
							continue;
						}
						foreach ($catOther as $skipCat) {
							$Sql.= $count>0 ? ' AND ': '';
							$Sql.= 'mail_subject!='.\SQL::Escape(gzcompress($skipCat,9));
							$count++;
						}


					}
				} else {
					$Sql.= $count>0 ? ' OR ': '';
					$Sql.= 'mail_subject='.\SQL::Escape(gzcompress($cateSearch,9));
					$count++;
				}
			}
			$Sql.=')';
		}

		$Sql.=' ORDER BY data_in DESC ';

		if (!is_null($page)){
			$Sql.=' LIMIT '.\MAIL\Create::$offset.' OFFSET '.(intval($page)*\MAIL\Create::$offset);
		}
		$Sql.=';';

		\SQL::Execute($Sql);
		$result=\SQL::$Result;
		return $result;
	}

	public static function getSpec($id){
		$Sql='SELECT * FROM tbl_mail where id='.intval($id).';';
		\SQL::Execute($Sql);
		$result=\SQL::$Result;
		if (isset($result[0])){
			return $result[0];
		}
		return false;
	}

	public static function countMail($sent=0){
		$Sql='SELECT count(id) as counter FROM tbl_mail where sent='.intval($sent).';';
		\SQL::Execute($Sql);
		$result=\SQL::$Result;
		return $result[0]['counter'];
	}

	public static function closetags($html) {
		preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
		$openedtags = $result[1];
		preg_match_all('#</([a-z]+)>#iU', $html, $result);
		$closedtags = $result[1];
		$len_opened = count($openedtags);
		if (count($closedtags) == $len_opened) {
			return $html;
		}
		$openedtags = array_reverse($openedtags);
		for ($i=0; $i < $len_opened; $i++) {
			if (!in_array($openedtags[$i], $closedtags)) {
				$html .= '</'.$openedtags[$i].'>';
			} else {
				unset($closedtags[array_search($openedtags[$i], $closedtags)]);
			}
		}
		return $html;
	}
	public static function removeSpecialTag($html) {
		 $outputA = preg_replace('/<\s*style.+?<\s*\/\s*style.*?>/si'," ",$html);
		 $outputB = preg_replace('/<\s*title.+?<\s*\/\s*title.*?>/si'," ",$outputA);
		 $outputC = preg_replace('/<\s*meta.+?\s*\/\s*.*?>/si'," ",$outputB);
		 $outputD = preg_replace('/<\s*link.+?\s*\/\s*.*?>/si'," ",$outputC);
		 $outputE = preg_replace('/<\s*script.+?<\s*\/\s*script.*?>/si'," ",$outputD);
		 $output = preg_replace('/<\s*script.+?\s*\/\s*.*?>/si'," ",$outputE);

		return $output;
	}


}
