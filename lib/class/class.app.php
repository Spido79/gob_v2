<?php
namespace APP;

class Parameter {
    protected $version=0;
    const PARAM_FOLDER =".config";
    const PARAM_FILE =".param";

    function __construct(){
        $testParam=$this->readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'log_path');
        if ($testParam===false){
            echo "SERVER STOP\nERROR: file .param not found!\n\n";
            exit;
        }

        if ($testParam===null){
            echo "SERVER STOP\nERROR: log path in .param not found!\n\n";
            exit;
        }
        $this->log_path=$testParam;
        $this->log_print=$this->readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'log_print');
        $this->log_screen=$this->readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'log_screen');
        $this->sql_nomehost=$this->readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'sql_nomehost');
        $this->sql_nomeuser=$this->readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'sql_nomeuser');
        $this->sql_password=$this->readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'sql_password');
        $this->sql_database_name=$this->readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'sql_database_name');
        $this->sql_database_port=$this->readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'sql_database_port');
        

    }
    
    public static function getSpec($parameter){
        return \APP\Parameter::readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,$parameter);
    }

    public static function getSecret(){
        return \APP\Parameter::readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'secret');
    }

    public static function version(){
        return \APP\Parameter::readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'version');
    }

    public static function serial(){
        return \APP\Parameter::readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'serial');
    }

    public static function release(){
        return \APP\Parameter::readfile(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE,'release');
    }

    public static function update($param, $newValue){
        $param=strtolower($param);
        $file= __dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE;
        if (!file_exists($file)){
            return false;
        }
        $readedFile=file_get_contents($file);
        $stringWrite=null;
        $lines=explode("\n",$readedFile);
        foreach ($lines as $line) {
            $variable=explode(':=', $line);
            if (isset($variable[1]) && strcasecmp(strtolower($variable[0]), $param)===0){
                $stringWrite.="{$param}:={$newValue}\n";    
                continue;
            }
            $stringWrite.="{$line}\n";
        }
        if ($readedFile!=$stringWrite && !is_null($stringWrite)){
            file_put_contents($file, $stringWrite);
        }
    }

    protected static function readFile($file,$param){
        if (!file_exists($file)){
            return false;
        }
        $array=explode("\n",file_get_contents($file));
        foreach ($array as $value) {
            $variable=explode(':=', $value);
            if (!isset($variable[1])){
                continue;
            }
            if (strcasecmp(strtolower($variable[0]), strtolower($param))===0){
                return trim($variable[1]);                
            }
        }
        return null;
    }

    public function set($var,$value){
        if (!isset($this->$var)){
            return false;
        }
        $this->$var=intval($value);
        return true;
    }

    public function get($var){
        if (!isset($this->$var)){
            return null;
        }
        return $this->$var;
    }
}