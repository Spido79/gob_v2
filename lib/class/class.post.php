<?php
/******************************************************/
/* CLASS COMMON FOR POST                              */
/******************************************************/
namespace POST;

class Set{
	protected $POST = array();
	protected $FILES = array();

	protected $url_Referer = array();
	protected $page = null;

	protected $hash=null; 
	function __construct() {
		if (!array_key_exists('P',$_GET)){
			return;
		}
		$this->hash=$_GET['P'];
		$this->url_Referer = parse_url($_SERVER['SCRIPT_NAME']);
		$this->page = basename($this->url_Referer['path']);
		if (isset($_SESSION['POST'][$this->hash][$this->page])) { //c'e' il post di questa pagina
			foreach ($_SESSION['POST'][$this->hash][$this->page] as $key => $value) {
				$this->POST[$this->hash][$key]=$value;
			}
		}
		if (isset($_SESSION['FILES'][$this->hash][$this->page])) { //c'e' il file di questa pagina
			foreach ($_SESSION['FILES'][$this->hash][$this->page] as $key => $value) {
				$this->FILES[$this->hash][$key]=$value;
			}
		}
	}
	
	public function get($var){
		if (isset($this->POST[$this->hash][$var])){
			return $this->POST[$this->hash][$var];
		} 
		return null;
	}

	public function getFile($var){
		if (isset($this->FILES[$this->hash][$var]) && isset($this->FILES[$this->hash][$var]['tmp_name'])){
			return $this->FILES[$this->hash][$var]['tmp_name'];
		} 
		return null;
	}

	public function VerifyPostData($fields){
		if (!is_array($fields)){
			return false;
		}
		foreach ($fields as $item ) {
			if ($this->hash==''|| !isset($this->POST[$this->hash])){
				return false;
			}
			if (!array_key_exists($item,$this->POST[$this->hash]) && !array_key_exists(strtolower($item),$this->POST[$this->hash]) ){
				return false;
			}
		}
		return true;
	}


	public function VerifyGetData($fields){
		if (!is_array($fields)){
			return false;
		}
		foreach ($fields as $item ) {
			if (!array_key_exists($item,$_GET) && !array_key_exists(strtolower($item),$_GET) ){
				return false;
			}
		}
		return true;
	}

	public function __destruct() {
		if (isset($_SESSION['POST'][$this->hash][$this->page])) { //c'e' il post di questa pagina
			unset($_SESSION['POST'][$this->hash][$this->page]);
		}

		if (isset($_SESSION['FILES'][$this->hash][$this->page])) { //c'e' il post di questa pagina
			foreach($_SESSION['FILES'][$this->hash][$this->page] as $field => $value){
				@unlink($this->getFile($field));
			}

			unset($_SESSION['FILES'][$this->hash][$this->page]);

		}
	}
}
