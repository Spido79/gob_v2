<?php

namespace IMG;

class Logo { //class per eseguire gli output css
	
	public static function png($img, $path='/img/'){
		$image= APP_URL."/img/logo_circle.png?".\APP\Parameter::serial();
		if (file_exists(APP_PATH_WWW."/{$path}/{$img}.png")){
			$image= APP_URL."/{$path}/{$img}.png?".\APP\Parameter::serial();	
		} else {
			//\LOGS\Log::write('IMG PNG not exist', $path.'/'.$img.'.png', false);
		}
		return $image;
	}

	public static function jpg($img, $path='img'){
		$image= APP_URL."/img/logo_circle.jpg?".\APP\Parameter::serial();
		if (file_exists(APP_PATH_WWW."/{$path}/{$img}.jpg")){
			$image= APP_URL."/{$path}/{$img}.jpg?".\APP\Parameter::serial();	
		} else {
			//\LOGS\Log::write('IMG JPG not exist', $path.'/'.$img.'.jpg', false);
		}
		return $image;
	}
}
