<?php
/**
 * Created by PhpStorm.
 * User: Ricky
 * Date: 27/12/15
 * Time: 13:12
 */
namespace BGG;
class Games{
    CONST Remote = 'http://www.boardgamegeek.com/xmlapi/boardgame/';
    CONST ImportMax=90000;
    CONST ImportMaxSql=30;
    CONST StepError=100;

    public static function downloadSpec($id){
        $params=\BGG\Games::getSpec($id);
        $Sql='DELETE FROM tbl_mag_ana WHERE id_bgg='.intval($id).';';
        \SQL::Execute($Sql);
        $Sql='INSERT INTO tbl_mag_ana (id_bgg, title, thumbnail, all_title) VALUES ';
        if($params['id_bgg']!=0){
            $Sql.='('.$params['id_bgg'].', ';
            if (is_null($params['title'])){
                $Sql.='null, ';
            } else {
                $Sql.=\Sql::Escape($params['title']).', ';
            }

            if (is_null($params['thumbnail'])){
                $Sql.='null, ';
            } else {
                $Sql.=\Sql::Escape($params['thumbnail']).', ';
            }
            if (is_null($params['all_title'])){
                $Sql.='null';
            } else {
                $Sql.=\Sql::Escape($params['all_title']);
            }
            $Sql.=')';
            $Sql.=';';
            \SQL::Execute($Sql);
            echo "Id: {$id} Imported\n";
        } else{
            echo "Skipped ID: {$id}.\n";
        }
    }

    public static function viewSpec($id){
        $params=\BGG\Games::getSpec($id, true);
        $xml = htmlentities(file_get_contents(\BGG\Games::Remote.$id.'?stats=1'),ENT_XML1);
        $xml= preg_replace('/\t/', " ", $xml);
        $xml= preg_replace('/(\s*\n)/', "\n", $xml);
        $error=\BGG\Games::updateSpec($params);
        return array($error,preg_replace('/\n\n/', "", $xml));
    }

    protected static function updateSpec($params){
        $Sql='REPLACE INTO tbl_mag_ana (id_bgg, title, thumbnail, all_title) VALUES ';
        if($params['id_bgg']!=0){
            $Sql.='('.$params['id_bgg'].', ';
            if (is_null($params['title'])){
                $Sql.='null, ';
            } else {
                $Sql.=\Sql::Escape($params['title']).', ';
            }
            if (is_null($params['thumbnail'])){
                $Sql.='null, ';
            } else {
                $Sql.=\Sql::Escape($params['thumbnail']).', ';
            }

            if (is_null($params['all_title'])){
                $Sql.='null';
            } else {
                $Sql.=\Sql::Escape($params['all_title']);
            }
            $Sql.=');';
            \SQL::Execute($Sql);
            return true;
        } else {
            return false;
        }
    }
    protected static function getSpec($id, $override=false){
        $xml = file_get_contents(\BGG\Games::Remote.$id.'?stats=1');
        @$temp_xml = simplexml_load_string($xml);
        $params=array(
            'title' => null,
            'id_bgg' => 0,
            'thumbnail' => null,
            'all_title' => null,
        );
        if ($temp_xml!=null){
            if (!$override && isset($temp_xml->boardgame->boardgameexpansion) && isset($temp_xml->boardgame->boardgameexpansion['inbound']) && $temp_xml->boardgame->boardgameexpansion['inbound']=='true'){
                return $params;
            }
            if (isset($temp_xml->statistics->ratings->ranks)){
                foreach ($temp_xml->statistics->ratings->ranks->rank as $item) {
                    if (!$override && isset($item['type']) && $item['type']=='subtype' && intval($item['id'])!=1){
                        return $params;
                    }
                }
            }

            if (isset($temp_xml->boardgame[0]) && isset($temp_xml->boardgame[0]['objectid'])){
                $params['id_bgg']=intval($temp_xml->boardgame[0]['objectid']);
            }

            if (isset($temp_xml->boardgame->thumbnail)){
                $params['thumbnail']=$temp_xml->boardgame->thumbnail;
            }
            $count=0;
            foreach ($temp_xml->boardgame->name as $item ) {
                if (isset($item['primary']) && $item['primary']=='true'){
                    $params['title']=$item;
                }

                if (is_null($params['thumbnail'])){
                    $params['thumbnail']=$item;
                }
                $params['all_title'].=$count >0? '|#|'.$item:$item;
                $count++;
            }
        }
        return $params;
    }

    public static function download($id=0){
        $Sql='SELECT MAX(id_bgg) as max_id FROM tbl_mag_ana;';
        \SQL::Execute($Sql);
        $maxId=intval(\SQL::$Result[0]['max_id'])+1;

        $delta=0;
        $SqlImported=0;
        $error=0;
        $Sql='INSERT INTO tbl_mag_ana (id_bgg, title, thumbnail, all_title) VALUES ';
        while ($delta<\BGG\Games::ImportMax){
            $params=\BGG\Games::getSpec($maxId);
            if($params['id_bgg']!=0){
                $error=0;
                $Sql.=$SqlImported>0?', ':'';
                $Sql.='('.$params['id_bgg'].', ';
                if (is_null($params['title'])){
                    $Sql.='null, ';
                } else {
                    $Sql.=\Sql::Escape($params['title']).', ';
                }

                if (is_null($params['thumbnail'])){
                    $Sql.='null, ';
                } else {
                    $Sql.=\Sql::Escape($params['thumbnail']).', ';
                }
                if (is_null($params['all_title'])){
                    $Sql.='null';
                } else {
                    $Sql.=\Sql::Escape($params['all_title']);
                }
                $Sql.=')';
                $delta++;
                $SqlImported++;
            } else{
                $error++;
                if ($error>=\BGG\Games::StepError){
                    if ($SqlImported>0){
                        //import last remained
                        $Sql.=';';
                        \SQL::Execute($Sql);
                    }
                    echo "Skipped {$error} ID: probably finished.... last ID: {$maxId}.\n";
                    exit;
                }
            }
            if ($SqlImported>=\BGG\Games::ImportMaxSql){
                $Sql.=';';
                \SQL::Execute($Sql);
                $Sql='INSERT INTO tbl_mag_ana (id_bgg, title, thumbnail, all_title) VALUES ';
                $SqlImported=0;
            }
            echo "Id: {$maxId} CHECKED - Imported: {$delta} games - error {$error}\n";
            if ($error>=\BGG\Games::StepError){
                if ($SqlImported>0){
                        //import last remained
                    $Sql.=';';
                    \SQL::Execute($Sql);
                }
                echo "Skipped {$error} ID: probably finished.... last ID: {$maxId}.\n";
                exit;
            }
            $maxId++;
        }
        $Sql.=';';
        \SQL::Execute($Sql);
    }
}
