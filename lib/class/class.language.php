<?php
/**********************************************
# Riccardo Scotti - 22/07/2015
# @link : https://www.facebook.com/spido79
# @PUBLIC function: word()
# @PUBLIC function: listLanguages()
 **********************************************/
namespace Dictonary;

class Translation{
    public static $language='it';
    public static $file=__dir__.'/../../language/translation.xml';
    public static $xml_file=false;

    public static function word($wordToTranslate, $pageName = null, $toLang = null){
        /**********************************************
        # Riccardo Scotti - 22/07/2015
        # @param : $wordToTranslate = XML word to translate in specific section
        # @param (optional) : $pageName = section where get translation in XML
        # @param (optional) : $toLang = translate word NOT in default setted language
        if the language passed not exist, get Default Language
         **********************************************/
        @\Dictonary\Translation::$xml_file = simplexml_load_file(\Dictonary\Translation::$file);
        if (\Dictonary\Translation::$xml_file === false) {
            echo 'ERROR: File Dictonary XML not valid! ';
            return;
        }

        if (\Dictonary\Translation::$xml_file === false) {
            return 'Not a Valid Dictonary XML was loaded. ';
        }

        if ($pageName == null){
            $currentFile=debug_backtrace()[0]['file'];
        } else {
            $currentFile = $pageName;
        }

        $parts = Explode('\\', $currentFile);

        if (count($parts)>1){
            $page_nameTemp= explode('.',$parts[count($parts) - 1]);
        } else {
            $parts = Explode('/', $currentFile);
            $page_nameTemp = explode('.',$parts[count($parts) - 1]);
        }

        $page_name = $page_nameTemp[0];

        if ($toLang !== null && $toLang == \Dictonary\Translation::$verify_language($toLang)) {
            $traductionLang = $toLang;
        } else {
            $traductionLang = \Dictonary\Translation::$language;
        }

        $words = \Dictonary\Translation::$xml_file->xpath("//language[@translation='$traductionLang']/page[@name='$page_name']/phrase[@variable='$wordToTranslate']");
        if (empty($words[0])){
            return "[!Warning: $traductionLang-$wordToTranslate@$page_name]";
        } else {
            return $words[0];
        }
    }

    public static function listLanguages($sort = 1) {
        /**********************************************
        # Riccardo Scotti - 22/07/2015
        # @param (optional) : $sort = 0-1(default) Sorting array language found in xml
        # @return : language found in XML
         **********************************************/
        if (\Dictonary\Translation::$xml_file === false) {
            echo 'Not a Valid Dictonary XML was loaded. ';
            return array();
        }

        $tempLanguage = array();
        foreach (\Dictonary\Translation::$xml_file->language as $translation){
            $tempLanguage[]=strval($translation->attributes()->translation);
        }

        if ($sort == 1) {
            sort($tempLanguage);
        }

        return $tempLanguage;
    }

    public static function verify_language($speakerLanguage) {
        /**********************************************
        # Riccardo Scotti - 22/07/2015
        # @param : $speakerLanguage = language format (used in XML)
        # @return : language found in XML
         **********************************************/
        $firstLanguage = null;

        foreach (\Dictonary\Translation::$languages(0) as $value) {
            if ($firstLanguage == null) {
                $firstLanguage = $value;
            }

            if ($value == $speakerLanguage) {
                return $value;
                break;
            }
        }
        return $firstLanguage;
    }
}