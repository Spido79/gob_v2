<?php
/**
 * Created by PhpStorm.
 * User: riccardo
 * Date: 30/11/2015
 * Time: 11:23
 */

namespace DOMAIN;

class Avaible{
    /*
     * DOOMINI ABILITATI al POST REMOTO
     * se metto il parametro {all} vuol dire tutti
     */

    public static function Verify_Enabled(){
        $domains=array(
            $_SERVER['SERVER_NAME'],
            '127.0.0.1',
        );
        $url_reqForm = parse_url($_SERVER['HTTP_REFERER']);
        foreach ($domains as  $item) {
            if (in_array( $url_reqForm['host'], $domains) || $domains=='{all}') {
                return true;
            }
        }
        return false;
    }

}