<?php

namespace PDF;

class Tessere {
	//Seleziona il tipo di tessera, 
	//ritorna il pdf da stampare
	private static $params=array(
		'author'			=> 'La Tana dei Goblin',
		'title'				=> 'Tessera goblin',
		'subject'			=> 'Tessera goblin',
		'keywords'			=> 'La Tana dei Goblin',
		'tessere_starting'	=> -5,
		'tessere_pagina'	=> 5,
		'tessere_jump'		=> 53,
	);

	public static $filename	='tessere';

	protected static $pdf;
	
	protected static $startx;

	public static function stream($params){
		/*
		$params= array(
			'elements' => array(),
			'dimension' => 1..3
		);
		*/
		self::$pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		self::$pdf->SetCreator(PDF_CREATOR);
		self::$pdf->SetAuthor(\PDF\Tessere::$params['author']);
		self::$pdf->SetTitle(\PDF\Tessere::$params['title']);
		self::$pdf->SetSubject(\PDF\Tessere::$params['subject']);
		self::$pdf->SetKeywords(\PDF\Tessere::$params['keywords']);

		self::create_page();

		self::$startx=\PDF\Tessere::$params['tessere_starting'];
		$tessere=0;
		foreach ($params['elements'] as $item) {
			if ($tessere>=\PDF\Tessere::$params['tessere_pagina']){
				self::$startx=\PDF\Tessere::$params['tessere_starting'];
				$tessere=0;
				self::$pdf->AddPage();
			} else if ($tessere>0){
				self::$startx=$tessere*\PDF\Tessere::$params['tessere_jump'];
			}

			\PDF\Tessere::draw($item, $params['dimension']);
			$tessere++;
		}
		return self::$pdf->Output(\PDF\Tessere::$filename.'.pdf', 'S');
	}
	
	protected static function draw($item, $dimension){
		switch ($dimension) {
			case 3: //medium
				$size=array(
					'font_nick' => 30,
					'y_nick' 	=> 100,
					'x_nick' 	=> 45,
					'picture' 	=> array(
							'x' => 154,
							'y'	=> 49
					),
					'y_common' 	=> 37,
					'font_name' => 10,
					'x_name' 	=> 25.1,
					'x_nick2'	=> 32.2,
					'x_card'	=> 39.3,
					'x_data'	=> 45.9,
					'x_tana'	=> 53.2,
					'font_code' => 6,
					'x_code'	=> 60.1,
					'dimension'	=> 'Dimensione - 7.7x4.9',
				);
				break;

			case 2: //big
				$size=array(
					'font_nick' => 32,
					'y_nick' 	=> 113,
					'x_nick' 	=> 48,
					'picture' 	=> array(
							'x' => 170,
							'y'	=> 52
					),
					'y_common' 	=> 38,
					'font_name' => 11,
					'x_name' 	=> 25.7,
					'x_nick2'	=> 33,
					'x_card'	=> 40.5,
					'x_data'	=> 48.1,
					'x_tana'	=> 55.6,
					'font_code' => 9,
					'x_code'	=> 63.2,
					'dimension'	=> 'Dimensione big: 8.5x5.2',
				);
				break;
			
			default: //standard
				$size=array(
					'font_nick' => 30,
					'y_nick' 	=> 99,
					'x_nick' 	=> 45,
					'picture' 	=> array(
							'x' => 152,
							'y'	=> 48
					),
					'y_common' 	=> 37,
					'font_name' => 10,
					'x_name' 	=> 25,
					'x_nick2'	=> 32,
					'x_card'	=> 39,
					'x_data'	=> 45.8,
					'x_tana'	=> 52.5,
					'font_code' => 6,
					'x_code'	=> 60,
					'dimension'	=> 'Dimensione standard: 7.2x4.5',
				);
				break;
		}
		self::$pdf->SetTextColor(0,102,0);
		self::$pdf->SetFont('jokerman', 'BI', $size['font_nick']);

		self::$pdf->Image(APP_PATH.'/lib/resources/Tesserino-TdG.png', 20, self::$startx+20, $size['picture']['x'], $size['picture']['y'], '', '', '', false, 300);

		self::$pdf->SetXY($size['y_nick'], self::$startx+$size['x_nick']);

		self::$pdf->Cell(70,18, $item['Nick'], 0,0,'C',false, '',1,false,'T','C');

		

		if (self::$startx==\PDF\Tessere::$params['tessere_starting']){
			self::$pdf->SetTextColor(100,100,100);
			self::$pdf->SetFont('helvetica', 'B', 7);
			self::$pdf->SetXY(20, 6);
			self::$pdf->Cell(63,7.5, $size['dimension'], 0,0,'L',false, '',1,false,'T','C');
		}

		self::$pdf->SetTextColor(0,0,0);
		self::$pdf->SetFont('helvetica', 'B', $size['font_name']);
		self::$pdf->SetXY($size['y_common'],self::$startx+$size['x_name']);
		self::$pdf->Cell(63,7.5, $item['Nome'].' '. $item['Cognome'], 0,0,'L',false, '',1,false,'T','C');

		self::$pdf->SetXY($size['y_common'],self::$startx+$size['x_nick2']);
		self::$pdf->Cell(63,7.5, $item['Nick'], 0,0,'L',false, '',1,false,'T','C');

		self::$pdf->SetXY($size['y_common'],self::$startx+$size['x_card']);
		$tessera=str_pad( $item['Tessera'], 5,'0',STR_PAD_LEFT);
		self::$pdf->Cell(63,7.5,$tessera, 0,0,'L',false, '',1,false,'T','C');

		self::$pdf->SetXY($size['y_common'],self::$startx+$size['x_data']);
		$dateAsso=new \DateTime( $item['Data']);
		self::$pdf->Cell(63,7.5,$dateAsso->format('d/m/Y'), 0,0,'L',false, '',1,false,'T','C');

		self::$pdf->SetXY($size['y_common'],self::$startx+$size['x_tana']);
		if (strlen($item['Tana'])>41){
			self::$pdf->SetFont('helvetica', 'B', 8); 
			self::$pdf->MultiCell(63,7.5,$item['Tana'], 0,'L');
		} elseif (strlen($item['Tana'])>35){
			self::$pdf->SetFont('helvetica', 'B', 8);
			self::$pdf->Cell(63,7.5,$item['Tana'], 0,0,'L',false, '',1,false,'T','C');
		} else {
			self::$pdf->SetFont('helvetica', 'B', 10);
			self::$pdf->Cell(63,7.5,$item['Tana'], 0,0,'L',false, '',1,false,'T','C');
		}

		self::$pdf->SetFont('helvetica', '', $size['font_code']);
		self::$pdf->SetXY($size['y_common'],self::$startx+$size['x_code']);
		self::$pdf->Cell(63,7.5, $item['Codice'], 0,0,'L',false, '',1,false,'T','C');
		return true;
	}
	
	protected static function create_page(){
		self::$pdf->setPrintHeader(false);
		self::$pdf->setPrintFooter(false);
		self::$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		self::$pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
		self::$pdf->SetAutoPageBreak(TRUE, 0);
		self::$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		self::$pdf->AddPage();
		return true;
	}

	public static function set($var, $value){
		if (self::$$var){
			self::$$var=$value;
			return true;
		}
		return false;
	}

}