<?php
namespace DATESPACE;

class Convert {
	protected static $month=array('Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre');

	protected static $monthShort=array('Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic');

	public static function dmyTOymd(&$date){
		if (substr_count($date, '/')!=2){
			return false;
		}
		$exploitDate=explode('/', $date);
		$date=$exploitDate[2].'-'.$exploitDate[1].'-'.$exploitDate[0];
		return true;
	}

	public static function dFY($date){
		$date=new \DateTime($date);
		return $date->format('d').' '.\DATESPACE\Convert::$month[$date->format('m')-1].' '.$date->format('Y');
	}

	public static function dM($date){
		$date=new \DateTime($date);
		return $date->format('d').' '.\DATESPACE\Convert::$monthShort[$date->format('m')-1];
	}

	public static function dMy($date){
		$date=new \DateTime($date);
		return $date->format('d').' '.\DATESPACE\Convert::$month[$date->format('m')-1].' '.$date->format('Y');
	}
}