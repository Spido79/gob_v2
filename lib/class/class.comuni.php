<?php
namespace COMUNI;

class Istat {
	protected static $link='https://raw.githubusercontent.com/matteocontrini/comuni-json/master/comuni.json';

	public static function listComuni($cap){
		$Sql='SELECT Comune, Provincia FROM tbl_comuni WHERE FIND_IN_SET('.\SQL::Escape($cap).', CAP);';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function getAll($params=array()) {
		/*
			$params=array(
				'map_fields'	=> array(
					'Comune'		=>'',
					'CAP'			=>'',
					'Provincia'		=>'',
					),
				'order'			=> 1,
				'distinct'		=> false,
			);
		*/
		$arrayExport = array(
			'Comune'		=>'',
			'CAP'			=>'',
			'Provincia'		=>'',
		);
		if (isset($params['map_fields']) && is_array($params['map_fields']) && count($params['map_fields'])>0){
			$arrayExport = $params['map_fields'];
		}

		$Sql='SELECT ';

		if (isset($params['distinct']) && $params['distinct']==true){
			$Sql.=' DISTINCT ';
		} 
		$count=0;
		foreach ($arrayExport as $key => $value) {
			$Sql.= $count>0 ? ', '.$key : $key;
			if (trim($value)!=''){
				$Sql.= " AS ".$value;
			}
			$count++;
		}

		$Sql.=' FROM tbl_comuni ';
		if (isset($params['order']) && intval($params['order'])>0 ){
			$Sql.=' ORDER BY '.intval($params['order']);
		}
		$Sql.=';';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function countComuni(){
		$Sql='SELECT count(id_comune) as counter FROM tbl_comuni;';
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return \SQL::$Result[0]['counter'];	
		}
		return 0;
	}

	public static function updateComuni(){
		$json = file_get_contents(\COMUNI\Istat::$link); 
		$data = json_decode($json);
		$Sql='TRUNCATE TABLE tbl_comuni;';
		\SQL::Execute($Sql);
		foreach ($data as $key => $value) {
			$sigle='xx';
			if ($value->sigla){
				$sigle=$value->sigla;
			}

			if (is_array($value->cap)){
				$caps=implode(',', $value->cap);
			} else {
				$caps=$value->cap;	
			}


			$Sql='INSERT INTO tbl_comuni (Istat, Comune, Provincia, Cap, CodFisco) VALUES(
			'.intval($value->codice).', 
			'.\SQL::Escape($value->nome).', 
			'.\SQL::Escape($sigle).', 
			'.\SQL::Escape($caps).', 
			'.\SQL::Escape($value->codiceCatastale).'
			);';
			\SQL::Execute($Sql);
			$aggiunti[]=$value->nome;
		}
		$Sql='INSERT INTO tbl_comuni (Istat, Comune, Provincia, Cap, CodFisco) VALUES(
			0, "-Estero-", "EE", "99999","99999");';
		\SQL::Execute($Sql);

		return (count($data)+1);
	}
}