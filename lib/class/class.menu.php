<?php

NAMESPACE MENU;

class Primary{
	const PARAM_FOLDER =".config";
    const PARAM_FILE =".menu";

	public static $Menu = array();
	
	function __construct(){
        if (!file_exists(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE)){
            echo "SERVER STOP\nERROR: file .menu not found!\n\n";
			exit;
        }
        self::$Menu=json_decode(file_get_contents(__dir__.'/../../'.self::PARAM_FOLDER.'/'.self::PARAM_FILE), true);
        if (self::$Menu==null){
        	echo "SERVER STOP\nERROR: file .menu is not a valid JSON code!\n\n";
			exit;	
        }

	}
	public static function searchLabel($page, $array = array()){
		foreach ($array as $key => $value) {
			if ($key==$page){
				return $value['label'];
			}
			if (isset($value['children']) && count($value['children'])>0){
				$itemReturned=\MENU\Primary::searchLabel($page, $value['children']);
				if (!is_null($itemReturned)){
					return $itemReturned;
				}
			}
		}

		return null;
	}
	
	public static function isActive($keyName, $children=array()){
		//echo \HTML\PAGE::$paramPassed['path'].'  --- '. $key.'<br><br>';
		if (strcasecmp(\HTML\PAGE::$paramPassed['path'], $keyName)==0){
			return true;
		}
		
		foreach ($children as $key => $value) {
			if (isset($value['children']) && count($value['children'])>0){
				$active=\MENU\Primary::isActive($key, $value['children']);	
			} else {
				$active=\MENU\Primary::isActive($key);	
			}
			if ($active){
				return true;
			}
		}
		return false;
	}

	public static function drawMenu($array = array(), $second=0){
		$myGroup= new \USERS\Groups(\USERS\Identify::UserGROUP());
		$lineWrite=null;
		foreach ($array as $key => $value) {
			

			//verifico gruppo
			$see=0;
			//Mod V2. lo vedo se ho la pagina settata nei permessi, se no non vedo nulla.
			//vedo se il mio gruppo ha questa pagina:
			
			if (\USERS\Identify::UserGROUP()!=0){
				foreach ($myGroup->pagesGroup(\USERS\Identify::UserGROUP()) as $pageGroup) {
					if ($pageGroup==$key){
						$see=1;
						break;
					}
				}
				
			}

			if (isset($value['children']) && count($value['children'])>0){
				$active=\MENU\Primary::isActive($key, $value['children']);	
			} else {
				if ($see==0){
					continue;
				}
				$active=\MENU\Primary::isActive($key);
			}
			

			$links=null;
			$target=null;
			$children=null;
			$dataHash=null;
			if ($value['link']!=''){
				$links=' href="'.$value['link'].'" ';
				if ($value['target']==''){	
					$dataHash=' data-hash="'.$value['link'].'" data-icon="'.$value['icon'].'" ';
				}
			}
			if ($value['target']!=''){
				$target=' target="'.$value['target'].'" ';
			}

			if (isset($value['children']) && count($value['children'])>0){
				$children='<span class="fa arrow"></span>';
			}

			if ($second==0){
				$voice="<span class='nav-label'>{$value['label']}</span>";
			} else {
				$voice=$value['label'];
			}
			
			if ($active){
				$temp_echo='<li class="active" '.$dataHash.'>';
			} else {
				$temp_echo="<li {$dataHash}>";
			}
			$temp_echo.= "<a {$links} {$target}>
				<i class='{$value['icon']}'></i>
				{$voice}
				{$children}
			</a>";

			$writedSomethin=null;
			$temp_echo_post=null;
			if (isset($value['children']) && count($value['children'])>0){
				if ($active){
					$temp_echo.='<ul class="nav nav-second-level">';
				} else {
					$temp_echo.='<ul class="nav nav-second-level collapse">';
				}
				$writedSomethin=\MENU\Primary::drawMenu($value['children'], 1);
				$temp_echo_post= '</ul>';
			}
			$temp_echo_post.= '</li>';
			if ($see==1 || $writedSomethin!=''){
				$lineWrite.=$temp_echo.$writedSomethin.$temp_echo_post;
			}
			
		}
		return $lineWrite;
	}
}