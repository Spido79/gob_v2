<?php
namespace TIMESPACE;

class Convert {
	protected $microtime=null;
	function __construct($unix_timestamp){
		$this->microtime=floatval($unix_timestamp);
	}

	public function addCentisecond($centisecond){
		$this->microtime+=intval($centisecond)/100;
	}

	public function getDate(){
		$dateToWrite=new \DateTime();
		$dateToWrite->setTimestamp(floor($this->microtime));
		$centisecond=floor(($this->microtime-floor($this->microtime))*1000);
		return $dateToWrite->format("Y-m-d H:i:s").'.'.$centisecond;
	}
}