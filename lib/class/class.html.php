<?php

NAMESPACE HTML;

class Page{
	const File_Home = 'home';
	const File_Login = 'login';
	const Folder='/pages/';
	const FolderJS='/js/php/';

	function __construct (){
		\HTML\Page::Actual();
	}
	
	protected static $pageSpecial=array( //pagine con bg-green e senza NAV e Special parts
		'login', 'resetPWD', 
		);

	public static $paramPassed=array(
		'created'	=>0,
		'verified'	=>0,
		'permitted'	=>0, //can be -1,0,1: 1ok, 0 404, -1 GO TO LOGIN
		'path'		=> null,
		'argv'		=> array(),
		);

	public static function ListPage_Phisical(){
		$files= scandir(APP_PATH.\HTML\Page::Folder);
		$array=array();
		foreach ($files as $file) {
			if (in_array($file,array(".",".."))){
				continue;
			}
			if (is_dir(APP_PATH.\HTML\Page::Folder.$file)){
				continue;	
			}
			$array[]=str_replace('.php', '', $file);
		}
		return $array;
	}

	public static function SpecialPage(){
		if (!isset(\HTML\PAGE::$paramPassed['path'])){
			return false;
		}

		if (in_array(\HTML\PAGE::$paramPassed['path'], \HTML\Page::$pageSpecial)){
			return true;
		}
		
		return false;
	}	
	public static function getParameter(){
		if (!isset(\HTML\Page::$paramPassed['argv'])){
			return array();
		}
		return \HTML\Page::$paramPassed['argv'];
	}

	public static function Publish($page){
		//header:

		if (file_exists((__DIR__.'/../../themes/'.strtolower($page).'.php')) ){
			require_once (__DIR__.'/../../themes/'.strtolower($page).'.php');
		}
		return null;
	}

	public static function Page($page){
		if (file_exists((__DIR__.'/../../pages/'.strtolower($page).'.php')) ){
			require_once (__DIR__.'/../../pages/'.strtolower($page).'.php');
		}
		return null;
	}

	public static function Page_404(){
		@ob_end_clean();
		@header('HTTP/1.0 404 Not Found');
		if (file_exists(__DIR__.'/../../errors/404.php')){
			include_once(__DIR__.'/../../errors/404.php');
		} else {
			echo "<h1>Error 404 Not Found</h1>";
			echo "The page that you have requested could not be found.";
		}
		exit;
	}
	
	public static function ActualJS(){
		if (file_exists(APP_PATH_WWW.\HTML\Page::FolderJS.\HTML\PAGE::$paramPassed['path'].'_js.php')) {
			return APP_PATH_WWW.\HTML\Page::FolderJS.\HTML\PAGE::$paramPassed['path'].'_js.php';
		}
		return null;
	}

	public static function Actual(){
		/*
			Identifico se, in base al mio gruppo, ho i permessi per vederla
			Ritorno la pagina che effettivamente vedo in array
        */
		if (\HTML\Page::$paramPassed['created']==0){
			\HTML\PAGE::CreateParam();
		}

		//Verifico se la pagina ESISTE IN DB, se HO I PERMESSI, se E' per tutto, se mi sbatte al login o cosoa.
		if (\HTML\Page::$paramPassed['verified']==0){
			\HTML\Page::VerifyPermissionPage();
		}

		//ora se ho permitted o il file non esiste, vado alla pagina default
		if (file_exists(APP_PATH.\HTML\Page::Folder.\HTML\PAGE::$paramPassed['path'].'.php') && \HTML\Page::$paramPassed['permitted']==1 && \HTML\Page::$paramPassed['verified']==1 ) {
			if (\USERS\Identify::UserID()>0 && \HTML\PAGE::$paramPassed['path']== \HTML\PAGE::File_Login){
					\HTML\PAGE::$paramPassed['path']=\HTML\PAGE::File_Home;
					return APP_PATH.\HTML\Page::Folder.\HTML\PAGE::File_Home.'.php';
			}
			return APP_PATH.\HTML\Page::Folder.\HTML\PAGE::$paramPassed['path'].'.php';
		} else if (\HTML\Page::$paramPassed['permitted']==-1) {
			\HTML\Page::Page_404();
		} else {
			//se sono loggato è la HOME, se no la LOGIN
			if (\USERS\Identify::UserID()==0){
				\HTML\PAGE::$paramPassed['path']=\HTML\PAGE::File_Login;
				return APP_PATH.\HTML\Page::Folder.\HTML\PAGE::File_Login.'.php';
			} else {
				\HTML\PAGE::$paramPassed['path']=\HTML\PAGE::File_Home;
				return APP_PATH.\HTML\Page::Folder.\HTML\PAGE::File_Home.'.php';	
			}
			
		}
	}
	
	public static function VerifyPermissionPage_PHP($page){
		$Sql='SELECT permission_actionLogin, permission_groups FROM tbl_permission WHERE permission_pages LIKE '.\SQL::Escape('%|#'.$page.'#|%').';';
		\SQL::Execute($Sql);
		foreach (\SQL::$Result as $item) {
			$permTemp=explode(',',$item['permission_groups']);
			foreach ($permTemp as $value) {
				if (intval($value)==\USERS\Identify::UserGROUP() || trim($value)=='{all}'){
					return true;
				}
			}
		}
		return false;
	}

	protected static function VerifyPermissionPage(){
		//prendo il gruppo dell'utente elo cerco in sql
		$arrayNotVeify=array(
			'_GHOST', 'login'
			);

		\HTML\Page::$paramPassed['verified']=1;
		if (in_array(\HTML\PAGE::$paramPassed['path'], $arrayNotVeify)){ //pagina in array e quindi sempre vera
			\HTML\Page::$paramPassed['permitted']=1;
			return;
		}
		
		if (\HTML\PAGE::$paramPassed['path']==''){
			if (\USERS\Identify::UserID()==0){
				\HTML\PAGE::$paramPassed['path']=\HTML\PAGE::File_Login;
			} else {
				\HTML\PAGE::$paramPassed['path']=\HTML\PAGE::File_Home;
			}	
		}
		
		$Sql='SELECT permission_actionLogin, permission_groups FROM tbl_permission WHERE permission_pages LIKE "%|#'.\HTML\PAGE::$paramPassed['path'].'#|%";';
		\SQL::Execute($Sql);
		$count=0;
		foreach (\SQL::$Result as $index => $item) {
			$permTemp=explode(',',$item['permission_groups']);
			if ($item['permission_actionLogin']==0){
				\HTML\Page::$paramPassed['permitted']=-1;
			}
			foreach ($permTemp as $value) {
				if (intval($value)==\USERS\Identify::UserGROUP() || trim($value)=='{all}'){
					\HTML\Page::$paramPassed['permitted']=1;
					return;
				}
			}
		}

	}

	protected static function CreateParam(){
		$pageInExecution=$_SERVER['SCRIPT_NAME'];
		if (isset($_SERVER['QUERY_STRING'])){
			//devo capire se ho "/" oppire sono "&"
			//devo esplodere sia per / che per &
			$tempRead=preg_split( '/(\/|&)/', $_SERVER['QUERY_STRING']);
			if (count($tempRead)>0){
				//explode per sotto parametri della pagina
				$count=0;
				foreach ($tempRead as $key => $value) {
					$tempSubParam=explode('=',$value);
					if (count($tempSubParam)==2){
						if (strtolower($tempSubParam[0])=='path'){
							\HTML\PAGE::$paramPassed[$tempSubParam[0]]=str_ireplace('.php', '',$tempSubParam[1]);
						} else {
							\HTML\PAGE::$paramPassed['argv'][$tempSubParam[0]]=$tempSubParam[1];
						}
					}else {
						\HTML\PAGE::$paramPassed['argv'][]=$tempSubParam[0];
					}

				}
			}
		}
		\HTML\PAGE::$paramPassed['created']=1;
	}
}