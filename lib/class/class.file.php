<?php

namespace FILES;

class Upload{

	public static function get($params=array()){
		/*
			$params=array(
				'left_join'	=> array(
						'tbl_tane'	=> array(
							'id_tana'	=> 'id_tana',
						)
					),
				'distinct'      => true,
				'map_fields'	=> array(
					'Nick'				=>'',
					'cognome_goblin'	=>'',
					'nome_goblin'		=>'',
					'id_goblin'			=>'',
					'email_goblin'		=>'',
					),
				'search'		=> '',
				'tag'			=> '',
				'list_tane'     => explode(',', \USERS\Identify::get('tane_riferimento'))),
				'order'			=> array(
					'field' 	=> '',
					'direction' => '', // asc/desc
					)
			);
		*/
		$array=array();
		
		$Sql='SELECT ';
		if (isset($params['distinct']) && $params['distinct']){
			$Sql.='DISTINCT ';
		}
		if (isset($params['map_fields']) && is_array($params['map_fields']) && count($params['map_fields'])>0){
			$count=0;
			foreach ($params['map_fields'] as $key => $value) {
				if (strpos($key, ':')>0){
					$Sql.= $count>0 ? ', '.explode(':',$key )[0].'('.explode(':',$key )[1].')' : explode(':',$key )[0].'('.explode(':',$key )[1].')';
				} else {
					$Sql.= $count>0 ? ', '.$key : $key;
				}
				if (trim($value)!=''){
					$Sql.= " AS ".$value;
				}
				$count++;
			}
		} else {

			$Sql.='id_file ';
		}

	

		$Sql.=' FROM tbl_files ';
		if (isset($params['left_join']) && is_array($params['left_join']) && count($params['left_join'])>0){
			foreach ($params['left_join'] as $key => $table) {
				$Sql.=' LEFT JOIN `'.$key.'` ON ';
				$count_join=0;
				foreach ($table as $field => $map) {
					$Sql.= $count_join>0? ' AND ': '';
					if (strpos($map, 'value:')===0){
						$Sql.= $key.'.'.$field.' = '.\SQL::Escape(explode(':',$map )[1]);
					} else {
						$Sql.= $key.'.'.$field.' = tbl_files.'.$map;	
					}
					$count_join++;
				}
			}
		}
		$temp_where=false;
		
		if (isset($params['list_tane']) && is_array($params['list_tane']) && count($params['list_tane'])>0){
			$Sql.= $temp_where ? ' AND (' : ' WHERE (';
			$count=0;
			foreach ($params['list_tane'] as $single) {
				$Sql.= $count>0? ' OR ': '';
					$Sql.=intval($single).' IN ( tbl_files.tane_riferimento )';
				$count++;
			}
			if ($count==0){
				$Sql='1=2';
			}
			$Sql.= ' OR tbl_files.tane_riferimento="ALL_TANE") ';
			$temp_where=true;
		}

		if (isset($params['tag']) && !is_null($params['tag'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			if ($params['tag']==''){
				$Sql.=' (tbl_files.tags = "" OR tbl_files.tags IS NULL) ';
			} else {
				$Sql.=' tbl_files.tags = '.\SQL::Escape($params['tag']);	
			}
			
			$temp_where=true;
		}

		if (isset($params['search']) && !is_null($params['search'])){
			$Sql.= $temp_where ? ' AND ' : ' WHERE ';
			$Sql.=' ( tbl_files.name like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' OR ';
			$Sql.=' tbl_files.tags like '.\SQL::Escape('%'.str_replace('%', '', $params['search']).'%').' ) ';
			$temp_where=true;
		}
		
		if (isset($params['order']) && is_array($params['order']) && isset($params['order']['field']) && !is_null($params['order']['field']) ) {
			$Sql.=' ORDER BY '.($params['order']['field']);
			if (isset($params['order']['direction']) && strtolower($params['order']['direction']) =='desc' ){
				$Sql.=' DESC ';
			}
		}

		$Sql.=';';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}

	public static function getSpec($id=0){
		$id=intval($id);
		if ($id<=0){
			return false;
		}

		$Sql='SELECT * FROM tbl_files WHERE id_file='.$id.';';
		\SQL::Execute($Sql);
		$result=\SQL::$Result;
		if (isset($result[0])){
			return \SQL::$Result[0];	
		}
		
		return false;
	}

	public static function update($id=0, $text=array()){
		$id=intval($id);
		if ($id<=0){
			return false;
		}
	
		if (!isset($text['description']) && !isset($text['tags'])){
			return false;
		}

		$Sql='UPDATE tbl_files SET ';

		$found=false;
		if (isset($text['description'])){
			$found=true;
			$Sql.='description= '.\SQL::Escape($text['description']);
		}

		if (isset($text['tags'])){
			$Sql.=$found? ', ': '';
			$found=true;
			$Sql.='tags= '.\SQL::Escape($text['tags']);
		}

		$Sql.=' WHERE id_file='.$id.';';
		\SQL::Execute($Sql);
		return true;
	}

	public static function del($id=0){
		$id=intval($id);
		if ($id<=0){
			return false;
		}

		$Sql='DELETE FROM tbl_files WHERE id_file='.$id.';';
		\SQL::Execute($Sql);
		return true;
	}

	public static function insert($file){
		if (file_exists($file)){
			$blob=file_get_contents($file);
			$Sql='INSERT INTO tbl_files (file, name) VALUES (
				'.\SQL::Escape($blob).', 
				'.\SQL::Escape(basename($file)).'
			);';
			\SQL::Execute($Sql);
			return true;
		}
		return false;
	}

	public static function tane($id_file, $action, $id_tana){
		$action=intval(filter_var($action, FILTER_VALIDATE_BOOLEAN));
		$id_file=intval($id_file);
		$id_tana=intval($id_tana);

		$activeTane=\GOBLINS\Tane::getActive();
		$activeTaneIds=array();
		foreach ($activeTane as $key => $value) {
			$activeTaneIds[]=$value['id_tana'];
		}

		$file=\FILES\Upload::getSpec($id_file);
		if (!$file){
			return false;
		}

		if ($id_tana==0 ){
			if ($action){
				$Sql="UPDATE tbl_files SET tane_riferimento = 'ALL_TANE' WHERE id_file= {$id_file};";
			} else {
				$Sql="UPDATE tbl_files SET tane_riferimento = NULL WHERE id_file= {$id_file};";
			}
			\SQL::Execute($Sql);
			return true;
		}
		if ($file['tane_riferimento']!='ALL_TANE'){
			$actualTane=explode(",", $file['tane_riferimento']);	
		} else {
			$actualTane=$activeTaneIds;
		}

		//clean actualTane
		foreach ($actualTane as $key => $idTa) {
			if (!in_array($idTa, $activeTaneIds)){
				$actualTane=array_diff($actualTane, array($idTa)); 
			}
		}

		if ($action == in_array($id_tana, $actualTane)){
			return false;
		}

		if ($action){
			$actualTane[]=$id_tana;
		} else {
			$actualTane=array_diff($actualTane, array($id_tana)); 
		}
		$actualTane=array_filter($actualTane);
		asort($actualTane);
 		
 		if (count($actualTane)==count($activeTaneIds)){
 			$Sql="UPDATE tbl_files SET tane_riferimento = 'ALL_TANE' WHERE id_file= {$id_file};";
 		} else {
 			$Sql="UPDATE tbl_files SET tane_riferimento = ".\SQL::Escape(implode(",",$actualTane))."
			WHERE id_file= {$id_file};";	
 		}
		
		\SQL::Execute($Sql);
		if (\SQL::$Rows==1){
			return true;
		}
		return false;
	}

	public static function getTags(){
		$Sql='SELECT DISTINCT tags FROM tbl_files WHERE tags IS NOT NULL AND tags !="" ORDER BY TAGS;';
		\SQL::Execute($Sql);
		return \SQL::$Result;
	}
}