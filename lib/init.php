<?php 
    /*
     * File INIT con definizione:
     * APP URL: link completo all'app
     * Caricamento di tutte le classi presenti in lib/class
     * Definizione variabili:
     * $userActive : dati utente navigazione
    */

    if (isset($no_error) && $no_error===true){
    	error_reporting(0);
		ini_set('display_errors', 0);
    } else {
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
	}

    ini_set('set_time_limit',600);
    ini_set('session.gc_maxlifetime',3600);
	ini_set('session.cookie_lifetime',0);
	ini_set('default_socket_timeout', 300);
    session_set_cookie_params(3600);
    
	header('Content-Type: text/html; charset=utf-8');
	mb_internal_encoding('utf-8');
    date_default_timezone_set('Europe/Rome');
	setlocale(LC_ALL, 'it_IT.utf8');
	if (session_id() == '') {
        session_start();
    }

	$directory = realpath(dirname(__FILE__));
	$document_root = realpath($_SERVER['DOCUMENT_ROOT']);
    if (isset($_SERVER['HTTP_HOST'])){
        $base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .$_SERVER['HTTP_HOST'];
        if(strpos($directory, $document_root)===0) {
            $base_url .= str_replace(DIRECTORY_SEPARATOR, '/', substr($directory, strlen($document_root)));
        }    
    } else {
        $base_url=dirname(__DIR__);
    }

	defined("APP_URL") ? null : define("APP_URL", str_replace("/lib", "", $base_url));
	defined("APP_PATH") ? null : define("APP_PATH", realpath(str_replace("/lib", "",__DIR__)));
    defined("APP_PATH_WWW") ? null : define("APP_PATH_WWW", realpath(str_replace("/lib", "",__DIR__)).'/html');

	if (isset($virtualPath)==false) {
        $virtualPath=null;
    }
    require_once(dirname(__DIR__).'/lib/function/autoload.php');

    $app=new APP\Parameter();
    $menu=new MENU\Primary();
    defined("LOG_PATH") ? null : define("LOG_PATH",$app->get('log_path') );
    defined("LOG_PRINT") ? null : define("LOG_PRINT",$app->get('log_print') );
    defined("LOG_SCREEN") ? null : define("LOG_SCREEN",$app->get('log_screen') );
    defined("SQL_NOMEHOST") ? null : define("SQL_NOMEHOST",$app->get('sql_nomehost') );
    defined("SQL_NOMEUSER") ? null : define("SQL_NOMEUSER",$app->get('sql_nomeuser') );
    defined("SQL_PASSWORD") ? null : define("SQL_PASSWORD",$app->get('sql_password') );
    defined("SQL_DATABASE_NAME") ? null : define("SQL_DATABASE_NAME",$app->get('sql_database_name') );
    defined("SQL_DATABASE_PORT") ? null : define("SQL_DATABASE_PORT",$app->get('sql_database_port') );

    LOGS\Log::start();
    /*LOGS\Log::write('Init','App initialized');
    LOGS\Log::write('LOG PATH', LOG_PATH, false);
    LOGS\Log::write('APP PATH', APP_PATH, false);
    LOGS\Log::write('APP URL' , APP_URL , false);
    */
    $MySql_Istance= new SQL();

    /*OBJ containing game status*/
    $post = new POST\Set();
    $userActive = new USERS\Identify();
    $page= new HTML\Page();