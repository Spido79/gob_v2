<?php
require_once(__dir__."/../lib/init.php");
if ( $_SERVER['REQUEST_METHOD'] == "POST" && \DOMAIN\Avaible::Verify_Enabled()==true) {
    $url_reqForm = parse_url($_SERVER['HTTP_REFERER']);
    if (isset($_POST['redirect_to'])) {
        //IF an input "hidden" with name="redirect_to" is set, i will redirect to this page
        $page_reqForm=$_POST['redirect_to'];
        $page_session=basename($page_reqForm);
    } else {
        //otherwise i will redirect to same page
        $page_reqForm= basename($url_reqForm['path']); //Pagina form inviato
        $page_session=$page_reqForm;
    }
    $randomize_number_post=md5(rand(0,100000000));
    //every $_POST is written in a SESSION VAR
    $_SESSION['POST'][$randomize_number_post][$page_session] = $_POST;
    if (isset($_FILES)){
        $target_dir =__dir__."/public/temp/";
        if (!file_exists($target_dir)){
            mkdir($target_dir, 0766, true);
        }
        foreach ($_FILES as $keyForm => $valueFile ) {
            $target_file = $target_dir . basename($valueFile["name"]);
            $_SESSION['FILES'][$randomize_number_post][$page_session][$keyForm]['tmp_name'] = $target_file;
            if (isset($valueFile['error']) && $valueFile['error'] >0 ){
                //ho un erorre
                echo 'error upload --> ';
                print_r($valueFile);
                exit;
            }
            move_uploaded_file($valueFile["tmp_name"], $target_file);
        }
    }
    
    $verPermission=str_replace(APP_URL.'/php/','', $page_reqForm);
    $verPermission=str_replace('.php','',$verPermission);
    $folderPerm=explode('/',$verPermission);
    if (count($folderPerm)>1){
        //page to check is $folderPerm[0];
        if (!\HTML\Page::VerifyPermissionPage_PHP($folderPerm[0])){
            \HTML\Page::Page_404();
        }
    }
  header('location: '.$page_reqForm.'?P='.$randomize_number_post); //finally redirect to the page
} else {
    \HTML\Page::Page_404();
}
