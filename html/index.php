<?php
/* Change output warning: false=yes true=no */
$no_error=false;
require_once(__dir__."/../lib/init.php");

/* Set VERBOSE to true, make output video, the log don't change!*/
\LOGS\Log::$Verbose=true;
\LOGS\Log::$Html=true;


\HTML\Page::Publish('Header');

if (!\HTML\Page::SpecialPage()){
	//insert NAV , MENU, CHAT, CHANGE LOG, etc etc.....
	\HTML\Page::Publish('main_start');
	\HTML\Page::Publish('navigation');
	\HTML\Page::Publish('central_start');
	\HTML\Page::Publish('central_notify');
	\HTML\Page::Publish('central_ribbon');
	\HTML\Page::Publish('central_page_start');
}
require_once(\HTML\Page::Actual());
if (!\HTML\Page::SpecialPage()){
	//chiusura main page
	\HTML\Page::Publish('central_page_stop');
	\HTML\Page::Publish('central_footer');
	\HTML\Page::Publish('central_end');
	\HTML\Page::Publish('main_end');
}

\HTML\Page::Publish('Footer');

$fileJS=\HTML\Page::ActualJS();
echo '<div id="content_central_ajax_js">';
if ($fileJS){
	require_once(\HTML\Page::ActualJS());	
}
echo '</div>';