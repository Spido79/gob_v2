<?php 
if (isset($post)==false){
    $virtualPath='.';
    require_once("../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
    \HTML\Page::Page_404();
}

if (\USERS\Identify::UserID()<=0){
	\HTML\Page::Page_404();	
}
//se login ok, clean, se no incrase.
\USERS\Identify::Disconnect();