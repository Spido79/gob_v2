<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id_stk','id_store','id_ana','id_owner','data_acquisto','origin','hidden','notes','locazione','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id=intval($post->get('id'));
$id_stk=intval($post->get('id_stk'));
$id_ana=intval($post->get('id_ana'));
$id_store=intval($post->get('id_store'));
$id_owner=intval($post->get('id_owner'));
$hidden=intval($post->get('hidden'));
$origin=trim($post->get('origin'));
$notes=trim($post->get('notes'));
$locazione=trim($post->get('locazione'));

$data_acquisto='';
if (trim($post->get('data_acquisto'))!=''){
	$data_acquisto=trim($post->get('data_acquisto'));
	\DATESPACE\Convert::dmyTOymd($data_acquisto);
}

$data_acquisto_sql=new \DateTime($data_acquisto);

if ($id !=$id_ana || $id==0 || $id_stk==0){
	\HTML\Page::Page_404();
}

$params=array(
	'id_creator' => \USERS\Identify::UserID(),
	'id_store' => $id_store,
	'id_ana' => $id_ana,
	'id_owner'=>$id_owner,
	'hidden' => $hidden,
	'origin' => $origin,
	'notes' => $notes,
	'locazione' => $locazione,
	'data_acquisto'=>$data_acquisto_sql->format('Y-m-d'),
	'data_store'=>$data_acquisto_sql->format('Y-m-d'),
);

$params['id_stock']=$id_stk;
$id_stock=\MAGAZZINO\Stock::modify($params);
\MAGAZZINO\Stock::logWrite($id_stock, 'Modificato articolo - Magazzino', 'fa-pencil', $id_owner, $id_store);
\LOGS\Log::write('STORE', 'Stock modificato id: '.$id.' per magazzino: '.$id_store, false, \USERS\Identify::UserID());
