<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['stock_destination','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_store=intval($post->get('id'));
$stock_destination=intval($post->get('stock_destination'));

if ($id_store<=10){
	\HTML\Page::Page_404();
}

\MAGAZZINO\Stores::delete($id_store, $stock_destination);
\LOGS\Log::write('STORE', 'Delete store'.$id_store." , trasnfer stock to {$stock_destination}" , false, \USERS\Identify::UserID());