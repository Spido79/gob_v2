<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Magazzini</h5>
	</div>
	<div class="ibox-content">
		<table class="table table-hover no-margins">
			<thead>
				<tr>
					<th>Magazzino</th>
					<th>Propietà</th>
					<th>N. Articoli</th>
					<th>Stato</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$params=array(
					'left_join' => 'tbl_tane ON tbl_tane.id_tana = tbl_mag_stores.id_tana'
				);
				$stores=\MAGAZZINO\Stores::getAll(array('id_store', 'active','store', 'tbl_mag_stores.id_tana','Nome') , false, $params);
				$selectMagOpt='';
				foreach ($stores as $item) {
					$countArt=\MAGAZZINO\Stores::qtaStock($item['id_store']);
					$nomeTana=str_replace('La Tana dei Goblin', '', $item['Nome']);
					if ($nomeTana==''){
						$nomeTana='<span class=\'text-warning\'>Tana Nazionale</span>';
					}
					if (!\GOBLINS\Tane::isActive($item['id_tana'])){
						$nomeTana='<span class=\'text-danger\'>'.$nomeTana.' - TANA DISATTIVATA</span>';
						if ($countArt>0){
							$countArt='<span class="text-danger animated flash infinite">'.$countArt.'</span>';
						}
					}
					if ($item['active']==1){
						$attivo='<span class="label label-primary">Attivo</span>';
						$selectMagOpt.='<option value=\''.$item['id_store'].'\'>'.$item['store'].'</option>';
					} else {
						$attivo='<span class="label label-danger">Inattivo</span>';
					}
					echo "<tr>
						<td>{$item['store']}</td>
						<td class='text-green'>{$nomeTana}</td>
						<td class='text-success text-right'>{$countArt}</td>
						<td class='text-green text-center'>{$attivo}</td>
						";
					if ($item['id_store']<=10){
						echo "<td>&nbsp;</td>";
					} else {
						echo "<td>
							<button class='btn btn-xs btn-success btn_storeEdit' value='{$item['id_store']}' data-toggle='popover' data-placement='left' data-content='Modifica magazzino' data-html='true' title=''><i class='fa fa-pencil'></i></button>
						";
					if ($item['active']==1){
						echo "<button class='btn btn-xs btn-danger btn_deaactivateStore' value='{$item['id_store']}' data-toggle='popover' data-placement='right' data-content='<nobr>Disattiva magazzino</nobr>' data-html='true' title=''><i class='fa fa-ban'></i></button>";
					} else {
						echo "<button class='btn btn-xs btn-primary btn_activateStore' value='{$item['id_store']}' data-toggle='popover' data-placement='top' data-content='<nobr>Attiva magazzino</nobr>' data-html='true' title=''><i class='fa fa-thumbs-up'></i></button>
							<button class='btn btn-xs btn-warning btn_delStore' value='{$item['id_store']}' data-toggle='popover' data-placement='right' data-content='<nobr>Cancella magazzino</nobr>' data-html='true' title=''><i class='fa fa-trash'></i></button>";
					}

					echo "</td>
						</tr>";
					}

				}
				?>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<button class='btn btn-xs btn-primary btn_storeEdit' value='0' data-toggle='popover' data-placement='left' data-content='Aggiungi un nuovo magazzino' data-html='true' title=''><i class='fa fa-plus'></i></button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {

	$('.btn_activateStore').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var id=$(this).val();
		swal({
			title: "Attiva Store",
			text: "Sei sicuro di voler attivare questo Magazzino?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				$('.magazzino_stores_box_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
				var postData = new FormData();
				postData.append('active',1);
				var returnEngine = call_ajax_page(postData,'magazzino_stores/deactivate_store',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					$('.magazzino_stores_box_right').empty().append();
					magazzino_stores_box_left();
				})
			}
		});
	});
	$('.btn_deaactivateStore').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var id=$(this).val();
		swal({
			title: "Disattiva Store",
			text: "Sei sicuro di voler disattivare questo Magazzino?<br><strong>Attenzione:</strong> lo stock presente in questo magazzino NON verrà trasferito, quindi <i>congelando</i> gli articoli presenti da parte delle affiliate che non potranno chiedere o muovere gli articoli da/verso questo magazzino.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				$('.magazzino_stores_box_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
				var postData = new FormData();
				postData.append('active',0);
				var returnEngine = call_ajax_page(postData,'magazzino_stores/deactivate_store',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					$('.magazzino_stores_box_right').empty().append();
					magazzino_stores_box_left();
				})
			}
		});
	});

	$('.btn_delStore').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var id=$(this).val();
		swal({
			title: "Elimina Store",
			text: "Sei sicuro di voler riunnovare questo Magazzino?<br><strong>Attenzione:</strong> lo stock presente in questo magazzino deve essere trasferito.<br>Seleziona un magazzino dove trasferire questa merce:<select class='form-control val_mag_destina_del'><?php echo $selectMagOpt;?></select>",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				$('.magazzino_stores_box_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
				var postData = new FormData();
				postData.append('stock_destination',$('.val_mag_destina_del').val());
				var returnEngine = call_ajax_page(postData,'magazzino_stores/del_store',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					$('.magazzino_stores_box_right').empty().append();
					magazzino_stores_box_left();
				})
			}
		});
	});

	$('.btn_storeEdit').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var id=$(this).val();
		magazzino_stores_box_right(id);
	});


	$(".btn_storeEdit").popover({ trigger: "hover focus" });
	$(".btn_delStore").popover({ trigger: "hover focus" });
	$(".btn_deaactivateStore").popover({ trigger: "hover focus" });
	$(".btn_activateStore").popover({ trigger: "hover focus" });

});
</script>
