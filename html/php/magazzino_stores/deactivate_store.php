<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['active','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_store=intval($post->get('id'));
$active=intval($post->get('active'));

if ($id_store<=10){
	\HTML\Page::Page_404();
}

\MAGAZZINO\Stores::activate($id_store, $active);
\LOGS\Log::write('STORE', 'Activate/deactivate store'.$id_store." value set:{$active}" , false, \USERS\Identify::UserID());