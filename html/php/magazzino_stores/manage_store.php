<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_store=intval($post->get('id'));
if ($id_store<=10 && $id_store>0){
	\HTML\Page::Page_404();
}

$store=\MAGAZZINO\Stores::getSpec($id_store);
$storeName=null;

$textTitle='<i class="fa fa-plus"></i> Inserisci un <span class="text-primary">NUOVO</span> magazzino';
$btnType='btn-primary';
$iconType='fa-plus';
$textType='Aggiungi';
$textSwal="<span class='text-primary'>AGGIUNGERE</span>";
$storeNotes="";
if ($id_store>0){
	$textSwal="<span class='text-success'>MODIFICARE</span>";
	$btnType='btn-success';
	$iconType='fa-pencil';
	$textType='Modifica';
	$storeName=$store['store'];
	$storeNotes=$store['notes'];
	$textTitle='<i class="fa fa-pencil"></i> Modifica il magazzino: <span class="text-success">'.$store['store'].'</span>';
}

?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5><?php echo $textTitle;?></h5>
	</div>
	<div class="ibox-content">
		<div class="form-horizontal">
			<p>Ogni magazzino deve avere una tana associata.</p>
			<div class="form-group">
				<label class="col-lg-2 control-label">Magazzino</label>
				<div class="col-lg-10">
					<input type="text" placeholder="Nome magazzino" class="txt_nameStock form-control" value="<?php echo $storeName;?>">
					<span class="help-block m-b-none"><strong>Obbligatorio:</strong> il nome del magazzino è puramente a titolo descrittivo.</span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-2 control-label">Tana di rif.</label>
				<div class="col-lg-10">
					<select class="form-control sel_tanaStock" required>
						<?php
						foreach (\GOBLINS\Tane::getActive() as $name) {
							if ($name['id_tana']==\APP\Parameter::getSpec('id_tana_nazionale')){
								$trim='Tana Nazionale';
							} else {
								$trim=str_replace('La Tana dei Goblin', '', $name['Nome']);
							}
							$checked='';
							if ($name['id_tana']==$store['id_tana']){
								$checked=' selected';
							}
							echo '<option value="'.$name['id_tana'].'" '.$checked.'>'.$trim.'</option>';
						}
						?>
					</select>
					<?php
						if (isset($store['id_tana']) && !\GOBLINS\Tane::isActive($store['id_tana'])){
							echo '<span class="help-block m-b-none text-danger"><strong>Attenzione:</strong> la tana attuale NON può essere selezionata perchè DISATTIVATA.</span>';
						}
					?>
					<span class="help-block m-b-none"><strong>Obbligatorio:</strong> il magazzino deve essere associato ad una tana.</span>
				</div>
			</div>

			<div class="form-group">
				<label class="col-lg-2 control-label">Note</label>
				<div class="col-lg-10">
					<textarea style="resize:none;" rows="5" placeholder="Note (facoltativo)" class="txt_notesStock form-control"><?php echo $storeNotes;?></textarea>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-xs 12 text-right">
				<button value="<?php echo $id_store;?>" class="btn btn_AddModStore btn-xs <?php echo $btnType;?>" data-text="<?php echo $textSwal;?>"><i class="fa <?php echo $iconType;?>"></i> <?php echo $textType;?></button>
			</div>
			<hr>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">

				<table class="table table-bordered table-hover table_stock_store_json">
					<thead>
						<th>UID</th>
						<th ><div class="td_visisble" data-placement='top' style='cursor:pointer' title='Titolo disponibile per il prestito'>Disp.</div></th>
						<th>Titolo</th>
						<th>Propietà</th>
						<th class="no-sort">Azioni</th>
						<th class="no-sort">ori</th>
						<th class="no-sort">not</th>
						<th class="no-sort">loc</th>
						<th class="no-sort">acq</th>
						<th class="no-sort">ana</th>
					</thead>
					<tbody>
					<?php
					$storeGames=\MAGAZZINO\Stores::getStock($id_store);
					foreach ($storeGames as $item) {
						$dataacq=new \DateTime($item['data_acquisto']);
						$nomeTana=str_replace('La Tana dei Goblin', '', $item['nome_tana']);
						if ($nomeTana==''){
							$nomeTana='Tana Nazionale';
						}
						if (!\GOBLINS\Tane::isActive($item['id_owner'])){
							$nomeTana='<span class=\'text-danger\'>'.$nomeTana.' - TANA DISATTIVATA</span>';
						}

						$elimPerm='';
						if (\USERS\Identify::UserGROUP()==1 || \USERS\Identify::UserGROUP()==2){
							$elimPerm="<li class=\"dropdown-divider\"></li>
							<li><a class=\"dropdown-item btn_act_store_ana_del\">Elimina</a></li>";
						}
						if ($item['hidden']==1){
							$hidden_field='<i class="fa fa-times-circle text-danger"></i>';
						} else {
							$hidden_field='<i class="fa fa-check text-green"></i>';
						}
						echo "<tr>
						<td>{$item['id_stock']}</td>
						<td data-check=\"{$item['hidden']}\">{$hidden_field}</td>
						<td data-id=\"{$item['id_ana']}\">{$item['title']}</td>
						<td data-id=\"{$item['id_owner']}\">{$nomeTana}</td>
						<td><div class=\"btn-group\">
						<button data-toggle=\"dropdown\" class=\"btn btn-sm btn-white dropdown-toggle w-100\">Azione <i class=\"fa fa-caret-down\"></i></button>
						<ul class=\"dropdown-menu\" style=\"left:-25px;\">
						<li><a class=\"dropdown-item btn_act_store_ana_mod\">Modifica</a></li>
						<li><a class=\"dropdown-item btn_act_store_ana_log\">Movimenti</a></li>
						{$elimPerm}
						</ul>
						</div>
						</td>
						<td>".htmlspecialchars($item['origin'])."</td>
						<td>".htmlspecialchars($item['notes'])."</td>
						<td>".htmlspecialchars($item['locazione'])."</td>
						<td>{$dataacq->format('d/m/Y')}</td>
						<td>".$item['id_ana']."</td>
						</tr>";
					}
					//elenco tutto lo stock di questo store

					?>
					</tbody>
				</table>
				<input type="hidden" class="countTotalStoreListGames" value="<?php echo count($storeGames)?>"/>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	<?php
	if (isset($store['id_tana']) && !\GOBLINS\Tane::isActive($store['id_tana'])){
		echo "$('.sel_tanaStock').val('');";
	}
	?>

	<?php
	if (\USERS\Identify::UserGROUP()==1 || \USERS\Identify::UserGROUP()==2){
	?>
	$('.btn_act_store_ana_del').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id =$(this).closest('tr').find('td:first-child').text();
		swal({
			title: "Elimina Stock",
			text: "Sei sicuro di voler riunnovare questo articolo dallo stock?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				var postData = new FormData();
				var returnEngine = call_ajax_page(postData,'magazzino_stores/del_stock',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					magazzino_stores_box_left();
					magazzino_stores_box_right(<?php echo $id_store;?>);
				});
			}
		});
	});
	<?php
	}
	?>

	$('.btn_act_store_ana_log').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id =$(this).closest('tr').find('td:first-child').text();
		$('button').prop('disabled',true);
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'magazzino_stores/log_stock',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.body_anaLogStore_modal').empty().append(returndata);
			$('.ana_stock_uid_st').empty().append(id);
			$('.modal_ana_store_log').modal('show');
		});

	});

	$('.btn_act_store_ana_mod').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id =$(this).closest('tr').find('td:first-child').text();
		var hidden_val =$(this).closest('tr').find('td:nth-child(2)').attr('data-check');
		var id_store =<?php echo $id_store;?>;
		var id_owner =$(this).closest('tr').find('td:nth-child(4)').attr('data-id');

		var row=dataTables_gestStockAna.row($(this).closest('tr')).data();

		var data_acq =row[8];
		var id_ana =row[9];

		$('input[data-field="id_ana"]').val(id_ana);
		$('.anaMagSt-action-addModd').empty().append('Modifica stock');
		$('.anaMagSt-title-add').empty().append(row[2]);
		//parse data

		$('select[data-field="id_store"]').val(id_store);
		$('select[data-field="id_owner"]').val(id_owner);
		$('input[data-field="data_acquisto"]').val(data_acq);
		$('input[data-field="hidden"]').prop('checked',hidden_val==1?true:false);

		$('select[data-field="origin"]').val(row[5]);

		$('textarea[data-field="notes"]').val(row[6].replace('&lt;','<').replace('&gt;','>'));
		$('input[data-field="locazione"]').val(row[7].replace('&lt;','<').replace('&gt;','>'));
		$('.btn_save_modal_anaMagStStock').attr('data-id',id);
		$('.data-field').parent().removeClass('has-error');
		$('.modal_anaMagStStock').modal('show');
	});

	var dataTables_gestStockAna= $('.table_stock_store_json').DataTable({
		"language": {
			"lengthMenu": "Mostra _MENU_ elementi",
			"zeroRecords": "Non è stato trovato nessun elemento in stock.",
			"info": "Pagina _PAGE_ di _PAGES_",
			"search": "Cerca",
			"paginate": {
				"previous": "Indietro",
				"next": "Avanti"
			},
			"infoEmpty": "Nessun elemento disponibile",
			"infoFiltered": "(filtrati da _MAX_ elementi totali)"
		},
		processing: true,
		lengthMenu: [[10, 25, $('.countTotalStoreListGames').val()], [10, 25, "Tutti"]],
		//serverSide: true,
		pageLength: 10,
		responsive: true,
		dom: '<"html5buttons"B>lTfgtip',
		buttons: [{
				extend: 'pdfHtml5',
				title: 'Elenco Stock',
				exportOptions: {
                    columns: [0,1,2,3],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			},{
				extend: 'csvHtml5',
				title: 'Elenco Stock CSV',
				fieldSeparator: ';',
				exportOptions: {
                    columns: [0,1,2,3],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			}
		],
		"order": [[ 2, "asc" ]],
		"columnDefs": [
		{
			"targets": 'no-sort',
			"orderable": false,
		},{
            "targets": [0],
            "className": 'text-right',
            "visible": true,
            "searchable": true,
            "orderable": false,
        },{
            "targets": [1],
            "className": 'text-center',
            "visible": true,
            "searchable": false,
            "orderable": false,
        },{
            "targets": [2,3],
            "className": 'text-left'
        },{
            "targets": [5,6,7,8,9],
            "visible": false,
            "searchable": false,
            "orderable": false,
        }],
	});

	$('.td_visisble').tooltip({html:true});
	$('.btn_AddModStore').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var textSwal=$(this).attr('data-text');
		var btnTemp= $(this);
		var nome=$.trim($('.txt_nameStock').val());
		var notes=$.trim($('.txt_notesStock').val());
		var tanaVal=$('.sel_tanaStock').val();
		var id=$(this).val();
		$('.txt_nameStock').parent().removeClass('has-error');
		$('.sel_tanaStock').parent().removeClass('has-error');
		error=0;
		if (nome.length<=0){
			$('.txt_nameStock').parent().addClass('has-error');
			error=1;
		}

		if (tanaVal<=0){
			$('.sel_tanaStock').parent().addClass('has-error');
			error=1;
		}

		if (error==1){
			return false;
		}
		swal({
			title: "Gestione magazzino",
			text: "Sei sicuro di voler "+textSwal+" il magazzino <strong>"+nome+"</strong>?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ok",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('nome',nome);
				postData.append('notes',notes);
				postData.append('tana',tanaVal);

				var returnEngine = call_ajax_page(postData,'magazzino_stores/save_store',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					magazzino_stores_box_left();
					$('.magazzino_stores_box_right').empty();
				})
			}
		});
	});

});
</script>