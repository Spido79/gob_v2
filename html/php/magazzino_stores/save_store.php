<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['tana','notes','nome','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_store=intval($post->get('id'));
$tana=intval($post->get('tana'));
$nome=trim($post->get('nome'));
$notes=trim($post->get('notes'));

\MAGAZZINO\Stores::updateStore($id_store, $nome, $notes, $tana);
\LOGS\Log::write('STORE', 'save_store for store '.$id_store." NAME {$nome}: ".$notes , false, \USERS\Identify::UserID());
