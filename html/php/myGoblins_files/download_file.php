<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
//leggi file
$id=intval($post->get('id'));
$file=\FILES\Upload::getSpec($id);

if (!$file){
	\HTML\Page::Page_404();	
}

if ($file['tane_riferimento']!='ALL_TANE'){
	$myTane= explode(',', \USERS\Identify::get('tane_riferimento'));
	$taneFile=explode(',', $file['tane_riferimento']);
	$found=false;
	foreach ($myTane as $myTana) {
		if (in_array($myTana, $taneFile)){
			$found=true;
			break;
		}
	}

	if (!$found){
		\HTML\Page::Page_404();	
	}
}
$explo=explode('.', $file['name']);
$exten=array_pop($explo);
$arrayRet=array(
	'file' 		=> base64_encode($file['file']),
	'extension'	=> $exten,
	'filename'	=> implode('.', $explo),
);
header("Content-type: text/javascript");
header('Content-Encoding: gzip'); 
echo gzencode(json_encode($arrayRet));