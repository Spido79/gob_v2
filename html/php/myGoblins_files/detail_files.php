<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_file=intval($post->get('id'));
$file=\FILES\Upload::getSpec($id_file);

if (!$file){
	\HTML\Page::Page_404();	
}

if ($file['tane_riferimento']!='ALL_TANE'){
	$myTane= explode(',', \USERS\Identify::get('tane_riferimento'));
	$taneFile=explode(',', $file['tane_riferimento']);
	$found=false;
	foreach ($myTane as $myTana) {
		if (in_array($myTana, $taneFile)){
			$found=true;
			break;
		}
	}

	if (!$found){
		\HTML\Page::Page_404();	
	}
}

$explo=explode('.', $file['name']);
$exten=strtolower(end($explo));
$preview='';
$xtrim=12;
$desc="<div class=\"col-md-{$xtrim}\">
			<label class=\"control-label\">Descrizione: </label>
			<p class=\"\">".nl2br($file['description'])."</p>
		</div>";
if ($file['description']==''){
	$desc='';
}

$tag=' - <i class="fa fa-tags"></i> '.$file['tags'];
if ($file['tags']==''){
	$tag='';
}
switch ($exten) {
	case 'pdf':
		$xtrim=6;
		$desc="<div class=\"col-md-{$xtrim}\">
			<label class=\"control-label\">Descrizione: </label>
			<p class=\"\">".nl2br($file['description'])."</p>
		</div>";
		if ($file['description']==''){
			$xtrim=12;
			$desc='';
		}
		$data= 'data:application/'.$exten.';base64,'.base64_encode($file['file']);
		$preview="<div class=\"col-md-{$xtrim}\"><embed width=\"100%\" height=\"400px\" src=\"{$data}\"/></div>";
		
		break;


	case 'jpg':
	case 'png':
	case 'gif':
		$xtrim=6;
		$desc="<div class=\"col-md-{$xtrim}\">
			<label class=\"control-label\">Descrizione: </label>
			<p class=\"\">".nl2br($file['description'])."</p>
		</div>";
		$maxHeight=335;
		if ($file['description']==''){
			$xtrim=12;
			$desc='';
			$maxHeight=335;
		}
		$data= 'data:image/'.$exten.';base64,'.base64_encode($file['file']);
		$preview="<div class=\"col-md-{$xtrim} text-center\"><img style='max-height: {$maxHeight}px;max-width: 100%;'  src=\"{$data}\"/></div>";
		break;

}
?>
<div class="ibox selected">
	<div class="ibox-content">
		<div class="tab-content">
			<div class="tab-pane active">
				<div class="row">
					<div class="col-xs-12 text-center">
						<h3>
							File: <span class="text-success"><?php echo $file['name'];?></span>
							<button class="btn btn-xs btn-success btn_downloadFile_myFile" value="<?php echo $id_file;?>">
								<i class="fa fa-download"></i>
							</button>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="row">
					<div class="col-md-12">
						File caricato il : <?php 
							$date=new DateTime($file['data_in']);
							echo $date->format('d.m.Y'). ' alle ore '.$date->format('H:i');
							echo $tag;
						?> 
					</div>
				</div>

				<div class="row">
					<?php
					echo $desc;
					echo $preview;
					?>
				</div>
			</div>
		</div>
	</div>
</div>