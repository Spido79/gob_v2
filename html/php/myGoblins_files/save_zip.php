<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['tag','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
//leggi file
$tag=trim($post->get('tag'));

$files=array();
$params=array(
        'map_fields'    => array(
            'name'      =>'',
            'file'       =>'',
        ),
        'tag'           => $tag,
        'list_tane'     => explode(',', \USERS\Identify::get('tane_riferimento')),
        'order'         => array(
            'field'     => 'name',
            'direction' => 'asc', // asc/desc
        )
    );

$files=\FILES\Upload::get($params);
if(!$files || count($files)<=0){
	\HTML\Page::Page_404();
}
$file = tempnam("tmp", "zip"); 
$zip = new ZipArchive;
$res = $zip->open($file, ZipArchive::CREATE);
if ($res === TRUE) {
	foreach ($files as $item) {
		$zip->addFromString($item['name'], $item['file']);
	}
    $zip->close();
} else {
	\HTML\Page::Page_404();	
}
$fileZipped = file_get_contents($file);
@unlink($file);
if ($tag==''){
	$tag='generale';
}
$arrayRet=array(
	'file' 		=> base64_encode($fileZipped),
	'extension'	=> 'zip',
	'title'	=> $tag,
);
header("Content-type: text/javascript");
header('Content-Encoding: gzip'); 
echo gzencode(json_encode($arrayRet));