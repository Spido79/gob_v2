<?php 
if (isset($post)==false){
    $virtualPath='..';
    require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
    \HTML\Page::Page_404();
}
$tags=\FILES\Upload::getTags();

$files=array();
$params=array(
        'map_fields'    => array(
            'name'      =>'',
            'id_file'       =>'',
        ),
        'distinct'      => true,
        'tag'           => '',
        'list_tane'     => explode(',', \USERS\Identify::get('tane_riferimento')),
        'order'         => array(
            'field'     => 'name',
            'direction' => 'asc', // asc/desc
                )
    );

$count=0;
array_unshift($tags,array('tags' => ''));
foreach ($tags as $tag) {
    $params['tag']=$tag['tags'];
    $myTag=\FILES\Upload::get($params);
    if (count($myTag)>0){
        $name=$tag['tags'];
        $oriTag=$tag['tags'];
        if ($tag['tags']==''){
            $oriTag=base64_encode(random_bytes(10));
            $name='<i>Generale</i>';
        }
        $state=$count >0 ? array('opened' => true) : array('opened' => true);
        $files[] = array(
            'id'    => $oriTag,
            'oid'   => $tag['tags'],
            'text'  => '<span data-tag="'.$tag['tags'].'"><i class="fa fa-download"></i></span> <span class="text-success">'.$name.'</span>',
            'state' => $state,
            'parent'=> '#',
            'type'  => 1,
            'icon'  => 'fa fa-folder-open-o'
        );
        foreach ($myTag as $key => $value) {
            $basename=explode('.', $value['name']);
            switch (strtolower(end($basename))) {
                case 'png':
                case 'jpg':
                case 'tiff':
                case 'gif':
                    $icon='fa fa-file-image-o';
                    break;
                
                case 'pdf':
                    $icon='fa fa-file-pdf-o';
                    break;

                case 'zip':
                    $icon='fa fa-asterisk';
                    break;

                case 'ini':
                case 'cfg':
                    $icon='fa fa-cogs';
                    break;

                default:
                    $icon='fa fa-file-o';
                    break;
            }
            $files[]=array(
                'id'    => $value['id_file'],
                'oid'   => $value['id_file'],
                'text'  => $value['name'],
                'parent'=> $oriTag,
                'type'  => 2,
                'icon'  => $icon,
            );
        }    
        $count++;
    }
}
$arrayReturn=array(
    'core' => array(
        'multiple'  => true,
        'data'      => $files,
    ),
);

header("Content-type: text/javascript");
header('Content-Encoding: gzip'); 
echo gzencode(json_encode($arrayReturn));
