<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['search','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$search=trim($post->get('search'));
?>
<table class="table table-striped table-hover">
	<thead>
		<th>Data</th>
		<th>Pagina</th>
		<th>Utente</th>
		<th>Azione</th>
		<th>Messaggio</th>
	</thead>
	<tbody>
		<?php
			$count=0;
			foreach (\LOGS\Log::read($search) as $item){
				$linLog=explode('|',$item);
				if (count($linLog)<5){
					continue;
				}
				$userT=new \USERS\Detail($linLog[2]);
				$pagina=basename($linLog[1]);
				if (stripos($pagina,'?')){
					$paginaT=explode('?',$pagina);
					$pagina=$paginaT[0];
				}

				echo "<tr>
						<td class='text-center'>{$linLog[0]}</td>
						<td class='text-left'>{$pagina}</td>
						<td class='text-left'>{$userT->get('user_surname')} {$userT->get('user_name')}</td>
						<td class='text-left'>{$linLog[3]}</td>
						<td class='text-left'>{$linLog[4]}</td>
					</tr>";
				$count++;
			}
			if ($count==0){
				if ($search!=''){
					echo '<td colspan="5"><i>Nessun risultato trovato per la ricerca: <strong>'.$search.'</strong></i></td>';
				} else {
					echo '<td colspan="5"><i>File log vuoto</i></td>';	
				}
			}
		?>
	</tbody>
</table>