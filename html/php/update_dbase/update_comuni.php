<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
// link comuni
/*
https://raw.githubusercontent.com/matteocontrini/comuni-json/master/comuni.json
*/
LOGS\Log::write('COMUNI', "START: start update comuni", false, \USERS\Identify::UserID());
$value = \COMUNI\Istat::updateComuni();
LOGS\Log::write('COMUNI', "END: updated {$value} comuni", false, \USERS\Identify::UserID());
echo $value;