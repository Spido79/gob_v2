<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$user_id=intval($post->get('id'));
if (!\USERS\Detail::isActive($user_id)){
	\HTML\Page::Page_404();	
}
$tempUser = new \USERS\Detail($user_id);

?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<span class="pull-right text-muted close_responsabile" style='cursor:pointer'><i class='fa fa-remove'></i></span>
		<h3>Gestione responsabile</h3>
		
	</div>
	<div class="ibox-content">
		<div class="tab-content">
			<div id="contact-1" class="tab-pane active">
				<div class="row m-b-lg">
					<div class="col-lg-4 text-center">
						<h2><?php echo "{$tempUser->get('user_surname')} {$tempUser->get('user_name')}";?></h2>

						<div class="m-b-sm">
							<img alt="image" class="img-circle profile_personal_img" src="<?php echo \IMG\Logo::png($user_id,'img/profiles'); ?>"
							style="width: 100px">
						</div>
					</div>
					<div class="col-lg-8">
						<strong>
							Contatti:
						</strong>
						<p>
							<i class='fa fa-envelope-o'></i>
							<a href='mailto:<?php echo $tempUser->get('user_login');?>'><?php echo $tempUser->get('user_login');?></a>
							<?php
								if ($tempUser->get('user_telephone')!=''){
									echo "<br>
										<i class='fa fa-phone'></i>
										<a href='tel:".$tempUser->get('user_telephone')."'>".$tempUser->get('user_telephone')."</a>
										";
								}
							?>
							
						</p>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-6'>
						<h3>Tane abilitate</h3>
						<div class="input-group">
							<input type="text" placeholder="Filtro" class="input form-control search_tana">
						</div>
					</div>
					<div class='col-xs-6 text-right'>
						<button class="btn btn-xs btn-primary btn_sel_all" value="1"><i class='fa fa-check-square-o'></i> Sel. tutte</button>
						<button class="btn btn-xs btn-danger btn_sel_all" value="0"><i class='fa fa-square-o'></i> Desel. tutte</button>
					</div>

				</div>
				
				<?php
				$ciclo=0;
				foreach (\GOBLINS\Tane::getActive() as $key => $value) {
					$checked='';
					$competenza='<span class="text-primary">Città</span>';
					echo $ciclo %2 ==0 ? '<div class="row">' : '';
					if (in_array($value['id_tana'], explode(',', $tempUser->get('tane_riferimento')))){
						$checked='checked';
					}
					if ($value['competenza_Prov']==0){
						$competenza='<span class="text-success">Provincia</span>';
					}

					echo "<div class='col-lg-6 filter_tane' style='border-bottom: 1px solid #eee'>
							<div class='checkbox checkbox-danger checkbox-circle' style='margin:2px;'>
								<input id='check_taneres_{$value['id_tana']}' type='checkbox' {$checked} class='check_taneres_enable' value='{$value['id_tana']}' data-user='{$user_id}'/>
								<label for='check_taneres_{$value['id_tana']}'>
									<b>{$value['Nome']}</b>
									<br><span class='small text-muted'>{$value['Provincia']} - {$value['Indirizzo']}</span>
									<br><span class='small text-muted'><i>competenza: {$competenza}</span>
								</label>
							</div>
						</div>";
					echo $ciclo %2 !=0 ? '</div>' : '';
					$ciclo++;
				}
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.search_tana').keyup(function(event) {
		app_filter_tane();
	});

	$('.close_responsabile').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		admin_responsabili_box_right(0);
	});

	$('.btn_sel_all').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var val=$(this).val();
		$('.search_tana').val('');
		app_filter_tane();
		var lenCheck=$('.check_taneres_enable:checked').length;
		if (val==true){
			lenCheck=$('.check_taneres_enable').not(':checked').length;
		}
		$('.check_taneres_enable').each(function(index, el) {
			if (val != $(this).prop('checked')){
				var thisCheck=$(this);
				var id=$(this).attr('data-user');
				var id_tana=$(this).val();
				var check=!$(this).prop('checked');
				var refresh=false;
				if (lenCheck ==index+1){
					var refresh=true;
				}
				call_remoteEnableTana(refresh, check, id_tana, thisCheck, id);
			}
		});
	});

	$('.check_taneres_enable').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var thisCheck=$(this);
		var id=$(this).attr('data-user');
		var id_tana=$(this).val();
		var check=$(this).prop('checked');
		call_remoteEnableTana(true, check, id_tana, thisCheck, id);
	});
});

</script>
