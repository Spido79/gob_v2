<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['page','filter','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$page=intval($post->get('page'));
$filter=trim($post->get('filter'));
?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h3>Gestione responsabili tana</h3>
		<small>clicca su un utente per modificarlo</small>
	</div>
	<div class="ibox-content">
		<table class="table table-striped table-hover no-margins dataTable_resptane_list">
			<thead>
				<tr>
					<th>Utente</th>
					<th>Tane affiliate</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach (\USERS\Detail::getActive() as $item) {
					echo "<tr style='cursor:pointer;' class='click_resp_tana' data-id='".$item['user_id']."'>
							<td>
								<img class='img-circle profile_personal_img' src='".\IMG\Logo::png($item['user_id'],'img/profiles')."' style='width:20px;' />
								<small>{$item['user_surname']} {$item['user_name']}</small>
							</td>
							<td>";
					$count=0;
					foreach (\GOBLINS\Tane::spec(explode(",", $item['tane_riferimento'])) as $id_tana => $valTane) {
						if (\GOBLINS\Tane::isActive($id_tana)){
							//echo $valTane."<br>";
							$trim=str_replace('La Tana dei Goblin', '', $valTane);
							if ($trim==''){
								$trim='<i>Tana Nazionale</i>';
							}
							echo "{$trim}<br>";	
							$count++;
						}
						
					}
					echo $count==0? '<span class="text-danger"><i>-Nessuna tana affiliata-</i></span>':'';
					
					echo "</td>
							<td>{$item['user_login']}</td>
						</tr>";
				}
				?>
			</tbody>
		</table>

	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.click_resp_tana').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var thisId=$(this).attr('data-id');
		admin_responsabili_box_right(thisId);
	});

	$('.dataTable_resptane_list').DataTable({
		"language": {
			"lengthMenu": "Mostra _MENU_ elementi",
			"zeroRecords": "Non è stato trovato niente - riprova",
			"info": "Pagina _PAGE_ di _PAGES_",
			"search": "Cerca",
			"paginate": {
				"previous": "Indietro",
				"next": "Avanti"
			},
			"infoEmpty": "Nessun elemento disponibile",
			"infoFiltered": "(filtrati da _MAX_ elementi totali)"
		},
		pageLength: 25,
		responsive: true,
		search: {
    		search: "<?php echo $filter;?>",
  		},
		displayStart: <?php echo $page;?>,
		dom: '<"html5buttons"B>lTfgtip',
		buttons: [{
				extend: 'pdfHtml5', 
				title: 'Responsabili Tane',
				exportOptions: {
                    columns: [ 0,1,2 ]
                }
			},

		],
		"order": [[ 0, "asc" ]],
		"columnDefs": [ {
			"targets": 'no-sort',
			"orderable": false,

		}],
	});
});
</script>
