<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id_tana','check','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$user_id=intval($post->get('id'));
$id_tana=intval($post->get('id_tana'));
$check=trim($post->get('check'));
\USERS\Detail::updateTane($user_id, $check, $id_tana);
\LOGS\Log::write('TANE', 'User id: '.$user_id.' - tana id:'.$id_tana. ' Enabled? '.$check, false, \USERS\Identify::UserID());