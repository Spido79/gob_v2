<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
//Stampa tessera
//sospendi associato ( e metti in tana nazionale)
$id_file=intval($post->get('id'));
$file=\FILES\Upload::getSpec($id_file);

if (!$file){
	\HTML\Page::Page_404();	
}
/*
	Det file e bozza
*/
$explo=explode('.', $file['name']);
$exten=strtolower(end($explo));
$preview='';
$xtrim=12;
switch ($exten) {
	case 'pdf':
		$data= 'data:application/'.$exten.';base64,'.base64_encode($file['file']);
		$preview="<div class=\"col-md-6\"><embed width=\"100%\" height=\"100%\" src=\"{$data}\"/></div>";
		$xtrim=6;
		break;


	case 'jpg':
	case 'png':
	case 'gif':
		$data= 'data:image/'.$exten.';base64,'.base64_encode($file['file']);
		$preview="<div class=\"col-md-6 text-center\"><img style='max-height: 135px;max-width: 100%;'  src=\"{$data}\"/></div>";
		$xtrim=6;
		break;

}
?>
<div class="ibox selected">
	<div class="ibox-content">
		<div class="tab-content">
			<div class="tab-pane active">
				<div class="row m-b-lg">
					<div class="col-xs-12 text-center">
						<h3>
							File: <span class="text-success"><?php echo $file['name'];?></span>
							<button class="btn btn-xs btn-success btn_downloadFile_gestFile">
								<i class="fa fa-download"></i>
							</button>
						</h3>
						<hr>
					</div>
					<div class="col-md-<?php echo $xtrim;?>">
						<label class="control-label">Descrizione: </label>
						<textarea rows="5" style="resize:vertical;" data-name="description" placeholder="Descrizione" class="form-control field"><?php echo htmlentities($file['description']);?></textarea>
					</div>
					<?php echo $preview;?>
					<div class="col-md-12">
						File caricato il : <?php 
							$date=new DateTime($file['data_in']);
							echo $date->format('d.m.Y'). ' alle ore '.$date->format('H:i');
						?>
					</div>
				</div>
				<div class="row m-b-lg">
					<div class="col-sm-6 form-group text-center">
						<input class="tagsinput_fileGest form-control" placeholder="tag" type="text" value="<?php echo $file['tags'];?>">
					</div>
					<div class="col-md-12">
						Il <strong>tag</strong> è un campo di identificazione libero per il file permettendone la ricerca. Inoltre il tag farà visualizzare il file sotto l'etichetta assegnata.
					</div>
				</div>
				<div class="row m-b-lg">
					<div class="col-xs-12 text-right">
						<button class="btn btn-md btn-danger btn_saveDel_detFile" value="0">
							<i class="fa fa-trash"></i> Elimina
						</button>
						<button class="btn btn-md btn-success btn_saveDel_detFile" value="1">
							<i class="fa fa-floppy-o"></i> Salva
						</button>
						<input type="hidden" class="id_file" value="<?php echo $id_file;?>"/>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="row m-b-lg">
					<div class="col-xs-12 text-left">
						<h5>Scegli le tane che possono visualizzare questo file:</h5>
					</div>
					<?php
					$count=0;
					foreach (\GOBLINS\Tane::getActive() as $key => $value) {
						$checked='';
						$nome=str_ireplace('La Tana dei Goblin', '', $value['Nome']);
						
						if (in_array($value['id_tana'], explode(',', $file['tane_riferimento']) ) || $file['tane_riferimento']=='ALL_TANE'){
							$checked='checked';
							$count++;
						}

						if ($value['id_tana'] == \APP\Parameter::getSpec('id_tana_nazionale')){
							$nome='<span class="text-danger">Tana Nazionale</span>';
						}
						echo "<div class='col-sm-3 filter_tane_file'>
						<div class='checkbox checkbox-danger checkbox-circle' style='margin:2px;'>
						<input id='check_tanefile_{$value['id_tana']}' type='checkbox' {$checked} class='check_tanefile_enable' value='{$value['id_tana']}'/>
						<label for='check_tanefile_{$value['id_tana']}'>
						{$nome}
						</label>
						</div>
						</div>";
					}
				?>
				</div>
				<div class="row m-b-lg">
					<?php	
					$btn_all='hidden';
					if ($count<count(\GOBLINS\Tane::getActive())){
						$btn_all='';
					}
					$btn_noall='hidden';
					if ($count>0){
						$btn_noall='';	
					}					
					?>
					<div class="col-xs-12">
						<button class="<?php echo $btn_all;?> btn btn-primary btn-sm btn_selAll_file" value="true">
							Seleziona tutte
						</button>
					</div>
					<div class="col-xs-12">
						<button class="<?php echo $btn_noall;?> btn btn-danger btn-sm btn_selAll_file" value="false">
							Deseleziona tutte
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	sel_tags_file();
});
</script>