<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
?>
<div class="hr-line-dashed"></div>
<div class="row m-b-lg">
	<div class="col-xs-12 text-left">
		<h5>Scegli le tane che possono visualizzare questo file:</h5>
	</div>
	<?php
	$count=0;
	foreach (\GOBLINS\Tane::getActive() as $key => $value) {
		$nome=str_ireplace('La Tana dei Goblin', '', $value['Nome']);

		if ($value['id_tana'] == \APP\Parameter::getSpec('id_tana_nazionale')){
			$nome='<span class="text-danger">Tana Nazionale</span>';
		}
		echo "<div class='col-sm-3 filter_tane_file'>
		<div class='checkbox checkbox-danger checkbox-circle' style='margin:2px;'>
		<input id='check_tanefileMulti_{$value['id_tana']}' type='checkbox' class='check_tanefileMulti_enable' value='{$value['id_tana']}'/>
		<label for='check_tanefileMulti_{$value['id_tana']}'>
		{$nome}
		</label>
		</div>
		</div>";
	}
	?>
</div>
<div class="row m-b-lg">
	<?php	
	$btn_all='hidden';
	if (count(\GOBLINS\Tane::getActive())>0){
		$btn_all='';
	}
	?>
	<div class="col-xs-12">
		<button class="<?php echo $btn_all;?> btn btn-primary btn-sm btn_selAllMulti_file" value="true">
			Seleziona tutte
		</button>
		<button style="float:right;" class="<?php echo $btn_all;?> btn btn-success btn-sm btn_activeAllMulti_file" value="true">
			Imposta tane selezionate
		</button>
	</div>
	</div>

</div>