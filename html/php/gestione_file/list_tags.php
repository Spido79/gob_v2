<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$arrayTags=array();
foreach (\FILES\Upload::getTags() as $item) {
	$arrayTags[]=$item['tags'];
}
$array=array(
	'tags'	=> $arrayTags,
);
header("Content-type: text/javascript");
header('Content-Encoding: gzip'); 
echo gzencode(json_encode($array));