<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['tane','action', 'id','redirect_to'])) {
	\HTML\Page::Page_404();
}
//leggi file
$id_file=intval($post->get('id'));
$action=trim($post->get('action'));
$tana=intval($post->get('tane'));

\FILES\Upload::tane($id_file, $action, $tana);