<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['action', 'id','redirect_to'])) {
	\HTML\Page::Page_404();
}
//leggi file
$id=intval($post->get('id'));
$action=intval($post->get('action'));

if ($action==0){
	\FILES\Upload::del($id);
	$id=0;
} else {
	if(!$post->VerifyPostData(['text','tag'])) {
		\HTML\Page::Page_404();
	}
	$texts=array(
		'description'	=> trim($post->get('text')),
		'tags'			=> trim($post->get('tag')),
	);
	\FILES\Upload::update($id, $texts);
}

$arrayRet=array(
	'files' 	=> count(\FILES\Upload::get()),
	'id'		=> $id,
);
echo json_encode($arrayRet);