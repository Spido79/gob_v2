<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['list','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$ListId=json_decode($post->get('list'));
$action=intval($post->get('id'));

if ($action==0){
	foreach ($ListId as $specFile) {
		\FILES\Upload::del($specFile);
	}

} else {
	if(!$post->VerifyPostData(['tag'])) {
		\HTML\Page::Page_404();
	}
	$texts=array(
		'tags'			=> trim($post->get('tag')),
	);
	foreach ($ListId as $specFile) {
		\FILES\Upload::update($specFile, $texts);	
	}
}

$arrayRet=array(
	'files' 	=> count(\FILES\Upload::get()),
);
echo json_encode($arrayRet);