<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['recordTotal','draw','length','start','order','search','id','redirect_to'])) {
	\HTML\Page::Page_404();
}	
$arrayReturn=array(
	"draw"            => intval($post->get('draw')),
	"recordsTotal"    => intval($post->get('recordTotal')),
	"recordsFiltered" => 0,
	"data"            => array(),
);
$paramsPass=array(
    'search'    => trim($post->get('search')['value']),
    'map_fields'	=> array(
    	'data_in'		=>'',
    	'name'			=>'',
    	'OCTET_LENGTH:file'=>'size',
    	'tags'			=>'',
    	'id_file'		=>'',
    ),
    'order'     => array(
      'field' => (intval($post->get('order')[0]['column'])+1),
      'direction' => trim($post->get('order')[0]['dir']),
    ),
);
$files=\FILES\Upload::get($paramsPass);
$arrayReturn['recordsFiltered']=count($files);
$count=0;
$counted=0;
foreach ($files as $item) {
	if (intval($post->get('start'))<=$count && $counted< intval($post->get('length'))){
		$data_file=new DateTime($item['data_in']);
		$arrayStep=array(
			"<span class=\"hidden\">{$item['data_in']}</span><small>{$data_file->format('d.m.y - H:i')}</small>",
			$item['name'],
			"<span class=\"hidden\">{$item['size']}</span>".\LOGS\Log::FileSizeConvert($item['size']),
			//mb_strimwidth($item['description'], 0, 25, "..."),
			mb_strimwidth($item['tags'], 0, 25, "..."),
			$item['id_file'],
		);
		$arrayReturn['data'][]=$arrayStep;
		$counted++;
	}
	$count++;
}
header("Content-type: text/javascript");
header('Content-Encoding: gzip'); 
echo gzencode(json_encode($arrayReturn));