<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['tane','list', 'id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$ListId=json_decode($post->get('list'));
$ListTane=json_decode($post->get('tane'));

foreach ($ListId as $id_file) {
	\FILES\Upload::tane($id_file, 0, 0);
	foreach ($ListTane as $id_tana) {
		\FILES\Upload::tane($id_file, 1, $id_tana);
	}
}