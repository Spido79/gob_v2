<?php 
if (isset($post)==false){
    $virtualPath='.';
    require_once("../../lib/init.php");
}

if(!$post->VerifyPostData(['email','id','redirect_to'])) {
    \HTML\Page::Page_404();
}

if (\IP_GEST\IP_list::verBlocked()){
	echo 404;
	//IP BLOCKED! Write email and segnalate to user_error()
	$subject ="Portale gestione - BLOCCO IP";
	\MAIL\Create::setVal('mail_from','spido@goblins.net');
	\MAIL\Create::setVal('mail_to','spido@goblins.net');
	\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
	\MAIL\Create::setVal('name_to','Spido');
	\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
	\MAIL\Create::setVal('mail_subject',$subject);

	$dateTime=new \DateTime();
	$arrayReplace=array(
		'subject'	=> $subject,
		'ip'		=> $_SERVER['REMOTE_ADDR'],
		'date'		=> $dateTime->format('d M Y'),
		'hour'		=> $dateTime->format('H:i'),
		'link'		=> APP_URL
		);
	\MAIL\Create::setTemplate('login/ip_blocked',$arrayReplace);
	\MAIL\Create::insert();
	exit;
}

//Verifico se l'ip è bloccato, se no tento il recupero creando un token di validità 12 ore
$email=trim($post->get('email'));

if (\USERS\Identify::CreateToken($email)){
	echo 200;
	$tempUser = new \USERS\Detail(0,$email);
	$subject ="Portale gestione - Reset Password";
	\MAIL\Create::setVal('mail_from','spido@goblins.net');
	\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
	
	\MAIL\Create::setVal('mail_to',$email);
	\MAIL\Create::setVal('name_to',trim($tempUser->get('user_surname').' '.$tempUser->get('user_name')));
	\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
	\MAIL\Create::setVal('mail_subject',$subject);

	$dateTime=new \DateTime();
	$arrayReplace=array(
		'subject'	=> $subject,
		'date'		=> $dateTime->format('d M Y'),
		'hour'		=> $dateTime->format('H:i'),
		'email'		=> $email,
		'name'		=> $tempUser->get('user_name'),
		'link'		=> APP_URL.'/resetPWD/'.$tempUser->get('token_hash').'/'.$email
		);
	\MAIL\Create::setTemplate('login/forget',$arrayReplace);
	\MAIL\Create::insert();
} else {
	echo 401;
	\IP_GEST\IP_list::IncraseIP($email,'');
}
