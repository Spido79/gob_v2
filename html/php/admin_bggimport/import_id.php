<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

//echo json formatted a list of nick (max 50?)
$id_bgg=intval($post->get('id'));

$spec= \BGG\Games::viewSpec($id_bgg);
if ($spec[0]){
	?>
	<div class="alert alert-success">
		<strong><i class="fa fa-warning"></i> Importato:</strong> l'elemento è stato importato correttamente!
	</div>
	<?php
} else {
	?>
	<div class="alert alert-danger">
		<strong><i class="fa fa-warning"></i> Attenzione:</strong> questo elemento di BGG non può essere importato causa errori.<br>Di seguito il dettaglio XML del file:
	</div>
	<?php
}
echo "<pre>";
print_r($spec[1]);
echo "</pre>";