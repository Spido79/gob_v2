<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['size','filename','list','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
//crea csv lista
$taneRif=array();
foreach (\GOBLINS\Tane::getActive() as $key => $value) {
    $taneRif[]=$value['id_tana'];
}
$paramsPass=array(
	'list_tane' => $taneRif,
	'list_goblins'=> json_decode($post->get('list')),
	'map_fields'	=> array(
		'Nick'				=>'Nick',
		'cognome_goblin'	=>'Cognome',
		'nome_goblin'		=>'Nome',
		'tessera_numero'	=>'Tessera',
		'data_associato'	=>'Data',
		'codice_tessera'	=>'Codice',
		'tbl_tane.Nome'		=>'Tana',
	),
	'left_join'	=> array(
		'tbl_tane'			=> array(
			'id_tana'	=> 'id_tana',
		)
	),
);

$params= array(
	'elements' => \GOBLINS\Manage::getAll(0, $paramsPass),
	'dimension' => intval($post->get('size')),
);

\PDF\Tessere::set('filename', trim($post->get('filename')));
echo base64_encode(\PDF\Tessere::stream($params));