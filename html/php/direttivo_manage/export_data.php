<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['list','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$taneRif=array();
foreach (\GOBLINS\Tane::getActive() as $key => $value) {
    $taneRif[]=$value['id_tana'];
}
//crea csv lista
$paramsPass=array(
	'list_tane' => $taneRif,
	'list_goblins'=> json_decode($post->get('list')),
	'map_fields'	=> array(
		'Nick'				=>'Nick',
		'cognome_goblin'	=>'Cognome',
		'nome_goblin'		=>'Nome',
		'email_goblin'		=>'Email',
	),
);
$GoblinsSelected=\GOBLINS\Manage::getAll(0, $paramsPass);
echo json_encode($GoblinsSelected);