<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['accept','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_goblin=intval($post->get('id'));
$accept=intval($post->get('accept'));
$goblin=\GOBLINS\Manage::getSpec($id_goblin);
$taneRif=array();
foreach (\GOBLINS\Tane::getActive() as $key => $value) {
    $taneRif[]=$value['id_tana'];
}
if (!in_array($goblin['id_tana'], $taneRif)){
	\HTML\Page::Page_404();	
}

$data_goblin=\GOBLINS\Manage::getSpec($id_goblin);
if (!$data_goblin){
	\HTML\Page::Page_404();	
}
$arrayGet=\GOBLINS\Request::get($id_goblin);
$arrayResp=array();
foreach (\GOBLINS\Tane::getResponsable($arrayGet['id_tana']) as $user_id) {
	$tempUser = new \USERS\Detail($user_id);
	$arrayResp[$tempUser->get('user_login')] = $tempUser->get('user_name').' '.$tempUser->get('user_surname');
}

if ($accept==1){
	
	$arrayData=array(
		'id_tana' 	=> $arrayGet['id_tana']
	);
	$tane=\GOBLINS\Tane::spec([$arrayGet['id_tana'], $data_goblin['id_tana']]);
	$tane[0]='--nessuna tana--';
	if (\GOBLINS\Manage::setSpec($id_goblin, $arrayData)){
		\LOGS\Log::write('Direttivo', 'transfer Goblin'.$id_goblin." ({$data_goblin['cognome_goblin']} {$data_goblin['nome_goblin']} - {$data_goblin['Nick']} ) " , false, \USERS\Identify::UserID());
		\LOGS\Tesserato::write($id_goblin, "Trasferito da {$tane[$data_goblin['id_tana']]} a {$tane[$arrayGet['id_tana']]}",'fa-superpowers');
	}
	$answer=1;
	//SEND MAIL!
	$subject ="TdG Portale Gestione - Richiesta accettata";
	$template="accept";
} else {
	$answer=2;
	//SEND MAIL!
	//RIFIUTO
	$subject ="TdG Portale Gestione - Richiesta rifiutata";
	$template="refuse";
}
$arrayReplace=array(
	'name_ori'	=> \USERS\Identify::get('user_name'). ' '.\USERS\Identify::get('user_surname'),
	'subject'	=> $subject,
	'goblin'	=> $data_goblin['Nick'],
);
foreach ($arrayResp as $mail => $name) {
	$arrayReplace['name']=$name;
	\MAIL\Create::setVal('mail_from','spido@goblins.net');
	\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
	\MAIL\Create::setVal('mail_to',$mail);
	\MAIL\Create::setVal('name_to',$name);
	\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
	\MAIL\Create::setVal('mail_subject',$subject);
	\MAIL\Create::setTemplate('direttivo_manage/'.$template,$arrayReplace);
	\MAIL\Create::insert();
}

\GOBLINS\Request::answer($id_goblin, $answer);

$paramsPass=array(
	'list_tane' => $taneRif,
);
echo count(\GOBLINS\Manage::getAll(0, $paramsPass));
