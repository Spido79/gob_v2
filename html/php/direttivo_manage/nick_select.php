<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
\GOBLINS\Manage::resetGoblin();
$idTane=intval($post->get('id'));

$taneRif=array();
foreach (\GOBLINS\Tane::getActive() as $key => $value) {
    $taneRif[]=$value['id_tana'];
}

$paramsPass=array(
	'list_tane' => $taneRif,
	'list_tane_exclude' => true,
	'map_fields'	=> array(
		'id_goblin'		=>'id',
		'Nick'			=>'label',
	),
);

$readFile=\GOBLINS\Manage::getAll($idTane, $paramsPass);
$array=array('Goblin' => $readFile);
header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);
