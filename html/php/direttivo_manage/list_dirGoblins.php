<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['req','recordTotal','draw','length','start','order','search','id','redirect_to'])) {
	\HTML\Page::Page_404();
}	
$arrayReturn=array(
    "draw"            => intval($post->get('draw')),
    "recordsTotal"    => intval($post->get('recordTotal')),
    "recordsFiltered" => 0,
    "data"            => array(),
);

$taneRif=array();
foreach (\GOBLINS\Tane::getActive() as $key => $value) {
    $taneRif[]=$value['id_tana'];
}

if (count($taneRif)>0){
    $paramsPass=array(
        'list_tane' => $taneRif,
        'search'    => trim($post->get('search')['value']),
        'search_tana' => trim($post->get('search')['value']),
        'map_fields'  => array(
            'Nick'          =>'',
            'cognome_goblin'=>'',
            'nome_goblin'   =>'',
            'tbl_goblins.id_goblin'     =>'',
            'email_goblin'  =>'',
            'id_request'    =>'',
            'tbl_tane.Nome' =>'tana_name',
            ),
        'order'     => array(
            'field' => (intval($post->get('order')[0]['column'])+1),
            'direction' => trim($post->get('order')[0]['dir']),
        ),
        'left_join' => array(
            'tbl_request_goblin'  => array(
                'id_goblin' => 'id_goblin',
                'action'    => 'value:0',
            ),
            'tbl_tane'  => array(
                'id_tana' => 'id_tana',
            ),

          ),
        'action'    => null,
    );
    if ($post->get('req')=='true'){
        $paramsPass['action']=1;
    }
    $allMyGobsearc=\GOBLINS\Manage::getAll(0, $paramsPass);
    $arrayReturn['recordsFiltered']=count($allMyGobsearc);
    $count=0;
    $counted=0;

    foreach ($allMyGobsearc as $item) {
        $founded=0;
        $nomeTana=str_replace('La Tana dei Goblin', '', $item['tana_name']);
        if ($nomeTana==''){
            $nomeTana='Tana Nazionale';
        }
        if (intval($post->get('start'))<=$count && $counted< intval($post->get('length'))){
            $arrayStep=array(
                $item['Nick'],
                $item['cognome_goblin'],
                $item['nome_goblin'],
                $item['id_goblin'],
                $item['email_goblin'],    
                $item['id_request'],
                $nomeTana,
            );
            $arrayReturn['data'][]=$arrayStep;
            $counted++;
        }
        $count++;
    }
}

echo json_encode($arrayReturn);