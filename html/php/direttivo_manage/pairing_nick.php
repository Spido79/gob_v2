<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
LOGS\Log::write('Direttivo', "START: start update nick", false, \USERS\Identify::UserID());
$valueArray = \GOBLINS\Manage::pairingNick();
LOGS\Log::write('Direttivo', "END: updated nick", false, \USERS\Identify::UserID());
echo json_encode($valueArray);