<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['future','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_goblin=intval($post->get('id'));
$future=intval($post->get('future'));
$goblin=\GOBLINS\Manage::getSpec($id_goblin);

$taneRif=array();
foreach (\GOBLINS\Tane::getActive() as $key => $value) {
    $taneRif[]=$value['id_tana'];
}

if (!in_array($goblin['id_tana'], $taneRif)){
	\HTML\Page::Page_404();
}

$data_goblin=\GOBLINS\Manage::getSpec($id_goblin);
if (!$data_goblin){
	\HTML\Page::Page_404();
}

$arrayData=array(
	'id_tana' 	=> $future,
	);

$tane=\GOBLINS\Tane::spec([$future, $data_goblin['id_tana']]);
$tane[0]='--nessuna tana--';
if (\GOBLINS\Manage::setSpec($id_goblin, $arrayData)){
	\LOGS\Log::write('Direttivo', 'removed association for Goblin'.$id_goblin." ({$data_goblin['cognome_goblin']} {$data_goblin['nome_goblin']} - {$data_goblin['Nick']} ) " , false, \USERS\Identify::UserID());
	\LOGS\Tesserato::write($id_goblin, "Rimosso tesseramento da {$tane[$data_goblin['id_tana']]} a {$tane[$future]}",'fa-superpowers');
}

$paramsPass=array(
	'list_tane' => $taneRif,
);
echo count(\GOBLINS\Manage::getAll(0, $paramsPass));
