<?php 
if (isset($post)==false){
    $virtualPath='..';
    require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['ipList','id','redirect_to'])) {
    \HTML\Page::Page_404();
}

$ipList=json_decode($post->get('ipList'));
foreach ($ipList as $value) {
	\IP_GEST\IP_list::CleanIP($value);
}
\LOGS\Log::write('IP_BLOCKED', 'clean IP '.json_encode($ipList) , false, \USERS\Identify::UserID());