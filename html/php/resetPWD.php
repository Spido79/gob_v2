<?php 
if (isset($post)==false){
    $virtualPath='.';
    require_once("../../lib/init.php");
}

if(!$post->VerifyPostData(['token','email','id','redirect_to'])) {
    \HTML\Page::Page_404();
}

if (\IP_GEST\IP_list::verBlocked()){
	echo 404;
	exit;
}

$email=trim($post->get('email'));
$token=trim($post->get('token'));

if (!\USERS\Identify::VerifyToken($email, $token)){
	\IP_GEST\IP_list::IncraseIP($email,$token);
	echo 401;
	exit;
}

//reset password
$newPass= \PASSWORD\Generator::NewPassword();
$user=new \USERS\Detail(0,$email);
if (!$user->updatePass(\PASSWORD\Generator::$DB_Encode, \PASSWORD\Generator::$Plain)){
	\IP_GEST\IP_list::IncraseIP($email,$token);
	echo 401;
	exit;
}

\USERS\Identify::ResetToken($email, $token);
echo 200;
//send psw by mail.
$subject ="Portale gestione - Nuova Password";
\MAIL\Create::setVal('mail_from','spido@goblins.net');
\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
	
\MAIL\Create::setVal('mail_to',$email);
\MAIL\Create::setVal('name_to',trim($user->get('user_surname').' '.$user->get('user_name')));
\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
\MAIL\Create::setVal('mail_subject',$subject);

$dateTime=new \DateTime();
$arrayReplace=array(
	'subject'	=> $subject,
	'date'		=> $dateTime->format('d M Y'),
	'hour'		=> $dateTime->format('H:i'),
	'email'		=> $email,
	'name'		=> $user->get('user_name'),
	'password'	=> \PASSWORD\Generator::$Plain,
	);
\MAIL\Create::setTemplate('login/resetPWD',$arrayReplace);
\MAIL\Create::insert();
