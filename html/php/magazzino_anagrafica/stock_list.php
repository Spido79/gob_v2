<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id=intval($post->get('id'));
$stockList=\MAGAZZINO\Games::getStock($id);
$countStock=count($stockList);
?>
<div class="ibox-title">
	<h5>Elenco stock</h5>

	<button class="btn btn-sm btn-primary btn_addStock" style='float: right !important;margin:-1px 0 0px 10px'>Aggiungi copia</button>
</div>
<div class="ibox-content">
	<p><b>Attenzione: </b> Le modifiche effettuate in questa lista sono dirette nell articolo e non movimentano l'articolo.</p>
	<div class="table-responsive" style='padding-bottom: 10px;'>
		<table class="table table-striped table-hover table_stock_ana_json" style="max-width: 100%!important">
			<thead>
				<tr>
					<th>UID</th>
					<th ><div class="td_visisble" data-placement='top' style='cursor:pointer' title='Titolo disponibile per il prestito'>Disp.</div></th>
					<th>Magazzino</th>
					<th>Proprietà</th>
					<th>Data trasferimento</th>
					<th class="no-sort">Azioni</th>
					<th class="no-sort">ori</th>
					<th class="no-sort">not</th>
					<th class="no-sort">loc</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($stockList as $item) {
					$transfer=new \DateTime($item['data_store']);
					$dataacq=new \DateTime($item['data_acquisto']);
					$nomeTana=str_replace('La Tana dei Goblin', '', $item['nome_tana']);
					if ($nomeTana==''){
						$nomeTana='Tana Nazionale';
					}
					if (!\GOBLINS\Tane::isActive($item['id_owner'])){
						$nomeTana='<span class=\'text-danger\'>'.$nomeTana.' - TANA DISATTIVATA</span>';
					}

					$elimPerm='';
					if (\USERS\Identify::UserGROUP()==1 || \USERS\Identify::UserGROUP()==2){
						$elimPerm="<li class=\"dropdown-divider\"></li>
						<li><a class=\"dropdown-item btn_act_stock_ana_del\">Elimina</a></li>";
					}
					if ($item['hidden']==1){
						$hidden_field='<i class="fa fa-times-circle text-danger"></i>';
					} else {
						$hidden_field='<i class="fa fa-check text-green"></i>';
					}
					echo "<tr>
						<td>{$item['id_stock']}</td>
						<td data-check=\"{$item['hidden']}\">{$hidden_field}</td>
						<td data-id=\"{$item['id_store']}\">{$item['store']}</td>
						<td data-id=\"{$item['id_owner']}\">{$nomeTana}</td>
						<td data-acq=\"{$dataacq->format('d/m/Y')}\">{$transfer->format('d/m/Y')}</td>
						<td><div class=\"btn-group\">
                            <button data-toggle=\"dropdown\" class=\"btn btn-sm btn-white dropdown-toggle w-100\">Azione <i class=\"fa fa-caret-down\"></i></button>
                            <ul class=\"dropdown-menu\" style=\"left:-15px;\">
                                <li><a class=\"dropdown-item btn_act_stock_ana_mod\">Modifica</a></li>
                                <li><a class=\"dropdown-item btn_act_stock_ana_log\">Movimenti</a></li>
                                {$elimPerm}
                            </ul>
                        </div>
						</td>
						<td>".htmlspecialchars($item['origin'])."</td>
						<td>".htmlspecialchars($item['notes'])."</td>
						<td>".htmlspecialchars($item['locazione'])."</td>
					</tr>";
				}
				?>
			</tbody>
		</table>
		<input type="hidden" class="countTotalStockAna" value='<?php echo $countStock;?>'/>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	<?php
	if (\USERS\Identify::UserGROUP()==1 || \USERS\Identify::UserGROUP()==2){
	?>
	$('.btn_act_stock_ana_del').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id =$(this).closest('tr').find('td:first-child').text();
		swal({
			title: "Elimina Stock",
			text: "Sei sicuro di voler rimuovere questo articolo dallo stock?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				var postData = new FormData();
				var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/del_stock',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					$('.btn-stock-manage-ana').click();
				});
			}
		});
	});
	<?php
	}
	?>

	$('.btn_act_stock_ana_log').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id =$(this).closest('tr').find('td:first-child').text();
		$('button').prop('disabled',true);
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/log_stock',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.body_anaLogStock_modal').empty().append(returndata);
			$('.ana_stock_uid').empty().append(id);
			$('.modal_ana_stock_log').modal('show');
		});

	});

	$('.btn_act_stock_ana_mod').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id =$(this).closest('tr').find('td:first-child').text();
		var hidden_val =$(this).closest('tr').find('td:nth-child(2)').attr('data-check');
		var id_store =$(this).closest('tr').find('td:nth-child(3)').attr('data-id');
		var id_owner =$(this).closest('tr').find('td:nth-child(4)').attr('data-id');
		var data_acq =$(this).closest('tr').find('td:nth-child(5)').attr('data-acq');

		$('input[data-field="id_ana"]').val(<?php echo $id?>);
		$('.anaMag-action-addModd').empty().append('Modifica stock');
		$('.anaMag-title-add').empty().append($('.main-title-ana-id').text());
		//parse data
		var row=dataTables_gestStockAna.row($(this).closest('tr')).data();
		$('select[data-field="id_store"]').val(id_store);
		$('select[data-field="id_owner"]').val(id_owner);
		$('input[data-field="data_acquisto"]').val(data_acq);
		$('input[data-field="hidden"]').prop('checked',hidden_val==1?true:false);

		$('select[data-field="origin"]').val(row[6]);

		$('textarea[data-field="notes"]').val(row[7].replace('&lt;','<').replace('&gt;','>'));
		$('input[data-field="locazione"]').val(row[8].replace('&lt;','<').replace('&gt;','>'));
		$('.btn_save_modal_anaMagStock').attr('data-id',id);
		$('.data-field').parent().removeClass('has-error');
		$('.modal_anaMagStock').modal('show');
	});


	$('.td_visisble').tooltip({html:true});
	$('.stock_qta').empty().append(<?php echo $countStock?>);
	$('.btn_addStock').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		$('input[data-field="id_ana"]').val(<?php echo $id?>);
		$('.anaMag-title-add').empty().append($('.main-title-ana-id').text());
		$('.anaMag-action-addModd').empty().append('Inserimento stock');

		$('select[data-field="id_store"]').val(0);
		$('select[data-field="id_owner"]').val(0);
		$('input[data-field="data_acquisto"]').val('');
		$('input[data-field="hidden"]').prop('checked',false);
		$('select[data-field="origin"]').val('');
		$('textarea[data-field="notes"]').val('');
		$('input[data-field="locazione"]').val('');
		$('.btn_save_modal_anaMagStock').attr('data-id',0);
		$('.data-field').parent().removeClass('has-error');
		$('.modal_anaMagStock').modal('show');
	});
	var dataTables_gestStockAna= $('.table_stock_ana_json').DataTable({
		"language": {
			"lengthMenu": "Mostra _MENU_ elementi",
			"zeroRecords": "Non è stato trovato nessun elemento in stock.",
			"info": "Pagina _PAGE_ di _PAGES_",
			"search": "Cerca",
			"paginate": {
				"previous": "Indietro",
				"next": "Avanti"
			},
			"infoEmpty": "Nessun elemento disponibile",
			"infoFiltered": "(filtrati da _MAX_ elementi totali)"
		},
		processing: true,
		lengthMenu: [[10, 25, $('.countTotalStockAna').val()], [10, 25, "Tutti"]],
		//serverSide: true,
		pageLength: 10,
		responsive: true,
		dom: '<"html5buttons"B>lTfgtip',
		buttons: [{
				extend: 'pdfHtml5',
				title: 'Elenco Stock',
				exportOptions: {
                    columns: [0,1,2,3,4],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			},{
				extend: 'csvHtml5',
				title: 'Elenco Stock CSV',
				fieldSeparator: ';',
				exportOptions: {
                    columns: [0,1,2,3,4],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			}
		],
		"order": [[ 2, "asc" ]],
		"columnDefs": [
		{
			"targets": 'no-sort',
			"orderable": false,
		},{
            "targets": [0],
            "className": 'text-right',
            "visible": true,
            "searchable": true,
            "orderable": false,
        },{
            "targets": [1],
            "className": 'text-center',
            "visible": true,
            "searchable": false,
            "orderable": false,
        },{
            "targets": [3],
            "className": 'text-left'
        },{
            "targets": [4],
            "className": 'text-center'
        },{
            "targets": [6,7,8],
            "visible": false,
            "searchable": false,
            "orderable": false,
        }],
	});
});
</script>