<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['title','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id=intval($post->get('id'));
$title=trim($post->get('title'));
\MAGAZZINO\Games::updateTitle($id, $title);
\LOGS\Log::write('ANA', 'Articolo id: '.$id.' titolo cambiato in: '.$title, false, \USERS\Identify::UserID());