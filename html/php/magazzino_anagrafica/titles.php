<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

//echo json formatted a list of nick (max 50?)
$params=array('id_ana','title', 'all_title');
$array=array('title' => array());
foreach (\MAGAZZINO\Games::getAll($params) as $value) {
	$allName=explode('|#|', $value['all_title']);
	foreach ($allName as $item) {
		if (!in_array($item, $array['title'])){
			$array['title'][]=$item;
		}
	}

}

header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);
