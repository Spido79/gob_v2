<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['title','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$games_title=trim($post->get('title'));
$data_game=\MAGAZZINO\Games::getSpec(0, $games_title, true);
if (!$data_game){
	\HTML\Page::Page_404();
}
$maxTitles=\MAGAZZINO\Games::$maxElements;
?>
<div class="ibox-title">
	<h5>Lista titoli trovati</h5>
</div>
<div class="ibox-content">
	<p>
		Seleziona il titolo che vuoi gestire
	</p>
	<div class="project-list">
		<table class="table table-hover">
			<tbody>
			<?php
			$count=0;
			$continue=0;
			foreach ($data_game as $item) {
				$names_all=explode('|#|',$item['all_title']);
				if ($names_all>1){
					$elemNew=array();
					foreach ($names_all as $key => $name) {
						if ($name==$item['title']){
							continue;
						}
						$elemNew[]=str_ireplace($games_title, '<span style="background-color:#fff100;color:#000000">'.$games_title.'</span>', $name);

					}
					$names='<br/><small>-'.implode('<br>-',$elemNew).'</small>';
				} else {
					$names='';
				}
				$title=str_ireplace($games_title, '<span style="background-color:#fff100;color:#000000">'.$games_title.'</span>', $item['title']);

				if ($item['active']==1){
					$active='<span class="label label-primary">Attivo</span>';
				} else {
					$active='<span class="label label-danger">Inattivo</span>';
				}
				echo "
				<tr>
					<td class=\"project-status\">
						{$active}
					</td>
					<td class=\"project-thumbnail\">
						<img alt=\"{$item['title']}\" style='max-width:50px;max-height:50px;' src=\"{$item['thumbnail']}\" />
					</td>
					<td class=\"project-title\">
						<b>{$title}</b>
						{$names}
					</td>
					<td class=\"project-bgg\">
						<button value=\"{$item['id_ana']}\" target=\"_blank\" class=\"btn_mag_ana_manageID btn btn-success btn-xs\"><i class=\"fa fa-arrow-right\"></i> Gestisci</button>
						<a href=\"https://www.boardgamegeek.com/boardgame/{$item['id_bgg']}\" target=\"_blank\" class=\"btn btn-white btn-xs\"><i class=\"fa fa-link\"></i> BGG</a>
					</td>
				</tr>
				";
				$count++;
				if ($count>=$maxTitles){
					$continue=1;
					break;
				}
			}

			if ($continue==1){
				echo "
				<tr>
					<td colspan=\"4\">
						<div class=\"text-right\">...più di {$maxTitles} titoli trovati.</div>
						<small class=\"text-success\"><i>Migliora la ricerca se il titolo ricercato non è tra quelli mostrati.</i></small>
					</td>
				</tr>
				";
			}
			?>
			</tbody>
		</table>
	</div>
</div>
