<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

if (\USERS\Identify::UserGROUP()!=1 && \USERS\Identify::UserGROUP()!=2){
	\HTML\Page::Page_404();
}

$id=intval($post->get('id'));
\MAGAZZINO\Games::delAna($id);
\LOGS\Log::write('ANA', 'Articolo id: '.$id.' DELETED', false, \USERS\Identify::UserID());