<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['active','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id=intval($post->get('id'));
$check=trim($post->get('active'));
\MAGAZZINO\Games::updateActive($id, $check);
\LOGS\Log::write('ANA', 'Articolo id: '.$id.' enabled: '.$check, false, \USERS\Identify::UserID());