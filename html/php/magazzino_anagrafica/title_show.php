<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['title','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_title=intval($post->get('id'));
$games_title=trim($post->get('title'));
$data_game=\MAGAZZINO\Games::getSpec($id_title, $games_title);
if (!$data_game){
	\HTML\Page::Page_404();
}

$qta_stock=\MAGAZZINO\Games::qtaStock($data_game['id_ana']);
?>
<div class="ibox float-e-margins">
	<div class="ibox-title">

		<h5>Modifica anagrafica: <strong class="main-title-ana-id"><?php echo $data_game['title'];?></strong></h5>
	</div>
	<div class="ibox-content content_core_options_goblim">
		<p>Dati importati da <code>BGG</code> il
			<code>
			<?php
			$varData=new DateTime($data_game['data_in']);
			echo $varData->format('d-m-Y');
			?></code>
			alle
			<code>
			<?php
			echo $varData->format('H:i');
			?></code>
			.
		</p>

		<div class="row">
			<div class="col-md-3">
				<img style='max-width:350px;max-height:350px;' src="<?php echo $data_game['thumbnail'];?>" />
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="col-md-9">
						<?php
						if ($data_game['active']==1){
							$checked='checked';
							$active='Attivo';
						} else {
							$checked='';
							$active='Inattivo';
						}
						echo "<input value=\"{$data_game['id_ana']}\" type=\"checkbox\" class=\"switch_ana_game\" {$checked}/>";
						echo "<b class=\"txt_check_ana\">{$active}</b>";
						?>
						<div class="">
							Qtà in stock: <span class="stock_qta"><?php echo $qta_stock;?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="form-group">
					<label class="col-md-3 control-label">Titolo attuale</label>
					<div class="col-md-9">
						<input type="text" placeholder="Titolo" class="mag_ana_titolo form-control" value="<?php echo htmlentities($data_game['title'],ENT_QUOTES);?>" readonly/>
					</div>
					<b>N.b.:</b> questo è il titolo che comparirà nelle ricerche.
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Titoli disponibili</label>
					<div class="col-md-9">
						<select class="form-control mag_ana_altTitle" size="4">
							<?php
							$allName=explode('|#|', $data_game['all_title']);
							foreach ($allName as $item) {
								$select='';
								if ($item==$data_game['title']){
									$select='selected';
								}
								echo '<option '.$select.' value="'.htmlentities($item,ENT_QUOTES).'">'.htmlentities($item,ENT_QUOTES).'</option>';
							}
							?>
						</select>
					</div>
				</div>

			</div>

		</div>

		<!-- 2 riga -->
		<div class="row" style='margin-top:10px;'>
			<div class="col-md-12">
				<span class="">
					<b>Attenzione:</b> se disattivi un titolo attivo questo non sarà più disponibile nella ricerca della richiesta prestito.
					<br>
					Le tane che lo avranno in stock lo continueranno a possedere, ma nessun'altra tana ne vedrà la disponibità (eccetto il direttivo).
				</span>
				<p class="m-t-md">
				<button class="btn btn-md btn-success btn-stock-manage-ana" style='float:right;' value="<?php echo $data_game['id_ana'];?>">Gestione Stock</button>
				<?php
				if (\USERS\Identify::UserGROUP()==1 || \USERS\Identify::UserGROUP()==2){
				?>
					<button class="btn btn-md btn-danger btn-del-item-maga-ana" value="<?php echo $data_game['id_ana'];?>">Elimina anagrafica</button>
					<br><u>Solo direttivo e Admin possono eliminare un titolo.</u>
				<?php
				}
				?>
				</p>
			</div>
		</div>
		<!-- end 2 riga -->
	</div>
</div>


<div class="ibox float-e-margins manage-stock-ana"></div>
<script type="text/javascript">
$(document).ready(function() {
	<?php
	if (\USERS\Identify::UserGROUP()==1 || \USERS\Identify::UserGROUP()==2){
	?>
	$('.btn-del-item-maga-ana').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id =$(this).val();
		swal({
			title: "Elimina anagrafica",
			text: "Sei sicuro di voler ELIMINARE questa anagrafica?<br><strong>Attenzione:</strong> tutti i titoli in stock di questa anagrafica verranno eliminati anche se in possesso di altre tane o titoli privati.<br><br><h2>Titoli in stock: "+$('.stock_qta').text()+"</h2>",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ok",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				var postData = new FormData();
				var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/del_ana',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					$('.admin_mag_ana_right').empty();
					$('.mag_ana_gameList').empty();
				})
			}
		});
	});
	<?php
	}
	?>

	$('.btn-stock-manage-ana').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$(this).val();
		$('input').prop('disabled',true);
		$('button').prop('disabled',true);
		$('select').prop('disabled',true);
		$('.manage-stock-ana').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/stock_list',id);
		returnEngine.always(function (returndata) {
			$('.manage-stock-ana').empty().append(returndata);
			$('input').prop('disabled',false);
			$('button').prop('disabled',false);
			$('select').prop('disabled',false);
		});
	});

	$('.mag_ana_altTitle').change(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var txt=$(this).val();
		$('.mag_ana_titolo').val(txt);
		$('.main-title-ana-id').empty().append(txt);
		var id =$('.switch_ana_game').val();
		var postData = new FormData();
		postData.append('title',txt);
		$('input').prop('disabled',true);
		$('select').prop('disabled',true);
		$('button').prop('disabled',true);
		var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/change_title',id);
		returnEngine.always(function (returndata) {
			$('input').prop('disabled',false);
			$('button').prop('disabled',false);
			$('select').prop('disabled',false);
		});
	});

	$('.switch_ana_game').change(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		if ($(this).prop('checked')){
			$('.txt_check_ana').empty().append('Attivo');
		} else {
			$('.txt_check_ana').empty().append('Inattivo');
		}
		var id =$(this).val();
		var postData = new FormData();
		postData.append('active',$(this).prop('checked'));
		$('input').prop('disabled',true);
		$('select').prop('disabled',true);
		$('button').prop('disabled',true);
		var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/activate_ana',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('input').prop('disabled',false);
			$('select').prop('disabled',false);
		});
	});

	var switchery_2 = new Switchery(document.querySelector('.switch_ana_game'), { color: '#1AB394', secondaryColor:'#ED5565' });


});
</script>