<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['CAP','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$newCap=trim($post->get('CAP'));
$readFile=\COMUNI\Istat::listComuni($newCap);
echo '<label class="control-label">Città</label>';
if (count($readFile)==0){
	echo '<input type="text" data-pv="" data-field="citta_goblin" placeholder="--Nessuna città trovata--" class="core_options_dati form-control" value="" required readonly/>';
} else if (count($readFile)==1) {
	echo '<input type="text" data-pv="'.$readFile[0]['Provincia'].'" data-field="citta_goblin" placeholder="Città" class="core_options_dati form-control" value="'.$readFile[0]['Comune'].'" required readonly/>';
} else {
	echo '<select data-field="citta_goblin" class="core_options_dati form-control">';
	foreach ($readFile as $value) {
		echo "<option data-pv=\"{$value['Provincia']}\" value=\"{$value['Comune']}\">{$value['Comune']}</option>";
	}
	echo '</select>';
}