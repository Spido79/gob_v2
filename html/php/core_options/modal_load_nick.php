<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_goblin=intval($post->get('id'));
$data_goblin=\GOBLINS\Manage::getSpec($id_goblin);
if (!$data_goblin){
	\HTML\Page::Page_404();	
}

//$goblins=\GOBLINS\Manage::getAll(-2);

$select='<select class=\'select2_modal_core_optionsNick form-control\'>';
/*foreach ($goblins as $nick) {
	if ($nick['id_goblin']==$id_goblin){
		continue;
	}
	$select.= '<option value=\''.$nick['id_goblin'].'\'>'.$nick['Nick'].'</option>';
}*/


$select.='</select><br/><button style=\'margin-top:3px;\' class=\'btn btn-primary btn-sm btn_confirmPopModalCoreOptionsNick\'>Conferma</button><button style=\'margin-top:3px;margin-left:3px;\' class=\'btn btn-white btn-sm btn_undoPopModalCoreOptionsNick\'>Annulla</button>';

?>
<p>
	Seleziona il nuovo nick per 
	<strong class="text-success">
		<?php 
		echo $data_goblin['cognome_goblin'].' '.$data_goblin['nome_goblin'];
		?>
	</strong>
	nick 
	<strong class="text-success">
		<?php 
		echo $data_goblin['Nick'];
		?>
	</strong>.
</p>
<p>
	<div class="alert alert-danger">
		<strong><i class="fa fa-warning"></i> Attenzione:</strong> il nick attuale verrà completamente <strong>cancellato</strong> dal sistema, e il nuovo nick otterrà i dati di questo nick sostituendo i dati attuali presenti.
	</div>
</p>
<h2 class="text-success core_options_nickActual" style="cursor:pointer;text-decoration: underline;"
	data-html="true" 
	data-toggle="popover" 
	data-placement='bottom'
	data-trigger="click" 
	title="<b>Seleziona il nuovo nick</b>" 
	data-content="<div class='text-center'><?php echo $select;?></div><script type='text/javascript'>$('.select2_modal_core_optionsNick').select2({
		placeholder: 'Seleziona il nuovo nick',
		allowClear: false,
		ajax: {
			transport: function (params, success, failure) {
				var postData = new FormData();
				postData.append('string',params.data.q);
				var returnEngine = call_ajax_page(postData,'core_options/nick_list',$('.hidden_coreOptions_nick').val());
				var ok= returnEngine.always(function (returndata) {
					success(JSON.parse(returndata));
				});
			}
		}
	});</script>" >
	<?php
	echo $data_goblin['Nick'];
	?>
</h2>
<input type="hidden"  class="hidden_coreOptions_nick" value="<?php echo $id_goblin;?>" data-name="<?php echo $data_goblin['cognome_goblin'].' '.$data_goblin['nome_goblin'];?>">
<script type="text/javascript">
var APP_URL = document.querySelector('script[data-id="common.min.js"]').getAttribute('data-site');
$(document).ready(function() {
	$('.core_options_nickActual').popover({trigger: 'manual'}).click(function(e) {
		$(this).popover('show'); /* show popover now it's setup */
		e.preventDefault();
		e.stopImmediatePropagation();
	});


	$(document).on('click', '.btn_undoPopModalCoreOptionsNick', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		$('.core_options_nickActual').popover('hide');
	});

	$(document).on('click', '.btn_confirmPopModalCoreOptionsNick', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTmp=$(this);
		var btnText=btnTmp.html();
		var nick=$('.select2_modal_core_optionsNick').val();
		if (nick<=0){
			return false;
		}
		var nickText=$('.select2_modal_core_optionsNick option[value='+nick+']').text();
		var name=$('.hidden_coreOptions_nick').attr('data-name');
		var id=$('.hidden_coreOptions_nick').val();
		swal({
			title: "Modifica il nick",
			text: "Continuando modificherai <strong>definitivamente</strong> il nick per <strong class='text-success'>"+name+"</strong>. Sicuro di voler continuare?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si, continua!",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTmp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('nick',nick);
				var returnEngine = call_ajax_page(postData,'core_options/change_nick',id);
				returnEngine.always(function (returndata) {
					$('.admin_core_options_right').empty();
					$('.select2_core_data_goblins').val(nickText);
					$('.btn_coreOptions_selectNick').click();
					$('button').prop('disabled',false);
					$('.modal_core_optionsNick').modal('hide');
					btnTmp.empty().append(btnText);
				})
			}
		});	
		
	});

});
</script>