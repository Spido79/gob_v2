<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['card','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_goblin=intval($post->get('id'));
$new_card=intval($post->get('card'));

$data_goblin=\GOBLINS\Manage::getSpec($id_goblin);
if (!$data_goblin){
	\HTML\Page::Page_404();	
}

$cards=\GOBLINS\Manage::getCards();
if (in_array($new_card, $cards)){
	\HTML\Page::Page_404();	
}

$old_card=$data_goblin['tessera_numero'];
if (\GOBLINS\Manage::setCards($id_goblin, $new_card)){
	\LOGS\Log::write('CORE_OPTIONS', 'change_card for user '.$id_goblin." ({$data_goblin['cognome_goblin']} {$data_goblin['nome_goblin']} - {$data_goblin['Nick']} ) FROM {$old_card} TO {$new_card}" , false, \USERS\Identify::UserID());
	\LOGS\Tesserato::write($id_goblin, "Modifica Tessera da {$old_card} a {$new_card} (CORE)", 'fa-id-card-o');
}

