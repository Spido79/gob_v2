<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['nick','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_goblin=intval($post->get('id'));
$new_nick=intval($post->get('nick'));

$data_goblin=\GOBLINS\Manage::getSpec($id_goblin);
if (!$data_goblin){
	\HTML\Page::Page_404();	
}

$gobNew=\GOBLINS\Manage::getSpec($new_nick);
if (!$gobNew){
	\HTML\Page::Page_404();	
}
$new_nick_lable=$gobNew['Nick'];
$old_nick=$data_goblin['Nick'];

$arrayData=array(
	'Nick' 	=> $new_nick_lable,
	);

if (\GOBLINS\Manage::delSpec($new_nick)){
	\LOGS\Log::write('CORE_OPTIONS', 'change_nick deleted old data for user '.$new_nick." ({$gobNew['cognome_goblin']} {$gobNew['nome_goblin']} - {$gobNew['Nick']} )" , false, \USERS\Identify::UserID());
	\LOGS\Tesserato::write($new_nick, "Eliminato (CORE)", 'fa-trash');
}

if (\GOBLINS\Manage::setSpec($id_goblin, $arrayData)){
	\LOGS\Log::write('CORE_OPTIONS', 'change_nick for user '.$id_goblin." ({$data_goblin['cognome_goblin']} {$data_goblin['nome_goblin']} - {$data_goblin['Nick']} ) FROM {$old_nick} TO {$new_nick_lable}" , false, \USERS\Identify::UserID());
	\LOGS\Tesserato::write($id_goblin, "Nick da {$old_nick} a {$new_nick_lable} (CORE)",'fa-refresh');
}



