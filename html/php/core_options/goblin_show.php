<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['nick','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_goblin=intval($post->get('id'));
$nick_goblin=trim($post->get('nick'));
$data_goblin=\GOBLINS\Manage::getSpec($id_goblin, $nick_goblin);
if (!$data_goblin){
	\HTML\Page::Page_404();	
}
?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
	<h5>Modifica dati per : <strong><?php echo $data_goblin['Nick'];?></strong></h5>
	</div>
	<div class="ibox-content content_core_options_goblim">
		<p>Dati nel database <code>tbl_goblins</code>.</p>
		<!-- prima riga -->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-4 control-label">Cognome</label>
					<div class="col-md-8">
						<input type="text" data-field="cognome_goblin" placeholder="Cognome" class="core_options_dati form-control" value="<?php echo $data_goblin['cognome_goblin'];?>" required />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-3 control-label">Nome</label>
					<div class="col-md-9">
						<input type="text" data-field="nome_goblin" placeholder="Nome" class="core_options_dati form-control" value="<?php echo $data_goblin['nome_goblin'];?>" required />
					</div>
				</div>
			</div>
		</div>
		<!-- end prima riga -->

		<!-- 2 riga -->
		<div class="row" style='margin-top:10px;'>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label">Email</label>
					<div class="col-md-10">
						<input type="email" data-field="email_goblin" placeholder="Email" class="core_options_dati form-control" value="<?php echo $data_goblin['email_goblin'];?>" required />
					</div>
				</div>
			</div>
		</div>
		<!-- end 2 riga -->

		<!-- 3 riga -->
		<div class="row" style='margin-top:10px;'>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label">Indirizzo</label>
					<div class="col-md-10">
						<input type="email" data-field="indirizzo_goblin" placeholder="Indirizzo" class="core_options_dati form-control" value="<?php echo $data_goblin['indirizzo_goblin'];?>" required />
					</div>
				</div>
			</div>
		</div>
		<!-- end 3 riga -->

		<!-- 4 riga -->
		<div class="row" style='margin-top:10px;'>
			<div class="col-lg-5">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Città</label>
						<input type="text" data-pv="<?php echo $data_goblin['provincia_goblin'];?>" data-field="citta_goblin" placeholder="Città" class="core_options_dati form-control" value="<?php echo $data_goblin['citta_goblin'];?>" required readonly/>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Cap</label>
						<input type="text" data-field="cap_goblin" placeholder="Cap" class="core_options_dati form-control" value="<?php echo $data_goblin['cap_goblin'];?>" required />
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Prov.</label>
						<input type="text" data-field="provincia_goblin" placeholder="Prov." class="core_options_dati form-control" value="<?php echo $data_goblin['provincia_goblin'];?>" required readonly />
					</div>
				</div>
			</div>
		</div>
		<!-- end 4 riga -->

		<!-- 5 riga -->
		<div class="row" style='margin-top:10px;'>
			<div class="col-md-6 col-lg-3">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Data nascita</label>
						<input type="text" data-field="dataNascita_goblin" placeholder="Data" class="core_options_dati form-control" value="<?php
							$data=new DateTime($data_goblin['dataNascita_goblin']);
							echo $data->format('d/m/Y');?>" required />
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Contatto</label>
						<input type="text" data-field="contatto_goblin" placeholder="Contatto" class="core_options_dati form-control" value="<?php echo $data_goblin['contatto_goblin'];?>">
					</div>
				</div>
			</div>

			<div class="col-lg-5">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Note</label>
						<textarea rows="3" style="resize:vertical;" data-field="note_goblin" placeholder="Note" class="core_options_dati form-control"><?php echo nl2br($data_goblin['note_goblin']);?></textarea>
					</div>
				</div>
			</div>
		</div>
		<!-- end 5 riga -->

		<!-- 6 riga -->
		<div class="row" style='margin-top:10px;'>
			<div class="col-md-6 col-lg-3">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Data Tesserato</label>
						<input type="text" data-field="data_associato" placeholder="Data" class="core_options_dati form-control" value="<?php
							$data=new DateTime($data_goblin['data_associato']);
							echo $data->format('d/m/Y');?>">
					</div>
				</div>
			</div>
			<?php 
			if ($data_goblin['tessera_numero']>0){
				?>
				<div class="col-md-6 col-lg-2">
					<div class="form-group">
						<div class="col-md-12">
							<label class="control-label">N.Tessera</label>
							<br>
							<button class="btn btn-sm btn-success btn_core_options_modalTessera" value="<?php echo $data_goblin['id_goblin'];?>" data-toggle='popover' data-placement='bottom' data-content='Modifica il numero di tessera' data-html='true' title=''>
								<i class="fa fa-credit-card"></i> 
								<?php
									echo str_pad($data_goblin['tessera_numero'], 5,"0",STR_PAD_LEFT);
								?>
							</button>
						</div>
					</div>
				</div>
				<?php
			}
			?>
			<div class="col-md-6 col-lg-3">
					<div class="form-group">
						<div class="col-md-12">
							<label class="control-label">Nick Nuovo</label>
							<br>
							<button class="btn btn-sm btn-warning btn_core_options_modalNick" value="<?php echo $data_goblin['id_goblin'];?>" data-toggle='popover' data-placement='bottom' data-content='Modifica il nick attuale con uno nuovo' data-html='true' title='Attenzione!'>
								<i class="fa fa-recycle"></i> 
								Sostituisci Nick
							</button>
						</div>
					</div>
				</div>
		</div>
		<!-- end 6 riga -->

		<div class="row" style='margin-top:10px;'>
			<div class="form-group">
				<div class="col-md-12 text-right">
					<button value="<?php echo $data_goblin['id_goblin']?>" class="btn btn-sm btn-danger btn_modifyUser_CoreOptions">
						<i class="fa fa-pencil"></i> Modifica			
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	//format field
		var actualCap=$('input[data-field="cap_goblin"]').val();
		$('input[data-field="dataNascita_goblin"]').datepicker();
		$('input[data-field="data_associato"]').datepicker();
		
		$('.core_options_dati[data-field="citta_goblin"]').change(function(event) {
			event.preventDefault();
			event.stopImmediatePropagation();
			var prov=$(this).attr('data-pv');
			if (typeof prov =='undefined'){
				prov=$('.core_options_dati[data-field="citta_goblin"] option:selected').attr('data-pv');
			}
			$('.core_options_dati[data-field="provincia_goblin"]').val(prov);
		});

		$('input[data-field="cap_goblin"]').change(function(event) {
			event.preventDefault();
			event.stopImmediatePropagation();
			var newCap=$(this).val();
			if (newCap==actualCap && actualCap!=''){
				return false;
			}
			actualCap==newCap;
			var postData = new FormData();
			postData.append('CAP',newCap);
			var returnEngine = call_ajax_page(postData,'core_options/city_list',0);
			returnEngine.always(function (returndata) {
				$('.core_options_dati[data-field="citta_goblin"]').parent().empty().append(returndata);
				var prov=$('.core_options_dati[data-field="citta_goblin"]').attr('data-pv');
				if (typeof prov =='undefined'){
					prov=$('.core_options_dati[data-field="citta_goblin"] option:selected').attr('data-pv');
				}
				$('.core_options_dati[data-field="provincia_goblin"]').val(prov);
			})
		});
	//END format field

	$('.btn_modifyUser_CoreOptions').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$(this).val();
		var btnTmp=$(this);
		var btnText=btnTmp.html();
		//verify all data
		$('.core_options_dati').parent().removeClass('has-error');
		var errorT=0;
		$('.core_options_dati').each(function(index, el) {
			var required=$(this).prop('required');
			if (required && $.trim($(this).val())==''){
				$(this).parent().addClass('has-error');
				errorT=1;
			}
		});
		if (errorT==1){
			return false;
		}
		swal({
			title: "Modifica goblin",
			text: "Continuando modificherai <strong>definitivamente</strong> i dati presenti con quelli attuali. Sicuro di voler continuare?",
			type: "success",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si, continua!",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTmp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				$('.core_options_dati').each(function(index, el) {
					var field=$(this).attr('data-field');
					var valTemp=$(this).val();
					postData.append(field, valTemp);
				});
				var returnEngine = call_ajax_page(postData,'core_options/change_data',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					btnTmp.empty().append(btnText);					
					$('.content_core_options_goblim').empty().append('<div class="row"><div class="col-xs-12 text-green text-center"><i class="fa fa-check fa-4x div-circle-success animated rubberBand" ></i><h1>Modifica effettuata</h1></div></div>');	
				})
			}
		});	
	});

	$(".btn_core_options_modalTessera").popover({ trigger: "hover focus" });	
	$('.btn_core_options_modalTessera').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id= $(this).val();
		$('button').prop('disabled',true);
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'core_options/modal_load',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.body_core_options').empty().append(returndata);
			$('.modal_core_options').modal('show');
		})
	});

	$(".btn_core_options_modalNick").popover({ trigger: "hover focus" });	
	$('.btn_core_options_modalNick').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id= $(this).val();
		$('button').prop('disabled',true);
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'core_options/modal_load_nick',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.body_core_optionsNick').empty().append(returndata);
			$('.modal_core_optionsNick').modal('show');
		})
	});

	
});
</script>