<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
\GOBLINS\Manage::resetGoblin();
$idTane=intval($post->get('id'));
$readFile=\GOBLINS\Manage::getAll($idTane);
$array=array('Nick' => array());
foreach ($readFile as $value) {
	$array['Nick'][]=$value['Nick'];
}
header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);
