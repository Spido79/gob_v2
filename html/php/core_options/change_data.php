<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData([
			'cognome_goblin',
			'nome_goblin',
			'email_goblin',
			'indirizzo_goblin',
			'citta_goblin',
			'cap_goblin',
			'provincia_goblin',
			'dataNascita_goblin',
			'contatto_goblin',
			'note_goblin',
			'data_associato',
			'id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_goblin=intval($post->get('id'));
$data_goblin=\GOBLINS\Manage::getSpec($id_goblin);
if (!$data_goblin){
	\HTML\Page::Page_404();	
}
$arrayData=array(
	'cognome_goblin' 	=> null,
	'nome_goblin' 		=> null,
	'email_goblin'		=> null,
	'indirizzo_goblin'	=> null,
	'citta_goblin'		=> null,
	'cap_goblin'		=> null,
	'provincia_goblin'	=> null,
	'dataNascita_goblin'=> null,
	'data_associato'	=> null,
	'contatto_goblin'	=> null,
	'note_goblin'		=> null
	);
foreach ($arrayData as $key => $value) {
	$arrayData[$key]=trim($post->get($key));

	if ($key!='note_goblin' && $key!='contatto_goblin'){
		if ($arrayData[$key]==''){
			\HTML\Page::Page_404();
		}
	}

	//date
	if ($key=='dataNascita_goblin' || $key=='data_associato'){
		\DATESPACE\Convert::dmyTOymd($arrayData[$key]);
	}
}
\GOBLINS\Manage::setSpec($id_goblin, $arrayData);

\LOGS\Log::write('CORE_OPTIONS', 'change_data for user '.$id_goblin." ({$data_goblin['cognome_goblin']} {$data_goblin['nome_goblin']} - {$data_goblin['Nick']})" , false, \USERS\Identify::UserID());

\LOGS\Tesserato::write($id_goblin, "Modificato (CORE)", 'fa-database');