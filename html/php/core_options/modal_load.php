<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_goblin=intval($post->get('id'));
$data_goblin=\GOBLINS\Manage::getSpec($id_goblin);
if (!$data_goblin){
	\HTML\Page::Page_404();	
}

$cards=\GOBLINS\Manage::getCards();
$select='<select class=\'select2_modal_core_optionsCard form-control\'>';
for ($i=1; $i <=max($cards)+1 ; $i++) { 
	if (!in_array($i, $cards)){
		$select.= '<option value=\''.$i.'\'>'.str_pad($i, 5,"0",STR_PAD_LEFT).'</option>';
	}
}
$select.='</select><br/><button style=\'margin-top:3px;\' class=\'btn btn-primary btn-sm btn_confirmPopModalCoreOptions\'>Conferma</button><button style=\'margin-top:3px;margin-left:3px;\' class=\'btn btn-white btn-sm btn_undoPopModalCoreOptions\'>Annulla</button>';

?>
<p>
	Seleziona il nuovo numero di tessera per 
	<strong class="core_options_nameActual text-success">
		<?php 
		echo $data_goblin['cognome_goblin'].' '.$data_goblin['nome_goblin'];
		?>
	</strong>
	nick 
	<strong class="core_options_nameActual text-success">
		<?php 
		echo $data_goblin['Nick'];
		?>
	</strong>
</p>
<h2 class="text-success core_options_cardActual" style="cursor:pointer;text-decoration: underline;"
	data-html="true" 
	data-toggle="popover" 
	data-placement='bottom'
	data-trigger="click" 
	title="<b>Seleziona la nuova tessera</b>" 
	data-content="<div class='text-center'><?php echo $select;?></div><script type='text/javascript'>$('.select2_modal_core_optionsCard').select2({placeholder: 'Seleziona la nuova tessera',allowClear: false});</script>" >
	<?php
	echo str_pad($data_goblin['tessera_numero'], 5,"0",STR_PAD_LEFT);
	?>
</h2>
<input type="hidden"  class="hidden_coreOptions_card" value="<?php echo $id_goblin;?>" data-name="<?php echo $data_goblin['cognome_goblin'].' '.$data_goblin['nome_goblin'];?>">
<script type="text/javascript">
$(document).ready(function() {
	$('.core_options_cardActual').popover({trigger: 'manual'}).click(function(e) {
		$(this).popover('show'); /* show popover now it's setup */
		e.preventDefault();
		e.stopImmediatePropagation();
	});

	$('.select2_modal_core_optionsCard').select2({
		placeholder: "Seleziona la nuova tessera",
		allowClear: false
	});
	$(document).on('click', '.btn_undoPopModalCoreOptions', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		$('.core_options_cardActual').popover('hide');
	});

	$(document).on('click', '.btn_confirmPopModalCoreOptions', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		//salvo nuova tessera, update e chiudo modale
		//chiedo SWAL se ok
		var btnTmp=$(this);
		var btnText=btnTmp.html();
		var card=$('.select2_modal_core_optionsCard').val();
		var name=$('.hidden_coreOptions_card').attr('data-name');
		var id=$('.hidden_coreOptions_card').val();
		swal({
			title: "Modifica la tessera",
			text: "Continuando modificherai <strong>definitivamente</strong> la tessera per <strong class='text-success'>"+name+"</strong>. Sicuro di voler continuare?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si, continua!",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTmp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('card',card);
				var returnEngine = call_ajax_page(postData,'core_options/change_card',id);
				returnEngine.always(function (returndata) {
					$('.btn_coreOptions_selectNick').click();
					$('button').prop('disabled',false);
					$('.modal_core_options').modal('hide');
					btnTmp.empty().append(btnText);
				})
			}
		});	
		
	});

});
</script>