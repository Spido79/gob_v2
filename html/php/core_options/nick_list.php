<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['string','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

//echo json formatted a list of nick (max 50?)
$id_goblin=intval($post->get('id'));
$string=trim($post->get('string'));
$dataResult=array();
$fieldMap=array('id','text');
$dataResult['results']=\GOBLINS\Manage::getAll(-2,50,$id_goblin,$string,1,$fieldMap);

$dataResult['pagination']=array("more",true);
echo json_encode($dataResult);
