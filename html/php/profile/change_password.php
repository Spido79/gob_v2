<?php 
if (isset($post)==false){
    $virtualPath='..';
    require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['actualPass','newPass','newBPass','id','redirect_to'])) {
    \HTML\Page::Page_404();
}

$actualPass=trim($post->get('actualPass'));
$newPass=trim($post->get('newPass'));
$newBPass=trim($post->get('newBPass'));

if (strlen($newPass)<5){
	echo 411;
	exit;
}

if ($newPass!=$newBPass){
	echo 412;
	exit;
}

if ($actualPass==$newPass){
	echo 409;
	exit;
}

$user=new \USERS\Detail(\USERS\Identify::UserID());
if ($user->get('user_password')!=\PASSWORD\Generator::generateEncode($actualPass)){
	echo 406;
	exit;	
}

$user->updatePass(\PASSWORD\Generator::generateEncode($newPass), $newPass);
echo 200;
\LOGS\Log::write('CHANGE PASSWORD', 'change his password' , false, \USERS\Identify::UserID());