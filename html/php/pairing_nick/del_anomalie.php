<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_goblin=intval($post->get('id'));
LOGS\Log::write('ANOMALIE', "Delete id: ".$id, false, \USERS\Identify::UserID());
$gobNew=\GOBLINS\Manage::getSpec($id_goblin);
if (!$gobNew){
	\HTML\Page::Page_404();	
}

\GOBLINS\Manage::delSpec($id_goblin);