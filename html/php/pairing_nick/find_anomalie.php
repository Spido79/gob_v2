<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$array=array(
	'message' => '',
	'table' => array(),
);
$anomalie=\GOBLINS\Manage::anomalieList();
if (count($anomalie)<=0){
	$array['message']='Nessuna anomalia trovata.';
} else {
	$array['message']='';
	$array['table']=$anomalie;
}

echo json_encode($array);