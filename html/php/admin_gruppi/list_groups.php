<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Gruppi gestione pagine</h5>
	</div>
	<div class="ibox-content">
		<table class="table table-hover no-margins">
			<thead>
				<tr>
					<th>Nome Gruppo</th>
					<th>Pagine Associate</th>
					<th>N. Utenti</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$groups=new USERS\Groups();
				foreach ($groups->get() as $item) {
					$countUsers=count($groups->usersGroup($item['id_group']));

					echo "<tr>
					<td>{$item['label_group']}</td>
					<td><small>";
						$count=0;
						foreach ($groups->pagesGroup($item['id_group']) as $itemPages) {
							
							$menuWrite=\MENU\Primary::searchLabel($itemPages, \MENU\Primary::$Menu);
							if (is_null($menuWrite)){
								continue;
								//echo "<i><code>{$itemPages}</code><i>";
							}
							if ($count>0){
								echo ', ';
							}
							echo $menuWrite;
							$count++;
						}
						echo "	</small></td>
						<td class='text-success text-center'>{$countUsers}</td>
						<td>
							<button class='btn btn-xs btn-success btn_editGroup' value='{$item['id_group']}' data-toggle='popover' data-placement='left' data-content='Modifica questo gruppo' data-html='true' title=''><i class='fa fa-pencil'></i></button>
							<button class='btn btn-xs btn-danger btn_editPeopleGroup' value='{$item['id_group']}' data-toggle='popover' data-placement='right' data-content='<nobr>Utenti affiliati</nobr>' data-html='true' title=''><i class='fa fa-users'></i></button>
						</td>
					</tr>";
				}
				?>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<button class='btn btn-xs btn-primary btn_editGroup' value='0' data-toggle='popover' data-placement='left' data-content='Aggiungi un nuovo gruppo' data-html='true' title=''><i class='fa fa-plus'></i></button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_editGroup').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var id=$(this).val();
		$('button').prop('disabled',true);
		$('.admin_gruppi_box_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'admin_gruppi/manage_groups',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.admin_gruppi_box_right').empty().append(returndata);
		})
	});

	$('.btn_editPeopleGroup').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var id=$(this).val();
		admin_gruppi_box_right_users(id);
	});


	$(".btn_editGroup").popover({ trigger: "hover focus" });
	$(".btn_editPeopleGroup").popover({ trigger: "hover focus" });
	
});
</script>
