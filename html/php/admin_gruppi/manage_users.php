<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_group=intval($post->get('id'));
if ($id_group==0){
	\HTML\Page::Page_404();	
}

$groups=new USERS\Groups($id_group);
$arrayToSplit=$groups->usersGroup($id_group);
$user_split=array();
if (count($arrayToSplit)>0){
	$user_split = array_chunk($arrayToSplit, ceil(count($arrayToSplit) / 2));	
}

?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Gestione utenti del <span class="text-success"><?php echo $groups->get()[0]['label_group'];?></span> </h5>
	</div>
	<div class="ibox-content">
		<div class="form-horizontal">
			<p>Vedrai la lista degli utenti affiliati a questo gruppo e potrai assocairne di nuovi, ricordati che un utente può essere affiliato ad <strong>UN SOLO</strong> gruppo.</p>
		</div>
		<div class="row">
			<div class="col-xs-6 text-center">
				<table class="table table-bordered table-">
					<thead>
						<th>Nome Utente</th>
					</thead>
					<tbody>
					<?php
					//elenco tutti gli utenti
					if (count($user_split)>0){
						foreach ($user_split[0] as $item) {
							$buttonModify="<button data-group='{$id_group}' value='{$item['user_id']}' class='pull-right btn btn-danger btn_modSpecUserGroup btn-xs' data-toggle='popover' data-placement='left' data-content='Cambia gruppo a questo utente' data-html='true' title=''>
											<i class='fa fa-pencil'></i>
										</button>";
							if ($item['user_id']==\USERS\Identify::UserID() || $item['user_id']==1){
								$buttonModify=null;
							}
							echo "<tr>
									<td class='text-left'>
										<img class='profile_personal_img' src='".\IMG\Logo::png($item['user_id'],'img/profiles')."' style='width:20px;' />
										{$item['user_surname_name']}
										{$buttonModify}
									</td>
								</tr>";
						}	
					} else {
						echo "<tr>
								<td class='text-left'><i>Nessun utente assegnato</i></td>
							</tr>";
					}
					?>		
					</tbody>
				</table>
				
			</div>
			<div class="col-xs-6 text-center">
				<?php
				//elenco tutti gli utenti
				if (count($user_split)>1){
					?>
					<table class="table table-bordered table-">
						<thead>
							<th>Nome Utente</th>
						</thead>
						<tbody>
						<?
						foreach ($user_split[1] as $item) {
							echo "<tr>
									<td class='text-left'>
										<img class='profile_personal_img' src='".\IMG\Logo::png($item['user_id'],'img/profiles')."' style='width:20px;' />
										{$item['user_surname_name']}
										<button data-group='{$id_group}' value='{$item['user_id']}' class='pull-right btn btn-danger btn_modSpecUserGroup btn-xs' data-toggle='popover' data-placement='left' data-content='Cambia gruppo a questo utente' data-html='true' title=''>
											<i class='fa fa-pencil'></i>
										</button>
									</td>
								</tr>";
						}	
						?>		
						</tbody>
					</table>
				<?php 
				}
				?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_modSpecUserGroup').popover({ trigger: "hover focus" });
	$('.btn_modSpecUserGroup').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var buttonTmp= $(this);
		var id = buttonTmp.val();
		var group = buttonTmp.attr('data-group');
		buttonTmp.empty().append('<i class="fa fa-pulse fa-spinner"></i>');
		$('button').prop('disabled',true);
		var postData = new FormData();
		postData.append('group',group);
		var returnEngine = call_ajax_page(postData,'admin_gruppi/modal_user',id);
		returnEngine.always(function (returndata) {
			$('.body_admin_gruppi').empty().append(returndata);
			$('.modal_admin_gruppi').modal('show');
			$('button').prop('disabled',false);
			buttonTmp.empty().append('<i class="fa fa-pencil"></i>');
		})
	});
});
</script>