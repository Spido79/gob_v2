<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['permissionList','nome','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_group=intval($post->get('id'));
$nome=trim($post->get('nome'));
$permissionList=json_decode($post->get('permissionList'));
\USERS\Groups::updateGroup($id_group, $nome, $permissionList);
\LOGS\Log::write('ADMIN_GRUPPI', 'save_groups for group '.$id_group." NAME {$nome} ".json_encode($permissionList) , false, \USERS\Identify::UserID());