<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_group=intval($post->get('id'));
$groups=new USERS\Groups($id_group);

$groupName=null;

$textTitle='<i class="fa fa-plus"></i> Inserisci un <span class="text-primary">NUOVO</span> gruppo';
$btnType='btn-primary';
$iconType='fa-plus';
$textType='Aggiungi';
$textSwal="<span class='text-primary'>AGGIUNGERE</span>";
$pages_group=array();
$readonly='';
if ($id_group==1 || $id_group==2){
	$readonly='readonly';
}
if ($id_group>0 && count($groups->get())>0){
	$textSwal="<span class='text-success'>MODIFICARE</span>";
	$pages_group=$groups->pagesGroup($id_group);
	$btnType='btn-success';
	$iconType='fa-pencil';
	$textType='Modifica';
	$groupName=$groups->get()[0]['label_group'];
	$textTitle='<i class="fa fa-pencil"></i> Modifica il gruppo: <span class="text-success">'.$groups->get()[0]['label_group'].'</span>';
}

?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5><?php echo $textTitle;?></h5>
	</div>
	<div class="ibox-content">
		<div class="form-horizontal">
			<p>Ogni utente ha un gruppo affiliato, queste sono i permessi nel sito di gestione.</p>
			<div class="form-group">
				<label class="col-lg-2 control-label">Gruppo</label>
				<div class="col-lg-10">
					<input type="text" placeholder="Nome" <?php echo $readonly;?> class="txt_nameGroup form-control" value="<?php echo $groupName;?>">
					<span class="help-block m-b-none"><strong>Obbligatorio:</strong> il nome del gruppo è puramente a titolo descrittivo.</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				<table class="table table-bordered table-hover">
					<thead>
						<th>&nbsp;</th>
						<th>Nome Pagina</th>
						<th>Nome Fisico</th>
					</thead>
					<tbody>
					<?php
					//elenco tutte le pagine

					foreach (\HTML\Page::ListPage_Phisical() as $key => $item) {
						$voiceMenu=\MENU\Primary::searchLabel($item, \MENU\Primary::$Menu);
						if (is_null($voiceMenu)){
							continue;
						}
						$checked=null;
						foreach ($pages_group as $itemPages) {
							if ($itemPages==$item){
								$checked=' checked ';
							}
						}
						echo "<tr>
								<td>
									<div class='checkbox checkbox-success' style='margin-top:0;margin-bottom:0;'>
										<input value=\"{$item}\" class='checkbox_selection_pageGroup' id='check_groupPage_{$key}' type='checkbox' {$checked}>
										<label for='check_groupPage_{$key}'></label>
									</div>
								</td>
								<td class='text-left'>{$voiceMenu}</td>
								<td class='text-left'>{$item}</td>
							</tr>";
					}
					?>
					</tbody>
				</table>

			</div>
			<div class="col-xs 12 text-right">
				<button value="<?php echo $id_group;?>" class="btn btn_AddModGroup btn-xs <?php echo $btnType;?>" data-text="<?php echo $textSwal;?>"><i class="fa <?php echo $iconType;?>"></i> <?php echo $textType;?></button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_AddModGroup').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var textSwal=$(this).attr('data-text');
		var btnTemp= $(this);
		var nome=$.trim($('.txt_nameGroup').val());
		var id=$(this).val();
		$('.txt_nameGroup').parent().removeClass('has-error');
		if (nome.length<=0){
			$('.txt_nameGroup').parent().addClass('has-error');
			return false;
		}
		var permissionList=[];
		$('.checkbox_selection_pageGroup:checked').each(function(index, el) {
			permissionList.push($(this).val());
		});
		swal({
			title: "Gestione gruppi",
			text: "Sei sicuro di voler "+textSwal+" il gruppo <strong>"+nome+"</strong> con i relativi <strong>"+permissionList.length+" permessi</strong> assegnati?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ok",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('permissionList',JSON.stringify(permissionList));
				postData.append('nome',nome);
				var returnEngine = call_ajax_page(postData,'admin_gruppi/save_groups',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					admin_gruppi_box_left();
					$('.admin_gruppi_box_right').empty();
				})
			}
		});
	});
});
</script>