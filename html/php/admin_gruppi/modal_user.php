<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['group','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_group=intval($post->get('group'));
$id=intval($post->get('id'));

if ($id_group==0 || $id==0){
	\HTML\Page::Page_404();	
}
$user=new \USERS\Detail($id);
$groups=new USERS\Groups();
?>

<div class="row">
	<div class="col-xs-12 text-center">
		<div class="contact-box">
			<div class="col-sm-4">
				<div class="text-center">
					<img alt="profilo" style='width:80px;' src="<?php echo \IMG\Logo::png($id, 'img/profiles');?>" class="profile_personal_img img-circle m-t-xs img-responsive" />
				</div>
			</div>
			<div class="col-sm-8">
				<h3><strong>
					<?php 
					echo $user->get('user_name');
					echo ' ';
					echo $user->get('user_surname');
					?>
				</strong></h3>
				<p><i class="fa fa-envelope-o"></i> <?php echo $user->get('user_login');?></p>

				<select class="form-control select_adminGruppi_newGroup" data-group="<?php echo $id_group; ?>" data-id="<?php echo $id; ?>">
					<?php
					foreach ($groups->get() as $item) {
						$checked='';
						if ($item['id_group']==$user->get('user_idGroup')){
							$checked='selected';
						}
						echo "<option {$checked} value='{$item['id_group']}'>{$item['label_group']}</option>";
					}
					?>
				</select>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$(document).on('click', '.btn_AdminGruppiModUser', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$('.select_adminGruppi_newGroup').attr("data-id");
		var old_group=$('.select_adminGruppi_newGroup').attr("data-group");
		var new_group=$('.select_adminGruppi_newGroup').val();
		//chiedi conferma e salva e aggiorna
		var btnTmp=$(this);
		var btnText=$(this).html();
		swal({
			title: "Modifica il gruppo",
			text: "Continuando modificherai il gruppo a questo utnete. Sei sicuro?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si, continua!",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTmp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('old_group',old_group);
				postData.append('new_group',new_group);
				var returnEngine = call_ajax_page(postData,'admin_gruppi/change_group',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					admin_gruppi_box_left();
					admin_gruppi_box_right_users(old_group);
					$('.modal_admin_gruppi').modal('hide');
					btnTmp.empty().append(btnText);
				})
			}
		});		
	});
});
</script>