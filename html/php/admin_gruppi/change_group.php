<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['old_group','new_group','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$old_group=intval($post->get('old_group'));
$new_group=intval($post->get('new_group'));
$user_id=intval($post->get('id'));

if ($user_id==\USERS\Identify::UserID()){
	\HTML\Page::Page_404();
}

$userTemp= new \USERS\Detail($user_id);
$userTemp->updateGroup($old_group, $new_group);
\LOGS\Log::write('ADMIN_GRUPPI', 'change_group for user '.$user_id." FROM {$old_group} TO {$new_group}" , false, \USERS\Identify::UserID());