<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$email=intval($post->get('id'));

$specEmail=\MAIL\Create::getSpec($email);
if (!$specEmail){
	$arrayRet=array(
		'subject' 	=> '',
		'body'		=> ''
	);
} else {
	$html=\MAIL\Create::closetags(gzuncompress($specEmail['mail_message']));
	$finalizeHtml=\MAIL\Create::removeSpecialTag($html);
	$arrayRet=array(
		'subject' 	=> gzuncompress($specEmail['mail_subject']),
		'body'		=> $finalizeHtml,
	);
}
echo json_encode($arrayRet);