<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$arrayRet=array(
	'sent' 		=> \MAIL\Create::countMail(1),
	'sending'	=> \MAIL\Create::countMail(0)
);
echo json_encode($arrayRet);