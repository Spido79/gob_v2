<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['page','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$inbox_type=intval($post->get('id'));
$page=intval($post->get('page'));
switch ($inbox_type) {
	case 2:
		$mail_list=\MAIL\Create::get(0,null, $page);
		$mail_count=\MAIL\Create::get(0,null);
		$textValue='In Uscita';
		break;

	case 3:
		$mail_list=\MAIL\Create::get(null, 'Gest. Goblins', $page);
		$mail_count=\MAIL\Create::get(null, 'Gest. Goblins');
		$textValue='Gest. Goblins';
		break;
	case 4:
		$mail_list=\MAIL\Create::get(null, 'Blocco IP', $page);
		$mail_count=\MAIL\Create::get(null, 'Blocco IP');
		$textValue='Blocco IP';
		break;
	case 5:
		$mail_list=\MAIL\Create::get(null, 'Credenziali', $page);
		$mail_count=\MAIL\Create::get(null, 'Credenziali');
		$textValue='Credenziali';
		break;
	
	default:
		$mail_list=\MAIL\Create::get(1, null, $page);
		$mail_count=\MAIL\Create::get(1, null);
		$textValue='Inviata';
		break;
}

$totalEmail=count($mail_count);
$totalPage=ceil($totalEmail/\MAIL\Create::$offset);
?>
<div class="animated fadeInRight">
	<div class="mail-box-header">
		<h2>
			<?php echo $textValue. ' ('.$totalEmail.')';?>
		</h2>
		<div class="mail-tools tooltip-demo m-t-md">
			<div class="btn-goup pull-right">
				<?php 
					echo ' Pagina  '.($page+1).' di '.($totalPage).' ';
					if ($page>0){
						echo '<button class="btn btn-white btn-sm btn_folder_list" data-id="'.$inbox_type.'" data-page="'.($page-1).'"><i class="fa fa-arrow-left"></i></button>';
					}
					
					if (($page+1) < $totalPage){
						echo '<button class="btn btn-white btn-sm btn_folder_list" data-id="'.$inbox_type.'" data-page="'.($page+1).'"><i class="fa fa-arrow-right"></i></button>';
					}

				?>
				
				

			</div>
			<button class="btn btn-white btn-sm btn_refresh_folder" data-id="<?php echo $inbox_type;?>">
				<i class="fa fa-refresh"></i> Refresh
			</button>
		</div>
	</div>
	<div class="mail-box">
		<table class="table table-hover table-mail">
			<tbody>
				<?php
				$interval = new DateInterval('P11M');
				if (count($mail_list)==0){
					echo '<tr class="read">
							<td>
								<i class="fa fa-remove"></i>
								<i>Nessuna email trovata per questa cartella.</i>
							</td>
							
						</tr>';	 
				}
				foreach ($mail_list as $mail) {
					$today=new DateTime();
					$today->setTime(0,0,0);
					$icon= $mail['sent']==0? 'fa-pause':'fa-paper-plane-o';
					$date=new DateTime($mail['data_in']);
					$writeDate=\DATESPACE\Convert::dM($mail['data_in']);
					if ($date>$today){
						$writeDate=$date->format('H:i');
					}
					$today->sub($interval);
					if ($date<$today){
						$writeDate=\DATESPACE\Convert::dMy($mail['data_in']);
					}

					echo '<tr class="read">
							<td><i class="fa '.$icon.'"></i></td>
							<td>
								<a class="detail_mail" data-value="'.$mail['id'].'">'.$mail['name_to'].'</a>
							</td>
							<td>
								<a class="detail_mail" data-value="'.$mail['id'].'">'.$mail['mail_to'].'</a>
							</td>
							<td>
								<a class="detail_mail" data-value="'.$mail['id'].'">'.gzuncompress($mail['mail_subject']).'</a>
							</td>
							<td class="text-right mail-date">'.$writeDate.'</td>
						</tr>';	
				}
				?>
		</tbody>
		</table>
	</div>
</div>