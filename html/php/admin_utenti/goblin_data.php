<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
\GOBLINS\Manage::resetGoblin();
$goblin=intval($post->get('id'));

$readFile=\GOBLINS\Manage::getSpec($goblin);
$array=array('Goblin' => $readFile);
header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);
