<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
if ($userActive->UserID()==1){
	$visPass="";
 } else {
	$visPass="hidden";
}

?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Gestione utenti accesso portale</h5>
	</div>
	<div class="ibox-content">
		<table class="table table-striped table-hover no-margins dataTable_admin_utenti_list">
			<thead>
				<tr>
					<th class="no-sort" style='width:90px;'>&nbsp;</th>
					<th>Utente</th>
					<th>Login</th>
					<th>Gruppo</th>
					<th class="<?php echo $visPass;?>">Pass</th>
					<th>Nick</th>
					<th>Contatto</th>
					<th class="no-sort" class='text-center'>Abilitato</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach (\USERS\Detail::getAll() as $item) {
					$checked='checked';
					if ($item['user_active']==0){
						$checked=null;
					}

					$disabled=null;
					if ($item['user_id']==1){
						$disabled='disabled';
					}

					$disabled_mod=null;
					if ($item['user_active']==0){
						$disabled_mod='disabled';
					}
					if ($userActive->UserID()==1){
						$pssel=urlencode(\PASSWORD\Generator::getPswClean($item['user_id']));
					} else {
						$pssel="";
					}
					
					echo "<tr>
							<td class='text-center'>
								<div class='btn-group'>
									<button data-toggle='dropdown' data-user=\"{$item['user_id']}\" data-active=\"{$item['user_active']}\" {$disabled_mod} class='btn btn-success btn-xs dropdown-toggle'>
										Action <span class='caret'></span>
									</button>
									<ul class='dropdown-menu'>
										<li><a class='modify_usersite' data-mod='1' data-user=\"{$item['user_id']}\">Modifica</a></li>
										<li><a class='modify_userpicture' data-pic=\"".\IMG\Logo::png($item['user_id'],'img/profiles')."\" data-user=\"{$item['user_id']}\">Cambia immagine</a></li>
										<li class='divider'></li>
										<li><a class='send_credential' data-mail=\"{$item['user_login']}\">Invia recupero password</a></li>
									</ul>
								</div>
							</td>
							<td>
								<img data-picuser='{$item['user_id']}' class='profile_personal_img img-circle' src='".\IMG\Logo::png($item['user_id'],'img/profiles')."' style='width:20px;' />
								<small>{$item['user_surname']} {$item['user_name']}</small>
							</td>
							<td><small>{$item['user_login']}</small></td>
							<td>{$item['label_group']}</td>
							<td class='{$visPass}'><button data='{$pssel}' class='btn btn-xs convertPassAster' data-placement='top' style='cursor:pointer' title='Doppio click per vedere la password'>********</span></td>";
					$getGoblin = GOBLINS\Manage::getSpec(0, null, $item['user_login']);
					if ($getGoblin){
						echo "<td>{$getGoblin['Nick']}</td>";
					} else {
						//try con nome/cognome
						$paramsT=array(
							'having_nome'		=>$item['user_name'],
							'having_cognome'	=>$item['user_surname'],
						);
						$getGoblin=\GOBLINS\Manage::getAll(0, $paramsT);
						if (isset($getGoblin[0])){
							echo "<td>{$getGoblin[0]['Nick']}</td>";
						} else {
							echo "<td><i>--non trovato--</td>";	
						}
						
					}
					echo	"<td>{$item['user_telephone']}</td>
							<td class='text-center'>
								<div class='checkbox checkbox-danger checkbox-circle' style='margin:0;'>
									<input {$disabled} id='check_admin_utenti_abilitati_{$item['user_id']}' type='checkbox' {$checked} class='checkAdmin_utenti_enable' value='{$item['user_id']}'/>
									<label for='check_admin_utenti_abilitati_{$item['user_id']}'></label>
								</div>
							</td>
						</tr>";
				}
				?>
			</tbody>
		</table>

	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	
	$('.convertPassAster').tooltip();

	$('.convertPassAster').dblclick(function(event) {
		$('.convertPassAster').empty().append('********');
		$('.convertPassAster').removeClass('btn-danger');
		$('.convertPassAster').removeClass('btn-success');
		event.stopImmediatePropagation();
		event.preventDefault();
		if ($(this).attr('data')==''){
			$(this).addClass('btn-success');
			$(this).empty().append('-no password-');
			return;
		}
		$(this).empty().append($(this).attr('data'));
		$(this).addClass('btn-danger');
	});

	$('.convertPassAster').mouseout(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		$(this).empty().append('********');
		$(this).removeClass('btn-danger');
		$(this).removeClass('btn-success');
	});

	$('.convertPassAster').focusout(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		$(this).empty().append('********');
		$(this).removeClass('btn-danger');
		$(this).removeClass('btn-success');
	});

	$('.modify_userpicture').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var user=$(this).attr('data-user');
		var picture=$(this).attr('data-pic');
		picture_user(user, picture);
	});
	
	$('.modify_usersite').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var user=$(this).attr('data-user');
		var mod=$(this).attr('data-mod');
		add_mod_user(user,mod);
	});

	$('.send_credential').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var email=$(this).attr('data-mail');
		swal({
			title:"Invia credenziali",
			text: "Sei sicuro di voler inviare le credenziali innescando la procedura di <strong>recupero password</strong> per l'utente?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si, continua!",
			html:true,
			closeOnConfirm: false,
  			showLoaderOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				var postData = new FormData();
				postData.append('email',email);
				var returnEngine = call_ajax_page(postData,'admin_utenti/send_credential',0);
				returnEngine.always(function (returndata) {
					 swal("Fatto!","Procedura di richiesta per il recupero passowrd inviata. Potranno volerci un paio di minuti alla ricezione dell'email","success");
				})
			}
		});
	});
	$('.checkAdmin_utenti_enable').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var thisCheck=$(this);
		var check=thisCheck.prop('checked');
		var id=thisCheck.val();
		var abilita="Abilita";
		var abilitare="ABILITARE";
		if (!check){
			abilita="Disabilita";
			abilitare="DISABILITARE";
		}
		swal({
			title: abilita+" utente",
			text: "Sei sicuro di voler <strong>"+abilitare+"</strong> l'utente all'accesso al portale?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si, continua!",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				thisCheck.prop('checked',check);
				$('button[data-user="'+id+'"]').attr('data-active',~~check);
				$('button').prop('disabled',true);
				var postData = new FormData();
				postData.append('check',check);
				var returnEngine = call_ajax_page(postData,'admin_utenti/enable_disable_user',id);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					$('button[data-active="0"]').prop('disabled',true);
				})
			}
		});
	});

	$('.dataTable_admin_utenti_list').DataTable({
		"language": {
			"lengthMenu": "Mostra _MENU_ elementi",
			"zeroRecords": "Non è stato trovato niente - riprova",
			"info": "Pagina _PAGE_ di _PAGES_",
			"search": "Cerca",
			"paginate": {
				"previous": "Indietro",
				"next": "Avanti"
			},
			"infoEmpty": "Nessun elemento disponibile",
			"infoFiltered": "(filtrati da _MAX_ elementi totali)"
		},
		pageLength: 25,
		responsive: true,
		dom: '<"html5buttons"B>lTfgtip',
		buttons: [{
				extend: 'pdfHtml5', 
				title: 'Utenti portale',
				exportOptions: {
                    columns: [ 1,2,3,5,6 ]
                }
			},{
				extend: 'csvHtml5', 
				title: 'Utenti portale CSV',
				fieldSeparator: ';',
				exportOptions: {
                    columns: [ 1,2,3,5,6 ],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			},{
				text: '<div class="label label-primary"><i class="fa fa-plus"></i> Aggiungi</div>',
				action: function ( e, dt, node, config ) {
					add_mod_user(0,0);
				}
			}
		],
		"order": [[ 1, "asc" ]],
		"columnDefs": [ {
			"targets": 'no-sort',
			"orderable": false,

		}],
	});
});

function add_mod_user(user, mod){
	$('button').prop('disabled',true);
	var postData = new FormData();
	postData.append('modify',mod);
	var returnEngine = call_ajax_page(postData,'admin_utenti/mod_user',user);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('button[data-active="0"]').prop('disabled',true);
		$('.body_adminuser_options').empty().append(returndata);
		$('.modal_adminuser').modal('show');
	});
}
</script>
