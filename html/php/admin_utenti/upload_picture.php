<?php 
if (isset($post)==false){
    $virtualPath='..';
    require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['imgString','redirect_to','id'])) {
    \HTML\Page::Page_404();
}

$id=intval($post->get('id'));
$imgString=explode(',',$post->get('imgString'));
$imgStringCode = base64_decode($imgString[1]);
file_put_contents(APP_PATH_WWW.'/img/profiles/'.$id.'.png',$imgStringCode);
$getSerial=intval(\APP\Parameter::serial());
if ($getSerial>=99999){
	$getSerial=0;
}
\APP\Parameter::update('serial',str_pad(($getSerial+1), 5, "0", STR_PAD_LEFT));
echo json_encode(array(
	'result' => 200,
	'image'  =>\IMG\Logo::png($id, 'img/profiles'),
	));