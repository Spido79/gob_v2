<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['check','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$user_id=intval($post->get('id'));
$check=trim($post->get('check'));
\USERS\Detail::updateActive($user_id, $check);
\LOGS\Log::write('USERS', 'User id: '.$user_id.' enabled: '.$check, false, \USERS\Identify::UserID());