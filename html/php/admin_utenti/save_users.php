<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['idGroup','name','surname','login','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$user_id=intval($post->get('id'));
$arrayReturn=array(
		'status' 	=> 200,
		'title' 	=> 'Utente modificato',
		'message' 	=> "L'utente è stato correttamente modificato.",
		'style' 	=> 'success',
	);

$arrayData=array(
	'user_login'		=> strtolower(trim($post->get('login'))),
	'user_name'			=> trim($post->get('name')),
	'user_surname'		=> trim($post->get('surname')),
	'user_idGroup'		=> intval($post->get('idGroup')),
	'user_telephone'	=> null,
	'user_password'		=> null,
	);
if ($user_id==0){
	if(!$post->VerifyPostData(['password'])) {
		\HTML\Page::Page_404();
	}
	//verify email already present
	$arrayReturn=array(
		'status' 	=> 200,
		'title' 	=> 'Nuovo utente aggiunto',
		'message' 	=> "L'utente è stato correttamente inserito.<br><strong>N.B.</strong> Ricorda di inviare la passowrd o la procedura di invio credenziali a questo nuovo utente!",
		'style' 	=> 'success',
	);


	$userTmp=new \USERS\Detail(null, 	$arrayData['user_login']);
	if ($userTmp->get('user_id')!=null){
		header("Content-Type: application/json;charset=utf-8");
		$arrayReturn=array(
			'status' 	=> 400,
			'title' 	=> 'Utente esistente',
			'message' 	=> "L'indirizzo mail per il login è già presente nel sistema per l'utente:<br><strong>".$userTmp->get('user_name')." ".$userTmp->get('user_surname')."</strong>",
			'style' 	=> 'error',
		);
		echo json_encode($arrayReturn);
		exit;
	}
}

if ($post->VerifyPostData(['password']) && trim($post->get('password')) != '' && strlen(trim($post->get('password')))>=8) {
	$arrayData['user_password'] = trim($post->get('password'));
}

if ($post->VerifyPostData(['telephone']) && trim($post->get('telephone')) != '') {
	$arrayData['user_telephone'] = trim($post->get('telephone'));
}

if ($user_id==0 && $arrayData['user_password'] ==null ){
	\HTML\Page::Page_404();
}
if (\USERS\Detail::updateData($user_id, $arrayData)){
	\LOGS\Log::write('USERS', 'User id: '.$user_id.' : add/mod user. Data : '.json_encode($arrayData), false, \USERS\Identify::UserID());	
} else{
	$arrayReturn=array(
		'status' 	=> 400,
		'title' 	=> 'Utente NON modificato',
		'message' 	=> "Nessuna modifica è stata eseguita per questo utente.",
		'style' 	=> 'warning',
	);
}

header("Content-Type: application/json;charset=utf-8");
echo json_encode($arrayReturn);