<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['email','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$email=trim($post->get('email'));

if (\USERS\Identify::CreateToken($email)){
	$tempUser = new \USERS\Detail(0,$email);
	$subject ="Portale gestione - Reset Password";
	\MAIL\Create::setVal('mail_from','spido@goblins.net');
	\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
	
	\MAIL\Create::setVal('mail_to',$email);
	\MAIL\Create::setVal('name_to',trim($tempUser->get('user_surname').' '.$tempUser->get('user_name')));
	\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
	\MAIL\Create::setVal('mail_subject',$subject);

	$dateTime=new \DateTime();
	$arrayReplace=array(
		'subject'	=> $subject,
		'date'		=> $dateTime->format('d M Y'),
		'hour'		=> $dateTime->format('H:i'),
		'email'		=> $email,
		'name'		=> $tempUser->get('user_name'),
		'link'		=> APP_URL.'/resetPWD/'.$tempUser->get('token_hash').'/'.$email
		);
	\MAIL\Create::setTemplate('login/forget',$arrayReplace);
	\MAIL\Create::insert();
}