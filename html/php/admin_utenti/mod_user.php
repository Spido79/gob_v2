<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['modify','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$user_id=intval($post->get('id'));
$modify=intval($post->get('modify'));
//if modify =0 : add new user
//if modify =1 : modify existant
$arrayData=array(
	'user_login'		=> null,
	'user_password'		=> null,
	'user_idGroup'		=> null,
	'user_name'			=> null,
	'user_surname'		=> null,
	'user_numConnection'=> null,
	'user_telephone'	=> null,
	'user_dateCreation'	=> null,
	'user_dateExpiration'=> null,
	'user_dateLastConnection'=> null,
	);
$pasVal='Inserisci';
$title_adminuser_modal='Aggiungi utente';
$title_admin_swal="inserire";
$private="";
$passTool='Doppio click per visualizzare/cambiare la password';
\PASSWORD\Generator::NewPassword();
$pasValWrite=\PASSWORD\Generator::$Plain;
if ($modify==1){
	$pasVal='Modifica';
	$title_adminuser_modal='Modifica utente';
	$title_admin_swal="modificare";
	$user=new \USERS\Detail($user_id);
	foreach ($arrayData as $key => $value) {
		$arrayData[$key]=$user->get($key);
	}
	$pasValWrite="";
	$private="private";
	$passTool='Doppio click per impostare una nuova password<br>Se lasciata vuota la password non verrà modificata';
}

$creation=new DateTime($arrayData['user_dateCreation']);
?>
<div class='row'>
	<div class='col-xs-6'>
		<div class="form-group">
			<label>Nome</label>
			<input placeholder="Inserisci il nome" class="form-control passData" data="name" type="text" value="<?php echo $arrayData['user_name'];?>"/>
		</div>
	</div>
	<div class='col-xs-6'>
		<div class="form-group">
			<label>Cognome</label>
			<input placeholder="Inserisci il cognome" class="form-control passData" data="surname" type="text" value="<?php echo $arrayData['user_surname'];?>"/>
		</div>
	</div>
</div>
<div class='row'>
	<div class='col-xs-6'>
		<div class="form-group">
			<label>Login/Email</label>
			<input placeholder="Inserisci una mail" class="form-control passData" type="email" data="login" value="<?php echo $arrayData['user_login'];?>"/>
		</div>
	</div>
	<div class='col-xs-6'>
		<div class="form-group">
			<label>Password</label>
			<input placeholder="<?php echo $pasVal;?> la password (minimo 8 caratteri)" class="form-control passData <?php echo $private;?>" data="password" type="Password" value="<?php echo $pasValWrite;?>" readonly style='cursor:pointer;' data-toggle="tooltip" data-placement="top" title='<?php echo $passTool;?>'>
		</div>
	</div>
</div>

<div class='row'>
	<div class='col-xs-6'>
		<div class="form-group">
			<label>Telefono</label>
			<input placeholder="Inserisci un telefono" class="form-control passData private" type="text" data="telephone" value="<?php echo $arrayData['user_telephone'];?>"/>
		</div>
	</div>
	<div class='col-xs-6'>
		<b>Data creazione:</b>
		<div style='margin-top:5px;height:35px;' class="border-top-bottom border-left-right border-size-md text-center">
			<?php echo  $creation->format('d M Y');?>
		</div>
	</div>
</div>
<?php if ($modify==1){ ?>
<div class='row'>
	<div class='col-xs-6'>
		<b>Connessioni:</b>
		<div style='margin-top:5px;height:35px;' class="border-top-bottom border-left-right border-size-md text-center">
			<?php echo  $arrayData['user_numConnection'];?>
		</div>
	</div>
	<div class='col-xs-6'>
		<b>Ultima Connessione:</b>
		<div style='margin-top:5px;height:35px;' class="border-top-bottom border-left-right border-size-md text-center">
			<?php
			$lasConn = new DateTime($arrayData['user_dateLastConnection']);
			echo $lasConn->format('d M Y');
			?>
		</div>
	</div>
</div>
<?php } ?>
<div class='row'>
	<div class='col-xs-6'>
		<div class="form-group">
			<label>Gruppo</label>
			<select placeholder="Seleziona un gruppo" class="form-control passData" data="idGroup">
			<?php
			$groups=new USERS\Groups();
				foreach ($groups->get() as $item) {
					$sel= $arrayData['user_idGroup']==$item['id_group'] ?'selected':'';
					echo "<option value='{$item['id_group']}' $sel>{$item['label_group']}</option>";
				}

			?>
			</select>
		</div>
	</div>
	<?php if ($modify==0){ ?>
	<div class='col-xs-6'>
		<div class="form-group">
			<label>Importa dati da Goblin</label><br>
			<div class="input-group">
				<input type="text" target-click=".btn_modUser_modNick" placeholder="Nick goblin..." class="select2_modUser_modNick form-control enter-focus" />
				<span class="input-group-btn">
					<button type="button" class="btn btn-primary btn_modUser_modNick">Importa</button>
				</span>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<script type="text/javascript">
$(document).ready(function() {
	<?php if ($modify==0){ ?>
	var json_returned_nick={};
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'admin_utenti/nick_select',0);
	returnEngine.always(function (returndata) {
		json_returned_nick=$.parseJSON(returndata);
		$(".select2_modUser_modNick").typeahead({ 
			source: function (query, process) {
				var concatSourceData = _.map(json_returned_nick.Goblin,function(item){
					return item.id + "|" + item.label;
				});
				process(concatSourceData);
			},
			matcher : function(item) {
				return this.__proto__.matcher.call(this,item.split("|")[1]);
			},
			highlighter: function(item) {
				return this.__proto__.highlighter.call(this,item.split("|")[1]);
			},
			updater: function(item) {
				var itemArray = item.split("|");
				return this.__proto__.updater.call(this,itemArray[1]);
			}
		});
	});
	$('.btn_modUser_modNick').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var text=$.trim($('.select2_modUser_modNick').val());
		$(this).removeClass('btn-danger');
		$('.select2_modUser_modNick').parent().removeClass('has-error');
		if (text.length<=0){
			$(this).addClass('btn-danger');
			$('.select2_modUser_modNick').parent().addClass('has-error');
			return false;	
		}
		var found=0;
		$.each(json_returned_nick.Goblin, function(index, val) {
			if (text.toLowerCase()==val.label.toLowerCase()){
				found=val.id;
				return;
			}
		});
		if (found==0){
			$('.select2_modUser_modNick').parent().addClass('has-error');
			$(this).addClass('btn-danger');
			return false;	
		}
		import_data_goblin(found);
	});
	
	function import_data_goblin(id){
		var json_returned_gob={};
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'admin_utenti/goblin_data',id);
		returnEngine.always(function (returndata) {
			json_returned_gob=$.parseJSON(returndata);
			$('input[data="name"]').val(json_returned_gob.Goblin['nome_goblin']);
			$('input[data="surname"]').val(json_returned_gob.Goblin['cognome_goblin']);
			$('input[data="login"]').val(json_returned_gob.Goblin['email_goblin']);
			$('input[data="telephone"]').val(json_returned_gob.Goblin['contatto_goblin']);
		});
	}

	<?php } ?>
	$('.title_adminuser_modal').empty().append('<?php echo $title_adminuser_modal?>');	
	
	$('input[data="password"]').tooltip({html:true});

	$('input[data="password"]').parent().dblclick(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		$(this).children('input').attr('readonly', false);
		$(this).children('input').prop('type',"text");
	});
	$('input[data="password"]').focusout(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var letP=$.trim($(this).val());
		$(this).val(letP);
		$(this).attr('readonly', true);
		$(this).prop('type',"password");
	});
	$(document).on('click', '.btn_save_mod_user_portal', function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		call_inform();
	});
});

function call_inform(){
	var postData = new FormData();
	var error=0;
	<?php 
	if ($user_id==1) {
		?>
		swal({
			title: "Attenzione",
			text: "Questo utente non può essere modificato.",
			type: "warning",
			showCancelButton: false,
			html:true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ok!",
		});
		return;
		<?php	
	}
	?>
	$('.passData').each(function(index, el) {
		$(this).parent().removeClass('has-error');
		$(this).parent().addClass('has-success');
		var textTemp=$.trim($(this).val());

		if ($(this).hasClass('private') && textTemp.length==0){
			return;
		}

		postData.append($(this).attr('data'),textTemp);
		if ($(this).attr('data')=='login' && !ValidateEmail(textTemp)) {
			error=1;
			$(this).parent().addClass('has-error');
			$(this).parent().removeClass('has-success');
		}

		if ($(this).attr('data')=='password' && textTemp.length<8){
			error=1;
			$(this).parent().addClass('has-error');
			$(this).parent().removeClass('has-success');
		}

		if (textTemp==''){
			error=1;
			$(this).parent().addClass('has-error');
			$(this).parent().removeClass('has-success');
		}
	});
	if (error>0){
		return false;
	}
	swal({
		title: "Salva utente",
		text: "Sei sicuro di voler <strong><?php echo $title_admin_swal;?></strong> questo utente?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Si, continua!",
		html:true,
		closeOnConfirm: true,
	},function(isConfirm){
		if (isConfirm) {
			$('button').prop('disabled',true);
			var returnEngine = call_ajax_page(postData,'admin_utenti/save_users',<?php echo $user_id;?>);
			returnEngine.always(function (returndata) {
				try {
					var json_returned=$.parseJSON(returndata);
				} catch (e) {
					swal({
						title: "Errore",
						text: "Un'errore è sopraggiunto. Riprovare.",
						type: "error",
						showCancelButton: false,
						html:true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Ok!",
					});
					$('button').prop('disabled',false);
					$('button[data-active="0"]').prop('disabled',true);
					return false;
				}

				if (json_returned.status>0){
					swal({
						title: json_returned.title,
						text: json_returned.message,
						type: json_returned.style,
						showCancelButton: false,
						html:true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Ok!",
					});
					if (json_returned.status==200) {
						$('.modal_adminuser').modal('hide');
						admin_utenti_box_left();
					}
				}

				$('button').prop('disabled',false);
				$('button[data-active="0"]').prop('disabled',true);

			})
		}
	});
}
</script>
