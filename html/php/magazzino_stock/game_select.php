<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$params=array('id_ana as id','title as label');
$readFile=\MAGAZZINO\Games::getActive($params);
$array=array('Games' => $readFile);
header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);
