<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

if (\USERS\Identify::UserGROUP()!=1 && \USERS\Identify::UserGROUP()!=2){
	\HTML\Page::Page_404();
}

$id_stock=intval($post->get('id'));
\MAGAZZINO\Stock::reenable($id_stock);

\MAGAZZINO\Stock::logWrite($id_stock, 'Riattivato articolo - Magazzino', 'fa-recycle', 0, 0);
\LOGS\Log::write('STOCK', 'Stock riattivato id: '.$id_stock, false, \USERS\Identify::UserID());