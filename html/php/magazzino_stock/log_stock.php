<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id=intval($post->get('id'));
$stockLog=\MAGAZZINO\Stock::log($id, 1);
$note=$stockLog[0]['notes'];
if ($note==''){
	$note="<i class='text-muted'>-nessuna nota-</i>";
}

echo '<div class="full-height-scroll ">
		<strong>Note</strong>
		<p>'.$note.'</p>
		<hr style="margin-top:10px;margin-bottom:10px;" />
		<strong>Cronologia articolo</strong>
		<button class="btn btn-xs btn-grey btn_stk_log_order_move_stk" style="float:right">ordina <i class="fa fa-sort-amount-desc"></i></button>
		<div id="stock_log_spec_list" class="vertical-container dark-timeline" style="margin-top:0;">
		';
$count=0;
foreach ($stockLog as $item) {
	$date_action=new \DateTime($item['data_in']);
	$color_bg=\MAGAZZINO\Stock::get_color_icon($item['icon']);
	if ($count==0){
		$color_bg .= ' blinking_white';
	}
	echo "<div class=\"vertical-timeline-block\">
			<div class=\"vertical-timeline-icon {$color_bg}\">
				<i class=\"fa {$item['icon']}\"></i>
			</div>
			<div class=\"vertical-timeline-content\" style=\"padding-top:0;padding-bottom:0;\">
				<p style=\"margin-bottom:2px;\">{$item['action']}</p>
				<span class=\"small text-muted\" style=\"float:right\">{$item['user']}</span>
				<span class=\"vertical-date small text-muted\"> {$date_action->format('d.m.Y H:i')} </span>
			</div>
		</div>";
	$count++;
}
echo "</div></div>";
?>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_stk_log_order_move_stk').click(function(event) {
		if ($(this).children('i').hasClass('fa-sort-amount-asc')){
			$(this).children('i').addClass('fa-sort-amount-desc');
			$(this).children('i').removeClass('fa-sort-amount-asc');
		} else {
			$(this).children('i').addClass('fa-sort-amount-asc');
			$(this).children('i').removeClass('fa-sort-amount-desc');
		}
		$('#stock_log_spec_list').append($('#stock_log_spec_list>').detach().get().reverse());
	});

});
</script>