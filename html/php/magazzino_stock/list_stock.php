<?php
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['req','recordTotal','draw','length','start','order','search','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$arrayReturn=array(
    "draw"            => intval($post->get('draw')),
    "recordsTotal"    => intval($post->get('recordTotal')),
    "recordsFiltered" => 0,
    "data"            => array(),
);
$count=0;
$counted=0;
$paramsPass=array(
    'search'    => trim($post->get('search')['value']),
	'order'     => array(
    	'field' => (intval($post->get('order')[0]['column'])+1),
		'direction' => trim($post->get('order')[0]['dir']),
	)
);

if ($post->get('req')=='true'){
    $paramsPass['dismiss']=1;
}

$allStock=\MAGAZZINO\Stock::getAllAna($paramsPass);
foreach ($allStock as $item) {
	if (intval($post->get('start'))<=$count && $counted< intval($post->get('length'))){
		$arrayStep=array(
			$item['Articolo'],
			$item['Qta'],
			$item['Note'],
			$item['id_ana'],
		);
		$arrayReturn['data'][]=$arrayStep;
		$counted++;
	}
	if ($counted>=intval($post->get('length'))){
		break;
	}
	$count++;
}
$arrayReturn['recordsFiltered']=count($allStock);
header("Content-type: text/javascript");
header('Content-Encoding: gzip');
echo gzencode(json_encode($arrayReturn));