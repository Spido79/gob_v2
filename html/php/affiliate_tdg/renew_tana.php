<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_tana=intval($post->get('id'));
if ($id_tana==\APP\Parameter::getSpec('id_tana_nazionale') || $id_tana==0){
	\HTML\Page::Page_404();
}

$params=array(
	'map_fields'	=> array(
		'Data_UltimoRinnovo' => '',
	),
	'list_tane'		=> array($id_tana),
);
$tanaSpec=\GOBLINS\Tane::getAll($params);
$arrayTana=array();
if (isset($tanaSpec[0])){
	$Data_UltimoRinnovo=new DateTime($tanaSpec[0]['Data_UltimoRinnovo']);
	$Data_UltimoRinnovo->add(new \DateInterval('P2Y'));
	$arrayTana=array(
		'Data_UltimoRinnovo'=> $Data_UltimoRinnovo->format('Y-m-d'),
	);
	\GOBLINS\Tane::change($id_tana, $arrayTana);

}


$array=array(
	'result'	=> 200,
	'id_tana'	=> $id_tana,
	'array'		=> $arrayTana,
);
header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);