<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_tana=intval($post->get('id'));
$tane=\GOBLINS\Tane::spec([$id_tana]);
$color='success';
$nomeTana=$tane[$id_tana];
if ($id_tana == \APP\Parameter::getSpec('id_tana_nazionale')){
	$color='warning';
	$nomeTana='Tana Nazonale';
}
if (!\GOBLINS\Tane::isActive($id_tana)){
	$color='danger';
}

$params=array(
	'list_tane' => array($id_tana),
);
$listTane=\GOBLINS\Tane::getAll($params);
if (count($tane)<=0 || count($listTane)<=0){
	?>
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Tana non trovata</h5>
		</div>
		<div class="ibox-content">
			<div class="form-horizontal">
				<p>La tana seleziona non è stata trovata... <p>
				<p> <i class="fa fa-warning"></i> <b>Attenzione:</b> contattare l'amministratore di sistema </p>
			</div>
		</div>
	</div>
	<?php	
	exit;
}
$detTana=$listTane[0];

?>
<div class="ibox float-e-margins">
	<div class="ibox-title text-center">
		<div class="jokerman" style="font-size:23px;">
			<span class="text-<?php echo $color;?>"><?php echo $nomeTana;?></span>
		</div>
		<?php
		switch ($detTana['status_tana']) {
			case 2:
			echo "<small class='font-bold'>-tana disdetta-</small>";
			break;

			case 0:
			echo "<small class='font-bold'>-tana disabilitata-</small>";
			break;

		}
		?>
	</div>
	<div class="ibox-content">
		<div class="row">
			<div class="col-xs-12 text-left ">
				<p class=" text-left">
					La <strong><?php echo $nomeTana; ?></strong> è stata fondata il
					<strong>
					<?php
						echo \DATESPACE\Convert::dFY($detTana['Data_Costituzione']);
					?>
					</strong>
				</p>
				<p class="text-left">
					Competenza:
					<?php
						if ($id_tana == \APP\Parameter::getSpec('id_tana_nazionale')){
							echo '<i class="fa fa-circle text-danger"></i> <span class="font-bold">Nazionale</span>';	
						} elseif ($detTana['competenza_Prov']==0){
							echo '<i class="fa fa-circle text-warning"></i> <span class="font-bold">Provincia</span>';	
							if ($detTana['Provincia']!=''){
								echo ' di <span class="font-bold">'.$detTana['Provincia'].'</span>';
							}
						} else {
							echo '<i class="fa fa-circle text-success"></i> <span class="font-bold">Città</font>';
							if ($detTana['Citta']!=''){
								echo ' di <span class="font-bold">'.$detTana['Citta'].'</span>';
							}
						}
						
					?>
					</span>
				</p>
				<p class="text-left">
					Indirizzo sede:<br>
					<?php
						echo $detTana['Indirizzo'];
					?>
				</p>
				
				<p class="text-left">
					<span>Presidente</span>
					<?php
					$userT=\GOBLINS\Manage::getSpec($detTana['id_presidente']);
					if ($detTana['id_presidente']==0 || !$userT){
						echo "<i class='text-warning'>-nessun presidente affiliato-</i>";
					} else {
						echo '<img src="'.\IMG\Logo::png('goblin_face').'" style="width:20px;" /> <span class="font-bold">'.$userT['Nick'].'</span>';
						if ($userT['email_goblin']!=''){
							echo '<br><i class="fa fa-envelope-o"></i> <a href="mailto:'.$userT['email_goblin'].'">'.$userT['email_goblin'].'</a>';
						}
						if ($userT['contatto_goblin']!=''){
							echo '<br><i class="fa fa-phone"></i> '.$userT['contatto_goblin'];
						}
					}
					?>
				</p>
				<?php
				//get resp
				$foundR=false;
				foreach (\USERS\Detail::getActive() as $user) {
					$tanaRif=explode(',', $user['tane_riferimento']);
					if (in_array($id_tana, $tanaRif)){
						if (!$foundR){
							echo '<p class="text-left">Responsabili tana';
						}
						echo '<br> - '.$user['user_name'].' '.$user['user_surname']. ' <span style="float:right;"><a href="mailto:'.$user['user_login'].'">'.$user['user_login'].'</a></span>';
						$foundR=true;
					}
				}
				if (!$foundR){
					echo '</p>';
				}

				if (trim($detTana['Note'])!=''){
					echo "<p class=\"text-left\"><strong>Note:</strong><br>".nl2br($detTana['Note'])."</p>";
				}
				?>
				
			</div>
		</div>
		<hr>
		<!-- RINNOVI -->
		<?php 
		if ($detTana['status_tana']!=2){
		?>
		<div class="row">
			<div class="col-xs-12">
				<h4 class="text-left">Gestione rinnovi</h4>
			</div>

			<div class="col-xs-4">
				<small class="stats-label">Data ultimo rinnovo</small>
				<h4>
					<?php
					$date=\GOBLINS\Tane::getDate($detTana['Data_UltimoRinnovo']);
					if ($detTana['status_tana']!=1){
						$date['rinnovo_f']='-';
						$date['scadenza_f']='-';
						$date['disdetta_f']='-';
						$date['day_to_expire']='-';
						$date['day_to_revoke']='-';
					} else {
						if ($date['status']!=1){
							$date['day_to_revoke']='scaduti';
						}	
					}
					echo $date['rinnovo_f'];
					?>
				</h4>
			</div>

			<div class="col-xs-4">
				<small class="stats-label">Data scadenza</small>
				<h4>
					<?php
					switch ($date['status']) {
						case 0:
							echo "<span class=\"text-danger\">{$date['scadenza_f']}</span>";
							break;
						
						case 2:
							echo "<span class=\"text-warning\">{$date['scadenza_f']}</span>";
							break;

						default:
							echo $date['scadenza_f'];
							break;
					}
					?>
				</h4>
			</div>
			<div class="col-xs-4">
				<small class="stats-label">Giorni alla scadenza</small>
				<h4>
					<?php
					switch ($date['status']) {
						case 0:
							echo "<span class=\"text-danger\">{$date['day_to_expire']}</span>";
							break;
						
						case 2:
							echo "<span class=\"text-warning\">{$date['day_to_expire']}</span>";
							break;

						default:
							echo $date['day_to_expire'];
							break;
					}
					?>
				</h4>
			</div>
			<div class="col-xs-4">
				<small class="stats-label">Disdetta possibile entro il</small>
				<h4>
					<?php
						switch ($date['status']) {
							case 0:
								echo "<span class=\"text-danger\">{$date['disdetta_f']}</span>";
								break;
							
							case 2:
								echo "<span class=\"text-danger\">{$date['disdetta_f']}</span>";
								break;

							default:
								echo "<span class=\"text-success\">{$date['disdetta_f']}</span>";
								break;
						}
						
					?>
				</h4>
			</div>
			<div class="col-xs-4">
				<small class="stats-label">Giorni alla disdetta</small>
				<h4>
					<?php
					switch ($date['status']) {
						case 0:
							echo "<span class=\"text-danger\">{$date['day_to_revoke']}</span>";
							break;
						
						case 2:
							echo "<span class=\"text-danger\">{$date['day_to_revoke']}</span>";
							break;

						default:
							echo "<span class=\"text-success\">{$date['day_to_revoke']}</span>";
							break;
					}
					?>
				</h4>
			</div>
		</div>
		<hr>
		<div class="row">
			
				<?php 
				if ($date['status']==0 || $date['status']==2){
					echo '<div class="col-xs-4">';
				} else {
					echo '<div class="col-xs-6">';
				}
				if ($id_tana != \APP\Parameter::getSpec('id_tana_nazionale')){ ?>
					<button class="btn btn-danger btn-xs btn_tana_enadis" data-id="<?php echo $id_tana;?>" data-action="2">
						<i class="fa fa-remove"></i> Disattiva
					</button>
					<?php
					/*if ($detTana['status_tana']==1){ ?>
					<button class="btn btn-danger btn-xs btn_tana_enadis" data-id="<?php echo $id_tana;?>" data-action="0">
						<i class="fa fa-ban"></i> Disabilita
					</button>
					<?php } else { ?>
					<button class="btn btn-primary btn-xs btn_tana_enadis" data-id="<?php echo $id_tana;?>" data-action="1">
						<i class="fa fa-check"></i> Abilita
					</button>
					<?php }*/
				}?>
			</div>
			
				<?php
				switch ($date['status']) {
					case 0:
					case 2:
						echo '<div class="col-xs-4 text-right">';
						echo '<button class="btn btn-primary btn-xs btn_tana_autoRenew" data-id="'.$id_tana.'">
							<i class="fa fa-level-up"></i> Auto Rinnova
						</button>
						</div>
						<div class="col-xs-4 text-right">';
						break;
					default:
						echo '<div class="col-xs-6 text-right">';
						break;
				}
				?>
				<button class="btn btn-success btn-xs btn_tana_mod" data-id="<?php echo $id_tana;?>">
					<i class="fa fa-pencil"></i> Modifica
				</button>
			</div>
		</div>
		<?php 
		} else {
		?>
		<!-- DISDETTA -->
		<div class="row">
			<div class="col-xs-12">
				<h4 class="text-left">Risoluzione</h4>
			</div>

			<div class="col-sm-4">
				<small class="stats-label">Data risoluzione</small>
				<p class="font-bold">
					<?php
					$dateRis=\GOBLINS\Tane::getDate($detTana['Data_Risoluzione']);
					echo $dateRis['rinnovo_f'];
					?>
				</p>
			</div>

			<div class="col-sm-8">
				<small class="stats-label">Motivo disdetta</small>
				<p class="text-left">
					<?php
					echo nl2br("<span class=\"text-success\">{$detTana['motivo_disdetta']}</span>");
					?>
				</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-12">
				<button class="btn btn-danger btn-xs btn_tana_enadis" data-id="<?php echo $id_tana;?>" data-action="3">
					<i class="fa fa-refresh"></i> Riattiva
				</button>
			</div>
		</div>
		<?php 
		}
		?>
	</div>
</div>