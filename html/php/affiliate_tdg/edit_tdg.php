<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$id_tana=intval($post->get('id'));

$params=array(
	'left_join'	=> array(
		'tbl_goblins' => array(
			'id_goblin'	=> 'id_presidente',
		)
	),
	'map_fields'	=> array(
		'Nome'		=>'',
		'Data_Costituzione' => '',
		'Data_UltimoRinnovo' => '',
		'Indirizzo' => '',
		'Citta' => '',
		'Provincia' => '',
		'Nick' => 'nick_presidente',
		'Note' => '',
		'competenza_Prov' => '',
	),
	'list_tane'		=> array($id_tana),
);
$tanaSpec=\GOBLINS\Tane::getAll($params);
$array=array(
	'title' 		=> 'Aggiungi una nuova tana',
	'enable_edit'	=> true,
	'date'			=> array('Data_Costituzione', 'Data_UltimoRinnovo'),
	'fields'		=> array(
		'button-modTana'	=> $id_tana,
		'id_tana'			=> $id_tana,
	),
);
if (isset($tanaSpec[0])){
	$array['title']=$tanaSpec[0]['Nome'];
	foreach ($tanaSpec[0] as $key => $value) {
		if ($key=='Data_Costituzione' || $key=='Data_UltimoRinnovo'){
			$dataC="";
			if ($value!=''){
				$dataC_date=new DateTime($value);
				$dataC=$dataC_date->format('d/m/Y');
			}
			$array['fields'][$key]=$dataC;		
			continue;
		}
		$array['fields'][$key]=$value;
	}
} else {
	foreach ($params['map_fields'] as $key => $value) {
		if ($value!=''){
			$array['fields'][$value]='';
		} else {
			$array['fields'][$key]='';
		}
		if ($key=='competenza_Prov'){
			$array['fields'][$key]=1;	
		}
	}
}

header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);
