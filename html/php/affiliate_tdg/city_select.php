<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$field=intval($post->get('id'));

$params=array(
	'order' => 'Comune',
	'map_fields'	=> array(
		'Comune'		=>'',
		'Provincia'		=>'',
	),
);
$allCity=\COMUNI\Istat::getAll($params);

$readCity=array();
$readProv=array();
$mapData=array(
	'city' => array(),
	'city_compare' => array(),
	'prov' => array(),
	'prov_compare' => array(),
);
foreach ($allCity as $city) {
	$readCity[]=$city['Comune'].'|'.$city['Provincia'];
	$mapData['city'][]=$city['Comune'];
	$mapData['city_compare'][]=strtolower($city['Comune']);
	$mapData['prov'][]=$city['Provincia'];
	$mapData['prov_compare'][]=strtolower($city['Provincia']);
	if (!in_array($city['Provincia'], $readProv)){
		$readProv[]=$city['Provincia'];
	}
}

$array=array(
	'city'		=> $readCity,
	'prov'		=> $readProv,
	'map'		=> $mapData,
);
header("Content-type: text/javascript");
header('Content-Encoding: gzip'); 
echo gzencode(json_encode($array));