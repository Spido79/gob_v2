<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['Note','nick_presidente','Provincia','Citta','competenza_Prov','id_tana','Indirizzo','Data_UltimoRinnovo','Data_Costituzione','Nome','id','redirect_to'])) {
	\HTML\Page::Page_404();
}

$id_tana=intval($post->get('id'));
$id_tanaVer=intval($post->get('id_tana'));
if (($id_tana!=$id_tanaVer) || $id_tanaVer==\APP\Parameter::getSpec('id_tana_nazionale') || $id_tana==0){
	//\HTML\Page::Page_404();	
}

$Data_UltimoRinnovo='';
if (trim($post->get('Data_UltimoRinnovo'))!=''){
	$Data_UltimoRinnovo=trim($post->get('Data_UltimoRinnovo'));
	\DATESPACE\Convert::dmyTOymd($Data_UltimoRinnovo);
}

$Data_Costituzione='';
if (trim($post->get('Data_Costituzione'))!=''){
	$Data_Costituzione=trim($post->get('Data_Costituzione'));
	\DATESPACE\Convert::dmyTOymd($Data_Costituzione);
}

$arrayTana=array(
	'Note'				=> null,
	'Provincia'			=> null,
	'Citta'				=> null,
	'competenza_Prov'	=> null,
	'Indirizzo'			=> null,
	'Nome'				=> null,
);
foreach ($arrayTana as $key => $value) {
	$arrayTana[$key]=trim($post->get($key));
}
$arrayTana['Data_UltimoRinnovo'] = $Data_UltimoRinnovo;
$arrayTana['Data_Costituzione'] = $Data_Costituzione;
$arrayTana['status'] = 1;
$gob=\GOBLINS\Manage::getSpec(0, trim($post->get('nick_presidente')));
if ($gob){
	$arrayTana['id_presidente'] = $gob['id_goblin'];
}

if ($id_tana==0){
	//insert
	$arrayTana['status_tana'] = 1;
	$id_tana=\GOBLINS\Tane::insert($arrayTana);

} else {
	\GOBLINS\Tane::change($id_tana, $arrayTana);
}

$array=array(
	'result'	=> 200,
	'id_tana'	=> $id_tana,
	'array'		=> $arrayTana,
);
header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);