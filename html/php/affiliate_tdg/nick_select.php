<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
\GOBLINS\Manage::resetGoblin();

$taneRif=array();
foreach (\GOBLINS\Tane::getActive() as $key => $value) {
    $taneRif[]=$value['id_tana'];
}

$paramsPass=array(
	'list_tane' => $taneRif,
	'map_fields'	=> array(
		'Nick'			=>'label',
	),
);

$readFile=array();
$readFileComp=array();
$res=\GOBLINS\Manage::getAll(0, $paramsPass);
foreach ($res as $value) {
	$readFile[]=$value['label'];
	$readFileComp[]=strtolower($value['label']);
}

$array=array(
	'gob' => $readFile,
	'gob_compare' => $readFileComp
);
header("Content-type: text/javascript");
header('Content-Encoding: gzip'); 
echo gzencode(json_encode($array));