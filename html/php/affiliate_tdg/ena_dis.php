<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['action','id','redirect_to'])) {
	\HTML\Page::Page_404();
}	
$action=intval($post->get('action'));
$id_tana=intval($post->get('id'));

/*ACTION:
case 0: //disabilita
case 1: //riabilita
case 2: //disattiva
case 3: //riattiva
*/
$detach=false;
switch ($action) {
	case 0:
		$detach=true;
		$msg='disabilitata';
		$status=0;
		break;

	case 1:
		$msg='abilitata';
		$status=1;
		break;

	case 2:
		$msg='disattivata';
		if(!$post->VerifyPostData(['motivo_disdetta','Data_Risoluzione'])) {
			\HTML\Page::Page_404();
		}	
		$detach=true;
		$status=2;
		break;

	case 3:
		$msg='attivata';
		$status=1;
		break;

}

$motivo_disdetta='';
if ($post->VerifyPostData(['motivo_disdetta']) && trim($post->get('motivo_disdetta'))!=''){
	$motivo_disdetta=trim($post->get('motivo_disdetta'));
}

$Data_Risoluzione='';
if ($post->VerifyPostData(['Data_Risoluzione']) && trim($post->get('Data_Risoluzione'))!=''){
	$Data_Risoluzione=trim($post->get('Data_Risoluzione'));
	\DATESPACE\Convert::dmyTOymd($Data_Risoluzione);
}

$params=array(
	'status'	=> $status,
	'detach_gb'	=> \APP\Parameter::getSpec('id_tana_nazionale'),
	'detach_us'	=> $detach,
	'motivo_disdetta' => $motivo_disdetta,
	'Data_Risoluzione' => $Data_Risoluzione,

);
\GOBLINS\Tane::change($id_tana, $params);
\LOGS\Log::write('Affiliate', 'tana id: '.$id_tana.' '.$msg, false, \USERS\Identify::UserID());
\GOBLINS\Tane::cleanTane();