<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['req','recordTotal','draw','length','start','order','search','id','redirect_to'])) {
	\HTML\Page::Page_404();
}	
$arrayReturn=array(
	"draw"            => intval($post->get('draw')),
	"recordsTotal"    => intval($post->get('recordTotal')),
	"recordsFiltered" => 0,
	"data"            => array(),
);

$paramsPass=array(
	'left_join'	=> array(
		'tbl_goblins' => array(
			'id_goblin'	=> 'id_presidente',
		)
	),
	'action'	=> 1,
	'map_fields'	=> array(
		'Nome'				=>'Nome',
		'Nick'				=>'Presidente',
		'Data_Costituzione'	=>'Data_Costituzione',
		'Data_UltimoRinnovo'=>'Data_UltimoRinnovo',
		'(Data_UltimoRinnovo)'=>'data_a',
		'(tbl_tane.Data_UltimoRinnovo)'=>'data_b',
		'Data_Risoluzione'	=>'Data_Risoluzione',
		'Citta'				=>'Citta',
		'tbl_tane.id_tana'	=>'id_tana',
		'status_tana'		=>'status_tana',
	),
	'search'		=> trim($post->get('search')['value']),
	'order'			=> array(
		'field'		=> (intval($post->get('order')[0]['column'])+1),
		'direction'	=> trim($post->get('order')[0]['dir']),
	),
);
if ($post->get('req')=='true'){
	$paramsPass['action']=null;
}

$allTane=\GOBLINS\Tane::getAll($paramsPass);
$count=0;
$counted=0;
$arrayReturn['recordsFiltered']=count($allTane);
foreach ($allTane as $item) {
	if (intval($post->get('start'))<=$count && $counted< intval($post->get('length'))){
		$rinn=new DateTime($item['Data_UltimoRinnovo']);
		$statusExpire=0;
		if ($item['Data_Costituzione']=='' || $item['Data_Costituzione']==null || $item['Data_Costituzione']=='0000-00-00'){
			$Data_Costituzione='-';
		} else {
			$Data_CostituzioneDate=new DateTime($item['Data_Costituzione']);
			$Data_Costituzione=$Data_CostituzioneDate->format('d.m.Y');
		}
		$dateTana=\GOBLINS\Tane::getDate($item['Data_UltimoRinnovo']);
		if ($item['status_tana']!=1){
			$dateTana['rinnovo_f']='-';
			$dateTana['scadenza_f']='-';
			$dateTana['disdetta_f']='-';
		}
		$arrayStep=array(
			$item['Nome'],
			$item['Presidente'],
			$Data_Costituzione,
			$dateTana['rinnovo_f'],
			$dateTana['scadenza_f'],
			$dateTana['disdetta_f'],
			$item['Citta'],
			$item['Data_Risoluzione'],
			$item['id_tana'],
			$dateTana['status'],
			$item['status_tana'],
		);
		$arrayReturn['data'][]=$arrayStep;
		$counted++;
	}
	$count++;
}


echo json_encode($arrayReturn);