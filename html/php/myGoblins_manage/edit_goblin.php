<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
\GOBLINS\Manage::resetGoblin();
$id_goblin=intval($post->get('id'));
$goblin=\GOBLINS\Manage::getSpec($id_goblin);
/*
	cases:
	a) New goblin (id_tana = 0) -> Associa
	b) My Goblin: edit data
	c) Goblin another tana: request to associate
*/

$enable_edit=true;
$email_request=false;
$is_new=false;
$title='Modifica questo goblin';
$button='<i class="fa fa-floppy-o"></i> Salva modifiche';
if (!in_array($goblin['id_tana'], explode(",","0,".\USERS\Identify::get('tane_riferimento')))){
	$enable_edit=false;
	$email_request=true;
	$title='Richiedi tesseramento per questo goblin';
	$button='<i class="fa fa-bullhorn"></i> Richiedi tesseramento';
}
if ($goblin['id_tana']==0){
	$is_new=true;
	$title='Tessera questo goblin';
	$button='<i class="fa fa-plus"></i> Aggiungi goblin';
}
$tanaName="";
foreach (\GOBLINS\Tane::spec([$goblin['id_tana']]) as $key => $value) {
	$tanaName=$value;
}
$dataNascita="";
if ($goblin['dataNascita_goblin']!=''){
	$dataNascita_date=new DateTime($goblin['dataNascita_goblin']);
	$dataNascita=$dataNascita_date->format('d/m/Y');
}
$array=array(
	'title' 		=> $title,
	'enable_edit'	=> $enable_edit,
	'is_new'		=> $is_new,
	'email_request'	=> $email_request,
	'fields'		=> array(
		'id_goblin'			=> $goblin['id_goblin'],
		'Nick'				=> $goblin['Nick'],
		'nome_goblin' 		=> $goblin['nome_goblin'],
		'cognome_goblin' 	=> $goblin['cognome_goblin'],
		'email_goblin' 		=> $goblin['email_goblin'],
		'indirizzo_goblin' 	=> $goblin['indirizzo_goblin'],
		'citta_goblin' 		=> $goblin['citta_goblin'],
		'cap_goblin' 		=> $goblin['cap_goblin'],
		'provincia_goblin' 	=> $goblin['provincia_goblin'],
		'dataNascita_goblin'=> $dataNascita,
		'contatto_goblin' 	=> $goblin['contatto_goblin'],
		'note_goblin' 		=> $goblin['note_goblin'],
		'id_tana' 			=> $goblin['id_tana'],
		'tana_name'			=> $tanaName,
		'button-mod'		=> $button,
	),
);
header("Content-Type: application/json;charset=utf-8");
echo json_encode($array);
