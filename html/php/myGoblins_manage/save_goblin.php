<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['dataNascita_goblin', 'id_goblin', 'id_tana', 'nome_goblin', 'cognome_goblin', 'indirizzo_goblin', 'citta_goblin', 'cap_goblin', 'provincia_goblin', 'email_goblin', 'contatto_goblin', 'note_goblin','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
\GOBLINS\Manage::resetGoblin();
$id_goblin=intval($post->get('id'));
$id_goblinVer=intval($post->get('id_goblin'));
if ($id_goblin!=$id_goblinVer){
	\HTML\Page::Page_404();	
}
$goblin=\GOBLINS\Manage::getSpec($id_goblin);
/*
	cases:
	a) New goblin (id_tana = 0) -> Associa
	b) My Goblin: edit data
	c) Goblin another tana: request to associate
*/
$dateTrim='';
if (trim($post->get('dataNascita_goblin'))!=''){
	$dateTrim=trim($post->get('dataNascita_goblin'));
	\DATESPACE\Convert::dmyTOymd($dateTrim);
}

$nato=new \DateTime($dateTrim);
$today=new \DateTime();
$arrayReturn['result']=202;
$arrayReturn['goblin']=$goblin['Nick'];
$arrayReturn=array(
	'result'	=> 200,
);
if (!in_array($goblin['id_tana'], explode(",","0,".\USERS\Identify::get('tane_riferimento')))){
	/*INSERT A TABLE OF REQUEST AND ACCEPTING*/
	if (\GOBLINS\Request::check($goblin['id_goblin'])){
		$arrayGet=\GOBLINS\Request::get($goblin['id_goblin']);
		$req=new DateTime($arrayGet['data_request']);
		$arrayReturn=array(
			'result'	=> 407,
			'message'	=> 'Questo Goblin ha già una richiesta pendente del '.$req->format('d-m-Y').' alle ore '.$req->format('H:i').' fatta da <strong>'.$arrayGet['user_name_surname'].'</strong> per <strong>'.$arrayGet['tana_name'].'</strong>.<br>Per poter inoltrare un\'altra richiesta deve essere annullata la richiesta precedente.<br>In caso di problemi contatta il direttivo.',
		);
		echo json_encode($arrayReturn);
		//request in action
		exit;
	}

	\GOBLINS\Request::add($goblin['id_goblin'],intval($post->get('id_tana')));
	/*Case C*/
	/*MAIL 1 - TO ME */
	$subject ="TdG Portale Gestione - Richiesta tesserato";
	
	$tane=\GOBLINS\Tane::spec([$goblin['id_tana'], intval($post->get('id_tana'))]);

	if ($goblin['id_tana']==\APP\Parameter::getSpec('id_tana_nazionale')){
		$oldTana='Tana Nazionale';
	} else {
		$oldTana=$tane[$goblin['id_tana']];		
	}

	if (intval($post->get('id_tana'))==\APP\Parameter::getSpec('id_tana_nazionale')){
		$newTana='Tana Nazionale';
	} else {
		$newTana=$tane[intval($post->get('id_tana'))];
	}

	$listResp ='';
	$arrayResp=array();
	foreach (\GOBLINS\Tane::getResponsable($goblin['id_tana']) as $user_id) {
		$tempUser = new \USERS\Detail($user_id);
		$listResp.="<tr>
			<td>".$tempUser->get('user_name').' '.$tempUser->get('user_surname')."</td>
			<td><a href='mailto:".$tempUser->get('user_login')."'>".$tempUser->get('user_login')."</a></td>
		</tr>";
		$arrayResp[$tempUser->get('user_login')] = $tempUser->get('user_name').' '.$tempUser->get('user_surname');
	}
	if ($listResp=''){
		$listResp='<tr><td colspan="2"><strong>Attenzione:</strong> questa tana non ha responsabili.<br>Contatta il direttivo.</td></tr>';
	}

	$listDir ='';
	$groups=new USERS\Groups();
	$arrayDir=array();
	foreach ($groups->usersGroup(2,1,4) as $user_val) {
		$listDir.="<tr>
			<td>".$user_val['user_name_surname']."</td>
			<td><a href='mailto:".$user_val['user_login']."'>".$user_val['user_login']."</a></td>
		</tr>";
		$arrayDir[$user_val['user_login']] =$user_val['user_name_surname'];
	}
	if ($listDir=''){
		$listDir='<tr><td colspan="2"><strong>Attenzione:</strong> non c\'è nessun membero nel direttivo<br>Contatta <a href="mailto:spido@goblins.net">spido@goblins.net</a></td></tr>';
	}

	$dateTime=new \DateTime();
	$arrayReplace=array(
		'name'		=> \USERS\Identify::get('user_name'),
		'name_ori'	=> \USERS\Identify::get('user_name'). ' '.\USERS\Identify::get('user_surname'),
		'mail_ori'	=> \USERS\Identify::get('user_login'),
		'subject'	=> $subject,
		'date'		=> $dateTime->format('d M Y'),
		'hour'		=> $dateTime->format('H:i'),
		'goblin'	=> $goblin['Nick'],
		'newTana'	=> $newTana,
		'oldTana'	=> $oldTana,
		'listResp'	=> $listResp,
		'listDirettivo'	=> $listDir,
	);

	\MAIL\Create::setVal('mail_from','spido@goblins.net');
	\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
	\MAIL\Create::setVal('mail_to',\USERS\Identify::get('user_login'));
	\MAIL\Create::setVal('name_to',\USERS\Identify::get('user_name').' '.\USERS\Identify::get('user_surname'));
	\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
	\MAIL\Create::setVal('mail_subject',$subject);
	\MAIL\Create::setTemplate('myGoblins_manage/to_me',$arrayReplace);
	\MAIL\Create::insert();

	/*MAIL 2 - TO RESP TANA GOBLIN*/
	foreach ($arrayResp as $mail => $name) {
			$arrayReplace['name']=$name;
			\MAIL\Create::setVal('mail_from','spido@goblins.net');
			\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
			\MAIL\Create::setVal('mail_to',$mail);
			\MAIL\Create::setVal('name_to',$name);
			\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
			\MAIL\Create::setVal('mail_subject',$subject);
			\MAIL\Create::setTemplate('myGoblins_manage/to_resp',$arrayReplace);
			\MAIL\Create::insert();
	}
	/*MAIL 3 - TO DIRETTIVO*/

	foreach ($arrayDir as $mail => $name) {
			$arrayReplace['name']=$name;
			\MAIL\Create::setVal('mail_from','spido@goblins.net');
			\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
			\MAIL\Create::setVal('mail_to',$mail);
			\MAIL\Create::setVal('name_to',$name);
			\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
			\MAIL\Create::setVal('mail_subject',$subject);
			\MAIL\Create::setTemplate('myGoblins_manage/to_dir',$arrayReplace);
			\MAIL\Create::insert();
	}

	//$tempUser = new \USERS\Detail(0,$email);
	$arrayReturn['result']=202;
	$arrayReturn['goblin']=$goblin['Nick'];
	echo json_encode($arrayReturn);
	exit;
}

$arrayData=array(
	'id_tana'			=> intval($post->get('id_tana')),
	'cognome_goblin' 	=> trim($post->get('cognome_goblin')),
	'nome_goblin' 		=> trim($post->get('nome_goblin')),
	'email_goblin'		=> trim($post->get('email_goblin')),
	'indirizzo_goblin'	=> trim($post->get('indirizzo_goblin')),
	'citta_goblin'		=> trim($post->get('citta_goblin')),
	'cap_goblin'		=> trim($post->get('cap_goblin')),
	'provincia_goblin'	=> trim($post->get('provincia_goblin')),
	'dataNascita_goblin'=> $nato->format('Y-m-d'),
	'data_associato'	=> $today->format('Y-m-d'),
	'contatto_goblin'	=> trim($post->get('contatto_goblin')),
	'note_goblin'		=> trim($post->get('note_goblin')),
);
$arrayName=array(
	'id_tana'			=> "Id Tana",
	'cognome_goblin' 	=> "Cognome",
	'nome_goblin' 		=> "Cognome",
	'email_goblin'		=> "Email",
	'indirizzo_goblin'	=> "Indirizzo",
	'citta_goblin'		=> "Città",
	'cap_goblin'		=> "Cap",
	'provincia_goblin'	=> "Provincia",
	'dataNascita_goblin'=> "Data di nascita",
	'data_associato'	=> "Data tesserato",
	'contatto_goblin'	=> "Contatto",
	'note_goblin'		=> "Note",
);

$arrayDataOld=array();
foreach ($arrayData as $key => $value) {
	$arrayDataOld[$key]=$goblin[$key];
}

$arrayDiff='';
$count=0;
if ($goblin['id_tana']!=0 ){
	unset($arrayData['data_associato']);
	unset($arrayDataOld['data_associato']);
	unset($arrayName['data_associato']);
}
if (isset($arrayData['id_tana']) && isset($arrayDataOld['id_tana'])){
	$tane=\GOBLINS\Tane::spec([$arrayData['id_tana'], $arrayDataOld['id_tana']]);
	$tane[0]='--nessuna tana--';	
}

foreach (array_diff($arrayDataOld, $arrayData) as $key => $value) {
	$arrayDiff.= $count>0 ? "<br>-":"-";
	if ($key=='id_tana'){
		if ($arrayDataOld['id_tana']==\APP\Parameter::getSpec('id_tana_nazionale')){
			$nameDa='Tana Nazionale';
		} else {
			$nameDa=str_replace('La Tana dei Goblin', '', $tane[$arrayDataOld['id_tana']]);
		}

		if ($arrayData['id_tana']==\APP\Parameter::getSpec('id_tana_nazionale')){
			$nameA='Tana Nazionale';
		} else {
			$nameA=str_replace('La Tana dei Goblin', '', $tane[$arrayData['id_tana']]);
		}
		$arrayDiff.="Tana da <b>{$nameDa}</b> a <b>{$nameA}</b>";
	} else {
		$arrayDiff.="{$arrayName[$key]} da <b>{$arrayDataOld[$key]}</b> a <b>{$arrayData[$key]}</b>";
	}
	$count++;
}

if ($count<=0){
	$arrayReturn['result']=406;
	echo json_encode($arrayReturn);
	exit;
}

\GOBLINS\Manage::setSpec($id_goblin, $arrayData);
\GOBLINS\Manage::addCards($id_goblin);
if ($goblin['id_tana']==0){
	/*Case A*/
	\LOGS\Log::write('MY GOBLINS', 'added user '.$id_goblin.": old values:(".json_encode($arrayDataOld).") new values:(".json_encode($arrayData).")" , false, \USERS\Identify::UserID());
	\LOGS\Tesserato::write($id_goblin, "Tesserato<br><i>dati modificati:</i><br>".$arrayDiff, 'fa-address-book-o');
	$arrayReturn['goblin']=$goblin['Nick'];
	$arrayReturn['tana']=$tane[$arrayData['id_tana']];
	$arrayReturn['result']=201;
	$paramsPass=array(
		'list_tane' => explode(',', \USERS\Identify::get('tane_riferimento')),
	);
	$arrayReturn['elements']=count(\GOBLINS\Manage::getAll(0, $paramsPass));
	echo json_encode($arrayReturn);
	exit;
}

/*Case b) My Goblin: edit data*/


\LOGS\Log::write('MY GOBLINS', 'change data user '.$id_goblin." : old values:(".json_encode($arrayDataOld).") new values:(".json_encode($arrayData).")", false, \USERS\Identify::UserID());

\LOGS\Tesserato::write($id_goblin, "Modificato<br><i>dati modificati:</i><br>".$arrayDiff, 'fa-id-card-o');
echo json_encode($arrayReturn);
