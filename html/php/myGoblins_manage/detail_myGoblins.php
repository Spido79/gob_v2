<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
//Stampa tessera
//sospendi associato ( e metti in tana nazionale)
$id_goblin=intval($post->get('id'));
$goblin=\GOBLINS\Manage::getSpec($id_goblin);
if (!in_array($goblin['id_tana'], explode(",",\USERS\Identify::get('tane_riferimento')))){
	\HTML\Page::Page_404();	
}

/*
	Determina tana di destinazione se disassociato. Text and id
*/
$futura_tana=array(
	'id' 	=>\APP\Parameter::getSpec('id_tana_nazionale'),
	'name'	=>\GOBLINS\Tane::spec([\APP\Parameter::getSpec('id_tana_nazionale')])[\APP\Parameter::getSpec('id_tana_nazionale')],
);
if ( $goblin['id_tana']== \APP\Parameter::getSpec('id_tana_nazionale')){
	$futura_tana=array(
		'id' 	=> 0,
		'name'	=> "<i>--nessuna tana--</i>",
	);	
}
?>
<div class="ibox selected">
	<div class="ibox-content">
		<div class="tab-content">
			<div class="tab-pane active">
				<div class="row m-b-lg">
					<div class="col-lg-6 text-center">
						<h2>
						<?php echo $goblin['nome_goblin'];?>
						<?php echo $goblin['cognome_goblin'];?>
						</h2>

						<div class="jokerman" style="font-size:23px;">
							<?php echo $goblin['Nick'];?>
						</div>

						<p>
							<i>email:</i><br>
							<span class="text-danger"><?php echo $goblin['email_goblin'];?></span>
							<?php 
							if ($goblin['contatto_goblin']!=''){
								echo "<br><i>contatto:</i><br><span class='text-success'>".$goblin['contatto_goblin'].'</span>';
							}
							?>
						</p>
					</div>
					<div class="col-lg-6">
						<h4>
							<?php echo \Dictonary\Translation::word('Personal data');?>
						</h4>
						
						<p>
							<i>nato il:</i> <br>
							<?php 
							$dateGob= new DateTime($goblin['dataNascita_goblin']);
							echo $dateGob->format('d.m.Y');
							?>
							<br>
							<i>residente:</i><br>
							<?php echo $goblin['indirizzo_goblin'];?><br>
							<?php echo $goblin['cap_goblin'];?> - <?php echo $goblin['citta_goblin'];?> (<?php echo $goblin['provincia_goblin'];?>)
						</p>
						<div class="btn-group btn-block">
							<button data-toggle="dropdown" class="btn btn-primary btn-sm btn-block dropdown-toggle">Azioni  <span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li>
									<a class="dropdown-item btn_printCardGob" ><i class="fa fa-id-card-o"></i> Download tessera</a>
								</li>
								<li class="divider"></li>
								<li>
									<a class="dropdown-item" href="mailto:<?php echo $goblin['email_goblin'];?>"><i class="fa fa-envelope-o"></i> Invia Email</a>
								</li>
								<li class="divider"></li>
								<li>
									<a class="dropdown-item btn_modGoblin" ><i class="fa fa-floppy-o"></i> Modifica dati</a>
								</li>
								<li class="divider"></li>
								<li>
									<a class="dropdown-item btn_myGObDisassocia" ><i class="fa fa-remove text-danger"></i> <span class="text-danger">Ditessera utente</span></a>
								</li>
							</ul>
						</div>
						<?php
						if (\GOBLINS\Request::check($id_goblin)){
						?>
						<div class="btn-group btn-block">
							<button data-toggle="dropdown" class="btn btn-warning btn-sm btn-block dropdown-toggle">Trasferimento  <span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li>
									<a class="dropdown-item btn_request" data-id='1'><i class="fa fa-check"></i> Accetta richiesta</a>
								</li>
								<li class="divider"></li>
								<li>
									<a class="dropdown-item btn_request" data-id='0'><i class="fa fa-remove"></i> Rifiuta richiesta</a>
								</li>
							</ul>
						</div>
						<span class='tras_tana hidden'>
							<?php 
							$arrayGet=\GOBLINS\Request::get($id_goblin);
							echo $arrayGet['tana_name'];
							?>
						</span>
						<?php
						} else {
							echo "<span class='tras_tana hidden'></span>";
						}
						?>
					</div>
					<input type="hidden" value="<?php echo $id_goblin;?>" class="list_goblins_selected_input" />
				</div>
				<div class='row'>
					<div class='col-xs-12'>
						<div class="client-detail" style='height: auto;'>
							<div class="full-height-scroll">

								<strong>Tesseramento:</strong>
								<?php 
								foreach (GOBLINS\Tane::spec(array($goblin['id_tana'])) as $value) {
									echo "<h4 class='tana_name' style='margin-bottom:0px;'>{$value}</h4>";
								}
								?>
									
								<ul class="list-group clear-list">
									<li class="list-group-item fist-item">
										<span class="float-right"> Tessera n. </span>
										<b><?php echo str_pad($goblin['tessera_numero'], 5,'0',STR_PAD_LEFT);?></b>
										del 
										<?php 
										$dateGob= new DateTime($goblin['data_associato']);
										echo $dateGob->format('d.m.Y');
										?>
									</li>
									<li class="list-group-item">
										<span class="float-right"> codice: </span>
										<i><?php echo str_pad($goblin['codice_tessera'], 4,'-',STR_PAD_LEFT);?></i>
									</li>
								</ul>
								<?php
								if ($goblin['note_goblin']!=''){
									echo "<strong>Note</strong>
										<p>{$goblin['note_goblin']}</p>";
								}
								?>
								<hr/>

								<?php
									$activity=\LOGS\Tesserato::read($id_goblin, 1);
									if(count($activity)>0){
									?>
									<strong>Attività Goblin</strong>
									<div id="vertical-timeline" class="vertical-container dark-timeline">
									<?php
									foreach ($activity as $item) {
										$dataMod=new DateTime($item['dataMod']);
										$userMod=new \USERS\Detail($item['id_user'],null,1);
										$nomeUM=$userMod->get('user_name'). ' '.$userMod->get('user_surname');
										if (trim($nomeUM)==''){
											$nomeUM=$item['id_user'];
										}
										echo'	<div class="vertical-timeline-block">
													<div class="vertical-timeline-icon navy-bg">
														<i class="fa '.$item['icon'].'"></i>
													</div>
													<div class="vertical-timeline-content">
														<p>'.$item['description'].'</p>
														<span class="vertical-date small text-muted"><i>da '.$nomeUM.'</i></span><br>
														<span class="vertical-date small text-muted">'.$dataMod->format('d.m.Y').' - '.$dataMod->format('H:i').'</span>
													</div>
												</div>';
									}
									?>
									</div>
									<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_modGoblin').click(function(event) {
		edit_goblin($('.list_goblins_selected_input').val());
	});

	$('.btn_request').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		var accept=$(this).attr('data-id');
		var id= $('.list_goblins_selected_input').val();
		var nameGob=$('.jokerman').text();
		var trasTan=$('.tras_tana').text();
		var textAccept="<i class='text-danger fa fa-warning infinite animated flash'></i> Sei sicuro di voler <span class='text-danger'>annullare la richiesta</span> di trasferimento per <strong>"+nameGob+"</strong>?<br>Il goblin <strong> NON</strong> verrà trasferito a <span class='text-danger'>"+trasTan+"</span>";
		if (accept==1){
			textAccept="<i class='text-danger fa fa-warning infinite animated flash'></i> Sei sicuro di voler <span class='text-danger'>trasferire</span> <strong>"+nameGob+"</strong>?<br>Il goblin verrà trasferito a <span class='text-danger'>"+trasTan+"</span>";
		}
		swal({
			title: "Trasferisci goblin",
			text: textAccept,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('accept',accept);
				var returnEngine = call_ajax_page(postData,'myGoblins_manage/transfer_goblin',id);
				returnEngine.always(function (returndata) {
					detail_myGoblins(0, '', 1);
					$('.countTotalMyGoblins').val(returndata);
					$('.disp_countTotalMyGoblins').empty().append(returndata+' elementi');
					$('.table_users_json').DataTable().ajax.reload();
					$('button').prop('disabled',false);
				})
			}
		});
	});
	$('.btn_myGObDisassocia').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var elements= $('.list_goblins_selected_input').val();
		var btnTemp=$(this);
		var nameGob=$('.jokerman').text();
		var attuale=$('.tana_name').text();
		var futura="<?php echo $futura_tana['name'];?>";
		swal({
			title: "Ditessera goblin",
			text: "<i class='text-danger fa fa-warning infinite animated flash'></i> Sei sicuro di voler <span class='text-danger'>ditesserare</span> <strong>"+nameGob+"</strong>?<br><br>Tesserato attualmente: <strong>"+attuale+"</strong><br>Nuova tana: <strong class='text-success'>"+futura+"</strong>",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('future',<?php echo $futura_tana['id'];?>);
				var returnEngine = call_ajax_page(postData,'myGoblins_manage/delete_goblin',elements);
				returnEngine.always(function (returndata) {
					detail_myGoblins(0, '', 1);
					$('.countTotalMyGoblins').val(returndata);
					$('.disp_countTotalMyGoblins').empty().append(returndata+' elementi');
					$('.table_users_json').DataTable().ajax.reload();
					$('button').prop('disabled',false);
					btnTemp.empty().append('<i class="fa fa-remove text-danger"></i> <span class="text-danger">Ditessera utente</span>');
				})
			}
		});
	});
});
</script>