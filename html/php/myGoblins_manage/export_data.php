<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}

if(!$post->VerifyPostData(['list','id','redirect_to'])) {
	\HTML\Page::Page_404();
}
//crea csv lista
$paramsPass=array(
	'list_tane' => explode(',', \USERS\Identify::get('tane_riferimento')),
	'list_goblins'=> json_decode($post->get('list')),
	'map_fields'	=> array(
		'Nick'				=>'Nick',
		'cognome_goblin'	=>'Cognome',
		'nome_goblin'		=>'Nome',
		'email_goblin'		=>'Email',
	),
);
$GoblinsSelected=\GOBLINS\Manage::getAll(0, $paramsPass);
echo json_encode($GoblinsSelected);