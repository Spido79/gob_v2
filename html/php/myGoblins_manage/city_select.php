<?php 
if (isset($post)==false){
	$virtualPath='..';
	require_once("../../../lib/init.php");
}
if(!$post->VerifyPostData(['id','redirect_to'])) {
	\HTML\Page::Page_404();
}
$field=intval($post->get('id'));

$params=array(
	'order' => 'Comune',
);
$allCity=\COMUNI\Istat::getAll($params);

$readCity=array();
$mapData=array(
	'city' => array(),
	'city_compare' => array(),
	'cap' => array(),
	'prov' => array(),
);
foreach ($allCity as $city) {
	$readCity[]=$city['Comune'].'|'.explode(',', $city['CAP'])[0].'|'.$city['Provincia'];
}

$params=array(
	'order' => 'CAP',
);
$allCity=\COMUNI\Istat::getAll($params);

$readCap=array();
foreach ($allCity as $city) {
	$capExplo=explode(',', $city['CAP']);
	foreach ($capExplo as $cap) {
		$readCap[]=$city['Comune'].'|'.$cap.'|'.$city['Provincia'];
		$mapData['city'][]=$city['Comune'];
		$mapData['city_compare'][]=strtolower($city['Comune']);
		$mapData['cap'][]=$cap;
		$mapData['prov'][]=$city['Provincia'];
	}
}

$array=array(
	'city'		=> $readCity,
	'cap'		=> $readCap,
	'map'		=> $mapData,
);
header("Content-type: text/javascript");
header('Content-Encoding: gzip'); 
echo gzencode(json_encode($array));