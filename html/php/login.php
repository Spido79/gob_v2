<?php 
if (isset($post)==false){
    $virtualPath='.';
    require_once("../../lib/init.php");
}

if(!$post->VerifyPostData(['pass','user','id','redirect_to'])) {
    \HTML\Page::Page_404();
}

if (\IP_GEST\IP_list::verBlocked()){
	echo 404;
	//IP BLOCKED! Write email and segnalate to user_error()
	$subject ="Portale gestione - BLOCCO IP";
	\MAIL\Create::setVal('mail_from','spido@goblins.net');
	\MAIL\Create::setVal('mail_to','spido@goblins.net');
	\MAIL\Create::setVal('name_from','Portale Gestione - Goblins');
	\MAIL\Create::setVal('name_to','Spido');
	\MAIL\Create::setVal('reply_to','no-replay@goblins.net');
	\MAIL\Create::setVal('mail_subject',$subject);

	$dateTime=new \DateTime();
	$arrayReplace=array(
		'subject'	=> $subject,
		'ip'		=> $_SERVER['REMOTE_ADDR'],
		'date'		=> $dateTime->format('d M Y'),
		'hour'		=> $dateTime->format('H:i'),
		'link'		=> APP_URL
		);
	\MAIL\Create::setTemplate('login/ip_blocked',$arrayReplace);

	\MAIL\Create::insert();
	exit;
}

//Verifico se l'ip è bloccato, se no tento il login.
$pass=trim($post->get('pass'));
$user=trim($post->get('user'));

//se login ok, clean, se no incrase.
if (\USERS\Identify::verify_connection($user,$pass)){
	\IP_GEST\IP_list::IncraseIP($user,$pass);
	\IP_GEST\IP_list::CleanIP();
	echo 200;
} else {
	\IP_GEST\IP_list::IncraseIP($user,$pass);
	echo 401;
}
