<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	var json_returned={};
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'core_options/goblins_select',-2);
	returnEngine.always(function (returndata) {
		json_returned=$.parseJSON(returndata);

		$(".select2_core_data_goblins").typeahead({ 
			source:json_returned.Nick,
		});
	});

	$('.btn_coreOptions_selectNick').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var text=$.trim($('.select2_core_data_goblins').val());
		$(this).removeClass('btn-danger');
		$('.select2_core_data_goblins').parent().removeClass('has-error');
		if (text.length<=0){
			$(this).addClass('btn-danger');
			$('.select2_core_data_goblins').parent().addClass('has-error');
			return false;	
		}
		var found=0;
		$.each(json_returned.Nick, function(index, val) {
			if (text.toLowerCase()==val.toLowerCase()){
				found=1;
				return;
			}
		});
		if (found==0){
			$('.select2_core_data_goblins').parent().addClass('has-error');
			$(this).addClass('btn-danger');
			return false;	
		}
		//nick found! open box
		call_core_goblin(0, text);
	});
});

function call_core_goblin(id, text){
	$('button').prop('disabled',true);
	$('.admin_core_options_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	postData.append('nick',text);
	var returnEngine = call_ajax_page(postData,'core_options/goblin_show',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.admin_core_options_right').empty().append(returndata);
	});
}
</script>