<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">

$(document).ready(function() {
	/*CROPPER*/
	var $image = $(".image-crop > img");
	$($image).cropper({
		aspectRatio: 1,
		done: function(data) {

		}
	});

	$(".btn_save_picProfile").click(function() {
		var imgString=$.trim($image.cropper("getDataURL"));
		if (imgString==''){
			swal({
				title: "Operazione non valida",
				text: "Carica e ritaglia un'immagine da usare nel tuo profilo.",
				type: "info",
				showCancelButton: false,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok",
				html:true,
				closeOnConfirm: true,
			});	
			return;
		}
		
		var btnTemp=$(this);
		btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
		$('button').prop('disabled',true);
		var postData = new FormData();
		postData.append('imgString',imgString);
		var returnEngine = call_ajax_page(postData,'profile/upload_picture',0);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			btnTemp.empty().append('<i class="fa fa-floppy-o"></i> Salva');
			swal({
				title: "Immagine sostituita",
				text: "L'immagine del tuo profilo è stata sostituita correttamente.",
				type: "success",
				showCancelButton: false,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok",
				html:true,
				closeOnConfirm: true,
			});	
			var json_ret=JSON.parse(returndata);
			$('.profile_personal_img').attr('src',json_ret.image);
			$('.modal_pictureProfile').modal('hide');
			$inputImage.val("");
		});

	});
	var $inputImage = $("#inputImageProfile");
	if (window.FileReader) {
		$inputImage.change(function() {
			var fileReader = new FileReader(),
			files = this.files,
			file;

			if (!files.length) {
				return;
			}

			file = files[0];

			if (/^image\/\w+$/.test(file.type)) {
				fileReader.readAsDataURL(file);
				fileReader.onload = function () {
					$inputImage.val("");
					$image.cropper("reset", true).cropper("replace", this.result);
				};
			} else {
				swal({
					title: "Immagine non valida.",
					text: "Seleziona un tipo di immagine valida (jpg/png)",
					type: "error",
					showCancelButton: false,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Ok",
					html:true,
					closeOnConfirm: true,
				});	
			}
		});
	} else {
		$inputImage.addClass("hide");
	}
	$('.btn_changeProfile_picture').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		$('.modal_pictureProfile').modal('show');
	});

	$(document).on('click', '.btn_changePassword', function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var actualPass=$.trim($('.actual_password').val());
		var newPass=$.trim($('.new_password').val());
		var newBPass=$.trim($('.newB_password').val());
		var errorT=0;
		$('.actual_password').parent().removeClass('has-error');
		$('.actual_password').parent().removeClass('has-warning');
		$('.new_password').parent().removeClass('has-error');
		$('.newB_password').parent().removeClass('has-error');
		$('.new_password').parent().removeClass('has-warning');
		$('.newB_password').parent().removeClass('has-warning');
	
		if (actualPass=='' || actualPass.length<5){
			$('.actual_password').parent().addClass('has-error');
			errorT=1;
		}
		
		if (newPass=='' || newPass.length<5){
			$('.new_password').parent().addClass('has-error');
			errorT=1;
		}
		if (newBPass=='' || newBPass.length<5){
			$('.newB_password').parent().addClass('has-error');
			errorT=1;
		}

		if (newBPass!=newPass){
			$('.new_password').parent().addClass('has-warning');
			$('.newB_password').parent().addClass('has-warning');
			errorT=1;
		}

		if (newPass==actualPass){
			$('.new_password').parent().addClass('has-warning');
			$('.actual_password').parent().addClass('has-warning');
			errorT=1;
		}

		if (errorT==1){
			return false;
		}
		$('button').prop('disabled',true);
		var htmlT=$(this).html();
		$(this).empty().append('<i class="fa fa-spinner fa-pulse"></i> Attendere');
		var buttonT=$(this);

		var postData = new FormData();
		postData.append('actualPass',actualPass);
		postData.append('newPass',newPass);
		postData.append('newBPass',newBPass);
		var returnEngine = call_ajax_page(postData,'profile/change_password',0);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			buttonT.empty().append(htmlT);
			switch(parseInt(returndata)){
				case 200:
					swal({
						title: "Password modificata",
						text: "La tua password di accesso è stata modifica correttamente.<br/>Ricordati che dal prossimo accesso dovrai utilizzare la tua nuova passowrd.",
						type: "success",
						showCancelButton: false,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Ok",
						html:true,
						closeOnConfirm: true,
					},function(){
						$('.row_changePassword_main').addClass('hidden');
						$('.row_changePassword_success').removeClass('hidden');
					});
					break;

				case 406:
					swal({
						title: "Password attuale non corretta",
						text: "La password <strong>attuale</strong> inserita non è corretta.<br/>Riscrivila e riprova.",
						type: "error",
						showCancelButton: false,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Ok",
						html:true,
						closeOnConfirm: true,
					},function(){
						$('.actual_password').val('');
					});
					break;

				case 409:
				case 411:
				case 412:
					swal({
						title: "Password non conformi",
						text: "Qualcosa non ha funzionato, riverifica le password e riprova.",
						type: "error",
						showCancelButton: false,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Ok",
						html:true,
						closeOnConfirm: true,
					},function(){
						$('.actual_password').val('');
						$('.new_password').val('');
						$('.newB_password').val('');
					});
					break;
			}
		});
	});
});
</script>