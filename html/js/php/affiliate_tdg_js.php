<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	city_tane_compile();
	$(document).keydown(function(e) {
		if ($('.table_TdG_json tbody .selected').length<=0){
			return;
		}
		switch(e.which) {
			case 38:
				$('.table_TdG_json tbody .selected').prev().click();
				break;

			case 40:
				$('.table_TdG_json tbody .selected').next().click();
			break;
			default: return;
		}
		e.preventDefault();
	});
	
	$(document).on('click', 'button.field[data-name="button-modTana"]', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		var btnTempTxt=$(this).html();
		var id=$('input.field[data-name="id_tana"]').val();
		var postData = new FormData();
		var error=0;
		$.each($('input.field, textarea.field'), function(index, val) {
			$(this).parent().removeClass('has-error');
			var valDef=$.trim($(this).val());
			if ($(this).prop('type')=='radio' && !$(this).prop('checked')){
				return;
			}
			if ($(this).attr('required') && valDef=='' && !$(this).attr('disabled') && !$(this).parent().hasClass('hidden')){
				error=1;
				$(this).parent().addClass('has-error');
			}

			if (($(this).attr('type') && $(this).attr('type').toLowerCase()=='email') && !ValidateEmail($(this).val()) ){
				error=1;
				$(this).parent().addClass('has-error');
			}
			postData.append($(this).attr('data-name'),valDef);
		});
		if (error==1){
			return false;
		}
		swal({
			title: "Tane",
			text: "Sei sicuro di voler continuare?",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var returnEngine = call_ajax_page(postData,'affiliate_tdg/save_tana',id);
				returnEngine.always(function (returndata) {
					detail_tdgManage(id);
					$('.table_TdG_json').DataTable().ajax.reload();
					$('button').prop('disabled',false);
					btnTemp.empty().append(btnTempTxt);
					$('.modal_editTana').modal('hide');
				});
			}
		});
	});

	$(document).on('click', '.btn_NewTana', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		call_tana_modify($(this));
	});

	$(document).on('click', '.btn_tana_mod', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		call_tana_modify($(this));
	});

	$(document).on('click', '.btn_tana_autoRenew', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		var btnTempTxt=$(this).html();
		var id=$(this).attr('data-id');
		var postData = new FormData();
		swal({
			title: "Tane",
			text: "Sei sicuro di voler riunnovare l'affiliazione di questa tana?",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var returnEngine = call_ajax_page(postData,'affiliate_tdg/renew_tana',id);
				returnEngine.always(function (returndata) {
					$('.table_TdG_json').DataTable().ajax.reload();
					$('button').prop('disabled',false);
					btnTemp.empty().append(btnTempTxt);
					detail_tdgManage(id);
				});
			}
		});
	});

	$(document).on('click', 'input.field[data-name="competenza_Prov"]', function(event) {
		verCheck_competenza();
	});

	$(document).on('click', '.btn_tana_enadis', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		call_tana_enaDis($(this));
	});
	
	$('.table_TdG_json tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected')) {
			$(this).removeClass('selected');
			detail_tdgManage(0);
			
		} else {
			$('.table_TdG_json tr.selected').removeClass('selected');
			var id_tdg= (dataTables_TdgAffiliate.row($(this).closest('tr')).data()[8]);
			detail_tdgManage(id_tdg);
		}
	});
	$('.disabled_chk').tooltip({html:true});

	$('#checkbox_tdg_disa').click(function(event) {
		$('.table_TdG_json').DataTable().ajax.reload();
	});
	
	var dataTables_TdgAffiliate= $('.table_TdG_json').DataTable({
		"language": {
			"lengthMenu": "Mostra _MENU_ elementi",
			"zeroRecords": "Non è stato trovato niente - riprova",
			"info": "Pagina _PAGE_ di _PAGES_",
			"search": "Cerca",
			"paginate": {
				"previous": "Indietro",
				"next": "Avanti"
			},
			"infoEmpty": "Nessun elemento disponibile",
			"infoFiltered": "(filtrati da _MAX_ elementi totali)"
		},
		processing: true,
		createdRow: function ( row, data, index ) {
			if (parseInt(data[8])==<?php echo \APP\Parameter::getSpec('id_tana_nazionale');?>){
				$('td', row).eq(0).empty().append('<span class="text-danger">Tana Nazionale</span>');
			}
			if (parseInt(data[9])==0){
				$('td', row).parent().addClass('selected_multi');
			} else if (parseInt(data[9])==2){
				$('td', row).parent().addClass('selected_req');
			}

			if (parseInt(data[10])!=1){
				$('td', row).parent().removeClass('selected_multi');
				$('td', row).parent().removeClass('selected_req');
				$('td', row).parent().addClass('selected_disabled');
			}
        },
		lengthMenu: [[10, 25, $('.countTotalTane').val()], [10, 25, "Tutti"]],
		serverSide: true,
		ajax: {
			url: APP_URL+'/_GHOST.php',
			type: 'POST',
	        async:true,
	        mimeType:"multipart/form-data",
	        cache: false,
			data: function ( d ) {
				d.redirect_to =APP_URL+"/php/affiliate_tdg/list_affiliate.php";
				d.id=1;
				d.recordTotal=$('.countTotalTane').val();
				d.req=$('#checkbox_tdg_disa').prop('checked');
			}
		},
		pageLength: 10,
		responsive: true,
		dom: '<"html5buttons"B>lTfgtip',
		buttons: [{
				extend: 'pdfHtml5', 
				title: 'Elenco Affiliate',
				exportOptions: {
                    columns: [ 0,1,4 ],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			},{
				extend: 'csvHtml5', 
				title: 'Elenco Affiliate CSV',
				fieldSeparator: ';',
				exportOptions: {
                    columns: [ 0,1,2,3,4,5 ],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			}
		],
		"order": [[ 4, "asc" ]],
		"columnDefs": [ {
			"targets": 'no-sort',
			"orderable": false,
		},{
                "targets": [6, 7, 8, 9, 10],
                "visible": false,
                "searchable": false
        }],
	});
});

var json_city_tane={};
var json_nick_tane={};
function city_tane_compile(){
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'affiliate_tdg/city_select',1);
	returnEngine.always(function (returndata) {
		json_city_tane=$.parseJSON(returndata);
	});

	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'affiliate_tdg/nick_select',1);
	returnEngine.always(function (returndata) {
		json_nick_tane=$.parseJSON(returndata);
	});
}

function city_select_tane(){
	$('input.field[data-name="Citta"]').typeahead('destroy');
	$('input.field[data-name="Citta"]').typeahead({ 
		source: function (query, process) {
			process(json_city_tane.city);
		},
		matcher : function(item) {
			return this.__proto__.matcher.call(this,item.split("|")[0]);
		},

		highlighter: function(item) {
			return this.__proto__.highlighter.call(this,item.split("|")[0]);
		},
		updater: function(item) {
			var itemArray = item.split("|");
			return this.__proto__.updater.call(this,itemArray[0]);
		}
	}).bind('change blur', function () {
		var indexInside=$.inArray($(this).val().toLowerCase(), json_city_tane.map.city_compare);
		if (indexInside === -1) {
			$(this).val('');
			$('input.field[data-name="Provincia"]').val('');
		} else {
			$(this).val(json_city_tane.map.city[indexInside]);
			$('input.field[data-name="Provincia"]').val(json_city_tane.map.prov[indexInside]);
		}
	});

	$('input.field[data-name="Provincia"]').typeahead('destroy');
	$('input.field[data-name="Provincia"]').typeahead({ 
		source: function (query, process) {
			process(json_city_tane.prov);
		},
		matcher : function(item) {
			return this.__proto__.matcher.call(this,item);
		},

		highlighter: function(item) {
			return this.__proto__.highlighter.call(this,item);
		},
		updater: function(item) {
			return this.__proto__.updater.call(this,item);
		}
	}).bind('change blur', function () {
		var indexInside=$.inArray($(this).val().toLowerCase(), json_city_tane.map.prov_compare);
		if (indexInside === -1) {
			$(this).val('');
		} else {
			$(this).val(json_city_tane.map.prov[indexInside]);
		}
		$('input.field[data-name="Citta"]').val('');
	});

	$('input.field[data-name="nick_presidente"]').typeahead('destroy');
	$('input.field[data-name="nick_presidente"]').typeahead({ 
		source: function (query, process) {
			process(json_nick_tane.gob);
		},
		matcher : function(item) {
			return this.__proto__.matcher.call(this,item);
		},

		highlighter: function(item) {
			return this.__proto__.highlighter.call(this,item);
		},
		updater: function(item) {
			return this.__proto__.updater.call(this,item);
		}
	}).bind('change blur', function () {
		var indexInside=$.inArray($(this).val().toLowerCase(), json_nick_tane.gob_compare);
		if (indexInside === -1) {
			$(this).val('');
		} else {
			$(this).val(json_nick_tane.gob[indexInside]);
		}
	});
}

function detail_tdgManage(id){
	if (id==0){
		$('.detail_TdGAffiliate').empty();
		return;
	}
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'affiliate_tdg/detail_tdg',id);
	returnEngine.always(function (returndata) {
		$('.detail_TdGAffiliate').empty().append(returndata);
	});
}

function call_tana_enaDis(btn){
	var id=btn.attr('data-id');
	var action=parseInt(btn.attr('data-action'));
	var stop=0;
	var parSwal={
		'title' : '',
		'message' : '',
		'type' : 'warning',
	};

	switch(action) {
		case 0: //disabilita
			parSwal['title']='Disabilita tana';
			parSwal['message']='<strong>Attenzione:</strong> disabilitando la tana selzionata, tutti i suoi tesserati verranno trasferiti alla "<span class="font-bold text-danger">Tana Nazionale</span>".<p>I responsabili di questa tana verranno <b>rimossi</b> dall\'incarico. Se vorrai riabilitare in futuro questa tana dovrai <u>riassociare nuovamente</u> i responsabili.</p>Sei sicuro di voler continuare?';
			break;
		
		case 1: //riabilita
			parSwal['title']='Abilita tana';
			parSwal['message']='Sei sicuro di voler riabilitare questa tana? Ricorda che dovrai asscoiare un responsabile se vuoi che venga gestita.';
			break;
		
		case 2: //disattiva
			parSwal['title']='Disattiva tana';
			parSwal['message']='<strong>Attenzione:</strong> disattivando la tana selzionata, tutti i suoi tesserati verranno trasferiti alla "<span class="font-bold text-danger">Tana Nazionale</span>".<p>I responsabili di questa tana verranno <b>rimossi</b> dall\'incarico.<br><br>Inserisci la motivazione e la data di disdetta della tana.<br><div class="col-sm-12 form-group text-left"><label>Note</label><textarea rows="4" placeholder="Motivazione" class="form-control motivo_disdetta" style="resize: none;"></textarea></div></textarea><br><div class="col-sm-12 form-group text-left"><label>Data Disdetta</label><input type="text" placeholder="Data disdetta" style="display: block;" class="form-control Data_Risoluzione" required></div><br> Se vorrai riabilitare in futuro questa tana dovrai <u>riassociare nuovamente</u> i responsabili.</p>Sei sicuro di voler continuare?';
			break;
		
		case 3: //riattiva
			parSwal['title']='Attiva tana';
			parSwal['message']='Sei sicuro di voler riattivare questa tana? Ricorda che dovrai asscoiare un responsabile se vuoi che venga gestita.';
			break;
		
		default:
		return false;
	} 

	swal({
		title: parSwal['title'],
		text: parSwal['message'],
		type:  parSwal['type'],
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Si",
		cancelButtonText: "Annulla",
		html:true,
		closeOnConfirm: false,
	},function(isConfirm){
		if (isConfirm) {
			execute_action(btn);
		}
	});
	$('input.Data_Risoluzione').datepicker({
		startView: 0,
		todayBtn: "linked",
		setStartDate: new Date(),
		keyboardNavigation: false,
		immediateUpdates:true,
		forceParse: true,
		autoclose: true,
		format: "dd/mm/yyyy",
		toggleActive:false,
	});
}

function verCheck_competenza(){
	var value=parseInt($('input:radio.field[data-name="competenza_Prov"]:checked').val());
	if ($('.row[data-nazionale] div:first-child').hasClass('hidden')){
		return;
	}
	if (value==0){
		$('input.field[data-name="Citta"]').parent().addClass('hidden');
		$('input.field[data-name="Provincia"]').parent().removeClass('hidden');
	} else {
		$('input.field[data-name="Provincia"]').parent().addClass('hidden');
		$('input.field[data-name="Citta"]').parent().removeClass('hidden');
	}
}

function execute_action(btn){
	var id=btn.attr('data-id');
	var action=parseInt(btn.attr('data-action'));
	var btnHtml=btn.html();
	var postData = new FormData();
	if ($('textarea.motivo_disdetta').length>0 || $('input.Data_Risoluzione').length>0){
		var motivo_disdetta=$.trim($('textarea.motivo_disdetta').val());
		var Data_Risoluzione=$.trim($('input.Data_Risoluzione').val());
		$('textarea.motivo_disdetta').parent().removeClass('has-error');
		$('input.Data_Risoluzione').parent().removeClass('has-error');
		var errorDis=0;
		if (motivo_disdetta==''){
			$('textarea.motivo_disdetta').parent().addClass('has-error');
			errorDis=1;
		}
		if (Data_Risoluzione==''){
			$('input.Data_Risoluzione').parent().addClass('has-error');
			errorDis=1;
		}
		if (errorDis==1){
			return false;
		}
		postData.append('motivo_disdetta',motivo_disdetta);
		postData.append('Data_Risoluzione',Data_Risoluzione);

	}
	swal.close(); 
	$('button').prop('disabled',true);
	btn.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
	
	postData.append('action',action);
	var returnEngine = call_ajax_page(postData,'affiliate_tdg/ena_dis',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		btn.empty().append(btnHtml);
		detail_tdgManage(id);
		$('.table_TdG_json').DataTable().ajax.reload();
	});
}

function call_tana_modify(btn){
	var id=btn.attr('data-id');
	var btnHtml=btn.html();
	btn.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
	$('button').prop('disabled',true);
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'affiliate_tdg/edit_tdg',id);
	returnEngine.always(function (returndata) {
		var json_returned=$.parseJSON(returndata);
		$('button').prop('disabled',false);
		btn.empty().append(btnHtml);
		$('.subtitle_modal_tdgAff').empty().append(json_returned.title);
		$.each(json_returned.fields, function(index, val) {
			if ($('input:radio.field[data-name="'+index+'"]').length>0 ){
				$('input:radio.field[data-name="'+index+'"]').prop('checked',false);
				$('input:radio.field[value="'+val+'"][type="radio"][data-name="'+index+'"]').prop('checked',true);
			} else if ($('input.field[data-name="'+index+'"]').length>0 || $('button.field[data-name="'+index+'"]').length>0 ){
				$('.field[data-name="'+index+'"]').val(val);
			} else if ($('.field[data-name="'+index+'"]').length>0){
				$('.field[data-name="'+index+'"]').empty().append(val);	
			}
			if (json_returned.enable_edit==true){
				$('input.field[data-name="'+index+'"], textarea.field[data-name="'+index+'"]').prop('disabled','');
			} else {
				$('input.field[data-name="'+index+'"], textarea.field[data-name="'+index+'"]').prop('disabled','disabled');
			}
		});
		$.each(json_returned.date, function(index, val) {
			if ($('input.field[data-name="'+val+'"]').length>0){
				$('input.field[data-name="'+val+'"]').datepicker('update');
			}
		});
		if (parseInt(id)==parseInt($('.row[data-nazionale]').attr('data-nazionale'))){
			$('.row[data-nazionale] div').addClass('hidden');
			$('.row[data-nazionale] div:last-child').removeClass('hidden');
			$('.field[data-name="Nome"]').prop('readonly', true);
		} else {
			$('.row[data-nazionale] div').removeClass('hidden');
			$('.field[data-name="Nome"]').prop('readonly', false);
		}
		$('.field').parent().removeClass('has-error');
		$('.modal_editTana').modal('show');
		city_select_tane();
		verCheck_competenza();
	});
}

</script>