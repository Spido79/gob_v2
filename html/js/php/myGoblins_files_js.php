<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
    callFIles();
    $(document).on('click', '.btn_downloadFile_myFile', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var id=$(this).val();
        $('button').prop('disabled',true);
        var btnTemp=$(this);
        var bthHtml=btnTemp.html();
        btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
        var postData = new FormData();
        var returnEngine = call_ajax_page(postData,'myGoblins_files/download_file',id);
        returnEngine.always(function (returndata) {
            var json_file=JSON.parse(returndata);
            saveBase64(json_file.file, json_file.filename, json_file.extension);
            $('button').prop('disabled',false);
            btnTemp.empty().append(bthHtml);
        });
    });
});

function call_det_fileMy(id){
    if (id==0){
        $('.detail_myFile').empty();
        return;    
    }
    $('.detail_myFile').empty().append('<h3><i class="fa fa-spinner fa-pulse"></i> Attendere...');
    var postData = new FormData();
    var returnEngine = call_ajax_page(postData,'myGoblins_files/detail_files',id);
    returnEngine.always(function (returndata) {
        $('.detail_myFile').empty().append(returndata);
    });
}

function callFIles(){
    $('.json_tree').empty().append('<h3><i class="fa fa-spinner fa-pulse"></i> Attendere...');
    var postData = new FormData();
    var returnEngine = call_ajax_page(postData,'myGoblins_files/list_files',0);
    returnEngine.always(function (returndata) {
        var json_ret=JSON.parse(returndata);
        $('.json_tree').empty().jstree(json_ret);
        $(".json_tree").on("select_node.jstree", function(evt, data){
            if (data.node.original.type==1){
                //download tags data.node.oid on zip
                swal({
                    title: "Preparazione file",
                    text: "Attendere preparazione del file... <br><h1><i class='fa fa-spinner fa-pulse'></i></h1>",
                    type:  'info',
                    showConfirmButton:false,
                    showCancelButton: false,
                    html:true,
                    closeOnConfirm: false,
                    closeOnEsc: false,
                    closeOnClickOutside:false,
                    allowEscapeKey : false,
                    allowOutsideClick: false
                });
                var postData = new FormData();
                postData.append('tag',data.node.original.oid);
                var returnEngine = call_ajax_page(postData,'myGoblins_files/save_zip',0);
                returnEngine.always(function (returndata) {
                    var json_file=JSON.parse(returndata);
                    saveBase64(json_file.file, json_file.title,  json_file.extension);
                    swal.close(); 
                });
            } else {
                //open detail data.node.id
                call_det_fileMy(data.node.original.oid);
            }
        });
    });
}
</script>