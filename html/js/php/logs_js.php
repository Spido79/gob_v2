<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	logs_load_file('');
	$('.btn_searchLOGS').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var search=$.trim($('.search_log').val());
		logs_load_file(search);
	});
});
function logs_load_file(search){
	$('button').prop('disabled',true);
	$('.load_logs_file').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	postData.append('search',search);
	var returnEngine = call_ajax_page(postData,'logs/file',0);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.load_logs_file').empty().append(returndata);
	});
}
</script>