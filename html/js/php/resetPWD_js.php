<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_confirmResetPWS').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var email=$(this).attr('data-email');
		var token=$(this).val();
		var postData = new FormData();
		postData.append('email',email);
		postData.append('token',token);
		var returnEngine = call_ajax_page(postData,'resetPWD',0);
		returnEngine.always(function (returndata) {
			switch(parseInt(returndata)){
                case 200:
                    swal({
                        title: "Password resettata",
                        text: "Ti verrà inviata una mail contenente la tua nuova password.<br/><br/>La mail potrebbe arrivare entro un <strong>paio di minuti</strong>.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        html:true,
                        closeOnConfirm: true,
                    },function(){
  						window.location.href=APP_URL;
  					});
                    break;

                case 401: //token/mail wrong
                    swal({
                        title: "Token inesistente",
                        text: "Il reset della password non è possibile per l'utente richiesto in quanto il link utilizzato <strong>non risulta attivo</strong> nel sistema, potrebbero essere passate più di <strong>12 ore</strong> dalla mail inviata, richiedine una nuova. <br/><strong>Attenzione:</strong> dopo alcuni tentativi con lo stesso link il tuo <strong>indirizzo ip</strong> verrà bloccato.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        html:true,
                        closeOnConfirm: true,
                    });
                    break;

                case 404: //ip blocked
                    swal({
                        title: "IP Bloccato",
                        text: "<strong>Attenzione:</strong> il tuo <strong>indirizzo ip</strong> risulta bloccato dal sistema a seguito di molti tentativi sbagliati.<br/><br/>Contatta gli amministratori di sistema per far rimuovere il <strong>blocco</strong>.",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        html:true,
                        closeOnConfirm: true,
                    });
                    break;
            }
		});
	});

});
</script>