<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	city_comiple();
	$('input.field[data-name="dataNascita_goblin"]').datepicker({
		startView: 2,
		todayBtn: "linked",
		keyboardNavigation: false,
		immediateUpdates:true,
		forceParse: true,
		autoclose: true,
		format: "dd/mm/yyyy",
		toggleActive:false,
	});

	$(document).keydown(function(e) {
		if ($('.table_users_json tbody .selected').length<=0){
			return;
		}
		switch(e.which) {
			case 38:
				$('.table_users_json tbody .selected').prev().click();
				break;

			case 40:
				$('.table_users_json tbody .selected').next().click();
			break;
			default: return;
		}
		e.preventDefault();
	});

	$('.multiSelect_chk').tooltip({html:true});

	var dataTables_myGoblins= $('.table_users_json').DataTable({
		"language": {
			"lengthMenu": "Mostra _MENU_ elementi",
			"zeroRecords": "Non è stato trovato niente - riprova",
			"info": "Pagina _PAGE_ di _PAGES_",
			"search": "Cerca",
			"paginate": {
				"previous": "Indietro",
				"next": "Avanti"
			},
			"infoEmpty": "Nessun elemento disponibile",
			"infoFiltered": "(filtrati da _MAX_ elementi totali)"
		},
		processing: true,
		createdRow: function ( row, data, index ) {
			if ($('#checkbox_myGob_multi').prop('checked')==true){
				var stop=false;
				$.each($('.list_goblins_selected_input').val().split(','), function(index, val) {
					if (stop==true){
						return true;
					}
					if (parseInt(data[3])==parseInt(val)){
						 $('td', row).parent().addClass('selected_multi');
						 stop=true;
					}
				});
			}
			if (parseInt(data[5])>0){
				$('td', row).parent().addClass('selected_req');
			}
        },
		lengthMenu: [[10, 25, $('.countTotalMyGoblins').val()], [10, 25, "Tutti"]],
		serverSide: true,
		ajax: {
			url: APP_URL+'/_GHOST.php',
			type: 'POST',
	        async:true,
	        mimeType:"multipart/form-data",
	        cache: false,
			data: function ( d ) {
				d.redirect_to =APP_URL+"/php/direttivo_manage/list_dirGoblins.php";
				d.id=1;
				d.recordTotal=$('.countTotalMyGoblins').val();
				d.req=$('#checkbox_myGob_req').prop('checked');
			}
		},
		pageLength: 10,
		responsive: true,
		dom: '<"html5buttons"B>lTfgtip',
		buttons: [{
				extend: 'pdfHtml5', 
				title: 'Elenco miei Goblins',
				exportOptions: {
                    columns: [ 0,1,2,4,6 ],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			},{
				extend: 'csvHtml5', 
				title: 'Elenco goblins CSV',
				fieldSeparator: ';',
				exportOptions: {
                    columns: [ 0,1,2,4,6 ],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			}
		],
		"order": [[ 0, "asc" ]],
		"columnDefs": [ {
			"targets": 'no-sort',
			"orderable": false,
		},{
                "targets": [3, 4, 5],
                "visible": false,
                "searchable": false
        }],
	});
	
	$('#checkbox_myGob_req').click(function(event) {
		$('.table_users_json').DataTable().ajax.reload();
	});

	$('#checkbox_myGob_multi').click(function(event) {
		detail_dirGoblins(0,'',0);
		$('.table_users_json tr.selected').removeClass('selected');
		$('.table_users_json tr.selected_multi').removeClass('selected_multi');
		if ($(this).prop('checked')==true){
			var htmlToWrite=`
<div class="ibox selected box_goblin_list">
	<div class="ibox-content">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h3>Lista Goblins selezionati</h3>
				<small>
					crea una lista di Goblins personalizzata e cliccando sui tasti funzione sottostanti potrai eseguire l'azione desiderata sull'elenco dei goblin selezionati.<br>
					Fai un singolo click sull'elenco dei Goblins per aggiungerlo alla lista, oppure clicca sulla "<i class="fa fa-remove"></i>" per rimuoverlo dalla lista.
				</small>
				<div class="well list_goblins_selected" style="padding:5px;"></div>
				<input class="list_goblins_selected_input" type="hidden"/>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 text-center">
				<button class="btn btn-sm btn-danger btn_printCardGob" style='margin:4px 0 0 10px'>
					<i class="fa fa-print"></i> Stampa tessere
				</button>
			</div>
			<div class="col-xs-6 text-center">
				<button class="btn btn-sm btn-primary btn_exportCsvGob" style='margin:4px 0 0 10px'>
					<i class="fa fa-exchange"></i> Esporta
				</button>
			</div>
		</div>
	</div>
</div>`;
			$('.detail_dirGoblins').empty().append(htmlToWrite);
		}
	});

	$(document).on('click', '.btn_printCardGob', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var filename='tessere_TdG';
		var btnTemp=$(this);
		var elements= $('.list_goblins_selected_input').val().split(',');
		if (!check_elem(elements)){
			return false;
		}
		var partPhrase='la tessera del Goblins selezionato';
		if (elements.length>1){
			partPhrase='le tessere dei Goblins selezionati';
		} else {
			if ($.trim($('.list_goblins_selected').text()).length>0){
				filename+='_'+$.trim($('.list_goblins_selected').text());	
			} else {
				filename+='_'+$.trim($('div.jokerman').text());
			}
			
		}
		swal({
			title: "Download tessere",
			text: "Sei sicuro di voler scaricare "+partPhrase+"?<br>al termine dell'operazione verrà scaricato un file <strong>PDF</strong><br>Seleziona il formato delle tessere:<select class='form-control val_tessere_mygob'><option value='1'>Standard (7.2x4.5)</option><option value='3'>Medium (7.7x4.9)</option><option value='2'>Big (8.5x5.2)</option></select>",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('list',JSON.stringify(elements));
				postData.append('filename',filename);
				postData.append('size',$('.val_tessere_mygob').val());
				var returnEngine = call_ajax_page(postData,'direttivo_manage/export_pdf',0);
				returnEngine.always(function (returndata) {
					savePDFbase64(returndata, filename);
					$('button').prop('disabled',false);
					btnTemp.empty().append('<i class="fa fa-print"></i> Stampa tessere');
				});
			}
		});
	});

	$('.btn_pairingNick').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
		$('button').prop('disabled',true);
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'direttivo_manage/pairing_nick',0);
		returnEngine.always(function (returndata) {
			var json_ret=JSON.parse(returndata);
			var success='success';
			var messErr='';
			if (json_ret.forum!=json_ret.forum){
				success='error';
				var messErr='<br><strong>Attenzione:</strong> i nick del forum non coincidono con quelli del portale. Verifica le anomalie nell\'area amministrativa';
			}
			$('button').prop('disabled',false);
			btnTemp.empty().append('<i class="fa fa-plus"></i> Pairing Nick');
			swal({
				title: "Pairing nick",
				text: "I Nick sono stati allineati tra il forum e il portale gestione.<br>Nick forum: <b>"+json_ret.forum+"</b><br>Nick portale: <b>"+json_ret.gest+'</b>'+messErr,
				type: success,
				showCancelButton: false,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok",
				html:true,
				closeOnConfirm: true,
			});	
		});
	});

	$(document).on('click', '.btn_exportCsvGob', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		var elements= $('.list_goblins_selected_input').val().split(',');
		if (!check_elem(elements)){
			return false;
		}
		swal({
			title: "Esporta dati",
			text: "Sei sicuro di voler esportare i dati dei Goblins selezionati?<br>al termine dell'operazione verrà scaricato un file <strong>CSV</strong>",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('list',JSON.stringify(elements));
				var returnEngine = call_ajax_page(postData,'direttivo_manage/export_data',0);
				returnEngine.always(function (returndata) {
					var headers = {
						Nick: 'Nick',
						Cognome: 'Cognome',
						Nome: 'Nome',
						Email: 'Email',
					};
					exportCSVFile(headers, JSON.parse(returndata), 'myGoblins');
					$('button').prop('disabled',false);
					btnTemp.empty().append('<i class="fa fa-exchange"></i> Esporta selezionati');
				});
			}
		});
	});

	$('.table_users_json tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ||  $(this).hasClass('selected_multi') ) {
			$(this).removeClass('selected');
			$(this).removeClass('selected_multi');
			if ($('#checkbox_myGob_multi').prop('checked')==false){
				detail_dirGoblins(0, '', 0);
			} else {
				var id_myG= (dataTables_myGoblins.row($(this).closest('tr')).data()[3]);
				var nick= (dataTables_myGoblins.row($(this).closest('tr')).data()[0]);
				detail_dirGoblins(id_myG, nick, 0);
			}
		} else {
			$('.table_users_json tr.selected').removeClass('selected');
			if (typeof(dataTables_myGoblins.row($(this).closest('tr')).data())==='undefined'){
				return false;
			}

			if ($('#checkbox_myGob_multi').prop('checked')==true){
				$(this).addClass('selected_multi');
			} else {
				$(this).addClass('selected');
			}
			var id_myG= (dataTables_myGoblins.row($(this).closest('tr')).data()[3]);
			var nick= (dataTables_myGoblins.row($(this).closest('tr')).data()[0]);
			detail_dirGoblins(id_myG, nick, 1);
		}
	});
	
	$(document).on('click', '.remove_goblin_list', function(event) {
		var id=parseInt($(this).attr('data-id'));
		dataTables_myGoblins.rows().eq( 0).filter( function (rowIdx) {
			if (id == parseInt(dataTables_myGoblins.cell( rowIdx, 3).data())){
				$('.table_users_json tbody tr:nth-child('+(rowIdx+1)+')').removeClass('selected_multi');
			}
		});
		detail_dirGoblins(id, '', 0);
	});

	$(document).on('click', '.btn_NewGoblin', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		edit_goblin(0);
	});

	$('button.field[data-name="button-mod"]').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		var btnTempTxt=$(this).html();
		var id=$('input.field[data-name="id_goblin"]').val();
		var postData = new FormData();
		var error=0;
		$.each($('input.field, select.field, textarea.field'), function(index, val) {
			$(this).parent().removeClass('has-error');
			var valDef=$.trim($(this).val());
			if ($(this).attr('required') && valDef=='' && !$(this).attr('disabled')){
				error=1;
				$(this).parent().addClass('has-error');
			}

			if (($(this).attr('type') && $(this).attr('type').toLowerCase()=='email') && !ValidateEmail($(this).val()) ){
				error=1;
				$(this).parent().addClass('has-error');
			}
			postData.append($(this).attr('data-name'),valDef);
		});
		if (error==1 && !$('select.field').hasClass('hidden')){
			return false;
		}
		swal({
			title: "Goblins",
			text: "Sei sicuro di voler continuare?",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var returnEngine = call_ajax_page(postData,'direttivo_manage/save_goblin',id);
				returnEngine.always(function (returndata) {
					var json_returned=$.parseJSON(returndata);
					$('button').prop('disabled',false);
					btnTemp.empty().append(btnTempTxt);
					if (json_returned.result==406){
						swal({
							title: "Dati Goblin",
							text: "Attenzione: nessun dato è stato modificato.",
							type: "warning",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Ok",
							html:true,
							closeOnConfirm: true,
						});	
					} else  if (json_returned.result==407){		
						swal({
							title: "Richiesta NON nviata.",
							text: json_returned.message,
							type: "error",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Ok",
							html:true,
							closeOnConfirm: true,
						});	
					} else  if (json_returned.result==202){		
						swal({
							title: "Richiesta inviata.",
							text: "La richiesta di assocazione presso la tana del Goblin <b>"+json_returned.goblin+"</b> è stata inviata correttamente.",
							type: "success",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Ok",
							html:true,
							closeOnConfirm: true,
						});	
						detail_dirGoblins(0, '', 1);
						$('.modal_editGoblin').modal('hide');
					} else  if (json_returned.result==201){	
						$('.modal_editGoblin').modal('hide');
						$('.countTotalMyGoblins').val(json_returned.elements);
						$('.disp_countTotalMyGoblins').empty().append(json_returned.elements+' elementi');
						$('.table_users_json').DataTable().search( json_returned.goblin ).ajax.reload(function(elements){

							$.each(elements.data, function(index, val) {
								 if(val[3]==id){
									$($('.table_users_json').DataTable().row(index).node()).addClass('selected');
									return;
								 }
							});
							detail_dirGoblins(id, '', 1);
							swal({
								title: "Goblin aggiunto!",
								text: "Il Goblin <b>"+json_returned.goblin+"</b> è stato aggiungo a<br><b>"+json_returned.tana+"<b>",
								type: "success",
								showCancelButton: false,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: "Ok",
								html:true,
								closeOnConfirm: true,
							});		
						});
						
					} else {
						detail_dirGoblins(id, '', 1);
						$('.modal_editGoblin').modal('hide');
					}
				});
			}
		});
	});
});

function check_elem(elements){
	var stop=true;
	$.each(elements, function(index, val) {
		if (parseInt(val)>0){
			stop=false;
			return true;
		}
	});
	if (stop==true){
		swal({
			title: "Esporta dati",
			text: "Seleziona almeno un Goblins dalla lista.",
			type: "warning",
			showCancelButton: false,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ok",
			html:true,
			closeOnConfirm: true,
		});	
		return false;
	}
	return true;
}
function detail_dirGoblins(id, nick, verso){
	if (id==0){
		if ($('#checkbox_myGob_multi').prop('checked')==false){
			$('.detail_dirGoblins').empty();
		}
		return;
	} else {
		if ($('#checkbox_myGob_multi').prop('checked')==false){
			var postData = new FormData();
			var returnEngine = call_ajax_page(postData,'direttivo_manage/detail_dirGoblins',id);
			returnEngine.always(function (returndata) {
				$('.detail_dirGoblins').empty().append(returndata);
			});
		} else {
			//add nick tag to the box
			var elements= $('.list_goblins_selected_input').val();
			var stop=false;
			var arrayElem=elements.split(',');
			$.each(arrayElem, function(index, val) {
				if (stop==true){
					return true;
				}
				if (parseInt(id)==parseInt(val)){
					if (verso==0){
						arrayElem.splice(index, 1);
						$('.remove_goblin_list[data-id="'+id+'"]').parent().remove();
						$('.list_goblins_selected_input').val(arrayElem.join(','));
						stop=true;
						return;
					}
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"progressBar": true,
						"preventDuplicates": true,
						"positionClass": "toast-top-center",
						"onclick": null,
						"showDuration": "400",
						"hideDuration": "400",
						"timeOut": "2000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
					toastr["info"]("Il Goblin selezionato NON è stato aggiunto perché già in elenco", "Attenzione!");
					stop=true;
				}
			});
			if (stop==true){
				return false;
			}
			if (elements!=''){
				$('.list_goblins_selected_input').val(elements+','+id);
			} else {
				$('.list_goblins_selected_input').val(id);	
			}
			$('.list_goblins_selected').append('<span class="label label-primary" style="display:inline-block;cursor:normal;margin:3px;"><i class="fa fa-remove remove_goblin_list" style="cursor:pointer" data-id="'+id+'"></i> '+nick+'</span>');
		}
	}
}

function edit_goblin(id){
	if(parseInt(id)==0){
		$('.select2_myGoblinse_data_goblins').val('');
		selectNick();
	} else {
		$('button').prop('disabled',true);
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'direttivo_manage/edit_goblin',id);
		returnEngine.always(function (returndata) {
			var json_returned=$.parseJSON(returndata);
			$('button').prop('disabled',false);
			$('.subtitle_modal_selectMyGoblin').empty().append(json_returned.title);
			$.each(json_returned.fields, function(index, val) {
				 if ($('input.field[data-name="'+index+'"]').length>0 || $('select.field[data-name="'+index+'"]').length>0){
				 	$('.field[data-name="'+index+'"]').val(val);
				 } else {
				 	$('.field[data-name="'+index+'"]').empty().append(val);	
				 }
				 
				 if (json_returned.enable_edit==true){
				 	$('input.field[data-name="'+index+'"], textarea.field[data-name="'+index+'"]').prop('disabled','');
				 	//$('select.field[data-name="'+index+'"]').removeClass('hidden');
				 	$('.text_for_select[data-name="'+index+'"]').removeClass('hidden');
				 	$('.text_for_select_move[data-name="'+index+'"]').addClass('hidden');
				 	$('span.field[data-name="'+index+'"]').addClass('hidden');
				 	$('.text_for_span[data-name="'+index+'"]').addClass('hidden');
				 } else {
				 	$('input.field[data-name="'+index+'"], textarea.field[data-name="'+index+'"]').prop('disabled','disabled');
				 	//$('select.field[data-name="'+index+'"]').addClass('hidden');
				 	$('.text_for_select[data-name="'+index+'"]').addClass('hidden');
				 	$('.text_for_select_move[data-name="'+index+'"]').removeClass('hidden');
				 	$('span.field[data-name="'+index+'"]').removeClass('hidden');
				 	$('.text_for_span[data-name="'+index+'"]').removeClass('hidden');
				 }
			});
			$('input.field[data-name="dataNascita_goblin"]').datepicker('update');
			$('.field').parent().removeClass('has-error');
			$('.modal_editGoblin').modal('show');
			city_select();
		});
	}
	
}

var json_city={};
function city_comiple(){
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'direttivo_manage/city_select',1);
	returnEngine.always(function (returndata) {
		json_city=$.parseJSON(returndata);
	});
}

function city_select(){
	$('input.field[data-city="1"][city-field="1"]').typeahead('destroy');
	$('input.field[data-city="1"][city-field="1"]').typeahead({ 
		source: function (query, process) {
			process(json_city.city);
		},
		matcher : function(item) {
			return this.__proto__.matcher.call(this,item.split("|")[0]);
		},

		highlighter: function(item) {
			return this.__proto__.highlighter.call(this,item.split("|")[0]);
		},
		updater: function(item) {
			var itemArray = item.split("|");
			return this.__proto__.updater.call(this,itemArray[0]);
		}
	}).bind('change blur', function () {
		var indexInside=$.inArray($(this).val().toLowerCase(), json_city.map.city_compare);
		if (indexInside === -1) {
			$(this).val('');
			$('input.field[data-city="1"][city-field="2"]').val('');
			$('input[data-name="provincia_goblin"]').val('');
		} else {
			$(this).val(json_city.map.city[indexInside]);
			$('input.field[data-city="1"][city-field="2"]').val(json_city.map.cap[indexInside]);
			$('input[data-name="provincia_goblin"]').val(json_city.map.prov[indexInside]);
		}
	});

	$('input.field[data-city="1"][city-field="2"]').typeahead('destroy');
	$('input.field[data-city="1"][city-field="2"]').typeahead({ 
		source: function (query, process) {
			process(json_city.cap);
		},
		matcher : function(item) {
			return this.__proto__.matcher.call(this,item.split("|")[1]);
		},

		highlighter: function(item) {
			return this.__proto__.highlighter.call(this,item.split("|")[1]);
		},
		updater: function(item) {
			var itemArray = item.split("|");
			return this.__proto__.updater.call(this,itemArray[1]);
		}
	}).bind('change blur', function () {
		var indexInside=$.inArray($(this).val().toLowerCase(), json_city.map.cap);
		if (indexInside === -1) {
			$(this).val('');
			$('input.field[data-city="1"][city-field="1"]').val('');
			$('input[data-name="provincia_goblin"]').val('');
		} else {
			$(this).val(json_city.map.cap[indexInside]);
			$('input.field[data-city="1"][city-field="1"]').val(json_city.map.city[indexInside]);
			$('input[data-name="provincia_goblin"]').val(json_city.map.prov[indexInside]);
		}
	});
}

function selectNick(){
	$('button').prop('disabled',true);
	
	$('.btn_NewGoblin').empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
	var json_returned={};
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'direttivo_manage/nick_select',0);
	returnEngine.always(function (returndata) {
		$('.btn_NewGoblin').empty().append('<i class="fa fa-plus"></i> Tessera Nuovo Goblin');
		$('button').prop('disabled',false);
		$('.modal_selectMyGoblin').modal('show');
		json_returned=$.parseJSON(returndata);
		$('.select2_myGoblinse_data_goblins').typeahead('destroy');
		$(".select2_myGoblinse_data_goblins").typeahead({ 
			source: function (query, process) {
				var concatSourceData = _.map(json_returned.Goblin,function(item){
					return item.id + "|" + item.label;
				});
				process(concatSourceData);
			},
			matcher : function(item) {
				return this.__proto__.matcher.call(this,item.split("|")[1]);
			},

			highlighter: function(item) {
				return this.__proto__.highlighter.call(this,item.split("|")[1]);
			},
			updater: function(item) {
				var itemArray = item.split("|");
				//callback(itemArray[0]);
				return this.__proto__.updater.call(this,itemArray[1]);
			}
		});

		$('.btn_myGoblins_selectNick').click(function(event) {
			event.preventDefault();
			event.stopImmediatePropagation();
			var text=$.trim($('.select2_myGoblinse_data_goblins').val());
			$(this).removeClass('btn-danger');
			$('.select2_myGoblinse_data_goblins').parent().removeClass('has-error');
			if (text.length<=0){
				$(this).addClass('btn-danger');
				$('.select2_myGoblinse_data_goblins').parent().addClass('has-error');
				return false;	
			}
			var found=0;
			$.each(json_returned.Goblin, function(index, val) {
				if (text.toLowerCase()==val.label.toLowerCase()){
					found=val.id;
					return;
				}
			});
			if (found==0){
				$('.select2_myGoblinse_data_goblins').parent().addClass('has-error');
				$(this).addClass('btn-danger');
				return false;	
			}
			
			$('.modal_selectMyGoblin').modal('hide');
			edit_goblin(found);
		});
	});
}
</script>