<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<script type="text/javascript">
var $image;

$(document).ready(function() {
	admin_utenti_box_left();
	var $inputImage = $("#inputImageProfileUser");
	if (window.FileReader) {
		$inputImage.change(function() {
			var fileReader = new FileReader(),
			files = this.files,
			file;

			if (!files.length) {
				return;
			}

			file = files[0];

			if (/^image\/\w+$/.test(file.type)) {
				fileReader.readAsDataURL(file);
				fileReader.onload = function () {
					$inputImage.val("");
					$image.cropper("reset", true).cropper("replace", this.result);
				};
			} else {
				swal({
					title: "Immagine non valida.",
					text: "Seleziona un tipo di immagine valida (jpg/png)",
					type: "error",
					showCancelButton: false,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Ok",
					html:true,
					closeOnConfirm: true,
				});	
			}
		});
	} else {
		$inputImage.addClass("hide");
	}

	$(".btn_save_picProfile_user").click(function() {
		var imgString=$.trim($image.cropper("getDataURL"));
		if (imgString==''){
			swal({
				title: "Operazione non valida",
				text: "Carica e ritaglia un'immagine da usare nel tuo profilo.",
				type: "info",
				showCancelButton: false,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok",
				html:true,
				closeOnConfirm: true,
			});	
			return;
		}
		
		var btnTemp=$(this);
		var id = $('.btn_save_picProfile_user').val();
		btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
		$('button').prop('disabled',true);
		var postData = new FormData();
		postData.append('imgString',imgString);
		var returnEngine = call_ajax_page(postData,'admin_utenti/upload_picture',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			btnTemp.empty().append('<i class="fa fa-floppy-o"></i> Salva');
			swal({
				title: "Immagine sostituita",
				text: "L'immagine del tuo profilo è stata sostituita correttamente.",
				type: "success",
				showCancelButton: false,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok",
				html:true,
				closeOnConfirm: true,
			});	
			$('.btn_save_picProfile_user').val('0');
			var json_ret=JSON.parse(returndata);
			$('.profile_personal_img[data-picuser="'+id+'"]').attr('src',json_ret.image);
			$('.modify_userpicture[data-user="'+id+'"]').attr('data-pic',json_ret.image);
			$('.modal_pictureProfile_user').modal('hide');
			$inputImage.val("");
		});

	});

});

function admin_utenti_box_left(){
	$('button').prop('disabled',true);
	$('.admin_utenti_box_left').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'admin_utenti/list_users',0);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.admin_utenti_box_left').empty().append(returndata);
	});
}

function picture_user(user, picture){
	$('.btn_save_picProfile_user').val(user);
	$('.modal_pictureProfile_user').modal('show');
	$image = $(".image-crop-profile > img");
	$($image).cropper({
		aspectRatio: 1,
		done: function(data) {

		}
	});
	$image.cropper("reset", true).cropper("replace", picture);
}
</script>