<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_updateDbase').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var page=$(this).val();
		update_dbase_general(page);
	});
});

function update_dbase_general(page){
	$('button').prop('disabled',true);
	$('.btn_updateDbase[value="'+page+'"]').empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'update_dbase/'+page,0);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('div[data-return="'+page+'"]').empty().append(returndata);
		$('.btn_updateDbase[value="'+page+'"]').empty().append('<i class="fa fa-cog"></i> Aggiorna');
	});
}
</script>