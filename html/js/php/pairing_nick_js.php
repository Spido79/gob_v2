<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$(document).on('click', '.btn_del_anomalia', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var val=$(this).val();
		swal({
			title: "Elimina",
			text: "Sei sicuro di voler <strong>eliminare</strong> questa anomalia?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			confirmButtonColor: "#DD6B55",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
			closeOnCancel: true,
		},function(confirm){
			if (confirm){
				var postData = new FormData();
				var returnEngine = call_ajax_page(postData,'pairing_nick/del_anomalie',val);
				returnEngine.always(function (returndata) {
					swal.close();
					update_dbase_general('update_nick');
				});
			} else {
				swal.close();
			}
			
		});
	});

	$('.btn_updateDbaseNick').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var page=$(this).val();
		update_dbase_general(page);
	});
	find_anomalie();

});

function find_anomalie(){
	$('.nick_anomalie_message').empty().append('<i class="fa fa-pulse fa-spinner"></i> Verifica...');
	$('.nick_anomalie_table').empty();
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'pairing_nick/find_anomalie',0);
	returnEngine.always(function (returndata) {
		var json_ret=JSON.parse(returndata);
		$('.nick_anomalie_message').empty().append(json_ret.message);
		if (json_ret.table.length>0){
			$('.nick_anomalie_message').empty().append('Trovate <b>'+json_ret.table.length+'</b> anomalie.');
			var string= '<table class="table table-striped table-hover"><thead><th>Nick</th><th>email</th><th>&nbsp;</th></thead><tbody>';
			$.each(json_ret.table, function(index, val) {
				 string+='<tr><td>'+val.Nick+'</td><td>'+val.Email+'</td>';
				 string+='<td><button class="btn btn-xs btn-danger btn_del_anomalia" value="'+val.ID+'"><i class="fa fa-trash"></i> Elimina</button></td>';
				 string+='</tr>';
			});
			string +='</tbody></table>';
			$('.nick_anomalie_table').empty().append(string);	
		}
	});
}

function update_dbase_general(page){
	$('button').prop('disabled',true);
	$('.btn_updateDbase[value="'+page+'"]').empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'pairing_nick/'+page,0);
	returnEngine.always(function (returndata) {
		find_anomalie();
		var json_ret=JSON.parse(returndata);
		$('button').prop('disabled',false);
		$('div[data-return="'+page+'_gest"]').empty().append(json_ret.gest);
		$('div[data-return="'+page+'_forum"]').empty().append(json_ret.forum);
		$('div[data-return="'+page+'_diff"]').empty().append(json_ret.gest-json_ret.forum);
		$('div[data-return="'+page+'_diff"]').removeClass('text-danger');
		$('div[data-return="'+page+'_diff"]').removeClass('text-warning');
		$('div[data-return="'+page+'_diff"]').removeClass('text-primary');
		if (json_ret.gest-json_ret.forum==0){
			$('div[data-return="'+page+'_diff"]').addClass('text-primary');
		} else if (json_ret.gest-json_ret.forum<0){
			$('div[data-return="'+page+'_diff"]').addClass('text-danger');
		} else {
			$('div[data-return="'+page+'_diff"]').addClass('text-warning');
		}
		$('.btn_updateDbaseNick[value="'+page+'"]').empty().append('<i class="fa fa-cog"></i> Aggiorna');
	});
}
</script>