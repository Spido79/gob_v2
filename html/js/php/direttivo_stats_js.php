<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
$array12All=array();
$today=new DateTime('first day of this month');
$endToday=new DateTime('last day of this month');
$interval = new DateInterval('P1M');
$dateInter=array();
for ($i=0; $i < 12; $i++) {
    $dateInter[12-$i]=$today->format('M Y');
    $params=array(
        'list_tane' => [0],
        'associated_from' => $today->format('Y-m-d'),
        'associated_to' => $endToday->format('Y-m-d'),
        'list_tane_exclude' => true,
    );
    $allLastYear=\GOBLINS\Manage::getAll(0, $params);
    $array12All[(12-$i)]=count($allLastYear);

    $today->sub($interval);
    $endToday=new DateTime($today->format('Y-m-d'));
    $endToday->modify('last day of this month');
}

$array12notAss=array();
$today=new DateTime('first day of this month');
$endToday=new DateTime('last day of this month');
for ($i=0; $i < 12; $i++) {
    $params=array(
        'list_tane' => [0],
        'subscribed_from' => $today->getTimestamp(),
        'subscribed_to' => $endToday->getTimestamp(),
    );
    $allLastYear=\GOBLINS\Manage::getAll(0, $params);
    $array12notAss[(12-$i)]=count($allLastYear);

    $today->sub($interval);
    $endToday=new DateTime($today->format('Y-m-d'));
    $endToday->modify('last day of this month');
}

$myeTane=array();
$today=new DateTime('first day of this month');
$endToday=new DateTime('last day of this month');
$myTane['La Tana dei Goblin']=array();
for ($i=0; $i < 12; $i++) {
    $params=array(
        'associated_from' => $today->format('Y-m-d'),
        'associated_to' => $endToday->format('Y-m-d'),
    );
    $myVal=\GOBLINS\Manage::getAll(-1, $params);
    $myTane['La Tana dei Goblin'][(12-$i)]=count($myVal);

    $today->sub($interval);
    $endToday=new DateTime($today->format('Y-m-d'));
    $endToday->modify('last day of this month');
}

$params=array(
    'list_tane' => [\APP\Parameter::getSpec('id_tana_nazionale')],
    'map_fields'    => array(
        'Nick'              =>'',
        'id_goblin'         =>'',
        'dataNascita_goblin' =>'',
    ),
);
$myAss=\GOBLINS\Manage::getAll(0, $params);

$params=array(
    'map_fields'    => array(
        'Nick'              =>'',
        'id_goblin'         =>'',
        'dataNascita_goblin' =>'',
    ),
);
$otherAss=\GOBLINS\Manage::getAll(0, $params);

$now=new DateTime();

$count=0;
$anniMiei=array();
foreach ($myAss as $gob) {
    $date = new DateTime($gob['dataNascita_goblin']);
    $interval = $now->diff($date);
    if (!isset($anniMiei[$interval->y])){
        $anniMiei[$interval->y]=0;
    }
    $anniMiei[$interval->y]++;
}

$count=0;
$anniGlonal=array();
foreach ($otherAss as $gob) {
    $date = new DateTime($gob['dataNascita_goblin']);
    $interval = $now->diff($date);
    if (!isset($anniGlonal[$interval->y])){
        $anniGlonal[$interval->y]=0;
    }
    $anniGlonal[$interval->y]++;
}
ksort($anniGlonal);
ksort($anniMiei);

?>
<script type="text/javascript">
$(document).ready(function() {
    var lineOptions = {
        responsive: true,
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                title: function(tooltipItems, data) {
                    return "Età: "+tooltipItems[0].xLabel;
                },
                label: function(tooltipItems, data) {
                    return "N. goblins: "+tooltipItems.yLabel;
                }
            }
        },
    };
    var lineDataMineYear = {
        labels: [<?php
            $count=0;
            foreach ($anniMiei as $year => $number) {
                if ($year<=0 || $year>=100){
                    continue;
                }
                echo $count>0? ', ':'';
                echo $year;
                $count++;
            }
            ?>],
        datasets: [{
            label: "Età goblin",
            backgroundColor: 'rgba(26,179,148,0.5)',
            borderColor: "rgba(26,179,148,0.7)",
            pointBackgroundColor: "rgba(26,179,148,1)",
            pointBorderColor: "#fff",
            data: [<?php
            $count=0;
            foreach ($anniMiei as $year => $number) {
                if ($year<=0 || $year>=100){
                    continue;
                }
                echo $count>0? ', ':'';
                echo $number;
                $count++;
            }
            ?>]}]
    };

    var lineDataGlonalYear = {
        labels: [<?php
            $count=0;
            foreach ($anniGlonal as $year => $number) {
                if ($year<=0 || $year>=100){
                    continue;
                }
                echo $count>0? ', ':'';
                echo $year;
                $count++;
            }
            ?>],
        datasets: [{
            label: "Età goblin",
            backgroundColor: 'rgba(220, 220, 220, 0.5)',
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(26,179,148,1)",
            data: [<?php
            $count=0;
            foreach ($anniGlonal as $year => $number) {
                if ($year<=0 || $year>=100){
                    continue;
                }
                echo $count>0? ', ':'';
                echo $number;
                $count++;
            }
            ?>]}]
    };

    var ctxMineYear = document.getElementById("yearsChartMine").getContext("2d");
    new Chart(ctxMineYear, {type: 'line', data: lineDataMineYear, options:lineOptions});
    var ctxGlobalYear = document.getElementById("yearsChartGlobal").getContext("2d");
    new Chart(ctxGlobalYear, {type: 'line', data: lineDataGlonalYear, options:lineOptions});


    $('.table_stats_json').DataTable({
        "language": {
            "lengthMenu": "Mostra _MENU_ elementi",
            "zeroRecords": "Non è stato trovato niente - riprova",
            "info": "Pagina _PAGE_ di _PAGES_",
            "search": "Cerca",
            "paginate": {
                "previous": "Indietro",
                "next": "Avanti"
            },
            "infoEmpty": "Nessun elemento disponibile",
            "infoFiltered": "(filtrati da _MAX_ elementi totali)"
        },
        processing: true,
        lengthMenu: [[10, 25, 1000], [10, 25, "Tutti"]],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgtip',
        buttons: [{
                extend: 'pdfHtml5',
                title: 'Statistiche La Tana dei Goblin',
                exportOptions: {
                    columns: [ 0,1,2],
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }
        ],
        "order": [[ 1, "desc" ]],
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false,
        }],
    });



    var sparklineCharts = function(){
        $("#sparkline1").sparkline([<?php echo implode(',', $array12All) ?>], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#1ab394',
            fillColor: "transparent"
        });

        $("#sparkline2").sparkline([<?php echo implode(',', $array12notAss); ?>], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#1ab394',
            fillColor: "transparent"
        });

    };

    var sparkResize;
    $(window).resize(function(e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparklineCharts, 500);
    });

    sparklineCharts();
    <?php
        $count=0;
        $maxVal=0;
        $stringV='';
        foreach ($myTane as $data => $items) {
            echo $count>0? "\n":'';
            echo "var dataVar{$count}={dataid:{$count}, label: \"{$data}\", data:[";

            $stringV.=$count>0? ',':'';
            $stringV.="dataVar{$count}";
            $countB=0;
            foreach ($items as $month => $val) {
                echo $countB>0? ', ':'';
                if ($val>$maxVal){
                    $maxVal=$val;
                }
                echo "[{$month},{$val}]";
                $countB++;
            }
            echo "]};";
            $count++;
        }

    ?>;
    var chartPlot = $.plot($("#flot-dashboard5-chart"), [<?php echo $stringV;?>],{
            series: {
                lines: {
                    show: false,
                    fill: true
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
                points: {
                    radius: 0,
                    show: true
                },
                shadowSize: 2
            },
            grid: {
                hoverable: true,
                clickable: true,

                borderWidth: 2,
                color: 'transparent'
            },
            colors: ["#1ab394", "#1C84C6"],
            xaxis:{
                 ticks: [
                        <?php
                        $count=0;
                        foreach ($dateInter as $key => $value) {
                            echo $count>0? ', ':'';
                            echo "[{$key}, '{$value}']";
                            $count++;
                        }
                        ?>
                    ],
                    color: "transparent"
            },
            yaxis: {
                ticks: <?php echo $maxVal>8 ? 8 : $maxVal; ?>
            },
            tooltip: false,
            legend: {
                show: true,
                position: "ne",
                labelBoxBorderColor:"transparent",
                backgroundColor:"transparent",
                backgroundOpacity:1,
                labelFormatter: function(label, series,dataid) {
                    return '<span class="text-success " data-id="'+series.dataid+'" >' + label + '</a>';
                }
           }
    });
});
</script>