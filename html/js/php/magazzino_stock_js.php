<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$('#magazzino_stock_chk_dismiss').click(function(event) {
		$('.btn_magStockNewStock').toggleClass('hidden');
		$('.table_stock_json').DataTable().ajax.reload();
		$('.magazzino_stock_details').empty();
	});

	$('input.data-field[data-field="data_acquisto"]').datepicker({
		startView: 2,
		todayBtn: "linked",
		keyboardNavigation: false,
		immediateUpdates:true,
		forceParse: true,
		autoclose: true,
		format: "dd/mm/yyyy",
		toggleActive:false,
	});

	$('.data-field').val('');
	$('.btn_save_modal_stockMagStock').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$('input.data-field[data-field="id_ana"]').val();
		var id_stk=$(this).attr('data-id');
		var postData = new FormData();
		postData.append('id_stk',id_stk);
		var error=false;
		$('.data-field').parent().removeClass('has-error');
		$.each($('.data-field'), function(index, val) {
			if ($(this).prop('required') && ($(this).val()=='' ||$(this).val()==null) ){
				$(this).parent().addClass('has-error');
				error=true;
			}
			if ($(this).attr('type')=='checkbox'){
				let valCheck=0;
				if($(this).prop('checked')){
					valCheck=1;
				}
				postData.append($(this).attr('data-field'),$.trim(valCheck));
			} else {
				postData.append($(this).attr('data-field'),$.trim($(this).val()));
			}

		});
		if (error==true){
			return false;
		}
		$('button').prop('disabled',true);
		var returnEngine = call_ajax_page(postData,'magazzino_stock/save_stock',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.modal_stockMagStock').modal('hide');
			$('.data-field').val('');
			$('.table_stock_json').DataTable().ajax.reload();
			call_magazzino_stock_details($('.table_stock_stock_json').attr('id-stock'));
		});
	});


	var dataTables_stock= $('.table_stock_json').DataTable({
		"language": {
			"lengthMenu": "Mostra _MENU_ elementi",
			"zeroRecords": "Non è stato trovato niente - riprova",
			"info": "Pagina _PAGE_ di _PAGES_",
			"search": "Cerca",
			"paginate": {
				"previous": "Indietro",
				"next": "Avanti"
			},
			"infoEmpty": "Nessun elemento disponibile",
			"infoFiltered": "(filtrati da _MAX_ elementi totali)"
		},
		processing: true,
		createdRow: function ( row, data, index ) {
        },
		lengthMenu: [[10, 25, $('.countTotalStock').val()], [10, 25, "Tutti"]],
		serverSide: true,
		ajax: {
			url: APP_URL+'/_GHOST.php',
			type: 'POST',
	        async:true,
	        mimeType:"multipart/form-data",
	        cache: false,
			data: function ( d ) {
				d.redirect_to =APP_URL+"/php/magazzino_stock/list_stock.php";
				d.id=1;
				d.recordTotal=$('.countTotalStock').val();
				d.req=$('#magazzino_stock_chk_dismiss').prop('checked');
			}
		},
		pageLength: 10,
		responsive: true,
		dom: '<"html5buttons"B>lTfgtip',
		buttons: [{
				extend: 'pdfHtml5',
				title: 'Elenco stock',
				exportOptions: {
                    columns: [ 0,1],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			},{
				extend: 'csvHtml5',
				title: 'Elenco stock CSV',
				fieldSeparator: ';',
				exportOptions: {
                    columns: [ 0,1],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			}
		],
		"order": [[ 0, "asc" ]],
		"columnDefs": [ {
			"targets": 'no-sort',
			"orderable": false,
		},{
                "targets": [1],
                "className": 'text-right',
                "searchable": false,
        },{
                "targets": [2],
                "visible": false,
                "searchable": false
        }],
	});

	$('.table_stock_json tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected')) {
			$(this).removeClass('selected');
			$('.magazzino_stock_details').empty();
		} else {
			$('.table_stock_json tr.selected').removeClass('selected');
			if (typeof(dataTables_stock.row($(this).closest('tr')).data())==='undefined'){
				$('.magazzino_stock_details').empty();
				return false;
			}
			$(this).addClass('selected');
			var id_stock= (dataTables_stock.row($(this).closest('tr')).data()[3]);
			call_magazzino_stock_details(id_stock);
		}
	});

	$(document).on('click', '.btn_magStockNewStock', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		$('.select2_stock_main_stock_selectGame').val('');
		selectGames();
	});

});

function edit_games(idAna){
	$('input[data-field="id_ana"]').val(idAna);
	$('.stockMag-title-add').empty().append($('.main-title-ana-id').text());
	$('.stockMag-action-addModd').empty().append('Inserimento stock');

	$('select[data-field="id_store"]').val(0);
	$('select[data-field="id_owner"]').val(0);
	$('input[data-field="data_acquisto"]').val('');
	$('input[data-field="hidden"]').prop('checked',false);
	$('select[data-field="origin"]').val('');
	$('textarea[data-field="notes"]').val('');
	$('input[data-field="locazione"]').val('');
	$('.btn_save_modal_stockMagStock').attr('data-id',0);
	$('.data-field').parent().removeClass('has-error');
	$('.modal_stockMagStock').modal('show');
}

function selectGames(){
	$('button').prop('disabled',true);

	$('.btn_magStockNewStock').empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
	var json_returned={};
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'magazzino_stock/game_select',0);
	returnEngine.always(function (returndata) {
		$('.btn_magStockNewStock').empty().append('<i class="fa fa-plus"></i> Aggiungi articolo');
		$('button').prop('disabled',false);
		$('.modal_magazzino_stock_main_stock').modal('show');
		json_returned=$.parseJSON(returndata);
		$('.select2_stock_main_stock_selectGame').typeahead('destroy');
		$(".select2_stock_main_stock_selectGame").typeahead({
			source: function (query, process) {
				var concatSourceData = _.map(json_returned.Games,function(item){
					return item.id + "|" + item.label;
				});
				process(concatSourceData);
			},
			matcher : function(item) {
				return this.__proto__.matcher.call(this,item.split("|")[1]);
			},

			highlighter: function(item) {
				return this.__proto__.highlighter.call(this,item.split("|")[1]);
			},
			updater: function(item) {
				var itemArray = item.split("|");
				//callback(itemArray[0]);
				return this.__proto__.updater.call(this,itemArray[1]);
			}
		});
		$('.btn_stock_main_stock_selectGame').click(function(event) {
			event.preventDefault();
			event.stopImmediatePropagation();
			var text=$.trim($('.select2_stock_main_stock_selectGame').val());
			$(this).removeClass('btn-danger');
			$('.btn_stock_main_stock_selectGame').parent().removeClass('has-error');
			if (text.length<=0){
			$(this).addClass('btn-danger');
				$('.btn_stock_main_stock_selectGame').parent().addClass('has-error');
				return false;
			}
			var found=0;
			$.each(json_returned.Games, function(index, val) {
				if (text.toLowerCase()==val.label.toLowerCase()){
					found=val.id;
					return;
				}
			});
			if (found==0){
				$('.btn_stock_main_stock_selectGame').parent().addClass('has-error');
				$(this).addClass('btn-danger');
				return false;
			}

			$('.modal_magazzino_stock_main_stock').modal('hide');
			edit_games(found);
		});
	});
}

function call_magazzino_stock_details(id){
	$('button').prop('disabled',true);
	$('.magazzino_stock_details').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	postData.append('dismiss',$('#magazzino_stock_chk_dismiss').prop('checked'));
	var returnEngine = call_ajax_page(postData,'magazzino_stock/detail_stock',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.magazzino_stock_details').empty().append(returndata);
	});
}
</script>