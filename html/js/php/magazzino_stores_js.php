<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	magazzino_stores_box_left();
	$('.btn_save_modal_anaMagStStock').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$('input.data-field[data-field="id_ana"]').val();
		var id_stk=$(this).attr('data-id');
		var postData = new FormData();
		postData.append('id_stk',id_stk);
		var error=false;
		$('.data-field').parent().removeClass('has-error');
		$.each($('.data-field'), function(index, val) {
			if ($(this).prop('required') && ($(this).val()=='' ||$(this).val()==null) ){
				$(this).parent().addClass('has-error');
				error=true;
			}
			if ($(this).attr('type')=='checkbox'){
				let valCheck=0;
				if($(this).prop('checked')){
					valCheck=1;
				}
				postData.append($(this).attr('data-field'),$.trim(valCheck));
			} else {
				postData.append($(this).attr('data-field'),$.trim($(this).val()));
			}

		});
		if (error==true){
			return false;
		}
		$('button').prop('disabled',true);
		var returnEngine = call_ajax_page(postData,'magazzino_stores/save_stock',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.modal_anaMagStStock').modal('hide');
			$('.data-field').val('');
			magazzino_stores_box_left();
			magazzino_stores_box_right($('.btn_AddModStore').val());
		});



	});
});
function magazzino_stores_box_left(){
	$('button').prop('disabled',true);
	$('.magazzino_stores_box_left').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'magazzino_stores/list_stores',0);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.magazzino_stores_box_left').empty().append(returndata);
	});
}

function magazzino_stores_box_right(id){
	$('button').prop('disabled',true);
	$('.magazzino_stores_box_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'magazzino_stores/manage_store',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.magazzino_stores_box_right').empty().append(returndata);
	});
}
</script>