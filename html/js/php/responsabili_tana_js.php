<?php
if (!defined('APP_URL')) {
	\HTML\Page::Page_404();
}
?>
<script type="text/javascript">
var onRunning=[];
$(document).ready(function() {
	admin_responsabili_box_left(0,'');
});
function admin_responsabili_box_left(page, filter){
	$('button').prop('disabled',true);
	$('.admin_responsabili_box_left').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	postData.append('page',page);
	postData.append('filter',filter);
	var returnEngine = call_ajax_page(postData,'responsabili_tana/list_users',0);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.admin_responsabili_box_left').empty().append(returndata);
	});
}

function admin_responsabili_box_right(id){
	if (id==0){
		$('.admin_responsabili_box_right').empty();
		return;
	}
	$('button').prop('disabled',true);
	$('.admin_responsabili_box_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'responsabili_tana/spec_user',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.admin_responsabili_box_right').empty().append(returndata);
	});
}

function call_remoteEnableTana(letUpdate, check, id_tana, thisCheck, id){
	$('button').prop('disabled',true);
	onRunning.push(id_tana);
	var postData = new FormData();
	postData.append('check',check);
	postData.append('id_tana',id_tana);
	var returnEngine = call_ajax_page(postData,'responsabili_tana/enable_disable_tana',id);
	returnEngine.always(function (returndata) {
		$.each(onRunning, function(index, val) {
			if (val==id_tana){
				onRunning.splice(index,1);
				return;
			}
		});
		thisCheck.prop('checked',check);
		if (letUpdate==true){
			//verify if onrunning, otherwais wait 
			$('button').prop('disabled',false);
			myLoop(onRunning.length);
		}
	});
}
function myLoop (lenOnR) {
   setTimeout(function () {
      if (lenOnR > 0) {
         myLoop(onRunning.length);
      } else {
    	var page=$('.dataTable_resptane_list').DataTable().page.info().page;
		var filter=$('.dataTable_resptane_list').DataTable().search();
      	admin_responsabili_box_left(page, filter);
      }
   }, 300)
}

function app_filter_tane(){
	var textF=$.trim($('.search_tana').val());
	if (textF==''){
		$('.filter_tane').removeClass('hidden');
		return;
	}

	$('.filter_tane').each(function(index, el) {
		if ($(this).text().toLowerCase().indexOf(textF.toLowerCase())>-1){
			$(this).removeClass('hidden');
		} else {
			$(this).addClass('hidden');
		}
	});

}
</script>