<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	admin_gruppi_box_left();
});
function admin_gruppi_box_left(){
	$('button').prop('disabled',true);
	$('.admin_gruppi_box_left').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'admin_gruppi/list_groups',0);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.admin_gruppi_box_left').empty().append(returndata);
	});
}

function admin_gruppi_box_right_users(id){
	$('button').prop('disabled',true);
	$('.admin_gruppi_box_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'admin_gruppi/manage_users',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.admin_gruppi_box_right').empty().append(returndata);
	});
}
</script>