<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	load_folder(1,0);
	$('.load_folder').click(function(event) {
		event.preventDefault();
        event.stopImmediatePropagation();
        load_folder($(this).attr('data-type'),0);
	});
	check_folder();

	setInterval(function() {
    	check_folder();
	}, 2 * 60 * 1000);

	$(document).on('click', '.btn_refresh_folder', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$(this).attr('data-id');
		load_folder(id,0);
	});
	$(document).on('click', '.btn_folder_list', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$(this).attr('data-id');
		var page=$(this).attr('data-page');
		load_folder(id,page);
	});

	$(document).on('click', '.detail_mail', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$(this).attr('data-value');
		load_email(id);
	});
	
});

function check_folder(){
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'email_checker/check_folder',0);
	returnEngine.always(function (returndata) {
		var json_folder=$.parseJSON(returndata);
		$('.folder_counter[data-folder="sending"]').empty().append(json_folder.sending);
		$('.folder_counter[data-folder="sent"]').empty().append(json_folder.sent);
	});
}


function load_email(id){
	$('button').prop('disabled',true);
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'email_checker/load_email',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		var json_email=$.parseJSON(returndata);
		$('.subject_email_checker').empty().append(json_email.subject);
		$('.body_email_checker').empty().append(json_email.body);
		$('.modal_emailChecker').modal('show');
	});
}



function load_folder(type, page){
	$('.email_checker_inbox').empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
	var postData = new FormData();
	postData.append('page',page);
	var returnEngine = call_ajax_page(postData,'email_checker/inbox',type);
	returnEngine.always(function (returndata) {
			$('.email_checker_inbox').empty().append(returndata);
	});
}
</script>