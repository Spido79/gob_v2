<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_searchIP').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var value=$.trim($('.search_ip').val());
		$('.ip-list').parent().removeClass('hidden');
		$('.ip-list_nobody').addClass('hidden');
		if (value.length<=0){
			return;
		}
		var visibile=0;
		$('.ip-list').each(function(index, el) {
			var ip_ver=$.trim($(this).attr('data-ip'));
			if (ip_ver.indexOf(value)<0){
				$(this).parent().addClass('hidden');
			} else {
				visibile=1;
			}
		});
		if (visibile==0){
			$('.ip-list_nobody').removeClass('hidden');
		}
	});	

	$('.btn_cleanIP').click(function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		var btnTemp=$(this);
		var ipList=[];
		$('.checkbox_selection_ip:checked').each(function(index, el) {
			ipList.push($(this).val());
		});
		if (ipList.length<=0){
			swal({
			title: "Seleziona IP",
			text: "Seleziona almeno un'indirizzo IP dall'elenco.",
			type: "info",
			showCancelButton: false,
			confirmButtonColor: "#62E54B",
			confirmButtonText: "Ok",
			html:true,
			closeOnConfirm: true,
			});
			return false;
		}		
		swal({
			title: "Pulisci IP Bloccati",
			text: "Continuando azzererai i tentativi di connessione di <strong>"+ipList.length+" IP</strong>.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ok",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('ipList',JSON.stringify(ipList));
				var returnEngine = call_ajax_page(postData,'ip_blocked/clean',0);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					window.location.reload();
				});
			}
		});
		
	});	
});
</script>
