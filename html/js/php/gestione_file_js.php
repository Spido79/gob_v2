<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$('.multiSelect_file_chk').tooltip({html:true});

	$(".zipGestDrop").dropzone({
		url: APP_URL+'/_GHOST.php',
		paramName: "file",
		maxFilesize: 20,
		init: function() {
			this.hiddenFileInput.removeAttribute('multiple');
			this.on("maxfilesexceeded", function(file) {
			});

			this.on("complete", function(file){
				$('.table_files_json').DataTable().ajax.reload( function ( json ) {
					$('.countTotalFiles').val(parseInt($('.countTotalFiles').val( ))+1);
				});
			});
			this.on("addedfile", function(file){
				$(file.previewElement).css('cursor', 'pointer');
				$(file.previewElement).find('*').each(function(){
					$(this).css('cursor', 'pointer');
					$(this).children().css('cursor', 'pointer');
				});
				file.previewElement.addEventListener("click", function(event) {
					$('.table_files_json').DataTable().search( file.name ).ajax.reload(function(elements){
						var id=0;
						$.each(elements.data, function(index, val) {
							if(val[0]==file.name){
								$($('.table_files_json').DataTable().row(index).node()).addClass('selected');
								id = val[3];
								return;
							}
						});
						detail_gestFile(id);
					});
  				});
			});
			this.on("sending", function(file,xhr,data){
				data.append("redirect_to",APP_URL+"/php/gestione_file/upload_files.php");
				data.append("type", 1);
				data.append("file", file);
			});
		},
		dictDefaultMessage: "<strong>Trascina i files qui o click per caricare. </strong></br>i files verranno caricati sul server e disponibili per essere distribuiti",
	});

	var dataTables_gestFile= $('.table_files_json').DataTable({
		"language": {
			"lengthMenu": "Mostra _MENU_ elementi",
			"zeroRecords": "Non è stato trovato niente - riprova",
			"info": "Pagina _PAGE_ di _PAGES_",
			"search": "Cerca",
			"paginate": {
				"previous": "Indietro",
				"next": "Avanti"
			},
			"infoEmpty": "Nessun elemento disponibile",
			"infoFiltered": "(filtrati da _MAX_ elementi totali)"
		},
		processing: true,
		createdRow: function ( row, data, index ) {
			if ($('#checkbox_myFile_multi').prop('checked')==true){
				var stop=false;
				$.each($('.list_files_selected_input').val().split(','), function(index, val) {
					if (stop==true){
						return true;
					}
					if (parseInt(data[4])==parseInt(val)){
						 $('td', row).parent().addClass('selected_multi');
						 stop=true;
					}
				});
			}
        },
		lengthMenu: [[10, 25, $('.countTotalFiles').val()], [10, 25, "Tutti"]],
		serverSide: true,
		ajax: {
			url: APP_URL+'/_GHOST.php',
			type: 'POST',
	        async:true,
	        mimeType:"multipart/form-data",
	        cache: false,
			data: function ( d ) {
				d.redirect_to =APP_URL+"/php/gestione_file/list_files.php";
				d.id=1;
				d.recordTotal=$('.countTotalFiles').val();
			}
		},
		pageLength: 10,
		responsive: true,
		dom: '<"html5buttons"B>lTfgtip',
		buttons: [{
				extend: 'pdfHtml5',
				title: 'Elenco files',
				exportOptions: {
                    columns: [ 1,3],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			},{
				extend: 'csvHtml5',
				title: 'Elenco files CSV',
				fieldSeparator: ';',
				exportOptions: {
                    columns: [ 1,3],
                    modifier: {
                    	search: 'applied',
                    	order: 'applied'
                	}
                }
			}
		],
		"order": [[ 1, "asc" ]],
		"columnDefs": [
		{
			"targets": 'no-sort',
			"orderable": false,
		},{
            "targets": [4],
            "visible": false,
            "searchable": false
        },{
            "targets": [2],
            "className": 'text-right'
        }],
	});

	$(document).on('click', '.btn_downloadFile_gestFile', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$('.id_file').val();
		$('button').prop('disabled',true);
		var btnTemp=$(this);
		var bthHtml=btnTemp.html();
		btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'gestione_file/download_file',id);
		returnEngine.always(function (returndata) {
			var json_file=JSON.parse(returndata);
			saveBase64(json_file.file, json_file.filename, json_file.extension);
			$('button').prop('disabled',false);
			btnTemp.empty().append(bthHtml);
		});
	});

	$(document).on('click', '.btn_saveDel_detFile', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$('.id_file').val();
		var action=parseInt($(this).val());
		var btnTemp=$(this);
		var bthHtml=btnTemp.html();
		var postData = new FormData();
		var title='Salva file';
		var message='Sei sicuro di voler aggiornare questa descrizione?';
		if (action==0){
			title='Elimina file';
			message='Sei sicuro di voler eliminare questo file?';
		} else {
			var text = $.trim($('textarea[data-name="description"]').val());
			var tag = $.trim($('input.tagsinput_fileGest').val());
			var errorDesc=0;
			$('textarea[data-name="description"]').parent().removeClass('has-error');
			//$('input.tagsinput_fileGest').parent().removeClass('has-error');
			if (text==''){
				$('textarea[data-name="description"]').parent().addClass('has-error');
				errorDesc=1;
			}

			/*if (tag==''){
				$('input.tagsinput_fileGest').parent().addClass('has-error');
				errorDesc=1;
			}*/


			if (errorDesc==1){
				return false;
			}

			postData.append('text', text);
			postData.append('tag', tag);
		}
		swal({
			title: title,
			text: message,
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				postData.append('action', action);
				var returnEngine = call_ajax_page(postData,'gestione_file/del_save',id);
				returnEngine.always(function (returndata) {
					var json_ret=JSON.parse(returndata);
					$('.countTotalFiles').val(json_ret.files);
					$('button').prop('disabled',false);
					btnTemp.empty().append(bthHtml);
					$('.table_files_json').DataTable().ajax.reload(function(elements){
						$.each(elements.data, function(index, val) {
							if(val[4]==id){
								$($('.table_files_json').DataTable().row(index).node()).addClass('selected');
								return;
							}
						});
						detail_gestFile(json_ret.id);
					});
				});
			}
		});
	});

	$('.btn_selAllFile').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		$('.table_files_json tbody tr').not('.selected_multi').click();
	});

	$('#checkbox_myFile_multi').click(function(event) {
		$('.btn_selAllFile').addClass('hidden');
		detail_gestFile(0);

		$('.table_files_json tr.selected').removeClass('selected');
		$('.table_files_json tr.selected_multi').removeClass('selected_multi');
		if ($(this).prop('checked')==true){
			$('.btn_selAllFile').removeClass('hidden')
			var tblHtml=$('.table_files_json').html();
			$('.table_files_json').parent().addClass('hidden');
			$('.view_table_reload').removeClass('hidden');
			var htmlToWrite=`
<div class="ibox selected box_goblin_list">
	<div class="ibox-content">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h3>Lista Files selezionati</h3>
				<small>
					crea una lista di files personalizzata e cliccando sui tasti funzione sottostanti potrai eseguire l'azione desiderata sull'elenco dei file.<br>
					Fai un singolo click sull'elenco dei Files per aggiungerlo alla lista, oppure clicca sulla "<i class="fa fa-remove"></i>" per rimuoverlo dalla lista.
				</small>
				<div class="well list_files_selected" style="padding:5px;"></div>
				<input class="list_files_selected_input" type="hidden"/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-offset-8 col-sm-4 text-center">
				<input class="tagsinput_multipleFile form-control" placeholder="tag" type="text" value="" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 text-center">
				<button class="btn btn-sm btn-warning btn_clearList" style='margin:4px 0 0 10px'>
					<i class="fa fa-eraser"></i> Pulisci Lista
				</button>
			</div>
			<div class="col-xs-4 text-center">
				<button class="btn btn-sm btn-danger btn_delFileMulti" style='margin:4px 0 0 10px'>
					<i class="fa fa-trash"></i> Elimina files
				</button>
			</div>
			<div class="col-xs-4 text-center">
				<button class="btn btn-sm btn-success btn_saveFileMulti" style='margin:4px 0 0 10px'>
					<i class="fa fa-floppy-o"></i> Salva tag
				</button>
			</div>
		</div>
`;
			var postData = new FormData();
			var returnEngine = call_ajax_page(postData,'gestione_file/list_tane',0);
			returnEngine.always(function (returndata) {
				$('.detail_file').empty().append(htmlToWrite+returndata+'</div></div>');
				$('.table_files_json').parent().removeClass('hidden');
				$('.view_table_reload').addClass('hidden');
			});
		}

	});

	$('.table_files_json tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ||  $(this).hasClass('selected_multi') ) {
			$(this).removeClass('selected');
			$(this).removeClass('selected_multi');
			if ($('#checkbox_myFile_multi').prop('checked')==false){
				detail_gestFile(0);
			} else {
				var id_myF= (dataTables_gestFile.row($(this).closest('tr')).data()[4]);
				var file= (dataTables_gestFile.row($(this).closest('tr')).data()[1]);
				detail_gestFile(id_myF, file, 0);
			}

		} else {
			$('.table_files_json tr.selected').removeClass('selected');
			if (typeof(dataTables_gestFile.row($(this).closest('tr')).data())==='undefined'){
				return false;
			}

			if ($('#checkbox_myFile_multi').prop('checked')==true){
				$(this).addClass('selected_multi');
			} else {
				$(this).addClass('selected');
			}
			var id_geF= (dataTables_gestFile.row($(this).closest('tr')).data()[4]);
			var file= (dataTables_gestFile.row($(this).closest('tr')).data()[1]);
			detail_gestFile(id_geF, file, 1);
		}
	});
	$(document).on('click', '.btn_clearList', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		swal({
			title: "Pulisci lista selezionati",
			text: "Sei sicuro di voler pulire la lista dei file selezionati?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('.list_files_selected_input').val('');
				$('.list_files_selected').empty();
				$('.tagsinput_multipleFile').val('');
				$('.selected_multi').removeClass('selected_multi');
				$('.check_tanefileMulti_enable').prop('checked', false);
			}
		});
	});

	$(document).on('click', '.btn_delFileMulti', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		var btnHtml=$(this).html();
		var elements= $('.list_files_selected_input').val().split(',');
		if (!check_elem_files(elements)){
			return false;
		}
		swal({
			title: "Elimina files",
			text: "Sei sicuro di voler eliminare i file selezionati?<br>al termine dell'operazione i files non saranno più disponibili e recuperabili",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('list',JSON.stringify(elements));
				var returnEngine = call_ajax_page(postData,'gestione_file/del_save_multi',0);
				returnEngine.always(function (returndata) {
					var json_returned_del=$.parseJSON(returndata);
					$('.list_files_selected_input').val('');
					$('.list_files_selected').empty();
					$('.tagsinput_multipleFile').val('');
					$('.check_tanefileMulti_enable').prop('checked', false);
					$('.countTotalFiles').val(json_returned_del.files);
					$('.table_files_json').DataTable().ajax.reload();
					$('button').prop('disabled',false);
					btnTemp.empty().append(btnHtml);
				});
			}
		});
	});

	$(document).on('click', '.check_tanefileMulti_enable', function(event) {
		var elements= $('.list_files_selected_input').val().split(',');
		if (!check_elem_files(elements)){
			return false;
		}
	});

	$(document).on('click', '.btn_selAllMulti_file', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var elements= $('.list_files_selected_input').val().split(',');
		if (!check_elem_files(elements)){
			return false;
		}
		$('.check_tanefileMulti_enable').prop('checked', true);
	});

	$(document).on('click', '.btn_activeAllMulti_file', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		var btnHtml=btnTemp.html();
		var elements= $('.list_files_selected_input').val().split(',');
		if (!check_elem_files(elements)){
			return false;
		}
		var tane=new Array();
		$('.check_tanefileMulti_enable').each(function(index, el) {
			if ($(this).prop('checked')==true){
				tane.push($(this).val());
			}
		});
		if (tane.length<=0){
			swal({
				title: "Imposta tane",
				text: "Seleziona almeno una Tana dalla lista.",
				type: "warning",
				showCancelButton: false,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok",
				html:true,
				closeOnConfirm: true,
			});
			return false;
		}
		swal({
			title: "Imposta tane per i files",
			text: "Sei sicuro di voler impostare le seguenti tane per i file selezionati?<br><span class='text-danger'>Attenzione:</span> se i seguenti file avevano altre tane selezionate verranno rimpiazzati con le tane impostate ora.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				$('button').prop('disabled',true);
				var postData = new FormData();
				postData.append('list',JSON.stringify(elements));
				postData.append('tane',JSON.stringify(tane));
				var returnEngine = call_ajax_page(postData,'gestione_file/set_tane_multi',0);
				returnEngine.always(function (returndata) {
					$('button').prop('disabled',false);
					btnTemp.empty().append(btnHtml);
				});
			}
		});
	});

	$(document).on('click', '.btn_saveFileMulti', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var btnTemp=$(this);
		var btnHtml=$(this).html();
		var elements= $('.list_files_selected_input').val().split(',');
		$('.tagsinput_multipleFile').parent().removeClass('has-error');
		if (!check_elem_files(elements)){
			return false;
		}
		var tagName=$.trim($('.tagsinput_multipleFile').val());
		if (tagName==''){
			$('.tagsinput_multipleFile').parent().addClass('has-error');
			return false;
		}
		swal({
			title: "Salva tag files",
			text: "Sei sicuro di voler salvare il seguente TAG per i file selezionati?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Si",
			cancelButtonText: "Annulla",
			html:true,
			closeOnConfirm: true,
		},function(isConfirm){
			if (isConfirm) {
				$('button').prop('disabled',true);
				btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
				var postData = new FormData();
				postData.append('list',JSON.stringify(elements));
				postData.append('tag',tagName);
				var returnEngine = call_ajax_page(postData,'gestione_file/del_save_multi',1);
				returnEngine.always(function (returndata) {
					var json_returned_del=$.parseJSON(returndata);
					$('.countTotalFiles').val(json_returned_del.files);
					$('.table_files_json').DataTable().ajax.reload();
					$('button').prop('disabled',false);
					btnTemp.empty().append(btnHtml);
				});
			}
		});
	});

	$(document).on('click', '.remove_file_list', function(event) {
		var id=parseInt($(this).attr('data-id'));
		dataTables_gestFile.rows().eq( 0).filter( function (rowIdx) {
			if (id == parseInt(dataTables_gestFile.cell( rowIdx, 4).data())){
				$('.table_files_json tbody tr:nth-child('+(rowIdx+1)+')').removeClass('selected_multi');
			}
		});
		detail_gestFile(id, '', 0);
	});

	$(document).on('click', '.check_tanefile_enable', function(event) {
		var id=$('.id_file').val();
		var tana=parseInt($(this).val());
		var action=$(this).prop('checked');
		$('button').prop('disabled',true);
		var postData = new FormData();
		postData.append('action', action);
		postData.append('tane', tana);
		var returnEngine = call_ajax_page(postData,'gestione_file/set_tane',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			check_button_gestFile();
		});
	});

	$(document).on('click', '.btn_selAll_file', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$('.id_file').val();
		var action=$(this).val();
		$('button').prop('disabled',true);
		var btnTemp=$(this);
		var bthHtml=btnTemp.html();
		btnTemp.empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
		var postData = new FormData();
		postData.append('action', action);
		postData.append('tane', 0);
		var returnEngine = call_ajax_page(postData,'gestione_file/set_tane',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			btnTemp.empty().append(bthHtml);
			if (action=='true'){
				$('.check_tanefile_enable').prop('checked', true);
			} else {
				$('.check_tanefile_enable').prop('checked', false);
			}
			check_button_gestFile();
		});
	});
});

function check_button_gestFile(){
	var total=0;
	var checked=0;
	$('.check_tanefile_enable').each(function(index, el) {
		total++;
		if ($(this).prop('checked')){
			checked++;
		}
	});
	console.log(checked+ ' - '+total);
	if (checked!=total){
		$('.btn_selAll_file[value="false"]').addClass('hidden');
		$('.btn_selAll_file[value="true"]').removeClass('hidden');
	} else {
		$('.btn_selAll_file[value="true"]').addClass('hidden');
		$('.btn_selAll_file[value="false"]').removeClass('hidden');
	}

}

function detail_gestFile(id, fileName, verso){
	if (id==0){
		$('.detail_file').empty();
		return;
	}
	if ($('#checkbox_myFile_multi').prop('checked')==false){
		$('.detail_file').empty().append('<i class="fa fa-pulse fa-spinner"></i> Attendere...');
		$('button').prop('disabled',true);
		var postData = new FormData();
		var returnEngine = call_ajax_page(postData,'gestione_file/detail_file',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.detail_file').empty().append(returndata);
		});
		return;
	}
	var elements= $('.list_files_selected_input').val();
	var stop=false;
	var arrayElem=elements.split(',');
	$.each(arrayElem, function(index, val) {
		if (stop==true){
			return true;
		}
		if (parseInt(id)==parseInt(val)){
			if (verso==0){
				arrayElem.splice(index, 1);
				$('.remove_file_list[data-id="'+id+'"]').parent().remove();
				$('.list_files_selected_input').val(arrayElem.join(','));
				stop=true;
				return;
			}
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"progressBar": true,
				"preventDuplicates": true,
				"positionClass": "toast-top-center",
				"onclick": null,
				"showDuration": "400",
				"hideDuration": "400",
				"timeOut": "2000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["info"]("Il File selezionato NON è stato aggiunto perché già in elenco", "Attenzione!");
			stop=true;
		}
	});

	if (stop==true){
		return false;
	}

	if (elements!=''){
		$('.list_files_selected_input').val(elements+','+id);
	} else {
		$('.list_files_selected_input').val(id);
	}
	$('.list_files_selected').append('<span class="label label-primary" style="display:inline-block;cursor:normal;margin:3px;"><i class="fa fa-remove remove_file_list" style="cursor:pointer" data-id="'+id+'"></i> '+fileName+'</span>');
}

function sel_tags_file(){
	$('button').prop('disabled',true);
	var json_returned={};
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'gestione_file/list_tags',0);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		json_returned=$.parseJSON(returndata);
		$('input.tagsinput_fileGest').typeahead('destroy');
		$('input.tagsinput_fileGest').typeahead({
			source: function (query, process) {
				process(json_returned.tags);
			},
			matcher : function(item) {
				return this.__proto__.matcher.call(this,item);
			},

			highlighter: function(item) {
				return this.__proto__.highlighter.call(this,item);
			},
			updater: function(item) {
				return this.__proto__.updater.call(this,item);
			}
		});
	});
}

function check_elem_files(elements){
	var stop=true;
	$.each(elements, function(index, val) {
		if (parseInt(val)>0){
			stop=false;
			return true;
		}
	});
	if (stop==true){
		swal({
			title: "Gestione files",
			text: "Seleziona almeno un File dalla lista.",
			type: "warning",
			showCancelButton: false,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ok",
			html:true,
			closeOnConfirm: true,
		});
		return false;
	}
	return true;
}

</script>