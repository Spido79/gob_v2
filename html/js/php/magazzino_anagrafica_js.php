<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	var json_returned={};
	/*var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/titles',0);
	returnEngine.always(function (returndata) {
		json_returned=$.parseJSON(returndata);
		$(".select2_magAna_title").typeahead({
			source:json_returned.title,
			minLength: 2
		});
	});*/
	$('.data-field').val('');
	$('.btn_save_modal_anaMagStock').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$('input.data-field[data-field="id_ana"]').val();
		var id_stk=$(this).attr('data-id');
		var postData = new FormData();
		postData.append('id_stk',id_stk);
		var error=false;
		$('.data-field').parent().removeClass('has-error');
		$.each($('.data-field'), function(index, val) {
			if ($(this).prop('required') && ($(this).val()=='' ||$(this).val()==null) ){
				$(this).parent().addClass('has-error');
				error=true;
			}
			if ($(this).attr('type')=='checkbox'){
				let valCheck=0;
				if($(this).prop('checked')){
					valCheck=1;
				}
				postData.append($(this).attr('data-field'),$.trim(valCheck));
			} else {
				postData.append($(this).attr('data-field'),$.trim($(this).val()));
			}

		});
		if (error==true){
			return false;
		}
		$('button').prop('disabled',true);
		var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/save_stock',id);
		returnEngine.always(function (returndata) {
			$('button').prop('disabled',false);
			$('.modal_anaMagStock').modal('hide');
			$('.data-field').val('');
			$('.btn-stock-manage-ana').click();
		});
	});


	$('input.data-field[data-field="data_acquisto"]').datepicker({
		startView: 2,
		todayBtn: "linked",
		keyboardNavigation: false,
		immediateUpdates:true,
		forceParse: true,
		autoclose: true,
		format: "dd/mm/yyyy",
		toggleActive:false,
	});

	$(document).on('click', '.btn_mag_ana_manageID', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var id=$(this).val();
		call_magAnaList(id,'');
	});

	$('.btn_magAna_selectGame').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var text=$.trim($('.select2_magAna_title').val());
		$(this).removeClass('btn-danger');
		$('.select2_magAna_title').parent().removeClass('has-error');
		if (text.length<=0){
			$(this).addClass('btn-danger');
			$('.select2_magAna_title').parent().addClass('has-error');
			return false;
		}

		//nick found! open box
		call_magAnaList(0, text);
	});
});

function call_magAnaList(id, text){
	$('button').prop('disabled',true);
	$('.admin_mag_ana_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	postData.append('title',text);
	var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/title_show',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		if (returndata.status==404){
			//possible multiple titles, select from a list
			call_magAnaList_multi(text);
			return false;
		}
		$("html, body").animate({ scrollTop: 0 }, "fast");
		$('.admin_mag_ana_right').empty().append(returndata);
	});
}

function call_magAnaList_multi(text){
	$('button').prop('disabled',true);
	var postData = new FormData();
	postData.append('title',text);
	var returnEngine = call_ajax_page(postData,'magazzino_anagrafica/list_titles',0);
	returnEngine.always(function (returndata) {
		if (returndata.status==404){
			swal({
				title: "Non trovato.",
				text: "<strong>Attenzione:</strong> il titolo ricercato non è stato trovato nel database.",
				type: "warning",
				showCancelButton: false,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok",
				html:true,
				closeOnConfirm: true,
			});
		}
		$('.admin_mag_ana_right').empty();
		$('button').prop('disabled',false);
		$('button').prop('disabled',false);
		$('.mag_ana_gameList').empty().append(returndata);
	});
}
</script>