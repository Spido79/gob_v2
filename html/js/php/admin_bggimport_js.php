<?php
if (!defined('APP_URL')) {
    \HTML\Page::Page_404();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn_admin_bggimport_import').click(function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var text=$.trim($('.btn_admin_bggimport_text').val());
		$(this).removeClass('btn-danger');
		$('.btn_admin_bggimport_text').parent().removeClass('has-error');
		if (text.length<=0 || parseInt(text)!=text){
			$(this).addClass('btn-danger');
			$('.btn_admin_bggimport_text').parent().addClass('has-error');
			return false;
		}

		call_admin_bggimport(text);
	});
});

function call_admin_bggimport(id){
	$('button').prop('disabled',true);
	$('.admin_bggimport_right').empty().append('<h3><i class="fa fa-pulse fa-spinner"></i> Attendere...</h3>');
	var postData = new FormData();
	var returnEngine = call_ajax_page(postData,'admin_bggimport/import_id',id);
	returnEngine.always(function (returndata) {
		$('button').prop('disabled',false);
		$('.admin_bggimport_right').empty().append(returndata);
	});
}
</script>