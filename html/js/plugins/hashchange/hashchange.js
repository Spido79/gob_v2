var APP_URL = document.querySelector('script[data-id="common.min.js"]').getAttribute('data-site');
$(function(){
    $(window).on('hashchange', function() {
        var hash = location.hash.replace('#','');
        if (hash.length<1){
            return false;
        }
        $("#content_central_ajax").fadeOut(200,function(){
            LoadHastag(hash);
        });
    });

    var hash = location.hash.replace('#','');
    if (hash.length>0){
        if ($('.navbar-default li').length>0){
            $('.navbar-default li').removeClass('active');
            $('.nav ul').removeClass('in');
            $('.nav-second-level li').removeClass('active');

            $('.nav-second-level li a[href="#'+hash+'"]').parents('li').addClass('active');
            $('.nav-second-level li a[href="#'+hash+'"]').closest('ul').addClass('in');    
        }
        //only first time on refresh!
        LoadHastag(hash);
    }
});

function LoadHastag(hash){
    $("#content_central_ajax").fadeOut(200,function(){
        $("#content_central_ajax").empty().append('<h2><i class="fa fa-gear fa-spin"></i> Attendere...</h2>');
        $("#content_central_ajax").fadeIn(250);
        $.ajax({
            url: APP_URL+'/_get_page.php?path='+hash,
            async: true,
            type: 'POST',
            success: function (returndata) {
                $("#content_central_ajax_js").empty();
                $("#content_central_ajax").empty().append(returndata);
            },
            error: function(returndata){
                $("#content_central_ajax_js").empty();
                $("#content_central_ajax").empty().append('error');
            }
        });
    });
}