var APP_URL = document.querySelector('script[data-id="common.min.js"]').getAttribute('data-site');
$(document).ready(function(){

    $(document).on('click', '.collapse-link', function(event) {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.children('.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    $(document).on('click', '.nav-second-level li', function(event) {
        $('.nav-second-level li').removeClass('active');
        $(this).addClass('active');
        var dataHash=$(this).attr('data-hash');
        var dataIcon=$(this).attr('data-icon');
        $('.button_ribbon button').each(function(index, el) {
            if ($(this).attr('data-hash')==dataHash){
                $(this).remove();
            }
        });
        $('.button_ribbon').append('<button class="btn btn-default dim btn_ribbon" data-icon="'+dataIcon+'" data-hash="'+dataHash+'" type="button"><i class="'+dataIcon+'"></i></button>');
        while ($('.button_ribbon button').length>5){
            $('.button_ribbon button').first().remove();
        }
    });


    
    $(document).on('click', '.btn_ribbon', function(event) {
        var hash=$(this).attr('data-hash');
        $('.nav-second-level li').removeClass('active');
        $('.nav-second-level li').parents('li').removeClass('active');
        $('.nav-second-level li').parents('ul').removeClass('in');
        $('.nav-second-level li[data-hash="'+hash+'"]').addClass('active');
        $('.nav-second-level li[data-hash="'+hash+'"]').parents('li').addClass('active');
        $('.nav-second-level li[data-hash="'+hash+'"]').parents('ul').addClass('in');
        location.hash=hash;
    });
    $(document).on('click', '.btn_forgotSend', function(event) {
        var email=$.trim($('.email_forgotSend').val());
        $('.email_forgotSend').parent().removeClass('has-error');
        if (!ValidateEmail(email)){
            $('.email_forgotSend').parent().addClass('has-error');
            return false;
        }
        var postData = new FormData();
        postData.append('email',email);
        var returnEngine = call_ajax_page(postData,'forgot',0);
        returnEngine.always(function (returndata) {
            $('.modal_login').modal('hide');
            $('.email_forgotSend').val('');
            switch(parseInt(returndata)){
                case 200:
                    swal({
                        title: "Procedura inviata",
                        text: "Ti verrà inviata una mail contenente il link e la procedura per il ripristono della password.<br/><br/>La mail potrebbe arrivare entro un <strong>paio di minuti</strong>.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        html:true,
                        closeOnConfirm: true,
                    });
                    break;

                case 401: //password wrong
                    swal({
                        title: "Utente inesistente",
                        text: "Il recupero password non è possibile per l'utente richiesto in quanto la mail inserita non è nel sistema. <br/><strong>Attenzione:</strong> dopo alcuni tentativi sbagliati il tuo <strong>indirizzo ip</strong> verrà bloccato.<br/><br/>Qualora tu non ricordassi la tua <strong>user</strong> contatta l'amministratore di sistema.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        html:true,
                        closeOnConfirm: true,
                    });
                    break;

                case 404: //ip blocked
                    swal({
                        title: "IP Bloccato",
                        text: "<strong>Attenzione:</strong> il tuo <strong>indirizzo ip</strong> risulta bloccato dal sistema a seguito di molti tentativi di login sbagliati.<br/><br/>Contatta gli amministratori di sistema per far rimuovere il <strong>blocco</strong>.",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        html:true,
                        closeOnConfirm: true,
                    });
                    break;

            }
        });
    })

    $(document).on('keyup', '.enter-focus', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        if ( event.which == 13 && $(this).attr('target-click').length>0) {
            var target=$(this).attr('target-click');
            if ($(target).length>0){
                $(target).click();
            }
        }
    });
    
    //logout button
    $(document).on('click', '.btn_logout', function(event) {
        swal({
            title: "Logout",
            text: "Sei sicuro di volerti <strong>disconnettere</strong> dal portale di gestione?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Annulla",
            html:true,
            closeOnConfirm: true,
            closeOnCancel: true,
            },function(){
                var postData = new FormData();
                var returnEngine = call_ajax_page(postData,'logout',0);
                returnEngine.always(function (returndata) {
                    window.location.reload();
            });
        });
    });
    //login button
    $(document).on('click', '.btn_login', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        //verifico se ho come bros field_pass e field_user
        if ($(this).parent().find('.field_user').length==0 || $(this).parent().find('.field_pass').length==0){
            return false;
        }
        $(this).parent().find('.field_user').parent().removeClass('has-error');
        $(this).parent().find('.field_pass').parent().removeClass('has-error');
        var user=$.trim($(this).parent().find('.field_user').val());
        var pass=$.trim($(this).parent().find('.field_pass').val());
        var errorT=0;
        if (user.length<=0){
            $(this).parent().find('.field_user').parent().addClass('has-error');
            errorT=1;
        }

        if (pass.length<=0){
            $(this).parent().find('.field_pass').parent().addClass('has-error');
            errorT=1;
        }

        if (errorT==1){
            return false;
        }
        $(this).blur();
        var postData = new FormData();
        postData.append('user',user);
        postData.append('pass',pass);
        var returnEngine = call_ajax_page(postData,'login',0);
        returnEngine.always(function (returndata) {
            switch(parseInt(returndata)){
                case 200:
                    window.location.reload();
                    break;

                case 401: //password wrong
                    swal({
                        title: "Password sbagliata",
                        text: "La password inserita non è corretta. <br/><strong>Attenzione:</strong> dopo alcuni tentativi il tuo <strong>indirizzo ip</strong> verrà bloccato.<br/><br/>Qualora tu non ricordassi la password puoi usare la funzione <span class='text-success'>Recupera password</span>.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        html:true,
                        closeOnConfirm: true,
                    });
                    break;

                case 404: //ip blocked
                    swal({
                        title: "IP Bloccato",
                        text: "<strong>Attenzione:</strong> il tuo <strong>indirizzo ip</strong> risulta bloccato dal sistema a seguito di molti tentativi di login sbagliati.<br/><br/>Contatta gli amministratori di sistema per far rimuovere il <strong>blocco</strong>.",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        html:true,
                        closeOnConfirm: true,
                    });
                    break;

            }
        });
    });
    //END login button


    //changing Language
    $(document).on('click', '.global_language', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var language=$(this).attr('data-lang');
        var postData = new FormData();
        postData.append('language',language);
        var returnEngine = call_ajax_page(postData,'change_language',0);
        returnEngine.always(function () {
            window.location.reload();
        });
    });
    //end changing language

});
function ValidateEmail(email){  
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/.test(email)){  
        return true;
    }
    return false;
}

function call_ajax_page(data,redirect_to,id){
    //example
    //	formData = new FormData($('#id_form')[0]);
    //  var returnCall = call_ajax_page(formData,varRedirectTo);
    if (typeof id == "undefined" || id == null){
    } else {
        data.append('id', id);
    }
    if (typeof redirect_to == "undefined" || redirect_to == null){
        //noting to do
    } else {
        if (redirect_to.indexOf('.php')==-1){
            redirect_to=APP_URL+'/php/'+redirect_to+'.php';
        }
        data.append('redirect_to', redirect_to);
    }
    return $.ajax({
        url: APP_URL+'/_GHOST.php',
        type: 'POST',
        data: data,
        async:true,
        mimeType:"multipart/form-data",
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
        },
        error: function(returndata){
        },
        always: function(returndata){
        }
    });
}

function load_element(path,force){

    var type= path.substr(-3).toLowerCase();
    switch(type){
        case 'css':
            var tag='link';
            var prop='href';
            if (typeof force!== 'undefined'){
                var loc=APP_URL+'/assets/'+force;
            } else {
                var loc=APP_URL+'/assets/css';
            }
    
            
            var append='head';
            var suffix='rel="stylesheet" /';
            var isPlughin='/';
            break;
        case '.js':
            var tag='script';
            var prop='src';
            var loc=APP_URL+'/assets';
            var append='body';
            var suffix=' ></script';
            var isPlughin='/plugins/';
            break;
        default:
            return false;
            break;

    }
    if (typeof $(tag+'['+prop+'="'+loc+isPlughin+path+'"]').prop(prop)==='undefined'){
        $(append).append('<'+tag+' '+prop+'="'+loc+isPlughin+path+'" '+suffix+'>');
    }
}

function initMap_GoogleTane() {
    var mapOptions1 = {
        zoom: 5,
        center: new google.maps.LatLng(41.38155, 2.13752),
        mapTypeId: 'roadmap',

        styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
    };
    var fenway = new google.maps.LatLng(42.345573, -71.098326);
    var panoramaOptions = {
        position: fenway,
        pov: {
            heading: 10,
            pitch: 10
        }
    };

    var myMapsId = 'zgmdvQPLj5a0.kL21R7jm4bes';
    var mapElement1 = document.getElementById('map1');
    var map1 = new google.maps.Map(mapElement1, mapOptions1);
    var ctaLayer = new google.maps.KmlLayer({
        map: map1,
        zoom:10,
        url: 'https://www.google.com/maps/d/kml?mid=' + myMapsId,
        preserveViewport: false,
    });
}

function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ';'
            line += array[i][index].replace(/\"/g , "\\\"");
        }

        str += line + '\r\n';
    }

    return str;
}


function exportCSVFile(headers, items, fileTitle) {
    if (headers) {
        items.unshift(headers);
    }
    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);
    var csv = this.convertToCSV(jsonObject);
    saveFile(csv, fileTitle, 'text/csv;charset=utf-8;', 'csv');   
}


function savePDFbase64(pdf, fileTitle) {
    var binaryString = window.atob(pdf);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    saveFile(bytes, fileTitle, 'application/octet-stream', 'pdf');
}

function saveBase64(octet, fileTitle, extension) {
    var binaryString = window.atob(octet);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    saveFile(bytes, fileTitle, 'application/octet-stream', extension);
}

function saveFile(data, fileTitle, type, extension) {
    var exportedFilenmae = fileTitle + '.'+extension || 'export.'+extension;
    var blob = new Blob([data], { type: type });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = window.URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFilenmae);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}